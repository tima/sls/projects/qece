/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.annotations

import scala.annotation.StaticAnnotation

/** Abstract class for indicating a range for a parameter */
abstract class ParamAnnotation extends StaticAnnotation {
  def get: List[Int]
}

// scalastyle:off class.name
/** dummy annotation to force constructor to expose params. */
class dummy extends ParamAnnotation {
  override def get: List[Int] = List(0)
}

/** linear parameter range
 *  @param step
 *    iteration step between two values
 *  @param minVal
 *    minimum value of the parameter
 *  @param maxVal
 *    maximum value of the parameter (included)
 */
class by(step: Int)(minVal: Int, maxVal: Int) extends ParamAnnotation {
  override def get: List[Int] = {
    val tmpMax = minVal max maxVal
    val myList = (minVal to tmpMax by step).toList
    myList contains tmpMax match {
      case true  => myList
      case false => myList :+ tmpMax
    }
  }
}

/** linear parameter range */
case class linear(min: Int, max: Int = -1) extends by(1)(min, max)

/** Simple step of 2 */
case class by2(min: Int, max: Int = -1) extends by(2)(min, max)

/** Simple step of 3 */
case class by3(min: Int, max: Int = -1) extends by(3)(min, max)

/** Simple step of 4 */
case class by4(min: Int, max: Int = -1) extends by(4)(min, max)

/** Simple step of 5 */
case class by5(min: Int, max: Int = -1) extends by(5)(min, max)

/** Simple step of 10 */
case class by10(min: Int, max: Int = -1) extends by(10)(min, max)

/** power parameter range
 *  @param power
 *    base of the power being used
 */
class pow(power: Int)(minVal: Int, maxVal: Int) extends ParamAnnotation {
  override def get: List[Int] = (minVal to (minVal max maxVal)).map(scala.math.pow(power, _).toInt).toList
}

/** parameter range for powers of two
 *  @param min
 *    minimum power of two
 *  @param max
 *    maximum power of two
 */
case class pow2(min: Int, max: Int = -1) extends pow(2)(min, max)

/** Simple enumeration of parameters
 *
 *  @param list
 *    list of parameters to be used
 */
case class enum(list: Int*) extends ParamAnnotation {
  override def get: List[Int] = list.toList
}
// scalastyle:on class.name
