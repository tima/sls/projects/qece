/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.components

import qece.exploration.annotations._
import qece.exploration.utils.Explorable

import chisel3._
import chisel3.util._
import chisel3.experimental._

private[qece] class DotProduct(
    // @qualityOfResult @linear(2, 32)  dynamic: Int,
    // @qualityOfResult @linear(2, 32)  precision: Int,
    // @qualityOfResult @enum(8)        nElem: Int,
    //                  @linear(0, 3)    parallelism: Int
    @qualityOfResult @linear(2, 20) dynamic: Int,
    @qualityOfResult @linear(2, 20) precision: Int,
    @qualityOfResult @enum(8) nElem: Int,
    @linear(0, 1) parallelism: Int
    // @qualityOfResult @pow2(3, 5)  dynamic: Int,
    // @qualityOfResult @pow2(3, 5)  precision: Int,
    // @qualityOfResult @pow2(3, 6)        nElem: Int,
    //                  @linear(0, 3)    parallelism: Int
) extends Explorable {
  require(log2Up(nElem) >= parallelism, s"Parallelism level $parallelism must be less than log2($nElem)")
  require((nElem & -nElem) == nElem, s"Number of elements $nElem must be a power of two")

  val bitWidth     = dynamic + precision
  val nElemByCycle = scala.math.pow(2, parallelism).toInt
  val tpe          = FixedPoint(bitWidth.W, precision.BP)

  val vec1      = IO(Input(Vec(nElemByCycle, tpe.cloneType)))
  val vec2      = IO(Input(Vec(nElemByCycle, tpe.cloneType)))
  val accuReset = IO(Input(Bool()))
  val out       = IO(Output(tpe.cloneType))

  val accuIn = WireInit((vec1 zip vec2).map { case (x, y) => x * y }.reduce(_ + _))

  val zero = FixedPoint(0, bitWidth.W, precision.BP)
  val accu = RegInit(tpe.cloneType, zero)

  accu := Mux(!accuReset, accu + accuIn, accuIn)

  out := accu
}
