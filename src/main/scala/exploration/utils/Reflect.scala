/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.utils

import qece.exploration.exceptions._

import qece.exploration.annotations._

import scala.tools.reflect.ToolBox
import scala.reflect.runtime._
import scala.reflect.runtime.universe._

import scala.collection.immutable.ListMap

import logger.LazyLogging

private[exploration] object Reflect extends Reflect

private[exploration] trait Reflect {

  protected val classLoaderMirror = runtimeMirror(getClass.getClassLoader)

  /** Encapsulates functionality to reflectively invoke the constructor for a given case class type `T`.
   *
   *  @tparam T
   *    the type of the case class this factory builds
   */
  class ProductFactory[T: TypeTag] extends LazyLogging {

    val tpe         = typeOf[T]
    val classSymbol = tpe.typeSymbol.asClass
    val tb          = currentMirror.mkToolBox()

    // if (!(tpe <:< typeOf[Product] && classSymbol.isCaseClass))
    if (!(tpe <:< typeOf[Product])) {
      throw new IllegalArgumentException("ProductFactory only applies to classes implementing Product!")
    }

    //TODO: manage ineer classes using InstanceMirror
    // https://stackoverflow.com/questions/36699907/can-i-get-an-instance-of-outer-class-using-reflection-on-inner-class-type
    val classMirror = classLoaderMirror.reflectClass(classSymbol)

    // val constructorSymbol = tpe.declaration(nme.CONSTRUCTOR)
    val constructorSymbol = tpe.decl(termNames.CONSTRUCTOR)

    val defaultConstructor =
      if (constructorSymbol.isMethod) { constructorSymbol.asMethod }
      else {
        val ctors = constructorSymbol.asTerm.alternatives
        ctors.map { _.asMethod }.find { _.isPrimaryConstructor }.get
      }

    val constructorMethod = classMirror reflectConstructor defaultConstructor

    /** Attempts to create a new instance of the specified type by calling the constructor method with the supplied
     *  arguments.
     *
     *  WARNING arguments must be set in the correct order
     *
     *  @param args
     *    the arguments to supply to the constructor method
     */
    def buildWith(args: Seq[_]): T = constructorMethod(args: _*).asInstanceOf[T]

// scalastyle:off method.length
    /** Build all possible tuples from parameters annotations
     *
     *  @todo
     *    manage when a parameter is not annotated
     *  @todo
     *    manage default values
     */
    def getParamSets: (ListMap[String, Seq[Int]], Seq[List[(String, Int)]]) = {
      // generate all possible Lists by combination
      def generator(x: List[List[(String, Int)]]): List[List[(String, Int)]] =
        x match {
          case Nil    => List(Nil)
          case h :: _ => h.flatMap(i => generator(x.tail).map(i :: _))
        }

      val params = constructorSymbol.asMethod.paramLists.flatten
      // use ListMap to preserve argument order
      // TODO: should be more simple
      val ranges = ListMap(
          (params
            .map {
              // retrieve first param annotation found. If no annotation found, assume using zero
              x =>
                val annos = x.annotations.filter(_.tree.tpe <:< typeOf[ParamAnnotation])
                x.name.toString -> annos.size match {
                  case (name, 0) =>
                    name -> Right(0)
                  case (name, 1) =>
                    name -> Left(annos.head)
                  case _ =>
                    val tpe = typeOf[ParamAnnotation]
                    throw ParamAnnotationException(s"Found more than one $tpe annotation on param ${x.name}")
                }
            }
            .filter {
              _ match {
                case (_, Left(_))  => true
                case (_, Right(_)) => false
              }
            }
            .map {
              case (name, Left(x))  => name -> tb.eval(tb.untypecheck(x.tree)).asInstanceOf[ParamAnnotation]
              case (name, Right(_)) => throw ParamAnnotationException(s"Can't find annotation for param $name")
            }
            .map { case (name, r) => name -> r.get }): _*
      )
      (ranges, generator(ranges.map { case (k, l) => l.map(v => k -> v) }.toList))
    }
// scalastyle:on method.length

    def getImpactingParams(m: ImpactMetric): List[String] = {
      constructorSymbol.asMethod.paramLists.flatten
        .map { p =>
          val annos = p.annotations
            .filter(_.tree.tpe <:< typeOf[ImpactMetric])
            .map(a => tb.eval(tb.untypecheck(a.tree)).asInstanceOf[ImpactMetric])
          (p.name.toString -> (annos.map(_.name).contains(m.name)))
        }
        .filter(_._2 == true)
        .map(_._1)
    }
  }
}
