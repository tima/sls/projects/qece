/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.utils

import qece.QeceContext

import scala.collection.parallel.{ParSeq, ForkJoinTaskSupport}

/** Trait for implementing parallel computations on a collection. */
private[qece] trait Parallel {

  /** Set the parallelism level of this ParSeq to a given value if defined, or to the default value (as defined in
   *  ProjetConfig) elsehow
   *
   *  @param seq
   *    ParSeq to set parallelism level to
   *  @param default
   *    if defined, wanted level of parallelism
   */
  def setParallelism[T](seq: ParSeq[T], default: Option[Int] = None): ParSeq[T] = {
    default match {
      case Some(x) =>
        seq.tasksupport = new ForkJoinTaskSupport(new java.util.concurrent.ForkJoinPool(x))
      case None =>
        seq.tasksupport = new ForkJoinTaskSupport(new java.util.concurrent.ForkJoinPool(QeceContext.parallel.numThread))
    }
    seq
  }

  /** Build a parallel set for a Seq, and an optional parallelism level
   *
   *  @param seq
   *    initial seq
   *  @param default
   *    if defined, wanted level of parallelism
   */
  def buildParallelSet[T](seq: Seq[T], default: Option[Int] = None): ParSeq[T] = setParallelism(seq.par, default)
}
