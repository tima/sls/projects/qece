/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.utils

import qece.exploration.exceptions._
import qece.exploration.annotations._
import qece.exploration.strategies.utils.{Point, Space, SeqSpace, MatrixSpace}

import scala.reflect.runtime.universe.TypeTag

/** Constructor class used to build set of objects based on annotations
 *
 *  @see
 *    paramMap Map of parameters ranges indexed by parameter name
 *  @see
 *    paramSets Sets of all generated parameter sets (as combination of every parameters), coupled with the parameter
 *    name
 *  @see
 *    paramValues Same as [[paramSets]], but without the parameter names
 */
class Constructor[T: TypeTag] {
  val factory           = new Reflect.ProductFactory[T]
  val className         = factory.classSymbol.name.toString
  override def toString = className

  lazy val (paramMap, paramSets) = factory.getParamSets
  lazy val paramValues           = paramSets.map(_.map(_._2))
  lazy val paramNames            = paramSets.map(_.map(x => x._1)).head
  lazy val namedParams           = paramSets.map(_.map(x => x._1 -> x._2))
  lazy val spaceSize             = paramValues.size

  /** Get size of space when reduced by a given impacting param
   *
   *  @param metric
   *    impacting metric
   */
  def getSpaceSize(metric: Option[ImpactMetric] = None): Int =
    metric match {
      case Some(m) =>
        val params = factory.getImpactingParams(m)
        paramMap.filter { case (k, _) => params contains k }.map(_._2.size).reduceOption(_ * _).getOrElse(0)
      case None => paramMap.map(_._2.size).reduceOption(_ * _).getOrElse(0)
    }

  /** Get size of space when reduced by a given impacting param
   *
   *  @param name
   *    impacting metric name
   */
  def getSpaceSize(name: String): Int = getSpaceSize(Some(new ImpactMetric(name)))

  /** Retrieve the max parameter for the given name
   *
   *  @param name
   *    parameter name
   */
  def getMax(name: String): Int = {
    paramMap contains name match {
      case true  => paramMap(name).reduce(_ max _)
      case false => throw ParamAnnotationException(s"Did not found param with name $name in class $className")
    }
  }

  /** Retrieve the min parameter for the given name
   *
   *  @param name
   *    parameter name
   */
  def getMin(name: String): Int = {
    paramMap contains name match {
      case true  => paramMap(name).reduce(_ min _)
      case false => throw ParamAnnotationException(s"Did not found param with name $name in class $className")
    }
  }

  /** Retrieve the index of a given parameter in the module parameters
   *
   *  @param name
   *    parameter name
   */
  def getIndex(name: String): Int = {
    paramMap contains name match {
      case true =>
        (paramMap.keys.zipWithIndex)
          .map {
            case (p, i) =>
              if (p == name) { (true -> i) }
              else { (false -> i) }
          }
          .reduce { (a, b) =>
            val i = if (a._1) a._2 else b._2
            ((a._1 || b._1) -> i)
          }
          ._2
      case false => throw ParamAnnotationException(s"Did not found param with name $name in class $className")
    }
  }

  /** Reorder a seq of arguments to feed it to the default constructor of the module
   *
   *  @param args
   *    seq of arguments to reorder
   */
  def buildArgs(args: Seq[(String, Int)]): Seq[Int] = {
    args.map(arg => (getIndex(arg._1) -> arg._2)).sortBy(_._1).map(_._2)
  }

  def buildArgMap(args: Seq[Int]): Map[String, Int] = {
    (paramNames zip args).map { case (n, v) => (n -> v) }.toMap
  }

  /** Build an ExtModule name from the module class and the parameter list
   *
   *  @param params
   *    parameters of the ExtModule being generated
   */
  def buildName(params: Seq[_]): String = s"${className}_${params.mkString("_")}"

  def buildWith(args: Seq[_]): T = factory.buildWith(args)

  def buildSeqSpace: Space[T] = SeqSpace[T](namedParams.map(Point(_)))

  def buildMatrixSpace: Space[T] =
    paramMap.size match {
      case 0 => throw CanNotBuildSpaceException("Can not build space with nul dimension")
      case 1 =>
        MatrixSpace[T](
            paramMap,
            Array.tabulate(paramMap.head._2.size) { p0 =>
              Point(List(paramMap.head._1 -> paramMap.head._2(p0)))
            }
        )
      case 2 =>
        MatrixSpace[T](
            paramMap,
            Array.tabulate(paramMap.head._2.size) { p0 =>
              Array.tabulate(paramMap.last._2.size) { p1 =>
                Point(
                    List(paramMap.head._1 -> paramMap.head._2(p0), paramMap.last._1 -> paramMap.last._2(p1))
                )
              }
            }
        )
      case 3 =>
        MatrixSpace[T](
            paramMap,
            Array.tabulate(paramMap.toSeq(0)._2.size) { p0 =>
              Array.tabulate(paramMap.toSeq(1)._2.size) { p1 =>
                Array.tabulate(paramMap.toSeq(2)._2.size) { p2 =>
                  Point(
                      List(
                          paramMap.toSeq(0)._1 -> paramMap.toSeq(0)._2(p0),
                          paramMap.toSeq(1)._1 -> paramMap.toSeq(1)._2(p1),
                          paramMap.toSeq(2)._1 -> paramMap.toSeq(2)._2(p2)
                      )
                  )
                }
              }
            }
        )
      case 4 =>
        MatrixSpace[T](
            paramMap,
            Array.tabulate(paramMap.toSeq(0)._2.size) { p0 =>
              Array.tabulate(paramMap.toSeq(1)._2.size) { p1 =>
                Array.tabulate(paramMap.toSeq(2)._2.size) { p2 =>
                  Array.tabulate(paramMap.toSeq(3)._2.size) { p3 =>
                    Point(
                        List(
                            paramMap.toSeq(0)._1 -> paramMap.toSeq(0)._2(p0),
                            paramMap.toSeq(1)._1 -> paramMap.toSeq(1)._2(p1),
                            paramMap.toSeq(2)._1 -> paramMap.toSeq(2)._2(p2),
                            paramMap.toSeq(3)._1 -> paramMap.toSeq(3)._2(p3)
                        )
                    )
                  }
                }
              }
            }
        )
      case 5 =>
        MatrixSpace[T](
            paramMap,
            Array.tabulate(paramMap.toSeq(0)._2.size) { p0 =>
              Array.tabulate(paramMap.toSeq(1)._2.size) {
                p1 =>
                  Array.tabulate(paramMap.toSeq(2)._2.size) { p2 =>
                    Array.tabulate(paramMap.toSeq(3)._2.size) { p3 =>
                      Array.tabulate(paramMap.toSeq(4)._2.size) { p4 =>
                        Point(
                            List(
                                paramMap.toSeq(0)._1 -> paramMap.toSeq(0)._2(p0),
                                paramMap.toSeq(1)._1 -> paramMap.toSeq(1)._2(p1),
                                paramMap.toSeq(2)._1 -> paramMap.toSeq(2)._2(p2),
                                paramMap.toSeq(3)._1 -> paramMap.toSeq(3)._2(p3),
                                paramMap.toSeq(4)._1 -> paramMap.toSeq(4)._2(p4)
                            )
                        )
                      }
                    }
                  }
              }
            }
        )
      case _ => buildSeqSpace
    }

}
