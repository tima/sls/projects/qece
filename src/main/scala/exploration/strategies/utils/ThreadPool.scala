/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import qece.{Utils, QeceContext}

import qece.exploration.strategies.utils.exceptions._

import qece.estimation.MetricMap
import qece.estimation.transforms.SynchroLogger

import java.util.concurrent.Semaphore

import scala.reflect.runtime.universe.TypeTag

private[exploration] class EstimationThread(
    synchro: Semaphore,
    logger: SynchroLogger,
    f: Point => MetricMap
)(val point: Point)
    extends Thread {
  setPriority(Thread.MAX_PRIORITY)

  @volatile var result: Option[(Point, MetricMap)] = None
  // Used to mark this thread as over, instead of using Thread.State.TERMINATED which causes race conditions
  @volatile var isOver: Boolean = false
  override def run() = {
    try {
      logger.addTask("estimation")
      result = Some((point -> f(point)))
      logger.removeTask("estimation")
      // /!\ need to mark thread as over BEFORE signaling that it is over
      isOver = true
      synchro.release
    } catch {
      case t: Throwable =>
        result = Some((point -> MetricMap.empty))
        isOver = true
        logger.removeTask("estimation")
        synchro.release
        throw t
    }
  }

  def getResult: (Point, MetricMap) =
    result match {
      case Some(x) => x
      case None    => throw UnfinishedThreadException(s"Did not finished execution for point ${point}")
    }
}

/** Pool of workers to perform operations over a design space
 *
 *  @param numThread
 *    available threads for this pool
 *  @param f
 *    function to apply on every point
 */
class ThreadPool(numThread: Int, f: Point => MetricMap) {
  private val synchro = new Semaphore(0)
  private val logger  = new SynchroLogger("*")

  class CustomWorker(p: Point) extends EstimationThread(synchro, logger, f)(p)

  private val workers = new scala.collection.mutable.ArrayBuffer[CustomWorker]()
  private val results = new scala.collection.mutable.ArrayBuffer[(Point, MetricMap)]()
  
  private def getResultMap: Map[Point, MetricMap] = results.toMap

  private def getPool(state: Option[Thread.State]): Seq[CustomWorker] =
    state match {
      case Some(s) => workers.filter(_.getState() == s).toSeq
      case None    => workers.toSeq
    }

  private def getPool(state: Thread.State): Seq[CustomWorker] = getPool(Some(state))
  private def getWorkers                                      = getPool(None)
  private def taskPool                                        = getPool(Thread.State.NEW)
  private def running                                         = getPool(Thread.State.RUNNABLE)
  private def blocked                                         = getPool(Thread.State.BLOCKED)
  private def waiting                                         = getPool(Thread.State.WAITING)
  private def timing                                          = getPool(Thread.State.TIMED_WAITING)
  private def terminated                                      = workers.filter(_.isOver).toSeq

  private def removeFromPool(w: CustomWorker) = {
    results += w.getResult
    workers -= w
  }

  private def logState =
    logger.print(
        s"<< [${Utils.getTime}] - " +
          s"${terminated.size} thread(s) terminated - " +
          s"${running.size} thread(s) running - " +
          s"${waiting.size} thread(s) waiting - " +
          s"${timing.size} thread(s) timed - " +
          s"${blocked.size} thread(s) blocked - " +
          s"${taskPool.size} task(s) non started - " +
          s"${workers.size} task(s) remaining [${getWorkers.size}]" +
          ">>"
    )

  private def startNewThread: Unit = {
    if (taskPool.size > 0) taskPool.head.start() 
    else throw NoThreadLeftException(s"Can not start a new thread as no task is remaining")
  }

  /** Run f over a whole space
   *
   *  @param space
   *    initial space to run f over
   */
  def run[T: TypeTag](space: Space[T]): Space[T] = {
    def doContinue: Boolean = (getWorkers.size > 0)
    space.isEmpty match {
      case true   => space
      case false  =>
        space.foreach(p => workers += new CustomWorker(p))
        workers.take(Seq(numThread, 1).max).foreach(_.start())
        logState
        while (doContinue) {
          QeceContext.parallel.activeWaiting match {
            case Some(s) => Thread.sleep(s)
            case None    => synchro.acquire
          }
          assert(!terminated.isEmpty, s"Pool was awaken but no terminated thread(s) were found")
          removeFromPool(terminated.head)
          try {
            startNewThread
          } catch {
            case _: NoThreadLeftException =>
            case e: Throwable             => throw e
          }
          logState
        }
        val resultMap = getResultMap
        space.mapPoint{p =>
          resultMap.getOrElse(p, MetricMap.empty) match {
            case MetricMap.empty  => new FailurePoint(p.params, p.metrics)
            case m                => p.copy(metrics = p.metrics ++& m)
          }
        }
    }
  }
}
