/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import qece.exploration.annotations.ImpactMetric

import qece.exploration.strategies.utils.exceptions._

import qece.estimation.MetricMap

import scala.reflect.runtime.universe.TypeTag

/** Represent a 2D matrix of implementations
 *
 *  Space may be sparse, with every missing point represented as a SparsePoint
 */
private[exploration] class Space2D[T: TypeTag](
    sets: Map[String, Seq[Int]],
    private[qece] val inside: Array[Array[Point]]
) extends MatrixSpace[T](sets) {
  require(
      inside.size == 0 ||
        inside.map(i => i.size == inside.head.size).reduce(_ & _),
      s"All dimensions should be of size ${inside.head.size}"
  )
  require(sets.size == 2, s"Expected a 2D space, found a ${sets.size}D one")

  override lazy val minOption: Option[Point] = {
    val indexes = indexWhere(sets.values.map(_.min).toSeq)
    inside(indexes(0))(indexes(1)) match {
      case _: SparsePoint => None
      case p              => Some(p)
    }
  }
  override lazy val maxOption: Option[Point] = {
    val indexes = indexWhere(sets.values.map(_.max).toSeq)
    inside(indexes(0))(indexes(1)) match {
      case _: SparsePoint => None
      case p              => Some(p)
    }
  }

  override def contains(p: Point): Boolean = {
    val indexes = indexWhere(p)
    !inside(indexes.head)(indexes.last).isInstanceOf[SparsePoint]
  }

  override def mapPoint(f: Point => Point): Space[T] =
    Space2D[T](sets, inside.map(_.map(p => if (!p.isInstanceOf[SparsePoint]) f(p) else p)))

  override def filter(f: Point => Boolean): Space[T] =
    Space2D[T](sets, inside.map(_.map(p => if (!p.isInstanceOf[SparsePoint] && f(p)) p else SparsePoint(p.params))))

  override def filterNot(f: Point => Boolean): Space[T] =
    Space2D[T](sets, inside.map(_.map(p => if (!p.isInstanceOf[SparsePoint] && !f(p)) p else SparsePoint(p.params))))

  override def toSeq: Seq[Point] = this.inside.flatMap(_.filterNot(_.isInstanceOf[SparsePoint]).toSeq)

  override def getNeighbours(p: Point, norm: Norm.Norm = Norm.One, maxDist: Int = 1): Seq[Point] =
    (norm match {
      case Norm.One =>
        val init = indexWhere(p)
        val indexes =
          init.zipWithIndex.map { case (i, ii) => (i - maxDist to i + maxDist).filter(t => t >= 0 && t < sizes(ii)) }
        (for (ix <- indexes.head; iy <- indexes.last) yield {
          if (((init.head - ix).abs + (init.last - iy).abs) <= maxDist) inside(ix)(iy) else EmptyPoint
        }).filterNot(_ == EmptyPoint) diff Seq(p)
      case Norm.Max =>
        val indexes =
          indexWhere(p).zipWithIndex.map {
            case (i, ii) => (i - maxDist to i + maxDist).filter(t => t >= 0 && t < sizes(ii))
          }
        (for (ix <- indexes.head; iy <- indexes.last) yield inside(ix)(iy)) diff Seq(p)
    }).filterNot(_.isInstanceOf[SparsePoint])

  override def rebuildSpace(metric: ImpactMetric)(initialSpace: Space[T]): Space[T] =
    initialSpace match {
      case m: MatrixSpace[T] =>
        val newKeys = constructor.paramNames diff constructor.factory.getImpactingParams(metric)
        assert(
            newKeys.size + this.dimension == m.dimension,
            s"Can not rebuild a ${m.dimension}D space from a ${this.dimension}D space and ${newKeys.size} new dimensions"
        )
        this.isEmpty match {
          case true   => m.mapPoint(p => SparsePoint(p.params))
          case false  =>
            val newMetricNames = this.toSeq.head.metricNames diff initialSpace.toSeq.head.metricNames
            m.mapPoint {
              case p: Point =>
                val indexes = indexWhere(p.remove(newKeys))
                val nearest = inside(indexes(0))(indexes(1))
                nearest match {
                  case x: SparsePoint => x
                  case _ =>
                    p.copy(metrics =
                      p.metrics ++&
                      new MetricMap(nearest.metrics.toMap.filter { case (k, _) => newMetricNames contains k })
                      )
                }
            }
        }
      case _ => SeqSpace[T](Point.rebuildSpace(metric)(initialSpace.toSeq, this.toSeq))
    }

  override def findMiddle(p: Point, q: Point): Point = {
    val indexes = (indexWhere(p) zip indexWhere(q)).map { case (ip, iq) => (iq + ip) / 2 }
    inside(indexes.head)(indexes.last) match {
      case _: SparsePoint => throw SpaceOperationException(s"Can't find middle in space")
      case s              => s
    }
  }
}

private[exploration] object Space2D {
  def apply[T: TypeTag](params: Map[String, Seq[Int]], mat: Array[Array[Point]]): Space2D[T] =
    new Space2D[T](params, mat)
}

private[exploration] object EmptySpace2D extends Space2D(Map.empty, Array.empty)
