/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import qece.exploration.annotations.ImpactMetric

import scala.reflect.runtime.universe.TypeTag

/** Simple implementation of a design space as a Seq of Point.
 *
 *  it uses methods defined in Point for most of the computations
 */
private[exploration] class SeqSpace[T: TypeTag](private[qece] val inside: Seq[Point]) extends OrderedSpace[T] {
  override val size: Int                      = inside.size
  override lazy val head: Point               = inside.head
  override val headOption: Option[Point]      = inside.headOption
  override lazy val last: Point               = inside.last
  override val lastOption: Option[Point]      = inside.lastOption
  override lazy val min: Point                = Point.min(this.inside)
  override lazy val max: Point                = Point.max(this.inside)

  override def contains(p: Point): Boolean = inside contains p

  override def mapPoint(f: Point => Point): Space[T] = SeqSpace[T](this.inside.map(f))

  override def map[X](f: Point => X): Seq[X] = this.inside.map(f)

  override def foreach(f: Point => Unit): Unit = this.inside.foreach(f)

  override def filter(f: Point => Boolean): Space[T] = SeqSpace[T](this.inside.filter(f))

  override def filterNot(f: Point => Boolean): Space[T] = SeqSpace[T](this.inside.filterNot(f))

  override def toSeq: Seq[Point] = this.inside

  override def getNeighbours(p: Point, norm: Norm.Norm = Norm.One, maxDist: Int = 1): Seq[Point] =
    p.getNeighbours(inside, norm, maxDist)

  override def reduceDimension(metric: ImpactMetric, toMin: Boolean): Space[T] =
    SeqSpace[T](Point.reduceDimension(metric, toMin)(this.inside))

  override def rebuildSpace(metric: ImpactMetric)(initialSpace: Space[T]): Space[T] =
    SeqSpace[T](Point.rebuildSpace(metric)(initialSpace.toSeq, this.toSeq))

  override def sortWith(f: (Point, Point) => Boolean): OrderedSpace[T] = SeqSpace[T](this.inside.sortWith(f))

  override def reverse: OrderedSpace[T] = SeqSpace(this.inside.reverse)

  override def indexWhere(f: Point => Boolean): Int = this.inside.indexWhere(f)

  override def findMiddle(p: Point, q: Point): Point = p.findMiddle(this.inside, q)

  override def toString: String = this.inside.map(_.toString).mkString("\n")
}

private[exploration] object SeqSpace {
  def apply[T: TypeTag](seq: Seq[Point]): SeqSpace[T] = new SeqSpace[T](seq)
  def apply[T: TypeTag](space: Space[T]): SeqSpace[T] = new SeqSpace[T](space.toSeq)
}

private[exploration] object EmptySeqSpace extends SeqSpace(Seq.empty)
