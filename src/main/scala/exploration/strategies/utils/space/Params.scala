/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import qece.exploration.exceptions.ParamNotFoundException

/** Store params.
 *
 *  @param inside
 *    map-like representation of building parameters for a design we use a Seq to preserve order for module constructors
 */
private[exploration] case class Params(private[qece] inside: Seq[(String, Int)]) {
  require(inside.map(_._1).distinct.size == inside.size, s"Multiple parameters can not be named the same")
  val names  = inside.map(_._1)
  val values = inside.map(_._2)
  val dim    = inside.size

  override def toString: String = inside.map(x => s"${x._1}: ${x._2}").mkString("(", ", ", ")")

  /** Retrieve parameter value for index i
   *
   *  @param i
   *    index of parameter being searched for
   *  @return
   *    value of parameter with index i
   */
  def apply(i: Int): Int = {
    i < dim match {
      case true  => values(i)
      case false => throw ParamNotFoundException(s"Can not access to parameter #$i. Only got $dim parameters")
    }
  }

  def toMap: Map[String, Int] = inside.toMap

  def filter(f: ((String, Int)) => Boolean): Map[String, Int] =
    inside.filter(f).toMap

  def filterNot(f: ((String, Int)) => Boolean): Map[String, Int] =
    inside.filterNot(f).toMap

  /** Retrieve parameter value name $s
   *
   *  @param s
   *    name of parameter being searched for
   *  @return
   *    value of parameter with name s
   */
  def apply(s: String): Int = {
    names contains s match {
      case true => inside(names.indexOf(s))._2
      case false =>
        throw ParamNotFoundException(s"Did not found any param with name $s in ${names.mkString(", ")}")
    }
  }

  override def equals(that: Any): Boolean =
    that match {
      case that: Params =>
        that.canEqual(this) &&
          (this.inside.map(that.inside contains _)).reduceOption(_ & _).getOrElse(true) &&
          (that.inside.map(this.inside contains _)).reduceOption(_ & _).getOrElse(true)
      case _ => false
    }

  def map[T](f: ((String, Int)) => T): Seq[T] = inside.map(f)

  /** Define an order between two set of parameters, based on Integer natural order
   *
   *  @param that
   *    set of parameter to be compared to
   *  @return
   *    weither this parameter set is greater than that or not
   */
  def >=(that: Params): Boolean = (this.values zip that.values).map { case (a, b) => a >= b }.reduce(_ & _)

  def >(that: Params): Boolean = (this.values zip that.values).map { case (a, b) => a > b }.reduce(_ & _)

  def <=(that: Params): Boolean = (this.values zip that.values).map { case (a, b) => a <= b }.reduce(_ & _)

  def <(that: Params): Boolean = (this.values zip that.values).map { case (a, b) => a < b }.reduce(_ & _)

  /** Add a new parameter dimension to this point
   *
   *  @param s
   *    key of the parameter (name)
   *  @param v
   *    value of the parameter
   */
  def add(s: String, v: Int): Params = this.copy(inside = inside :+ (s -> v))

  /** Update the value of the parameter named s
   *
   *  @param s
   *    name of the parameter to be updated
   *  @param v
   *    value to update parameter with
   */
  def update(s: String, v: Int): Params = this.remove(Seq(s)).add(s, v)

  /** Remove all dimensions named in the argument
   *
   *  @param s
   *    names of the dimensions to be removed for this [[Params]]
   */
  def remove(s: Seq[String]): Params = this.copy(inside = inside.filterNot(k => s contains k._1))

  /** Compute the distance (Manhattan distance) between this [[Params]] and an other
   *
   *  @param p
   *    other point to compute distance from
   */
  def distance(p: Params): Double =
    ((this.values zip p.values).map { case (x, y) => (x - y).abs }.sum).toDouble
}
