/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import qece.exploration.annotations.ImpactMetric
import qece.exploration.strategies.utils.exceptions._

import scala.reflect.runtime.universe.TypeTag

/** Abstract design space implementation as a matrix of Point.
 *
 *  @param sets
 *    value sets for each dimension
 */
private[exploration] abstract class MatrixSpace[T: TypeTag](private[qece] val sets: Map[String, Seq[Int]])
  extends Space[T] {
  val sizes: Seq[Int] = sets.values.map(_.size).toSeq
  val dimension: Int  = sets.size

  override val size: Int = toSeq.size
  
  override lazy val head: Point          = toSeq.head
  override val headOption: Option[Point] = toSeq.headOption
  override lazy val last: Point          = toSeq.last
  override val lastOption: Option[Point] = toSeq.lastOption

  // try to estimate the min point using only the structure
  protected val minOption: Option[Point]
  // try to estimate the max point using only the structure
  protected val maxOption: Option[Point]

  override lazy val min: Point = 
    minOption match {
      case Some(p)  => p
      case _        => toSeq.sortWith{ case (a, b) => indexWhere(a).reduce(_+_) < indexWhere(b).reduce(_+_)}.head
    }

  override lazy val max: Point = 
    maxOption match {
      case Some(p)  => p
      case _        => toSeq.sortWith{ case (a, b) => indexWhere(a).reduce(_+_) > indexWhere(b).reduce(_+_) }.head
    }

  override def map[X](f: Point => X): Seq[X] = toSeq.map(f)

  override def foreach(f: Point => Unit): Unit = toSeq.foreach(f)

  override def toString: String = toSeq.mkString("\n")

  // for non sparse matrix, could use previous implementation
  override def reduceDimension(metric: ImpactMetric, toMin: Boolean): Space[T] = {
    val prunedKeys    = constructor.paramNames diff constructor.factory.getImpactingParams(metric)
    prunedKeys.size match {
      case 0                                => this
      case s if 1 to sets.size contains s   => MatrixSpace(SeqSpace(this.toSeq).reduceDimension(metric, toMin))
      case _  => throw SpaceOperationException(s"Can not reduce ${getClass.getSimpleName} to null space")
    }
  }


  /** Return the set of values possible for a given parameter position
   *
   *  @param i
   *    parameter position
   */
  def getSet(i: Int): Seq[Int] = {
    require(i < sets.size, s"Can not access to dimension $i in a ${sets.size} dimensional set")
    sets.values.toSeq(i)
  }

  /** Return the set of values possible for a given parameter name
   *
   *  @param n
   *    parameter name
   */
  def getSet(n: String): Seq[Int] =
    sets contains n match {
      case true  => sets(n)
      case false => throw UndefinedDimensionException(s"Did not found dimension with name $n")
    }

  /** Return the dimension of the parameter of name n
   *
   *  @param n
   *    name of the parameter
   */
  def getPosition(n: String): Int =
    sets contains n match {
      case true  => sets.keys.toSeq.indexWhere(_ == n)
      case false => throw UndefinedDimensionException(s"Did not found dimension with name $n")
    }

  /** Return a Seq of index to get position of this point in the space
   *
   *  @param p
   *    point being searched for
   */
  def indexWhere(p: Point): Seq[Int] = {
    val realParams = p.params.toMap
    require(realParams.size == sets.size, s"Can not position ${realParams.size}D point in ${sets.size}D space")
    sets.map { case (k, v) => v.indexWhere(_ == realParams(k)) }.toSeq
  }

  def indexWhere(params: Seq[Int]): Seq[Int] = {
    require(params.size == sets.size, s"Can not position ${params.size}D point in ${sets.size}D space")
    sets.values.zipWithIndex.map { case (v, i) => v.indexWhere(_ == params(i)) }.toSeq
  }

  def isSparse: Boolean = this.sizes.reduce(_*_) != this.size
}

private[exploration] object MatrixSpace {
  def apply[T: TypeTag](sets: Map[String, Seq[Int]], mat: Array[Point]): MatrixSpace[T] =
    Space1D[T](sets, mat)
  def apply[T: TypeTag](sets: Map[String, Seq[Int]], mat: Array[Array[Point]]): MatrixSpace[T] =
    Space2D[T](sets, mat)
  def apply[T: TypeTag](sets: Map[String, Seq[Int]], mat: Array[Array[Array[Point]]]): MatrixSpace[T] =
    Space3D[T](sets, mat)
  def apply[T: TypeTag](sets: Map[String, Seq[Int]], mat: Array[Array[Array[Array[Point]]]]): MatrixSpace[T] =
    Space4D[T](sets, mat)
  def apply[T: TypeTag](sets: Map[String, Seq[Int]], mat: Array[Array[Array[Array[Array[Point]]]]]): MatrixSpace[T] =
    Space5D[T](sets, mat)

  def buildFromSpace[T: TypeTag](space: Space[T], sets: Map[String, Seq[Int]]): Array[_] = {
    sets.size match {
      case 1 =>
        Array.tabulate(sets.toSeq(0)._2.size) { p0 =>
          val point = Point(List(sets.toSeq(0)._1 -> sets.toSeq(0)._2.toSeq(p0)))
          space.filter(_ == point).headOption match {
            case Some(p) => p
            case None    => SparsePoint(point.params)
          }
        }.asInstanceOf[Array[Point]]
      case 2 =>
        Array.tabulate(sets.toSeq(0)._2.size) { p0 =>
          Array.tabulate(sets.toSeq(1)._2.size) { p1 =>
            val point =
              Point(
                List(
                  sets.toSeq(0)._1 -> sets.toSeq(0)._2.toSeq(p0),
                  sets.toSeq(1)._1 -> sets.toSeq(1)._2.toSeq(p1)
                )
              )
            space.filter(_ == point).headOption match {
              case Some(p) => p
              case None    => SparsePoint(point.params)
            }
          }
        }.asInstanceOf[Array[Array[Point]]]
      case 3 =>
        Array.tabulate(sets.toSeq(0)._2.size) { p0 =>
          Array.tabulate(sets.toSeq(1)._2.size) { p1 =>
            Array.tabulate(sets.toSeq(2)._2.size) { p2 =>
              val point =
                Point(
                  List(
                    sets.toSeq(0)._1 -> sets.toSeq(0)._2.toSeq(p0),
                    sets.toSeq(1)._1 -> sets.toSeq(1)._2.toSeq(p1),
                    sets.toSeq(2)._1 -> sets.toSeq(2)._2.toSeq(p2)
                  )
                )
              space.filter(_ == point).headOption match {
                case Some(p) => p
                case None    => SparsePoint(point.params)
              }
            }
          }
        }.asInstanceOf[Array[Array[Array[Point]]]]
      case 4 =>
        Array.tabulate(sets.toSeq(0)._2.size) { p0 =>
          Array.tabulate(sets.toSeq(1)._2.size) {
            p1 =>
              Array.tabulate(sets.toSeq(2)._2.size) { p2 =>
                Array.tabulate(sets.toSeq(3)._2.size) { p3 =>
                  val point =
                    Point(
                      List(
                        sets.toSeq(0)._1 -> sets.toSeq(0)._2.toSeq(p0),
                        sets.toSeq(1)._1 -> sets.toSeq(1)._2.toSeq(p1),
                        sets.toSeq(2)._1 -> sets.toSeq(2)._2.toSeq(p2),
                        sets.toSeq(3)._1 -> sets.toSeq(3)._2.toSeq(p3)
                      )
                    )
                  space.filter(_ == point).headOption match {
                    case Some(p) => p
                    case None    => SparsePoint(point.params)
                  }
                }
              }
          }
        }.asInstanceOf[Array[Array[Array[Array[Point]]]]]
      case 5 =>
        Array.tabulate(sets.toSeq(0)._2.size) { p0 =>
          Array.tabulate(sets.toSeq(1)._2.size) { p1 =>
            Array.tabulate(sets.toSeq(2)._2.size) {
              p2 =>
                Array.tabulate(sets.toSeq(3)._2.size) { p3 =>
                  Array.tabulate(sets.toSeq(4)._2.size) { p4 =>
                    val point =
                      Point(
                        List(
                          sets.toSeq(0)._1 -> sets.toSeq(0)._2.toSeq(p0),
                          sets.toSeq(1)._1 -> sets.toSeq(1)._2.toSeq(p1),
                          sets.toSeq(2)._1 -> sets.toSeq(2)._2.toSeq(p2),
                          sets.toSeq(3)._1 -> sets.toSeq(3)._2.toSeq(p3),
                          sets.toSeq(4)._1 -> sets.toSeq(4)._2.toSeq(p4)
                        )
                      )
                    space.filter(_ == point).headOption match {
                      case Some(p) => p
                      case None    => SparsePoint(point.params)
                    }
                  }
                }
            }
          }
        }.asInstanceOf[Array[Array[Array[Array[Array[Point]]]]]]
      case _ => throw SpaceOperationException(s"Can not build a ${sets.size}D matrix space")
    }
  }

  def apply[T: TypeTag](space: Space[T]): MatrixSpace[T] =
    space match {
      case m: MatrixSpace[T] => m
      case _ =>
        val params  = space.toSeq.map(_.params)
        val sets    = params.head.map { case (k, _) => (k -> params.map(_(k))) }
          .map { case (k, v) => (k -> v.distinct.toSeq.sortWith(_ < _)) }.toMap
        val inside = buildFromSpace(space, sets)
        sets.size match {
          case 1 =>
            Space1D(sets, inside.asInstanceOf[Array[Point]])
          case 2 =>
            Space2D(sets, inside.asInstanceOf[Array[Array[Point]]])
          case 3 =>
            Space3D(sets, inside.asInstanceOf[Array[Array[Array[Point]]]])
          case 4 =>
            Space4D(sets, inside.asInstanceOf[Array[Array[Array[Array[Point]]]]])
          case 5 =>
            Space5D(sets, inside.asInstanceOf[Array[Array[Array[Array[Array[Point]]]]]])
        }
    }
}

private[exploration] object EmptySpace extends Space2D(Map.empty, Array.empty)
