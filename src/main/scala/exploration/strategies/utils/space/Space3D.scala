/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import scala.reflect.runtime.universe.TypeTag

import qece.exploration.annotations.ImpactMetric

import qece.estimation.MetricMap

import qece.exploration.strategies.utils.exceptions._

/** Represent a 3D matrix of implementations
 *
 *  Space may be sparse, with every missing point represented as a SparsePoint
 */
private[exploration] class Space3D[T: TypeTag](
    sets: Map[String, Seq[Int]],
    private[qece] val inside: Array[Array[Array[Point]]]
) extends MatrixSpace[T](sets) {
  require(
      inside.size == 0 ||
        inside.map(i => i.size == inside.head.size).reduce(_ & _) ||
        inside.map(i => i.map(ii => ii.size == inside.head.head.size).reduce(_ & _)).reduce(_ & _),
      s"All dimensions should be of size ${inside.head.size}"
  )
  require(sets.size == 3, s"Expected a 3D space, found a ${sets.size}D one")

  override lazy val minOption: Option[Point] = {
    val indexes = indexWhere(sets.values.map(_.min).toSeq)
    inside(indexes(0))(indexes(1))(indexes(2)) match {
      case _: SparsePoint => None
      case p              => Some(p)
    }
  }
  override lazy val maxOption: Option[Point] = {
    val indexes = indexWhere(sets.values.map(_.max).toSeq)
    inside(indexes(0))(indexes(1))(indexes(2)) match {
      case _: SparsePoint => None
      case p              => Some(p)
    }
  }

  override def contains(p: Point): Boolean = {
    val indexes = indexWhere(p)
    !inside(indexes(0))(indexes(1))(indexes(2)).isInstanceOf[SparsePoint]
  }

  override def mapPoint(f: Point => Point): Space[T] =
    Space3D[T](sets, inside.map(_.map(_.map(p => if (!p.isInstanceOf[SparsePoint]) f(p) else p))))

  override def filter(f: Point => Boolean): Space[T] =
    Space3D[T](
      sets, 
      inside.map(_.map(_.map(p => if (!p.isInstanceOf[SparsePoint] && f(p)) p else SparsePoint(p.params))))
    )

  override def filterNot(f: Point => Boolean): Space[T] =
    Space3D[T](
      sets,
      inside.map(_.map(_.map(p => if (!p.isInstanceOf[SparsePoint] && !f(p)) p else SparsePoint(p.params))))
    )

  override def toSeq: Seq[Point] =
    this.inside.flatMap(_.flatMap(_.filterNot(_.isInstanceOf[SparsePoint]).toSeq))

  override def getNeighbours(p: Point, norm: Norm.Norm = Norm.One, maxDist: Int = 1): Seq[Point] =
    (norm match {
      case Norm.One =>
        val init = indexWhere(p)
        val indexes =
          init.zipWithIndex.map {
            case (i, ii) => (i - maxDist to i + maxDist).filter(t => t >= 0 && t < sizes(ii))
          }.toSeq
        (for (ix <- indexes(0); iy <- indexes(1); iz <- indexes(2)) yield {
          if (Seq((init(0) - ix).abs, (init(1) - iy).abs, (init(2) - iz).abs).sum <= maxDist) {
            inside(ix)(iy)(iz)
          } else EmptyPoint
        }).filterNot(_ == EmptyPoint) diff Seq(p)
      case Norm.Max =>
        val indexes =
          indexWhere(p).zipWithIndex.map {
            case (i, ii) => (i - maxDist to i + maxDist).filter(t => t >= 0 && t < sizes(ii))
          }.toSeq
        (for (ix <- indexes(0); iy <- indexes(1); iz <- indexes(2)) yield inside(ix)(iy)(iz)) diff Seq(p)
    }).filterNot(_.isInstanceOf[SparsePoint])

  override def reduceDimension(metric: ImpactMetric, toMin: Boolean): Space[T] = {
    val prunedKeys    = constructor.paramNames diff constructor.factory.getImpactingParams(metric)
    val prunedIndexes = prunedKeys.map(getPosition(_))
    prunedKeys.size match {
      case 0 => this
      case 1 =>
        Space2D(
            sets.filterNot { case (k, _) => prunedKeys.contains(k) },
            prunedIndexes.head match {
              case 0 => inside(0)
              case 1 => inside.map(_(0))
              case 2 => inside.map(_.map(_(0)))
              case _ => throw SpaceOperationException(s"Impossible combination for reducing dimensions")
            }
        )
      case 2 =>
        Space1D(
            sets.filterNot { case (k, _) => prunedKeys.contains(k) },
            (prunedIndexes(0), prunedIndexes(1)) match {
              case (0, 1) => inside(0)(0)
              case (1, 2) => inside.map(_(0)(0))
              case (0, 2) => inside(0).map(_(0))
              case (_, _) => throw SpaceOperationException(s"Impossible combination for reducing dimensions")
            }
        )
      case _ => throw SpaceOperationException(s"Can not reduce 3D space to nul space")
    }
  }

  override def rebuildSpace(metric: ImpactMetric)(initialSpace: Space[T]): Space[T] =
    initialSpace match {
      case m: MatrixSpace[T] =>
        val newKeys = constructor.paramNames diff constructor.factory.getImpactingParams(metric)
        assert(
            newKeys.size + this.dimension == m.dimension,
            s"Can not rebuild a ${m.dimension}D space from a ${this.dimension}D space and ${newKeys.size} new dimensions"
        )
        this.isEmpty match {
          case true   => m.mapPoint(p => SparsePoint(p.params))
          case false  => 
            val newMetricNames = this.toSeq.head.metricNames diff initialSpace.toSeq.head.metricNames
            m.mapPoint {
              case p: Point =>
                val indexes = indexWhere(p.remove(newKeys))
                val nearest = inside(indexes(0))(indexes(1))(indexes(2))
                nearest match {
                  case x: SparsePoint => x
                  case _ =>
                    p.copy(metrics =
                      p.metrics ++&
                      new MetricMap(nearest.metrics.toMap.filter { case (k, _) => newMetricNames contains k })
                      )
                }
            }
        }
      case _ => SeqSpace[T](Point.rebuildSpace(metric)(initialSpace.toSeq, this.toSeq))
    }

  override def findMiddle(p: Point, q: Point): Point = {
    val indexes = (indexWhere(p) zip indexWhere(q)).map { case (ip, iq) => (iq + ip) / 2 }.toSeq
    inside(indexes(0))(indexes(1))(indexes(2)) match {
      case _: SparsePoint => throw SpaceOperationException(s"Can't find middle in space")
      case s              => s
    }
  }
}

private[exploration] object Space3D {
  def apply[T: TypeTag](params: Map[String, Seq[Int]], mat: Array[Array[Array[Point]]]): Space3D[T] =
    new Space3D[T](params, mat)
}

private[exploration] object EmptySpace3D extends Space3D(Map.empty, Array.empty)
