/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import qece.exploration.utils.Constructor
import qece.exploration.annotations.ImpactMetric

import scala.reflect.runtime.universe.TypeTag

private[exploration] abstract class Space[T: TypeTag] {
  // call it lazy in order for EmptySpace to work
  protected lazy val constructor = new Constructor[T]

  val size: Int
  val head: Point
  val headOption: Option[Point]
  val last: Point
  val lastOption: Option[Point]
  val min: Point
  val max: Point

  /** Check if a given point is in this space (w.r.t. to [[Point]] equality definition)
   *
   *  @param p
   *    point to check against
   */
  def contains(p: Point): Boolean

  /** Apply a given function over the whole space, and return the updated space
   *
   *  @param f
   *    function to apply
   */
  def mapPoint(f: Point => Point): Space[T]

  /** Apply a given function over the whole space, and return the generated Seq
   *
   *  @param X
   *    type of return elements
   *  @param f
   *    function to apply
   */
  def map[X](f: Point => X): Seq[X]

  /** Apply a given function over the whole space
   *
   *  @param f
   *    function to apply
   */
  def foreach(f: Point => Unit): Unit

  /** Filter a given space
   *
   *  @param f
   *    function used for filtering
   */
  def filter(f: Point => Boolean): Space[T]

  /** Filter a given space
   *
   *  @param f
   *    function used for filtering
   */
  def filterNot(f: Point => Boolean): Space[T]

  /** Map this space to a Seq[Point]. */
  def toSeq: Seq[Point]

  /** Retrieve all neighbours of a given [[Point]] in this space.
   *
   *  @param p
   *    point to check neighbourhood
   *  @param norm
   *    norm to be used for distance definition
   *  @param maxDist
   *    maximum distance to be considered in the neighbourhood
   */
  def getNeighbours(p: Point, norm: Norm.Norm = Norm.One, maxDist: Int = 1): Seq[Point]

  /** Reduce dimension of this space w.r.t. to a particular metric (as annotated in Module constructor).
   *
   *  Reduction is performed by projecting each [[Point]] in this space on given dimensions,
   *  keeping only one value for each dimension: 
   *  considering points (ax, bx, cx), projecting (a1, b2, c3) on dimension c consists in finding all points
   *  (a1, b2, cx), and keep only one.
   *
   *  @param metric
   *    metric used to define which dimensions to remove
   *  @param toMin
   *    define how projection is performed. 
   *    If true, project on smallest value found for this projection
   *    If false, project on greatest value
   */
  def reduceDimension(metric: ImpactMetric, toMin: Boolean = true): Space[T]

  /** Rebuild this space from an initial one, assuming an exploration pass has been performed.
   *
   *  Generated metrics are propagated to all points restored.
   *
   *  @param metric
   *    metric used to define which dimensions are to be restored
   *  @param initialSpace
   *    space as it was before reduceDimension was applied
   */
  def rebuildSpace(metric: ImpactMetric)(initialSpace: Space[T]): Space[T]

  /** Return the middle of two [[Point]]s in this space. */
  def findMiddle(p: Point, q: Point): Point

  def toString: String

  def isEmpty: Boolean = this.size == 0

  def structure: String = this.getClass.getSimpleName

  override def equals(t: Any): Boolean = {
    t match {
      case s: Space[T] =>
        (this.size == s.size) && (this.map(s.contains _).reduce(_ & _)) && (s.map(this.contains _).reduce(_ & _))
      case _ => false
    }
  }
}
