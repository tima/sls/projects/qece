/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import qece.QeceContext

import qece.estimation.{MetricMap, Metric}

import qece.exploration.utils.Constructor
import qece.exploration.annotations.ImpactMetric

import scala.reflect.runtime.universe.TypeTag

/** Store a design space belonging point, as well as possible already found metrics.
 *
 *  @param params
 *    [[Params]] used for the design building
 *  @param metrics
 *    [[qece.estimation.MetricMap]] representing metrics associated to this implementation
 *  @param costs
 *    costs associated to this implementation
 */
private[qece] case class Point(
  params: Params,
  metrics: MetricMap = MetricMap.empty,
  costs: Seq[Double] = Seq.empty
) {
  lazy val paramSeq: Seq[Int]        = params.values
  lazy val toParamNames: Seq[String] = params.names
  lazy val paramDim: Int             = params.dim
  lazy val metricNames: Set[String]  = metrics.keys

  def cost: Double = costs.headOption.getOrElse(QeceContext.core.defaultCost)

  def :+(cost: Double) = this.copy(costs = Seq(cost) ++ this.costs)

  def canEqual(a: Any) = a.isInstanceOf[Point]

  override def equals(that: Any): Boolean =
    that match {
      case that: Point => that.canEqual(this) && that.params == this.params
      case _           => false
    }

  def >=(that: Any): Boolean =
    that match {
      case that: Point => that.canEqual(this) && (this.params >= that.params)
      case _           => false
    }

  def >(that: Any): Boolean =
    that match {
      case that: Point => that.canEqual(this) && (this.params > that.params)
      case _           => false
    }

  def <=(that: Any): Boolean =
    that match {
      case that: Point => that.canEqual(this) && (this.params <= that.params)
      case _           => false
    }

  def <(that: Any): Boolean =
    that match {
      case that: Point => that.canEqual(this) && (this.params < that.params)
      case _           => false
    }

  override def toString: String =
    Seq(
        s"Params: $params",
        "Metrics:",
        metrics.prettyToString(2),
        "Costs: " + costs.mkString("[", ", ", "]")
    ).mkString("\n")

  def remove(s: Seq[String]): Point = this.copy(params = params.remove(s))
  def empty: Point                  = this.copy(metrics = MetricMap.empty)

  /** Retrieve alli possible values for each dimensions
   *
   *  @param space
   *    input space
   *  @return
   *    a list of list of all possible values for each dimension
   */
  private def getDimensionValues(space: Seq[Point]): Seq[Seq[Int]] =
    space.head.params.names.map {
      case name =>
        space.map(_.params(name)).distinct.sortWith(_ < _)
    }

  /** Compute a distance between two point in a given space. Distance is computed using only possible values in the
   *  space. e.g. if a dim is {1, 4, 1000}, d(1, 4) == d(4, 1000) = 1/2 d(1, 1000)
   *
   *  @param dim
   *    possible values of each dimension, as computed with getDimensionValues
   *  @param norm
   *    norm to use (a.t.m. either sum or max of coordinate diff is considered
   *
   *  @param p
   *    first point
   *  @param q
   *    second point
   *
   *  @return
   *    the distance between p and q in the given space
   */
  private def computeDistance(dim: Seq[Seq[Int]], norm: Norm.Norm)(p: Point, q: Point): Int = {
    norm match {
      case Norm.One =>
        dim.zipWithIndex.map {
          case (d, i) =>
            (d.indexWhere(_ == p.params(i)) - d.indexWhere(_ == q.params(i))).abs
        }.sum
      case Norm.Max =>
        dim.zipWithIndex.map {
          case (d, i) =>
            (d.indexWhere(_ == p.params(i)) - d.indexWhere(_ == q.params(i))).abs
        }.max
    }
  }

  /** Retrieve all neighbours of [[this]] point in a given space
   *
   *  @param space
   *    design space to search in
   *  @param norm
   *    norm to use to define neighbourhood.
   *  @param maxDist
   *    maximum distance as accepted in this neighbourhood
   *
   *  Neighbourhood is defined as every points with a distance of maxDist to this point (in term of implementations),
   *  with respect to chosen norm
   *
   *  @return
   *    the set of neighbour points for this particular point
   */
  def getNeighbours(space: Seq[Point], norm: Norm.Norm = Norm.One, maxDist: Int = 1): Seq[Point] = {
    def distance(p: Point, q: Point): Int = computeDistance(getDimensionValues(space), norm)(p, q)
    space.filterNot(_ == this).filter(p => distance(this, p) <= maxDist)
  }

  /** Check if a given point is a neighbour of [[this]], with respect to a given norm and maximum distance See
   *  [[this.getNeighbours]] for more info on neighbourhoods
   *
   *  @param space
   *    space to look into
   *  @param norm
   *    norm to use to define distance between two points
   *  @param maxDist
   *    maximum distance to be a neighbour
   *
   *  @param that
   *    distant point to check
   *
   *  @return
   *    weither [[this]] and [[that]] are neighbours, given a particular norm and maximum distance
   */
  def isNeighbour(space: Seq[Point], norm: Norm.Norm = Norm.One, maxDist: Int = 1)(that: Point): Boolean =
    (this != that) && (computeDistance(getDimensionValues(space), norm)(this, that) <= maxDist)

  /** Find the point in the space that is closest to this point
   *
   *  @param space
   *    input space to search in
   */
  def findNearest(space: Seq[Point]): Point =
    space.sortWith { case (a, b) => a.params.distance(this.params) < b.params.distance(this.params) }.head

  /** Find the middle point of this and a high point in a space
   *
   *  @param space
   *    space to search in
   *  @param high
   *    high point for middle search
   */
  def findMiddle(space: Seq[Point], high: Point): Point =
    Point(this.params.names.map(n => (n -> (this.params(n) + (high.params(n) - this.params(n)) / 2))).toList)
      .findNearest(space)
}

object Point {
  def apply(params: (String, Int)*) =
    new Point(Params(params), new MetricMap(params.map { case (k, v) => k -> Metric(v) }.toMap))
  def apply(params: List[(String, Int)]) =
    new Point(Params(params), new MetricMap(params.map { case (k, v) => k -> Metric(v) }.toMap))

  /** Reduce parameter dimensions of a design space
   *
   *  @param metric
   *    metric annotation to specify which metric is being explored
   *  @param toMin
   *    if true, project on lowest value, else on greatest
   *  @param space
   *    initial design space
   *  @return
   *    the design space where useless dimensions have been removed 
   */
  def reduceDimension[T: TypeTag](metric: ImpactMetric, toMin: Boolean = true)(space: Seq[Point]): Seq[Point] = {
    val constructor = new Constructor[T]
    val keys        = constructor.paramNames diff constructor.factory.getImpactingParams(metric)
    val resultMap   = new scala.collection.mutable.HashMap[Params, Point]()
    def sorting(a: Int, b: Int) = if (toMin) (a < b) else (a > b)
    space.foreach{ point =>
      val params = point.params.remove(keys)
      resultMap.get(params) match {
        case Some(p)  => sorting(keys.map(p.params(_)).sum, keys.map(point.params(_)).sum) match {
          case true   => // nothing to update
          case false  => resultMap(params) = point
        }
        case None     => resultMap(params) = point
      }
    }
    resultMap.values.map(_.remove(keys)).toSeq
  }

  /** Increase parameter dimensions of an already reduced design space, using initial design space to do so
   *
   *  @param initialSpace
   *    initial space as was before pruning
   *  @param prunedSpace
   *    new space (with new metrics) to be restored
   *  @return
   *    initialSpace enhanced with new metrics
   */
  def rebuildSpace[T: TypeTag](metric: ImpactMetric)(initialSpace: Seq[Point], prunedSpace: Seq[Point]): Seq[Point] = {
    val constructor    = new Constructor[T]
    val keys           = constructor.paramNames diff constructor.factory.getImpactingParams(metric)
    val newMetricNames = prunedSpace.head.metricNames diff initialSpace.head.metricNames

    def findPoint(point: Point): Option[Point] = {
      prunedSpace.find(p => p.remove(keys).params == point.remove(keys).params)
    }

    initialSpace
      .map(point =>
        findPoint(point) match {
          case Some(p) =>
            point.copy(metrics =
              point.metrics ++&
                new MetricMap(p.metrics.toMap.filter { case (k, _) => newMetricNames contains k })
            )
          case None => EmptyPoint
        }
      )
      .filterNot(_ == EmptyPoint)
  }

  /** Find point in space with minimal parameters, assuming natural order of Int
   *
   *  @param space
   *    space to search in
   */
  def min(space: Seq[Point]): Point =
    Point(space.head.params.names.map(n => n -> space.map(_.params(n)).min).toList).findNearest(space)

  /** Find point in space with maximal parameters, assuming natural order of Int
   *
   *  @param space
   *    space to search in
   */
  def max(space: Seq[Point]): Point =
    Point(space.head.params.names.map(n => n -> space.map(_.params(n)).max).toList).findNearest(space)
}

private[exploration] class SparsePoint(params: Params, metrics: MetricMap) extends Point(params, metrics, Seq.empty)

private[exploration] object SparsePoint {
  def apply(params: Params): SparsePoint =
    new SparsePoint(params, MetricMap(params.map { case (k, v) => (k -> Metric(v)) }: _*))
}

private[exploration] object EmptyPoint extends SparsePoint(new Params(Seq.empty), MetricMap.empty)

private[exploration] class FailurePoint(params: Params, metrics: MetricMap) extends SparsePoint(params, metrics) {
  def apply(p: Point): FailurePoint = new FailurePoint(p.params, MetricMap.empty)
}

private[exploration] object EmptyFailurePoint extends FailurePoint(new Params(Seq.empty), MetricMap.empty)

