/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import qece.{QeceContext, FileUtils}
import qece.exploration.exceptions._

import scala.reflect.runtime.universe.TypeTag

import scala.collection.mutable.HashMap

import qece.exploration.utils.Constructor

private[exploration] trait WithCsvResults {
  // cache results for display purposes
  val results = HashMap[String, Space[_]]()

  /** Formatting part. */
  private def getCsvContent(top: String, sep: String): String = {
    val res = getResults(top).toSeq
    res.size match {
      case 0 => ""
      case _ =>
        val costNumber = res.head.costs.size
        Seq(
            res.head.metrics.getCsvHeader(
                Seq("Board", "Top") ++ (0 until costNumber).map(i => s"Cost[$i]"),
                sep
            ),
            res
              .map(x =>
                x.metrics.asCsvEntry(
                    (Seq(QeceContext.backend.targetBoard, top) ++ x.costs).map(_.toString),
                    sep
                )
              )
              .mkString("\n")
        ).mkString("\n")
    }
  }

  /** Write back the exploration result for a given top to a file. If no top was specified, check if only one exists and
   *  write it back, or throw an exception
   *
   *  @param filename
   *    file where result will be written
   *  @param top
   *    top being targeted. If not provided, assume there is only one
   *  @param sep
   *    separator character for the file being generated
   */
  def writeBack(
    filename: String,
    top: Option[String] = None,
    sep: String = QeceContext.emission.csvSeparator
  ): Unit = {
    val t = (top, results.keySet.size) match {
      case (Some(t), _) => t
      case (None, 1)    => results.keySet.head
      case (None, 0) =>
        throw ResultsNotFoundException(s"Did not found any result for this strategy. Did you run exploration ?")
      case (None, _) =>
        throw ManyTopFoundException(
            s"Found more than one top: ${results.keySet.mkString("[", ", ", "]")}\n" +
              "Please specify the exploration target."
        )
    }
    getCsvContent(t, sep).writeTo(filename)
  }

  def writeBack[T: TypeTag](filename: String, sep: String): Unit = {
    writeBack(filename, Some(new Constructor[T].className), sep)
  }

  /** Return the exploration result for a given top, if available
   *
   *  @param top
   *    top target
   */
  def getResults[T: TypeTag](top: String): Space[T] = {
    results contains top match {
      case true  => results(top).asInstanceOf[Space[T]]
      case false => throw ResultsNotFoundException(s"Did not found results for top $top. Did you run exploration ?")
    }
  }

  /** Return the exploration result for a given top, if given
   *
   *  @param T
   *    type T of the top target
   */
  def getResults[T: TypeTag]: Space[T] = {
    getResults(new Constructor[T].className)
  }
}
