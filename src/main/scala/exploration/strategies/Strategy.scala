/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies

import qece.CustomLogging

import qece.estimation.WithTransform
import qece.estimation.transforms.TransformSeq

import qece.exploration.utils.Constructor
import qece.exploration.annotations.ImpactMetric
import qece.exploration.strategies.utils.{Point, FailurePoint, Space, WithCsvResults}

import scala.reflect.runtime.universe.TypeTag

/** Class for storing an exploration strategy
 *
 *  All the strategy calls should be done from a [[StrategyBuilder]], to properly use the [[qece.QeceContext]].
 *
 *  @param transforms
 *    transforms to be runned for this exploration
 *  @param metric
 *    if defined, define which parameters are important for this exploration
 */
abstract class Strategy(
    val transforms: TransformSeq,
    val metric: Option[ImpactMetric] = None
) extends WithTransform
    with CustomLogging
    with WithCsvResults {

  /** Function to explore and sort implementations
   *
   *  @param space
   *    design space being explored
   *  @return
   *    another design space. new design space may include new metrics, and may be pruned, sorted, ... according to
   *    strategies
   */
  protected def optimize[T: TypeTag](space: Space[T]): Space[T]

  /** Wrapper for the optimize function, enabling to reduce space dimension before exploration to restrain the number of
   *  implementations, and extrapolate results to rebuild space at the end
   *
   *  @param space
   *    initial design space
   *  @return
   *    design space after running optimization
   */
  def runOptimize[T: TypeTag](space: Space[T]): Space[T] =
    metric match {
      case None    => optimize[T](space)
      case Some(m) => 
        optimize[T](space.reduceDimension(m)).rebuildSpace(m)(space)
    }

  /** Run the exploration process on a given MetaDesign
   *
   *  @param T
   *    type of the module being explored
   *  @return
   *    returns another design space. new design space may include new metrics, and may be pruned, sorted, ... according
   *    to strategies
   */
  def explore[T: TypeTag]: Space[T] = {
    val constructor = new Constructor[T]
    // using a matrix space to preserve dimensions
    val initialSpace = constructor.buildMatrixSpace
    mylog(
        s"Exploring ${initialSpace.size} different implementations with ${this.getClass.getSimpleName}."
    )
    results += (constructor.className -> runOptimize[T](initialSpace))
    mylog(
        s"Returned ${results(constructor.className).size} different implementations with ${this.getClass.getSimpleName}."
    )
    results(constructor.className).asInstanceOf[Space[T]]
  }

  protected def partitionSpace[T: TypeTag](space: Space[T]): (Seq[Point], Seq[Point]) =
    space.toSeq.partition(!_.isInstanceOf[FailurePoint])

  protected def logBegin(str: String, size: Int): Unit =
    mylog(
        " " * 4 +
          s"Running $str on $size different implementations using transforms ${transforms.serialize}"
    )
}
