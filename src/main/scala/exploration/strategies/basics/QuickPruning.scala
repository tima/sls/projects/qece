/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.basics

import qece.estimation.{MetricMap, Metric}
import qece.estimation.transforms.TransformSeq

import qece.exploration.strategies.utils.{Point, FailurePoint, Space, SeqSpace, MatrixSpace, Norm}
import qece.exploration.strategies.func.PruningFunction
import qece.exploration.annotations.ImpactMetric
import qece.exploration.exceptions._

import scala.reflect.runtime.universe.TypeTag
import scala.collection.mutable.ArrayBuffer

/** A multi-thread quick pruning strategy for exploration.
 *
 *  This algorithm is based on the hypothesis that the design space is partitionned in two compact sets between pruned
 *  and non pruned implementations, meaning that we can partition the space by finding the frontier between those two
 *  sets and extrapolate estimations to all implementations above this frontier
 *
 *  Frontier search is done in two times:
 *    - we start by finding a starting point on the frontier, by running a dichotomous search on the space
 *    - if no starting point is found, it means that the whole space is either to be pruned or not
 *    - if a starting point is found, we use it to build a frontier by iteratively exploring neigbhourhood of points
 *      that are found on the frontier
 */
class QuickPruning(
    tfs: TransformSeq,
    prune: PruningFunction,
    metric: Option[ImpactMetric] = None
) extends ParallelStrategy(tfs, metric) {
  val cache        = new ArrayBuffer[Point]()
  val failureCache = new ArrayBuffer[Point]()

  // check weither a value is in cache
  private def inCaches(p: Point): Boolean = (cache contains p) || (failureCache contains p)

  // retrieve point with same params but updated metrics, if in cache
  private def hitCaches(p: Point): Option[Point] =
    cache contains p match {
      case false =>
        failureCache contains p match {
          case true  => Some(new FailurePoint(p.params, p.metrics))
          case false => None
        }
      case true => Some(cache(cache.indexWhere(_ == p)))
    }

  private def updateCaches[T: TypeTag](seq: Seq[Point]): Unit = {
    // only run estimations for not cached points
    seq.filterNot(inCaches(_)).distinct.toSeq match {
      case Seq()  => 
      case s      =>
        //println(s"[${Utils.getTime}] updating cache with ${seq.size} [${s.size}]")
        val results = runParallelWithPartition(SeqSpace(s))
        cache ++= results._1
        failureCache ++= results._2
    }
  }

  private def evaluate(p: Point): Boolean =
    hitCaches(p) match {
      case Some(point) =>
        point match {
          case _: FailurePoint => true
          case a              => prune.evaluate(a.metrics)
        }
      case None =>
        updateCaches(Seq(p))
        evaluate(p)
    }

  // We consider a point to be on the frontier if and only if it is not pruned and at least a point in
  // its neighbourhood is pruned
  private def isOnFrontier[T: TypeTag](p: Point, space: Space[T]): Boolean = {
    val neighbours = space.getNeighbours(p, Norm.Max)
    updateCaches(neighbours)
    !evaluate(p) & (neighbours.map(evaluate).reduce(_ | _))
  }

  // find a first point on the frontier
  private def findFrontierPoint[T: TypeTag](space: Space[T]): Option[Point] = {

    // define min and max point in space
    val (min, max) = (space.min, space.max)

    // build an diagonal from min to max
    def buildDiag(min: Point, max: Point): Seq[Point] = {
      (min == max) match {
        case true => Seq.empty
        case false =>
          val middle = space.findMiddle(min, max)
          (min == middle, middle == max) match {
            // cases where only one point differs from min to max
            case (true, false) => Seq(min, max)
            case (false, true) => Seq(min, max)
            // case were middle is a real middle
            case (false, false) => (buildDiag(min, middle) ++ buildDiag(middle, max)).distinct.toSeq
            // case were min == middle == max but min != max...
            // TODO: use real exception
            case _ =>
              throw new EqualityFailedException(s"min == middle == max and min != max")
          }
      }
    }

    // populate diag with estimations
    // TODO: populate it by dicho, exploiting at most numThread
    val diag = buildDiag(min, max)
    updateCaches(diag)

    // case where first point is not pruned, thus all space is accepted
    if (!evaluate(min)) return Some(min)

    // case where last point is pruned, thus all space is pruned
    if (evaluate(max)) return None

    // find a first candidate on the diagonal, and populate its neighbourhood to check that it's on frontier
    // TODO: populate it by dicho, exploiting at most numThread
    val firstCandidate = diag.map(p => (p -> evaluate(p))).filterNot(_._2).head._1
    updateCaches(space.getNeighbours(firstCandidate, norm = Norm.Max))

    // return a starting point, which is either on the diagonal, or in its neighbourhood
    isOnFrontier(firstCandidate, space) match {
      case true => Some(hitCaches(firstCandidate).get)
      case false =>
        val neighbourhood = space.getNeighbours(firstCandidate, norm = Norm.Max)
        // precompute all neighbourhoods to avoid running computations sequentially
        updateCaches(neighbourhood.flatMap(space.getNeighbours(_, norm = Norm.Max)))
        neighbourhood.filter(isOnFrontier(_, space)).headOption match {
          case Some(p) => Some(hitCaches(p).get)
          case None    => throw new CanNotStartException(s"Did not found first point")
        }
    }
  }

  // We build the frontier by neighbourhood exploration
  private def buildFrontier[T: TypeTag](space: Space[T], start: Point): Seq[Point] = {
    //println(s"[${Utils.getTime}] Building frontier from\n${start.toString}")
    val frontier = new ArrayBuffer[Point]()
    var currents = Seq(start)
    frontier += start

    while (!currents.isEmpty) {
      val neighbours = currents.flatMap(c => space.getNeighbours(c, Norm.Max))
      // precompute all neighbourhoods to avoid running computations sequentially
      updateCaches(neighbours.flatMap(space.getNeighbours(_, Norm.One)))
      val onFrontier =
        neighbours.filter(isOnFrontier(_, space)).filterNot(frontier contains _).map(hitCaches(_).get)
      frontier ++= onFrontier
      currents = onFrontier
      //println(s"[${Utils.getTime}] ${currents.size} current points")
    }
    //println(s"[${Utils.getTime}] ENDED FRONTIER SEARCH")

    frontier.toSeq
  }

  private def enhanceSpace[T: TypeTag](space: Space[T], frontier: Seq[Point]): Space[T] = {
    def isAbove(p: Point): Boolean = frontier.map(f => p >= f).reduce(_ | _)

    // define non estimated metrics using first point on the frontier, for non pruned implementations not estimated
    val generatedMetrics =
      (frontier.head.metricNames diff space.filter(_ == frontier.head).head.metricNames)
        .map(n => (n -> new Metric(frontier.head.metrics(n))))

    // filter points strictly under the frontier, and enhance non estimated points before returning the new space
    space.filter(isAbove).mapPoint { p =>
      hitCaches(p) match {
        case None        => p.copy(metrics = p.metrics ++& new MetricMap(generatedMetrics.toMap))
        case Some(point) => point
      }
    }
  }

  override def optimize[T: TypeTag](space: Space[T]): Space[T] = {
    logBegin("quick pruning", space.size)
    val mySpace = MatrixSpace(space)
    val result = findFrontierPoint(mySpace) match {
      case None =>
        throw new UnsupportedOperationException(s"Can't find a point in space")
      case Some(p) => enhanceSpace(mySpace, buildFrontier(mySpace, p))
    }
    mylog(
        " " * 4 +
          s"Explored ${(cache ++ failureCache).size} different implementations using quickPruning " +
          s"[${failureCache.size} estimation failures]."
    )
    result
  }
}
