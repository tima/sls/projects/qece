/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.basics

import qece.QeceContext
import qece.estimation.MetricMap
import qece.estimation.transforms.TransformSeq

import qece.exploration.annotations.ImpactMetric
import qece.exploration.strategies.Strategy
import qece.exploration.strategies.utils.{Space, SeqSpace, Point, SparsePoint, WithParallel}

import scala.reflect.runtime.universe.TypeTag
import java.util.concurrent.Semaphore

import qece.exploration.strategies.utils.ThreadPool

private[exploration] abstract class ParallelStrategy(
    transforms: TransformSeq,
    metric: Option[ImpactMetric] = None
) extends Strategy(transforms, metric)
    with WithParallel {
  override def numThread = QeceContext.parallel.numThread

  // monitoring threads
  private val semaphore = new Semaphore(1)
  private val running   = new scala.collection.mutable.HashMap[Point, String]()

  private def addPoint(p: Point, s: String): Unit = {
    semaphore.acquire
    running += (p -> s)
    mysyncdebug(
        s"Running $s on ${p.params}\t" +
          s"${running.size} running jobs: ${running.map { case (k, v) => s"${k.params}[${v}]" }.mkString("{", ", ", "}")}"
    )
    semaphore.release
  }

  private def removePoint(p: Point, s: String): Unit = {
    semaphore.acquire
    running.remove(p)
    mysyncdebug(
        s"Ending $s on ${p.params}\t" +
          s"${running.size} running jobs: ${running.map { case (k, v) => s"${k.params}[${v}]" }.mkString("{", ", ", "}")}"
    )
    semaphore.release
  }

  private def markFailure(seq: Seq[(Point, MetricMap)]): Seq[Point] =
    seq.map{ case (p, m) =>
      m match {
        case MetricMap.empty  => new SparsePoint(p.params, p.metrics)
        case _                => p.copy(metrics=p.metrics ++& m)
      }
    }

  // private def run[T: TypeTag](space: Space[T]): Seq[(Point, MetricMap)] = {
  private def run[T: TypeTag](space: Space[T]): Space[T] = {
    QeceContext.parallel.useScalaParSeq match {
      case true =>
        QeceContext.logger.enableSyncDebug match {
          case false => 
            SeqSpace(
              markFailure(
                buildParallelSet(space.toSeq).map(ps => (ps -> (estimate[T](transforms)(ps)))).seq
              )
            )
          case true =>
            SeqSpace(
              markFailure(
                buildParallelSet(space.toSeq).map {
                  case ps =>
                    addPoint(ps, transforms.toString)
                    val tmp = (ps -> (estimate[T](transforms)(ps)))
                    removePoint(ps, transforms.toString)
                    tmp
                }.seq
              )
            )
        }
      case false => new ThreadPool(numThread, estimate[T](transforms)(_)).run(space)
    }
  }

  def runParallel[T: TypeTag](space: Space[T]): Space[T] = run(space)

  def runParallelWithPartition[T: TypeTag](space: Space[T]): (Seq[Point], Seq[Point]) = partitionSpace(run(space))

  override protected def logBegin(str: String, size: Int): Unit =
    mylog(
        " " * 4 +
          s"Running $str on $size different implementations using transforms ${transforms.serialize} [$numThread threads]"
    )
}
