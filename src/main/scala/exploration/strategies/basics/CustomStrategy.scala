/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.basics

import qece.estimation.transforms.TransformSeq

import qece.exploration.strategies.Strategy
import qece.exploration.strategies.utils.Space

import scala.reflect.runtime.universe._

/** Custom strategy builder, using a simple function.
 *
 *  @param func
 *    function to be run in this strategy
 */
class CustomStrategy[T: TypeTag](
    func: Space[T] => Space[T]
) extends Strategy(TransformSeq.empty) {
  private def optimize(space: Space[T]): Space[T] = func(space)

  def optimize[X: TypeTag](space: Space[X]): Space[X] =
    (typeOf[X] =:= typeOf[T]) match {
      case true  => optimize(space.asInstanceOf[Space[T]]).asInstanceOf[Space[X]]
      case false => throw new UnsupportedOperationException(s"Types were not matching")
    }
}
