/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.basics

import qece.estimation.transforms.TransformSeq

import qece.exploration.annotations.ImpactMetric
import qece.exploration.strategies.utils.Space

import scala.reflect.runtime.universe.TypeTag

/** A multi-thread base exhaustive map strategy for exploration. */
class ExhaustiveApply(
    tfs: TransformSeq,
    metric: Option[ImpactMetric]
) extends ParallelStrategy(tfs, metric) {

  override def optimize[T: TypeTag](space: Space[T]): Space[T] = {
    logBegin("exhaustive apply", space.size)
    runParallel(space)
  }
}
