/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.basics

import qece.QeceContext

import qece.estimation.transforms.TransformSeq

import qece.exploration.strategies.utils.{Point, Space, MatrixSpace, SeqSpace, EmptyFailurePoint}
import qece.exploration.strategies.func.CostFunction
import qece.exploration.exceptions.CanNotStartException
import qece.exploration.annotations.ImpactMetric

import scala.reflect.runtime.universe.TypeTag

/** A multi-thread gradient descent based strategy for exploration. */
class GradientDescent(
    tfs: TransformSeq,
    cost: CostFunction,
    initValue: Option[Point] = None,
    metric: Option[ImpactMetric] = None
) extends ParallelStrategy(tfs, metric) {

  // exploit min(2*sizeof(space), numThread) parallel threads
  private def run[T: TypeTag](space: Seq[Point]): (Seq[Point], Seq[Point]) = {
    // apply cost evaluation on successes only
    def func(s: Seq[Point], f: Seq[Point]) = (s.map(p => p :+ cost.evaluate(p.metrics)), f)
    (func _).tupled(runParallelWithPartition(SeqSpace(space)))
  }

  /* Simple gradient based greedy algorithm */
  override def optimize[T: TypeTag](space: Space[T]): Space[T] = {
    logBegin("gradient descent", space.size)
    // cache all results - even failures - to avoid multiple estimation of the same point
    val cache        = scala.collection.mutable.ArrayBuffer[Point]()
    val failureCache = scala.collection.mutable.ArrayBuffer[Point]()
    val firstPoint = initValue match {
      case Some(x) => x
      case None    => space.head
    }
    // assume that a MatrixSpace is the better structure available for the space, as we will access a lot of neighbours
    val newSpace = MatrixSpace(space)
    // estimate first point and its neighbourhood to optimize exploration
    val (s, f) = run[T](newSpace.getNeighbours(firstPoint) :+ firstPoint)
    if (s.isEmpty)
      throw CanNotStartException(s"Did not found any viable starting point for gradient descent around:\n$firstPoint")
    cache ++= s.sortWith((x, y) => cost.compare(x.cost, y.cost))
    failureCache ++= f
    var (current, diff) = (cache.head, QeceContext.core.defaultCost)
    do {
      val (success, failure) = run[T](
          newSpace
            .getNeighbours(current)
            .filterNot(x => (cache ++ failureCache).map(_.params) contains x.params)
      )
      cache ++= success
      failureCache ++= failure
      val (tmp, tmpDiff) =
        success
          .map(x => x -> (x.cost - current.cost))
          .sortWith((x, y) => cost.compare(x._2, y._2))
          .headOption
          .getOrElse((EmptyFailurePoint -> QeceContext.core.defaultCost))
      cost.compare(tmp.cost, current.cost) match {
        case true =>
          current = tmp
          diff = tmpDiff
        case false =>
          diff = QeceContext.core.defaultCost
      }
    } while (cost.compare(diff, QeceContext.core.defaultCost))
    mylog(
        " " * 4 +
          s"Explored ${(cache ++ failureCache).size} different implementations using gradient descent " +
          s"[${failureCache.size} estimation failures]."
    )
    SeqSpace(cache.sortWith((x, y) => cost.compare(x.cost, y.cost)))
  }
}
