/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies

import qece._

import qece.estimation.MetricMap
import qece.estimation.transforms.TransformSeq

import qece.exploration.strategies.basics._
import qece.exploration.annotations.ImpactMetric
import qece.exploration.utils.Constructor
import qece.exploration.strategies.utils.{Space, Point}
import qece.exploration.strategies.func.{CostFunction, PruningFunction}

import scala.reflect.runtime.universe.TypeTag

/** Main entry point of the API: 
 *  
 *  it is used to build an exploration strategy using a given configuration, by composing basic strategies in a 
 *  sequential fashion.
 *
 *  @param config
 *    QECE configuration to execute in. 
 *    If not provided, use the default one.
 */
class StrategyBuilder(config: QeceConfig = QeceContext.value) {

  private def executeWithContext[T: TypeTag](
    func: Space[T] => Space[T],
    space: Space[T],
    numThread: Int = config.parallel.numThread
  ): Space[T] = QeceContext.withValue(config.funcCopy(parallel = _.copy(numThread = numThread))){ func(space) }

  /** Create a copy of the current configuration, and potentially update it.
   *
   *  @param core
   *    function to update the core config
   *  @param parallel
   *    function to update the parallel config
   *  @param backend
   *    function to update the backend config
   *  @param simulation
   *    function to update the simulation config
   *  @param characterization
   *    function to update the characterization config
   *  @param logger
   *    function to update the logger config
   *  @param emission
   *    function to update the emission config
   *  @param timeout
   *    function to update the timeout config
   */
  def copy(
    core:               CoreConfig => CoreConfig                          = (m => m),
    parallel:           ParallelConfig => ParallelConfig                  = (m => m),
    backend:            BackendConfig  => BackendConfig                   = (m => m),
    simulation:         SimulationConfig => SimulationConfig              = (m => m),
    characterization:   CharacterizationConfig => CharacterizationConfig  = (m => m),
    logger:             LoggerConfig => LoggerConfig                      = (m => m),
    emission:           EmissionConfig => EmissionConfig                  = (m => m),
    timeout:            TimeoutConfig => TimeoutConfig                    = (m => m)
  ): StrategyBuilder = new StrategyBuilder(config.funcCopy(
    core              = core,
    parallel          = parallel,
    backend           = backend,
    simulation        = simulation,
    characterization  = characterization,
    logger            = logger,
    emission          = emission,
    timeout           = timeout
  ))

  /** Copy this [[StrategyBuilder]] and update the emission folder
   *
   *  @param path
   *    emission path
   */
  def withPath(path: String): StrategyBuilder = 
    new StrategyBuilder(config.funcCopy(emission = _.copy(targetPath = path)))

  /** Display the design space of the module identified by its type. */
  def displaySpace[T: TypeTag]: Unit = {
    val constructor = new Constructor[T]
    val space       = constructor.namedParams.map(Point(_))
    println(s"Found a ${space.size} wide space for kernel $constructor")
    space.foreach(x => println(s"${x.params}"))
  }

  /** Apply a TransformSeq to a whole design space 
   *
   *  @param tfs
   *    transforms to run
   *  @param numThread
   *    number of thread to be used. If not specified, use the config one
   *  @param space
   *    initial space to explore
   *  @return
   *    initial space enhanced with new metrics associated to runned transforms
   */
  def map[T: TypeTag](
    tfs: TransformSeq,
    metric: Option[ImpactMetric] = None
  )(space: Space[T]): Space[T] = executeWithContext(new ExhaustiveApply(tfs, metric).runOptimize[T], space)

  private def sort[T: TypeTag](
    tfs: TransformSeq,
    cost: CostFunction,
    numThread: Int,
    metric: Option[ImpactMetric]
  )(space: Space[T]): Space[T] = 
    executeWithContext(new ExhaustiveSort(tfs, cost, metric).runOptimize[T], space, numThread)

  /** Run an exhaustive exploration to a design space, and sort results w.r.t. to a cost and a compare function
   *
   *  @param tfs
   *    transforms to run
   *  @param func
   *    cost evaluation function
   *  @param cmp
   *    cost comparison function
   *  @param numThread
   *    number of thread to be used. If not specified, use the config one
   *  @param space
   *    initial space to explore
   *  @return
   *    initial space sorted and enhanced with new metrics associated to runned transforms
   */
  def sort[T: TypeTag](
      tfs: TransformSeq,
      func: MetricMap => Double,
      cmp: (Double, Double) => Boolean,
      numThread: Int = config.parallel.numThread,
      metric: Option[ImpactMetric] = None
  )(space: Space[T]): Space[T] = sort[T](tfs, new CostFunction(func, cmp), numThread, metric)(space)

  private def prune[T: TypeTag](
      tfs: TransformSeq,
      prune: PruningFunction,
      numThread: Int,
      metric: Option[ImpactMetric]
  )(space: Space[T]): Space[T] =
    executeWithContext(new ExhaustivePruning(tfs, prune, metric).runOptimize[T], space, numThread)

  /** Run an exhaustive exploration to a design space, and prune results w.r.t. to a pruning function
   *
   *  @param tfs
   *    transforms to run
   *  @param func
   *    pruning function
   *  @param numThread
   *    number of thread to be used. If not specified, use the config one
   *  @param space
   *    initial space to explore
   *  @return
   *    initial space pruned and enhanced with new metrics associated to runned transforms
   */
  def prune[T: TypeTag](
      tfs: TransformSeq,
      func: MetricMap => Boolean,
      numThread: Int = config.parallel.numThread,
      metric: Option[ImpactMetric] = None
  )(space: Space[T]): Space[T] = prune[T](tfs, new PruningFunction(func), numThread, metric)(space)

  private def gradient[T: TypeTag](
      tfs: TransformSeq,
      cost: CostFunction,
      init: Option[Point],
      numThread: Int
  )(space: Space[T]): Space[T] =
    executeWithContext(new GradientDescent(tfs, cost, init).runOptimize[T], space, numThread)

  /** Run a gradient based exploration to a design space, and sort results w.r.t. to a cost and a compare function
   *
   *  @param tfs
   *    transforms to run
   *  @param func
   *    cost evaluation function
   *  @param cmp
   *    cost comparison function
   *  @param init
   *    if provided, will use as first point for gradient descent, if not, will use head of the initial space
   *  @param numThread
   *    number of thread to be used. If not specified, use the config one
   *  @param space
   *    initial space to explore
   *  @return
   *    pruned space sorted and enhanced with new metrics associated to runned transforms
   */
  def gradient[T: TypeTag](
      tfs: TransformSeq,
      func: MetricMap => Double,
      cmp: (Double, Double) => Boolean,
      init: Option[Point] = None,
      numThread: Int = config.parallel.numThread
  )(space: Space[T]): Space[T] = gradient[T](tfs, new CostFunction(func, cmp), init, numThread)(space)

  private def quickPrune[T: TypeTag](
      tfs: TransformSeq,
      prune: PruningFunction,
      numThread: Int,
      metric: Option[ImpactMetric]
  )(space: Space[T]): Space[T] =
    executeWithContext(new QuickPruning(tfs, prune, metric).runOptimize[T], space, numThread)

  /** Run a quick pruning strategy based on frontier approximation and neighbourhood exploration to prune results w.r.t.
   *  to a pruning function
   *
   *  @param tfs
   *    transforms to run
   *  @param func
   *    pruning function
   *  @param numThread
   *    number of thread to be used. If not specified, use the config one
   *  @param space
   *    initial space to explore
   *  @return
   *    initial space pruned and enhanced with new metrics associated to runned transforms
   */
  def quickPrune[T: TypeTag](
      tfs: TransformSeq,
      func: MetricMap => Boolean,
      numThread: Int = config.parallel.numThread,
      metric: Option[ImpactMetric] = None
  )(space: Space[T]): Space[T] = quickPrune[T](tfs, new PruningFunction(func), numThread, metric)(space)

  /* Reduce dimensions of a given space with respect to a given metric (as annotated in module constructor)
   *
   *  @param metric
   *    metric to be removed from consideration in space
   *  @param space
   *    initial space to explore
   *  @return
   *    initial space with reduced dimensions
   */
  def reduceDimension[T: TypeTag](
      metric: ImpactMetric,
      toMin: Boolean = true
  )(space: Space[T]): Space[T] = 
    executeWithContext(new ReduceDimension(metric, toMin).runOptimize[T], space)

  /** Build a space exploration function by composing space exploration functions
   *
   *  @param funcs
   *    exploration functions to be composed
   */
  def compose[T: TypeTag](funcs: (Space[T] => Space[T])*)(space: Space[T]): Space[T] =
    funcs.foldLeft(space) { case (sp, func) => func(sp) }

  private def compose_[T: TypeTag](funcs: Seq[Space[T] => Space[T]])(space: Space[T]): Space[T] =
    funcs.foldLeft(space) { case (sp, func) => func(sp) }

  /** Build a [[qece.exploration.strategies.basics.CustomStrategy]] using config and a given exploration function
   *
   *  @param func
   *    space exploration function
   */
  def buildStrategy[T: TypeTag](func: (Space[T] => Space[T])*): Strategy =
    new CustomStrategy(compose_[T](func))

  /** Run a design space exploration function on a given module
   *
   *  @param T
   *    module to be explored
   *  @param func
   *    design space exploration to be used
   */
  def explore[T: TypeTag](func: Space[T] => Space[T]): Space[T] = 
    QeceContext.withValue(config){ buildStrategy[T](func).explore[T] }

  /** Apply an exploration function to a design space
   *
   *  @param func
   *    exploration function to be used
   *  @param space
   *    initial design space to explore
   *  @return
   *    new design space w.r.t. exploration function
   */
  def run[T: TypeTag](func: Space[T] => Space[T])(space: Space[T]): Space[T] = 
    executeWithContext(func, space)
}

/** Companion object for [[StrategyBuilder]]. */
object StrategyBuilder {

  /** Build a [[StrategyBuilder]] using a given configuration. 
   *  
   *  @param config
   *    configuration of the strategy. 
   *    If not provided, use the default one
   */
  def apply(config: QeceConfig = QeceContext.value): StrategyBuilder = new StrategyBuilder(config)

  /** Simple helper to build a [[StrategyBuilder]] from an emission path.
   *
   *  @param targetPath
   *    emission path to emite files to
   */
  def withPath(
    targetPath: String = QeceContext.emission.targetPath,
  ): StrategyBuilder = new StrategyBuilder(QeceContext.copy(emission = _.copy(targetPath = targetPath)))
}
