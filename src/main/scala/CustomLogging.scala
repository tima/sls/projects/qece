/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece

import logger.{LazyLogging, Logger}

trait CustomLogging extends LazyLogging {
  Logger.setLevel(this.getClass, QeceContext.logger.logLevel)

  protected def mylog(str: String) = {
    QeceContext.logger.enableLog match {
      case true  => logger.warn(s"[${Utils.getTime}] $str")
      case false =>
    }
  }

  protected def mytrace(str: String) = {
    QeceContext.logger.enableTrace match {
      case true  => logger.warn(s"[${Utils.getTime}]" + " " * 8 + s"$str")
      case false =>
    }
  }

  protected def mydebug(str: String) = {
    QeceContext.logger.enableDebug match {
      case true  => logger.warn(s"[DEBUG][${Utils.getTime}] $str")
      case false =>
    }
  }

  protected def mysyncdebug(str: String) = {
    QeceContext.logger.enableSyncDebug match {
      case true  => logger.warn(s"[SYNCHRO][${Utils.getTime}] $str")
      case false =>
    }
  }

  protected def timeError(str: String) = {
    QeceContext.logger.enableError match {
      case true  => logger.error(s"[${Utils.getTime}] $str")
      case false =>
    }
  }
}
