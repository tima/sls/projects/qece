/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece

import logger.LogLevel

import qece.estimation.macros.MacroBlock
import qece.estimation.macros.components._

import scala.util.DynamicVariable

/** Configuration of the QECE core, i.e. estimation and exploration features.
 *
 *  @todo manage the default transforms here
 *  @param enableInvalidEstimation
 *    either fail on invalid estimation, or just continue and mark as failure
 *  @param defaultCost
 *    default cost for estimated metrics (e.g. on estimation failure)
 */
case class CoreConfig(
  enableInvalidEstimation: Boolean  = true,
  defaultCost: Double               = 0.0
)

/** Configuration of the parallelism management.
 *
 *  @param useScalaParSeq
 *    either use scala built-in ParSeq (which resulted in starving threads) or custom ThreadPool
 *  @param activeWaiting
 *    if useScalaParSeq == false, define the ThreadPool behavior, either using event driven waiting, or active waiting
 *  @param numThread
 *    define the number of threads to be used in the different processes (can be overrided by more specific parameters)
 */
case class ParallelConfig(
  useScalaParSeq: Boolean           = false,
  activeWaiting: Option[Int]        = None,
  numThread: Int                    = 4
)

/** Configuration of the FPGA backend.
 *
 *  @param targetBoard
 *    define the name of the target board, with respect to the ones defined in the backend package
 *  @param vivadoVersion
 *    define the vivado version to be used for synthesis
 *  @param backendThreadNumber
 *    define the number of threads used in each FPGA backend call.
 *    set to 1 as syntheses are difficult to parallelize efficiently
 *  @param period
 *    define the target period for FPGA backend calls (in ns)
 */
case class BackendConfig(
  targetBoard: String               = "Zedboard",
  vivadoVersion: String             = "2017.3",
  backendThreadNumber: String       = "1",
  targetPeriod: Int                 = 10
)

/** Configuration of the simulation backend.
 *
 *  @param simulationBackend
 *    define which backend to be used for simulations
 */
case class SimulationConfig(
  backend: String         = "treadle"
)
  
/** Configuration of the characterization system, including the macro block mechanism.
 *
 *  @todo manage defaultMacroSeq configuration here ?
 *  @param maxDspRegister
 *    max. amount of registers that can be absorbed in a MacUnit pattern
 *  @param maxMemoryBlockAddr
 *    max. number of addresses available on MemoryBlock (as a power of two)
 *  @param maxMuxBlockInput
 *    max. number of inputs for a MuxBlock (as a power of two)
 *  @param characterizationBitMap
 *    specify the bitwidths to use for characterization. This may use expert knowledge about the target, to cope with
 *    transfert effects on the technology.
 *  @param characterizeRegisters
 *    either use backend to characterize the registers, or use naive estimation through primitive counting
 *  @param baseWidth
 *    base bitwidth to run characterization on
 */
case class CharacterizationConfig(
  maxDspRegister: Int               = 3,
  maxMemoryBlockAddr: Int           = 16,
  maxMuxBlockInput: Int             = 8,
  characterizationBitMap: Map[MacroBlock[_], List[Int]] =
    Map[MacroBlock[_], List[Int]](MemoryBlock -> List(1, 8, 32, 64, 128, 256, 512, 1024))
      .withDefaultValue(List(1, 2, 4, 8, 15, 16, 32, 64, 128, 256)),
  characterizeRegisters: Boolean    = true,
  baseWidth: Int                    = 1
)

/** Configuration of the logging system.
 *
 *  @param logLevel
 *    global logging level
 *  @param enableTrace
 *  @param enableLog
 *  @param enableError
 *  @param enableDebug
 *  @param enableSyncDebug
 *  @param chiselLogLevel
 *    define chisel logging level
 *  @param stackDepth
 *    define the size of the stack trace to be displayed on exception handling
 */
case class LoggerConfig(
  logLevel: LogLevel.Value          = LogLevel.Warn,
  enableTrace: Boolean              = true,
  enableLog: Boolean                = true,
  enableError: Boolean              = true,
  enableDebug: Boolean              = false,
  enableSyncDebug: Boolean          = false,
  chiselLogLevel: LogLevel.Value    = LogLevel.None,
  stackDepth: Int                   = 0
)

/** Configuration of the emission system.
 *
 *  @param genDirectory
 *    default generation directory for the emitted files
 *  @param csvSeparator
 *    define the csv separator used for result files
 *  @param disableSynthesis
 *    force synthesis disable (for debug purposes)
 *  @param disableImplementation
 *    force implementation (P&R) disable (for debug purposes)
 */
case class EmissionConfig(
  basePath: String                  = "chisel_gen/",
  targetPath: String                = "default/",
  csvSeparator: String              = ";",
  disableSynthesis: Boolean         = false,
  disableImplementation: Boolean    = false
)

/** Various timeouts configuration
 *
 *  @param totalEstimation
 *    timeout for a whole estimation process
 *  @param replaceMacro
 *    timeout for the replace macro transform
 *  @param primitiveReport
 *    timeout for the primitive report transform
 *  @param estimateResource
 *    timeout for the estimate resource transform
 *  @param endpointReport
 *    timeout for the endpoint report transform
 *  @param criticalPath
 *    timeout for the critical path estimation transform
 *  @param synthesis
 *    define synthesis timeout, using the `timeout format`
 *  @param synthesisTransform
 *    useless as the timeout is already defined at backend level
 */
case class TimeoutConfig(
  totalEstimation: String     = "4h",
  replaceMacro: String        = "30min",
  primitiveReport: String     = "30min",
  estimateResource: String    = "Inf",
  endpointReport: String      = "2h",
  criticalPath: String        = "Inf",
  synthesis: String           = "2h",
  synthesisTransform: String  = "Inf"
)

/** Global configuration for the project.
 *
 *  @param core
 *    QECE core configuration, i.e. estimation and exploration features
 *  @param parallel
 *    thread management configuration
 *  @param backend
 *    FPGA backend configuration
 *  @param simulation
 *    simulation backend configuration
 *  @param characterization
 *    characterization system configuration
 *  @param logger
 *    logging system configuration
 *  @param emission
 *    emission system configuration
 *  @param timeout
 *    timeout system configuration
 */
case class QeceConfig(
  core:             CoreConfig              = CoreConfig(),
  parallel:         ParallelConfig          = ParallelConfig(),
  backend:          BackendConfig           = BackendConfig(),
  simulation:       SimulationConfig        = SimulationConfig(),
  characterization: CharacterizationConfig  = CharacterizationConfig(),
  logger:           LoggerConfig            = LoggerConfig(),
  emission:         EmissionConfig          = EmissionConfig(),
  timeout:          TimeoutConfig           = TimeoutConfig()
) {

  /** Create a copy of this by update members.
   *
   *  @param core
   *    function to update the core config
   *  @param parallel
   *    function to update the parallel config
   *  @param backend
   *    function to update the backend config
   *  @param simulation
   *    function to update the simulation config
   *  @param characterization
   *    function to update the characterization config
   *  @param logger
   *    function to update the logger config
   *  @param emission
   *    function to update the emission config
   *  @param timeout
   *    function to update the timeout config
   */
  def funcCopy(
    core:               CoreConfig => CoreConfig                          = (m => m),
    parallel:           ParallelConfig => ParallelConfig                  = (m => m),
    backend:            BackendConfig  => BackendConfig                   = (m => m),
    simulation:         SimulationConfig => SimulationConfig              = (m => m),
    characterization:   CharacterizationConfig => CharacterizationConfig  = (m => m),
    logger:             LoggerConfig => LoggerConfig                      = (m => m),
    emission:           EmissionConfig => EmissionConfig                  = (m => m),
    timeout:            TimeoutConfig => TimeoutConfig                    = (m => m)
  ): QeceConfig = this.copy(
    core              = core(this.core),
    parallel          = parallel(this.parallel),
    backend           = backend(this.backend),
    simulation        = simulation(this.simulation),
    characterization  = characterization(this.characterization),
    logger            = logger(this.logger),
    emission          = emission(this.emission),
    timeout           = timeout(this.timeout)
  )
}

/** A global object used to build the global configuration of the project.
 *
 *  A DynamicVariable is used to allow overriding of the basic configuration, as well as some syntactic sugar
 *  to allow users to do it in a concise way.
 */
object QeceContext extends DynamicVariable(QeceConfig()){
  def core              = value.core
  def parallel          = value.parallel
  def backend           = value.backend
  def simulation        = value.simulation
  def characterization  = value.characterization
  def logger            = value.logger
  def emission          = value.emission
  def timeout           = value.timeout

  /** Run a block of code in a context where the context is updated.
   *
   *  @param core
   *    function to update the core config
   *  @param parallel
   *    function to update the parallel config
   *  @param backend
   *    function to update the backend config
   *  @param simulation
   *    function to update the simulation config
   *  @param characterization
   *    function to update the characterization config
   *  @param logger
   *    function to update the logger config
   *  @param emission
   *    function to update the emission config
   *  @param timeout
   *    function to update the timeout config
   */
  def withValue[S](
    core:               CoreConfig => CoreConfig                          = (m => m),
    parallel:           ParallelConfig => ParallelConfig                  = (m => m),
    backend:            BackendConfig  => BackendConfig                   = (m => m),
    simulation:         SimulationConfig => SimulationConfig              = (m => m),
    characterization:   CharacterizationConfig => CharacterizationConfig  = (m => m),
    logger:             LoggerConfig => LoggerConfig                      = (m => m),
    emission:           EmissionConfig => EmissionConfig                  = (m => m),
    timeout:            TimeoutConfig => TimeoutConfig                    = (m => m)
  )(thunk: => S): S = withValue(
    this.copy(
      core = core,
      parallel = parallel,
      backend = backend,
      simulation = simulation,
      characterization = characterization,
      logger = logger,
      emission = emission,
      timeout = timeout
    )
  )(thunk)

  /** Create a copy of the current configuration, and potentially update it.
   *
   *  @param core
   *    function to update the core config
   *  @param parallel
   *    function to update the parallel config
   *  @param backend
   *    function to update the backend config
   *  @param simulation
   *    function to update the simulation config
   *  @param characterization
   *    function to update the characterization config
   *  @param logger
   *    function to update the logger config
   *  @param emission
   *    function to update the emission config
   *  @param timeout
   *    function to update the timeout config
   */
  def copy(
    core:               CoreConfig => CoreConfig                          = (m => m),
    parallel:           ParallelConfig => ParallelConfig                  = (m => m),
    backend:            BackendConfig  => BackendConfig                   = (m => m),
    simulation:         SimulationConfig => SimulationConfig              = (m => m),
    characterization:   CharacterizationConfig => CharacterizationConfig  = (m => m),
    logger:             LoggerConfig => LoggerConfig                      = (m => m),
    emission:           EmissionConfig => EmissionConfig                  = (m => m),
    timeout:            TimeoutConfig => TimeoutConfig                    = (m => m)
  ): QeceConfig = this.value.funcCopy(
    core              = core,
    parallel          = parallel,
    backend           = backend,
    simulation        = simulation,
    characterization  = characterization,
    logger            = logger,
    emission          = emission,
    timeout           = timeout
  )
}
