/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.backend

/** Representation of FPGA resources
 *
 *  @param lut
 *    number of lut used in design
 *  @param ff
 *    number of registers used in design
 *  @param dsp
 *    number of dsp used in designed
 *  @param memory
 *    amount of memory used in design (in kb)
 */
case class Resources(lut: Int, ff: Int, dsp: Int, memory: Int) {
  def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + s"lut: $lut",
        " " * indent + s"ff: $ff",
        " " * indent + s"dsp: $dsp",
        " " * indent + s"memory: $memory"
    ).mkString("\n")
  }
}

/** Object representing an empty [[Resources]] instance. */
object EmptyResources extends Resources(-1, -1, -1, -1)

/** Representation of a timing
 *
 *  @param delay
 *    delay of critical path for a design (in ns)
 *  @param frequency
 *    maximum operating frequency (in MHz)
 */
case class Timing(delay: Double, frequency: Double) {
  def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + s"delay: ${delay}ns",
        " " * indent + s"frequency: ${frequency}MHz"
    ).mkString("\n")
  }
}

/** Object representing an empty [[Timing]]. */
object EmptyTiming extends Timing(-1, -1)
