/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.backend

import io.circe._, io.circe.parser._
import scala.io.Source

import java.io.{File, FileWriter}

import sys.process._

/** Utilities for file system access and parsing. */
package object utils {

  /** Add file and system utilities to String
   *  @param path
   *    a String representing a path to a file/exec
   */
  private[backend] implicit class FileUtil(path: String) {
    def getResourceContent: String = {
      Source.fromInputStream(this.getClass.getResourceAsStream(path)).getLines.mkString("\n")
    }
    def getDirectory                  = path.split("/").slice(0, path.split("/").length - 1).mkString("/")
    def getFilename                   = path.split("/")(path.split("/").length - 1)
    def parseJSON                     = parse(Source.fromFile(path).getLines.mkString("")).getOrElse(Json.Null).hcursor
    def fileExists                    = new File(path).exists
    def getLines(key: String)         = Source.fromFile(path).getLines.filter { s: String => s.contains(key) }
    def sed(from: String, to: String) = Seq("sed", "-i", s"s|$from|$to|g", path).!!
    def copyTo(to: String)            = Seq("cp", path, to).!!
    def writeTo(to: String) = {
      val fw = new FileWriter(new File(to))
      fw.write(path)
      fw.close()
    }
    // Avoid printing stack trace to std.out
    // Using loggers result in IOException: StreamClosed in tests...
    def call(args: Seq[String]) = ((Seq(path) ++ args).mkString(" ") #> new java.io.File("/dev/null")).!
    def touch: Unit = {
      s"mkdir -p ${path.getDirectory}".!
      s"touch $path".!
    }
    // remove all files in a path, to clear some space in cases of failure
    def rmAll: Unit = s"rm -rf $path".!
  }

  /** Cursor utilities, for JSON parsing
   *  @param a
   *    cursor to the JSON resource
   */
  private[backend] implicit class CursorUtil(a: ACursor) {
    def getInt(key: String)                          = a.get[Int](key).getOrElse(-1)
    def getString(key: String, default: String = "") = a.get[String](key).getOrElse(default)
  }
}
