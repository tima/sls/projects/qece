/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.backend

import qece.QeceContext

import qece.backend.utils._
import qece.backend.exceptions._

/** Abstract class for Xilinx Virtex 7 family */
abstract class Virtex7 extends FPGA {
  protected val part: String
  protected val synthScriptContent =
    s"/backend/xilinx/defaultSynth_${QeceContext.backend.vivadoVersion.replace(".", "_")}.tcl".getResourceContent
  protected val synthScriptName   = "synth.tcl"
  protected val constraintContent = "/backend/xilinx/constraints.xdc".getResourceContent
  protected val constraintName    = "constraints.xdc"
  protected val synthProject      = "synth_project"
  protected val reportDir         = s"$synthProject/synth.runs/synth_1"
  protected val synthCompleted    = s"$reportDir/__synthesis_is_complete__"
  protected val synthFailed       = s"$reportDir/__synthesis_failed__"
  protected val timing            = "timing.rpt"
  protected val utilization       = "utilization_synth.rpt"
  protected val logFile           = "vivado.log"

  private val bramCapacity = 36 // Block RAM use 36 kb

  //TODO: add verilog catch error + process loging
  //see: https://github.com/ekiwi/icesugar-chisel/blob/main/src/fpga/boards/icesugar/IceSugar.scala

// scalastyle:off method.length
  override def runSynthesis(path: String, top: String): Unit = {
    val synth = s"$path/$synthScriptName"
    val const = s"$path/$constraintName"

    val replaceMap = Map(
        "PART"          -> part,
        "CONSTRAINT"    -> const,
        "PROJECT_PATH"  -> s"$path/$synthProject",
        "TOP_MODULE"    -> s"$path/$top.v",
        "TIMING_REPORT" -> s"$path/$reportDir/${top}_timing.rpt",
        "NB_THREAD"     -> nbThread
    )
    val replaceConstraints = Map[String, Double](
        "PERIOD" -> QeceContext.backend.targetPeriod,
        "SEMI"   -> QeceContext.backend.targetPeriod / 2.0
    )

    if (synthesisCompleted(path)) return
    if (synthesisFailed(path)) {
      throw SynthesisAlreadyFailedException(s"Synthesis in $path already failed")
    }
    synthScriptContent.writeTo(synth)
    constraintContent.writeTo(const)

    replaceMap.foreach { case (key, value) => synth.sed(key, value) }
    replaceConstraints.foreach { case (key, value) => const.sed(key, f"$value%2.3f".replace(",", ".")) }
    val status = "timeout".call(
        timeoutOpt ++
          Seq("vivado", "-mode", "batch", "-nolog", "-nojournal", "-source", synth)
    )
    status match {
      case 0 =>
        completeSynthesis(path)
      case _ =>
        markSynthesisFailure(path)
    }
  }
// scalastyle:on method.length

  override protected def reportSynthesisResources(path: String, top: String): Resources = {
    val resourceMap = Map("lut" -> "LUT as Logic", "ff" -> "Register as Flip Flop", "dsp" -> "DSPs")
    val res = resourceMap.map {
      case (key, value) =>
        (key, s"$path/$reportDir/${top}_$utilization".getLines(value).toSeq(0).split('|')(2).trim().toInt)
    }
    val bram = (s"$path/$reportDir/${top}_$utilization"
      .getLines("Block RAM Tile")
      .toSeq(0)
      .split('|')(2)
      .trim()
      .toDouble * bramCapacity).toInt
    new Resources(res("lut"), res("ff"), res("dsp"), bram)
  }

// scalastyle:off method.length
  override protected def reportSynthesisTiming(path: String, top: String): Timing = {
    val timingMap = Map("slack" -> "(required time - arrival time)", "clock" -> "Requirement")
    try {
      val res = timingMap.map {
        case (key, value) =>
          (
              key ->
                s"$path/$reportDir/${top}_$timing"
                  .getLines(value)
                  .toSeq(0)
                  .split(':')(1)
                  .split("ns")(0)
                  .trim()
                  .toDouble
          )
      }
      val delay = res("clock") - res("slack")
      new Timing(delay, 1.0 / delay * scala.math.pow(10, 3))
    } catch {
      //TODO: quite unsatisfying - problem with simple mult block where DSP absorb wrap registers
      case _: java.lang.IndexOutOfBoundsException =>
        try {
          val delay = s"$path/$reportDir/${top}_$timing"
            .getLines("Clock Path Skew")
            .toSeq(0)
            .split(':')(1)
            .trim()
            .split(' ')(0)
            .trim()
            .split("ns")(0)
            .trim()
            .toDouble
          new Timing(delay, 1.0 / delay * scala.math.pow(10, 3))
        } catch {
          case _: java.lang.IndexOutOfBoundsException =>
            val delay = s"$path/$reportDir/${top}_$timing"
              .getLines("Data Path Delay")
              .toSeq(0)
              .split(':')(1)
              .trim()
              .split(' ')(0)
              .trim()
              .split("ns")(0)
              .trim()
              .toDouble
            new Timing(delay, 1.0 / delay * scala.math.pow(10, 3))
        }
      case t: Throwable => throw t
    }
  }
// scalastyle:on method.length

  //TODO: add timeout detection && error detection
  override def synthesisCompleted(path: String): Boolean = {
    s"$path/$synthCompleted".fileExists
  }

  override def synthesisFailed(path: String): Boolean = {
    s"$path/$synthFailed".fileExists
  }

  private def completeSynthesis(path: String): Unit = {
    s"$path/$synthCompleted".touch
  }

  private def markSynthesisFailure(path: String): Unit = {
    s"$path/$reportDir".rmAll
    s"$path/$synthFailed".touch
    throw SynthesisFailedException(s"Detected synthesis error in $path")
  }

  override lazy val maxMemory: Int =
    (scala.math.log(maxResources.memory * 1024) / scala.math.log(2)).floor.toInt
}
