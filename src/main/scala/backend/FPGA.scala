/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.backend

import qece.{Utils, QeceContext, CustomLogging}

import qece.backend.exceptions._
import qece.backend.utils._

/** Companion object for [[FPGA]]. */
object FPGA {
  private val knownBoards = List(new VC709, new Zedboard).map(b => b.name -> b).toMap
  def apply(name: String): FPGA = {
    knownBoards.get(name) match {
      case Some(x) => x
      case _       => throw UnknownBoardException(s"Did not found board with name $name")
    }
  }
  def default: FPGA = FPGA(QeceContext.backend.targetBoard)
}

/** FPGA class for backend access. */
abstract class FPGA extends CustomLogging {
  /** Name of this particular [[FPGA]]. */
  val name       = this.getClass.getSimpleName

  protected val nbThread   = QeceContext.backend.backendThreadNumber

  protected val timeoutOpt = Seq("-s", "SIGKILL", "-k", "1s", QeceContext.timeout.synthesis)

  /** Run a full synthesis, and report both timing and resources
   *  @param path
   *    directory where Verilog sources are to be found, and where synthesis files wille be written
   *  @param top
   *    name of the top module for this synthesis
   *  @return
   *    the synthesis report, as a ([[qece.backend.Resources]], [[qece.backend.Timing]] ) tuple
   */
  def reportSynthesis(path: String, top: String): (Resources, Timing) = {
    logger.trace(s"[${Utils.getDate}] Running synthesis on top $top (in $path)")
    QeceContext.emission.disableSynthesis match {
      case true => (EmptyResources, EmptyTiming)
      case false =>
        try {
          runSynthesis(path, top)
          (reportSynthesisResources(path, top), reportSynthesisTiming(path, top))
        } catch {
          case e: SynthesisAlreadyFailedException =>
            throw SynthesisFailedException(s"previous synthesis failure on top $top in $path", e)
          case e: Throwable =>
            throw e
        }
    }
  }

  /** Run a synthesis process for this [[FPGA]] instance
   *  @param path
   *    root directory where Verilog sources are to be found, and where synthesis files will be written
   *  @param top
   *    name of the top module for this synthesis
   *  @return
   *    either the synthesis process has failed or not
   */
  protected def runSynthesis(path: String, top: String): Unit = {
    throw new UnsupportedOperationException(s"Synthesis run for top '$top' on board $this (in $path)")
  }

  /** Report post-synthesis resources
   *
   *  Do not call if synthesis wasn't run before
   *  @param path
   *    root directory where synthesis has been run, and where reports are to be found
   *  @param top
   *    name of the top module for this synthesis
   *  @return
   *    a report of post-synthesis resources
   */
  protected def reportSynthesisResources(path: String, top: String): Resources = {
    throw new UnsupportedOperationException(s"Synthesis resource report for top '$top' on board $this (in $path)")
  }

  /** Report post-synthesis timing
   *
   *  Do not call if synthesis wasn't run before
   *  @param path
   *    root directory where synthesis has been run, and where reports are to be found
   *  @param top
   *    name of the top module for this synthesis
   *  @return
   *    a report of post-synthesis timing
   */
  protected def reportSynthesisTiming(path: String, top: String): Timing = {
    throw new UnsupportedOperationException(s"Synthesis timing report for top '$top' on board $this (in $path)")
  }

  /** Run full implementation, and report both timing and resources
   *  @param root
   *    directory where Verilog sources are to be found, and where implementation files wille be written
   *  @param top
   *    name of the top module for this implementation
   *  @return
   *    the implementation report, as a ([[qece.backend.Resources]], [[qece.backend.Timing]] ) tuple
   */
  def reportImplementation(path: String, top: String): (Resources, Timing) = {
    logger.info(s"Running implementation on top $top (in $path)")
    QeceContext.emission.disableImplementation match {
      case true => (EmptyResources, EmptyTiming)
      case false =>
        try {
          runImplementation(path, top)
          (reportImplementationResources(path, top), reportImplementationTiming(path, top))
        } catch {
          case e: Throwable => throw e
        }
    }
  }

  /** Run an implementation process for this [[qece.backend.FPGA]] instance
   *  @param path
   *    directory where Verilog sources are to be found, and where implementation files will be written
   *  @param top
   *    name of the top module for this implementation
   *  @return
   *    either the synthesis process has failed or not
   */
  protected def runImplementation(path: String, top: String): Unit = {
    throw new UnsupportedOperationException(s"Implementation run for top '$top' on board $this (in $path)")
  }

  /** Report post-implementation resources
   *
   *  Do not call if implementation wasn't run before
   *  @param path
   *    root directory where synthesis has been run, and where reports are to be found
   *  @param top
   *    name of the top module for this implementation
   *  @return
   *    a report of post-implementation resources
   */
  protected def reportImplementationResources(path: String, top: String): Resources = {
    throw new UnsupportedOperationException(s"Implementation resource report for top '$top' on board $this (in $path)")
  }

  /** Report post-implementation timing
   *
   *  Do not call if implementation wasn't run before
   *  @param path
   *    root directory where synthesis has been run, and where reports are to be found
   *  @param top
   *    name of the top module for this implementation
   *  @return
   *    a report of post-implementation timing
   */
  protected def reportImplementationTiming(path: String, top: String): Timing = {
    throw new UnsupportedOperationException(s"Implementation timing report for top '$top' on board $this (in $path)")
  }

  /** Textual representaton of the board */
  override def toString: String = name

  /** Delete all files used for backend calls
   *  @param path
   *    directory where backend was called
   *  @todo
   *    remove only files used for backend, and not all files in path
   */
  def clear(path: String) = "rm".call(Seq("-rf", path))

  /** Check if a synthesis did complete in the given folder.
   *
   *  @param path
   *    folder where the synthesis results should be
   *  @return 
   *    weither the synthesis completed or not.
   */
  def synthesisCompleted(path: String): Boolean = {
    throw new UnsupportedOperationException(s"Synthesis completed not defined for board $this (at $path)")
  }

  /** Check if a synthesis failed in the given folder.
   *
   *  @param path
   *    folder where the synthesis results should be
   *  @return 
   *    weither the synthesis failed or not.
   */
  def synthesisFailed(path: String): Boolean = {
    throw new UnsupportedOperationException(s"Synthesis failed not defined for board $this (at $path)")
  }

  /** Get the maximum resources available on the board */
  val maxResources: Resources

  /** Get the maximum memory capacity available (in Kb), as a power of two */
  val maxMemory: Int
}
