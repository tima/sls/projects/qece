/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
import java.io.{File, FileWriter}
import sys.process._

package object qece {
  /** Helpers for file management.
   *
   *  @param path
   *    file path
   */
  implicit class FileUtils(path: String) {
    /** check if a given file exist. */
    def fileExists = new File(path).exists

    /** write the content of the path to a given file.
     *
     *  WARNING: erase the file content, if it is not empty
     *
     *  @param to
     *    path of the file to write to
     */
    def writeTo(to: String) = {
      to.mkdir
      val fw = new FileWriter(new File(to))
      fw.write(path)
      fw.close()
    }

    /** append the content of the path to a given file.
     *
     *  @param to
     *    path of the file to write to
     */
    def appendTo(to: String) = {
      val fw = new FileWriter(new File(to), true)
      fw.write(path)
      fw.close()
    }

    /** get the directory part of the path. */
    def getDirectory: String = {
      path.split("/").dropRight(1).mkString("/")
    }

    /** get the directory of the path, and create it. */
    def mkdir: Unit =
      path.getDirectory match {
        case "" =>
        case x  => Seq("mkdir", "-p", x).!!
      }
  }
}
