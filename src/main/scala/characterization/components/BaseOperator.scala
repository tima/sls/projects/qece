/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization.components

import qece.estimation.utils.Operators

import chisel3._
import chisel3.util._

/** Module class for generating basic operators
 *
 *  @param operators
 *    kind of operator to generate
 *  @param bitwidth
 *    input bitwidth for the operator
 *  @param nbOp
 *    number of operator to generate
 *  @todo
 *    TODO: better management for dshift, after experiments
 */
class BaseOperator(
  operator: Operators.Operator,
  bitwidth: Int,
  nbOp: Int = 1
) extends MultiIOModule {
  val op1    = IO(Input(Vec(nbOp, UInt(bitwidth.W))))
  val op2    = IO(Input(Vec(nbOp, UInt(bitwidth.W))))
  val cond   = IO(Input(Vec(nbOp, Bool())))
  val result = IO(Output(Vec(nbOp, UInt())))

  for (i <- 0 until nbOp) {
    operator match {
      case Operators.REG =>
        val reg = RegNext(op1(i))
        result(i) := reg
      case Operators.MULT   => result(i) := op1(i) * op2(i)
      case Operators.ADD    => result(i) := op1(i) + op2(i)
      case Operators.BIN    => result(i) := op1(i) ^ op2(i)
      case Operators.MUX    => result(i) := Mux(cond(i), op1(i), op2(i))
      case Operators.COMP   => result(i) := op1(i) > op2(i)
      case Operators.DSHIFT => result(i) := op1(i)(0 max bitwidth - 2, 0) << op2(i)(log2Ceil(bitwidth))
      case _ =>
        throw new UnsupportedOperationException(s"Unrecognized operator $operator for operator instanciation")
    }
  }
}
