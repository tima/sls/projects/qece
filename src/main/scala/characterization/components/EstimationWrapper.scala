/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization.components

import chisel3._
import chisel3.experimental.{DataMirror}
import chisel3.ActualDirection

import collection.mutable.ArrayBuffer

import logger.LazyLogging

/** Build a wrapper module to register every IO of a given design, in order to properly estimate paths in combinatorial
 *  designs
 *
 *  @tparam T
 *    type of module used
 *  @param mod_gen
 *    module to wrap aroung
 */
class EstimationWrapper[T <: MultiIOModule](mod_gen: => T)
  extends MultiIOModule with LazyLogging {
  val inputLinks  = ArrayBuffer[(Data, Data)]()
  val outputLinks = ArrayBuffer[(Data, Data)]()

  private def buildLinks[T <: Data](modIo: T, io: T): Unit = {
    val dig = DataMirror.directionOf(modIo) match {
      case ActualDirection.Input =>
        inputLinks += ((modIo, io))
        false
      case ActualDirection.Output =>
        outputLinks += ((modIo, io))
        false
      case ActualDirection.Bidirectional(_) => true
      case d =>
        logger.warn(s"[WARNING) Ignoring $modIo because its direction is $d")
        false
    }
    if (!dig) return

    (modIo, io) match {
      case (mod_b: Record, b: Record) =>
        for (((me_name, me), (e_name, e)) <- mod_b.elements.zip(b.elements)) {
          buildLinks(me, e)
        }

      case (mv: Vec[_], v: Vec[_]) =>
        for ((mve, ve) <- mv.getElements.zip(v.getElements)) {
          buildLinks(mve, ve)
        }
      case _ => logger.warn(s"[WARNING] Ignoring $modIo ($io)")
    }
  }
  // Instance of module
  val inst    = Module(mod_gen)
  val ios     = ArrayBuffer[Data]()
  val ioNames = ArrayBuffer[String]()

  for (instIO <- inst.getPorts.filterNot(p => Seq("clock", "reset") contains p.id.instanceName)) {
    val io = IO(chiselTypeOf(instIO.id)).suggestName(instIO.id.instanceName)
    ios += io
    ioNames += instIO.id.instanceName
    // Get inputs interfaces
    buildLinks(instIO.id, io)
  }
  for ((modI, i) <- inputLinks) {
    val reg = RegNext(i)
    modI := reg
  }
  for ((modO, o) <- outputLinks) {
    val reg = RegNext(modO)
    o := reg
  }
}
