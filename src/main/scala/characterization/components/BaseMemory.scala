/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization.components

import chisel3._
import chisel3.util._

/** Class for memory characterization.
 *
 *  @param nBanks
 *    number of banks in the memory
 *  @param nElem
 *    number of adresses for each bank
 *  @param elemW
 *    size of each element, in bit
 */
class BaseMemory(nBanks: Int, nElem: Int, elemW: Int) extends Module {
  val io = IO(new Bundle {
    val dataIn  = Input(Vec(nBanks, UInt(elemW.W)))
    val addr    = Input(Vec(nBanks, UInt(log2Up(nElem).W)))
    val ren     = Input(Vec(nBanks, Bool()))
    val wen     = Input(Vec(nBanks, Bool()))
    val dataOut = Output(Vec(nBanks, UInt(elemW.W)))
  })

  val memory = Seq.fill(nBanks)(SyncReadMem(nElem, UInt(elemW.W)))
  for (i <- 0 until nBanks) {
    val bank = memory(i)
    when(io.wen(i.U)) { bank.write(io.addr(i.U), io.dataIn(i.U)) }
    io.dataOut(i.U) := bank.read(io.addr(i.U), io.ren(i.U))
  }
}
