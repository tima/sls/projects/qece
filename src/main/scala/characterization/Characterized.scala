/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization

import qece.characterization.utils.{WithJson, Observation}
import qece.characterization.estimators.ParametrizedEstimator

import qece.estimation.exceptions._
import qece.estimation.macros.MacroBlock
import qece.estimation.utils.Operators

/** Abstract class representing a characterized component.
 *
 *  @constructor create a new characterized component
 *  @param estimators
 *    [[qece.characterization.estimators.ParametrizedEstimator]](s) used for characterization
 *  @param intervals
 *    intervals of validity for this characterization
 */
abstract class Characterized(
  val estimators: Seq[ParametrizedEstimator],
  val intervals: Seq[Range]
) {

  /** Retrieve an estimator from its resource name
   *
   *  @param resource
   *    resource name to search for
   *  @return the corresponding [[qece.characterization.estimators.ParametrizedEstimator]]
   */
  def getEstimator(resource: String): ParametrizedEstimator = {
    estimators.filter(_.resource == resource) match {
      case Seq() =>
        throw KeyNotFoundException(s"Did not found resource with name $resource in ${this.getClass.getSimpleName}")
      case Seq(x) => x
      case _ =>
        throw ManyKeyFoundException(
            s"Found more than one resource with name $resource in ${this.getClass.getSimpleName}"
        )
    }
  }

  /** Update the fidelity of each [[qece.characterization.estimators.ParametrizedEstimator]](s), using new observations.
   *
   *  @param observations
   *    new observations to update the fidelity
   */
  def updateFidelity(observations: Seq[Observation]): Unit = estimators.foreach(est => est.updateFidelity(observations))
}

/** Used to characterize a [[qece.estimation.macros.MacroBlock]]
 *
 *  @param macroKind
 *    kind of block being characterized
 */
class CharacterizedMacro(
    val macroKind: MacroBlock[_],
    estimators: Seq[ParametrizedEstimator],
    intervals: Seq[Range]
) extends Characterized(estimators, intervals)
    with WithJson {

  /** define if this macro is characterized using linear interpolation. */
  val linear = macroKind.linearBitwidth

  override def prettyToString(indent: Int = 0): String = {
    Seq(" " * indent + s"Macros of kind $macroKind:", estimators.map(_.prettyToString(indent + 2)).mkString("\n"))
      .mkString("\n")
  }
}

/** Companion object for [[CharacterizedMacro]]. */
object CharacterizedMacro {

  /** Build a [[CharacterizedMacro]] based on [[qece.characterization.estimators.ParametrizedEstimator]] (s).
   *
   *  @param macroKind
   *    macro kind to characterize
   *  @param observations
   *    [[qece.characterization.utils.Observation]] (s) to use for estimator building
   */
  def apply(macroKind: MacroBlock[_], observations: Seq[Observation]): CharacterizedMacro = {
    val filtered = observations.filter(x => x.kind == Right(macroKind))
    val estimators = filtered.head.values.keys.map { r =>
      ParametrizedEstimator(r, filtered, macroKind.argDim, macroKind.linearBitwidth)
    }.toSeq
    val intervals = new scala.collection.mutable.ArrayBuffer[Range]()
    for (i <- 0 until estimators.head.intervals.size) {
      intervals += (estimators.map(_.intervals(i).start).max to estimators.map(_.intervals(i).end).min)
    }
    new CharacterizedMacro(macroKind, estimators, intervals)
  }
}

/** Characterized operator with associated estimators
 *
 *  @param kind
 *    kind of operator to estimate
 */
class CharacterizedOperator(
  val kind: Operators.Operator,
  estimators: Seq[ParametrizedEstimator],
  intervals: Seq[Range]
) extends Characterized(estimators, intervals) with WithJson {

  override def prettyToString(indent: Int = 0): String = {
    Seq(" " * indent + s"Operators of kind $kind:", estimators.map(_.prettyToString(indent + 2)).mkString("\n"))
      .mkString("\n")
  }
}

/** Companion object for the [[CharacterizedOperator]] class. */
object CharacterizedOperator {

  /** Build a [[CharacterizedOperator]] based on [[qece.characterization.estimators.ParametrizedEstimator]] (s).
   *
   *  @param kind
   *    [[qece.estimation.utils.Operators.Operator]] kind
   *  @param observations
   *    [[qece.characterization.utils.Observation]] (s) to use for estimator building
   */
  def apply(kind: Operators.Operator, observations: Seq[Observation]): CharacterizedOperator = {
    val filtered   = observations.filter(x => x.kind == Left(kind))
    val estimators = filtered.head.values.keys.map(r => ParametrizedEstimator(r, filtered)).toSeq
    val intervals  = new scala.collection.mutable.ArrayBuffer[Range]()
    for (i <- 0 until estimators.head.intervals.size) {
      intervals += (estimators.map(_.intervals(i).start).max to estimators.map(_.intervals(i).end).min)
    }
    new CharacterizedOperator(kind, estimators, intervals)
  }
}
