/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization.estimators

import qece.QeceContext

import qece.characterization.UnvalidEstimationException

/** Class to evaluate points from a gradient method
 *
 *  @param points
 *    observation [[Point]] (s) as a definition of the lattices
 *  @todo
 *    TODO: build a real method for dim > 2
 */
private[characterization] case class Gradient(points: Seq[Point], linear: Boolean = false) {
  require(!points.isEmpty, s"Points can't be empty")
  points.foreach(p => require(p.dim == points.head.dim, s"Arguments for coefficients must be of equal dimensions"))
  require(size > argDim, s"Argument dimension $argDim should be lower than the number of coefficient $size")

  lazy val argDim = points.head.dim
  lazy val size   = points.size
  lazy val intervals = {
    val intervals = new scala.collection.mutable.ArrayBuffer[Range]()
    for (i <- 0 until argDim) {
      val coeff = points.map(_.args(i))
      intervals += (coeff.min to coeff.max by 1)
    }
    intervals
  }

  /** Evaluate the given coordinates
   *
   *  @param args
   *    coordinates to evaluate at
   */
  def evaluate(args: Seq[Int]): Double = {
    if (args.size != argDim) throw UnvalidEstimationException(s"Arguments $args must be of dimension $argDim")
    if (!QeceContext.core.enableInvalidEstimation) {
      // if not enabled, check for interval belonging before computing
      args.zipWithIndex.foreach {
        case (arg, i) =>
          if (!(intervals(i) contains arg)) {
            throw UnvalidEstimationException(s"Argument $arg is not in validity interval ${intervals(i)}")
          }
      }
    }

    //TODO: adapt the number of nearest points
    points.map(_.args).indexWhere(_ == args) match {
      // use barycenter to evaluate value
      case -1 =>
        val withNorm = points
          .map {
            case point =>
              (point.from(args), point.args, point.value, point.isUnder(args))
          }
          .sortBy(_._1)
          .take(2)
        // TODO: three cases : above, in and under
        withNorm.map(_._4).reduce(_ & _) match {
          case false =>
            if (!QeceContext.core.enableInvalidEstimation) {
              args.zipWithIndex.foreach {
                case (arg, i) =>
                  if (!(intervals(i) contains arg)) {
                    //TODO: add under case. Check every case to fit better
                    throw UnvalidEstimationException(s"Argument $arg is not in validity interval ${intervals(i)}")
                  }
              }
            }
            // point is in validity interval
            val totalDist = withNorm.map(_._1).reduce(_ + _)
            withNorm.map { case (dist, _, value, _) => value * (totalDist - dist) }.reduce(_ + _) / totalDist
          case true =>
            // point is above validity interval
            val sortedPoints = withNorm.map { case (_, args, value, _) => Point(args, value) }
            val prevDist     = sortedPoints.head.from(sortedPoints.tail.head.args)
            val gradient     = (sortedPoints.head.value - sortedPoints.tail.head.value) / prevDist
            withNorm.head match {
              case (dist, _, value, _) => value + dist * gradient
            }
        }
      // case where it is on bound
      case x => points(x).value
    }
  }
}
