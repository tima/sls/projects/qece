/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization.estimators

import qece.characterization.utils.Observation

/** Utility object to easily compute fidelity of a function. */
private[characterization] object RSquare {

  /** Get the rsquare value of a function, to quantify fidelity
   *
   *  @param evaluate
   *    function to evaluate
   *  @param points
   *    sample points used to build the evaluation function
   *  @return
   *    the least square value for this estimator
   */
  def get(resource: String, evaluate: (Seq[Int] => Double), points: Seq[Observation]): Double = {
    val residualSumOfSquares =
      points.map(p => scala.math.pow(p.values.get(resource).get - evaluate(p.args), 2)).reduce(_ + _)
    val average = points.map(p => p.values.get(resource).get).reduce(_ + _) / points.length
    val tmp     = points.map(p => scala.math.pow(p.values.get(resource).get - average, 2)).reduce(_ + _)
    // care to Double rounding
    val totalSumOfSquares = if (tmp < 0.001) 0 else tmp
    val ratio             = residualSumOfSquares / totalSumOfSquares
    // println(s"resource: $resource, residu: $residualSumOfSquares, average: $average, total: $totalSumOfSquares,
    // ratio: $ratio")
    // Return an encodable double, thus avoiding NaN and infinity values, as they are not managed correctly by io.circe
    (1 - ratio) match {
      case x if x.isNaN      => 1
      case x if x.isInfinity => 1
      case i                 => i
    }
  }

  /** Get the rsquare value of a function, to quantify fidelity
   *
   *  @param resource
   *    being evaluate, to retrieve points in the [[qece.characterization.utils.Observation]] (s)
   *  @param evaluate
   *    function to evaluate
   *  @param points
   *    sample points used to build the evaluation function
   *  @return
   *    the least square value for this estimator
   *  @todo
   *    TODO: factorize with the other
   */
  def get(resource: String, evaluate: ((String, Seq[Int]) => Double), points: Seq[Observation]): Double = {
    val residualSumOfSquares = points
      .map(p => scala.math.pow(p.values.get(resource).get - evaluate(p.params.mkString("-"), p.args), 2))
      .reduce(_ + _)
    val average           = points.map(p => p.values.get(resource).get).reduce(_ + _) / points.length
    val tmp               = points.map(p => scala.math.pow(p.values.get(resource).get - average, 2)).reduce(_ + _)
    val totalSumOfSquares = if (tmp < 0.001) 0 else tmp
    val ratio             = residualSumOfSquares / totalSumOfSquares
    // Return an encodable double, thus avoiding NaN and infinity values, as they are not managed correctly by io.circe
    (1 - ratio) match {
      case x if x.isNaN      => 1
      case x if x.isInfinity => 1
      case i                 => i
    }
  }
}
