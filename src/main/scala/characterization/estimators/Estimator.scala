/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization.estimators

import qece.QeceContext

import qece.characterization.utils.{WithJson, Observation}
import qece.characterization.{UnvalidEstimationException, UnknownEstimatorException}

/** Abstract represention of a resource estimator
 *
 *  @param resource
 *    resource to estimate with this [[Estimator]]
 *  @param fidelity
 *    fidelity of the estimator
 *  @param intervals
 *    validity interval
 */
abstract class Estimator(
  val resource: String,
  var fidelity: Double,
  val intervals: Seq[Range]
) extends WithJson {

  /** Pretty print this [[Estimator]]
   *
   *  @param indent
   *    indentation level for the printing
   */
  override def prettyToString(indent: Int = 0): String =
    " " * indent +
      s"Estimating resource $resource with fidelity $fidelity on intervals ${intervals.mkString("[", ", ", "]")}"

  /** Check if the coordinates are the validity intervals of the estimator
   *
   *  @param args
   *    coordinates to evaluate on
   */
  protected def assertValid(args: Seq[Int]): Unit = {
    if (args.size != intervals.size) throw UnvalidEstimationException(s"Arg dim must be the same as interval dim")
    if (!QeceContext.core.enableInvalidEstimation) {
      args.zipWithIndex.foreach {
        case (arg, i) =>
          intervals(i) contains arg match {
            case false => throw UnvalidEstimationException(s"Argument $arg is not in validity interval ${intervals(i)}")
            case true  =>
          }
      }
    }
  }

  /** Modify fidelity of a given estimator
   *
   *  @param value
   *    new fidelity to use
   */
  def setFidelity(value: Double): Unit = this.fidelity = value
}

/** Simple estimator. */
abstract class SimpleEstimator(resource: String, fidelity: Double, intervals: Seq[Range])
    extends Estimator(resource, fidelity, intervals) {

  /** Evaluate a component from one argument only.
   *
   *  @param x
   *    argument to evaluate on
   */
  def evaluate(x: Int): Double = evaluate(Seq(x))

  /** Evaluate a component from its coordinates.
   *
   *  @param args
   *    coordinates to evaluate on
   */
  def evaluate(args: Seq[Int]): Double
}

/** Wrapping class for [[SimpleEstimator]] (s), in case multiple configurations were to exists
 *
 *  @param estimators
 *    map parameter sets to [[SimpleEstimator]] (s)
 */
class ParametrizedEstimator(
    resource: String,
    fidelity: Double,
    intervals: Seq[Range],
    val estimators: Map[String, SimpleEstimator]
) extends Estimator(resource, fidelity, intervals) {

  /** Evaluate a component from its constructor params and one argument only.
   *
   *  @param x
   *    argument to evaluate on
   */
  def evaluate(params: String, x: BigInt): Double = evaluate(params, Seq(x.toInt))

  /** Evaluate a component from its constructor params and one argument only.
   *
   *  @param x
   *    argument to evaluate on
   */
  def evaluate(params: String, x: Int): Double = evaluate(params, Seq(x))

  /** Evaluate a component from its constructor params and arguments.
   *
   *  @param params
   *    representation of the parameter set used to build the component being estimated
   *  @param args
   *    coordinates to evaluate on
   */
  def evaluate(params: String, args: Seq[Int]): Double = {
    estimators.get(params) match {
      case Some(est) => est.evaluate(args)
      case _         => throw UnknownEstimatorException(s"Can't find estimator on params ${params}")
    }
  }

  /** Evaluate a component from one argument only.
   *
   *  @param x
   *    argument to evaluate on
   */
  def evaluate(x: Int): Double = evaluate(Seq(x))

  /** Evaluate a component from arguments, assuming no constructor parameters
   *
   *  @param args
   *    coordinates to evaluate on
   */
  def evaluate(args: Seq[Int]): Double = evaluate("", args)

  def updateFidelity(observations: Seq[Observation]): Unit = {
    estimators.foreach {
      case (k, v) =>
        val rs = RSquare.get(resource, v.evaluate(_), observations.filter(x => x.params.mkString("-") == k))
        v.setFidelity(rs)
    }
    val rs = RSquare.get(resource, evaluate(_, _), observations)
    this.setFidelity(rs)
  }
}

/** Companion object for [[ParametrizedEstimator]]. */
object ParametrizedEstimator {

  /** Build a [[ParametrizedEstimator]] for a given resource, based on [[GradientEstimator]] (s)
   *
   *  @param resource
   *    kind of resource being estimated
   *  @param observations
   *    [[qece.characterization.utils.Observation]] (s) to be used for building
   *  @param argDim
   *    dimension of arguments used in this estimator
   *  @param firstArg
   *    use to avoid characterizing on bitwidth when it's linear
   *  @todo
   *    TODO manage first arg
   */
  def apply(
      resource: String,
      observations: Seq[Observation],
      argDim: Int = 1,
      linear: Boolean = false
  ): ParametrizedEstimator = {
    val paramSets = observations.map(_.params).toSet
    val estimators = paramSets.map { params =>
      params.mkString("-") -> GradientEstimator(resource, observations.filter(_.params == params), argDim, linear)
    }.toMap
    val intervals = new scala.collection.mutable.ArrayBuffer[Range]()
    for (i <- 0 until observations.head.argDim) {
      intervals += (observations.map(_.args(i)).min to observations.map(_.args(i)).max by 1)
    }
    new ParametrizedEstimator(resource, 1.0, intervals, estimators)
  }
}

/** Estimator using gradient on known points
 *
 *  @param resource
 *    kind of resources being estimated
 *  @param fidelity
 *    fidelity of the estimator build
 *  @param intervals
 *    validity intervals
 *  @param gradient
 *    gradient object to use for evaluation
 */
class GradientEstimator(
  resource: String,
  fidelity: Double,
  intervals: Seq[Range],
  val gradient: Gradient
) extends SimpleEstimator(resource, fidelity, intervals) {
  override def evaluate(args: Seq[Int]): Double = gradient.evaluate(args)
}

/** Companion object for [[GradientEstimator]]. */
object GradientEstimator {

  /** Build a new [[GradientEstimator]] from observations
   *
   *  @param resource
   *    resource estimated by this [[Estimator]]
   *  @param observations
   *    [[qece.characterization.utils.Observation]] (s) used to build this [[Estimator]]
   *  @param argDim
   *    dimension of arguments used in this estimator
   */
  def apply(
      resource: String,
      observations: Seq[Observation],
      argDim: Int,
      linear: Boolean = false
  ): GradientEstimator = {
    observations
      .map(_.args)
      .foreach(args => require(args.size == argDim, s"Observation arguments must be of the given dimension"))
    val points = (observations.map(_.args) zip observations.map(_.values.get(resource).get)) map {
      case (x, y) => Point(x, y)
    }

    val intervals = new scala.collection.mutable.ArrayBuffer[Range]()
    for (i <- 0 until observations.head.argDim) {
      intervals += (observations.map(_.args(i)).min to observations.map(_.args(i)).max by 1)
    }

    new GradientEstimator(resource, 1.0, intervals, Gradient(points, linear))
  }
}
