/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization

import qece.QeceContext

import qece.backend.FPGA
import qece.estimation.macros.MacroSeq

import qece.estimation.utils.Access

/** Object to build a characterization-based library of macro and components.
 *  Parameters are the target board, the seq of macros, a characterization bit map, a target path, and a level of 
 *  parallelism. */
object LibraryBuilder extends App {
  /** Target board. */
  val board = FPGA.default

  CharacterizedBoard(
      board = board,
      macros = MacroSeq.default,
      bitMap = QeceContext.characterization.characterizationBitMap,
      path = Some(s"library-${board.name}"),
      parallelism = Some(24)
  ).save
}

/** Read the default board characterized library, and display it */
object LibraryReader extends App with Access {
  /** Target [[qece.backend.FPGA]]. */
  val board = FPGA.default

  println(getEstimatorsFromBoard(board).prettyToString())
}
