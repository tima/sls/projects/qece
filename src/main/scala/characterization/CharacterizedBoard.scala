/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization

import qece.QeceContext

import qece.characterization.components.{BaseOperator, EstimationWrapper}
import qece.characterization.utils.{WithJson, Observation, EmptyObservation}

import qece.estimation.WithTransform
import qece.estimation.exceptions._
import qece.estimation.utils.{Access, Operators}
import qece.estimation.macros.{MacroSeq, MacroBlock, EmptyMacroBlock}
import qece.estimation.transforms.TransformSeq
import qece.estimation.resources.transforms.TopRegisterReport
import qece.estimation.backend.SynthesisExtractor

import qece.exploration.utils.Parallel

import qece.backend.FPGA

import logger.LazyLogging

import scala.io.Source, java.io.{File, FileWriter}
import scala.collection.parallel.ParSeq

import chisel3.util.log2Up

/** A characterized board, with estimators for both basic operators and some macros
 *
 *  @param board
 *    target board being characterized
 *  @param macros
 *    [[CharacterizedMacro]] (s) used for estimations of macros
 *  @param operators
 *    [[CharacterizedOperator]] (s) used for operator estimation
 */
case class CharacterizedBoard(
  board: FPGA,
  macros: Seq[CharacterizedMacro],
  operators: Seq[CharacterizedOperator]
) extends WithJson with Access {
  val macroSeq = MacroSeq(macros.map(_.macroKind).toList)

  override def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + s"Board $board with macros $macroSeq:",
        macros.map(_.prettyToString(indent + 2)).mkString("\n"),
        operators.map(_.prettyToString(indent + 2)).mkString("\n")
    ).mkString("\n")
  }

  /** Retrieve a [[CharacterizedMacro]] instance
   *
   *  @param macroKind
   *    kind of macro being searched
   */
  def getCharacterizedMacro(macroKind: MacroBlock[_]): CharacterizedMacro = {
    macros.filter(_.macroKind == macroKind) match {
      case Seq()  => throw KeyNotFoundException(s"Did not found macro with kind $macroKind in $board config")
      case Seq(x) => x
      case _      => throw ManyKeyFoundException(s"Found more than one macro with kind $macroKind in $board config")
    }
  }

  /** Retrieve a [[CharacterizedOperator]] instance
   *
   *  @param kind
   *    kind of operator being searched
   */
  def getCharacterizedOperator(operator: Operators.Operator): CharacterizedOperator = {
    operators.filter(_.kind == operator) match {
      case Seq()  => throw KeyNotFoundException(s"Did not found operator with kind $operator in $board config")
      case Seq(x) => x
      case _      => throw ManyKeyFoundException(s"Found more than one operator with kind $operator in $board config")
    }
  }

  /** Save this [[CharacterizedBoard]] to a file, in the lib directory. */
  def save: Unit = {
    // Force directory creation
    val file = new File(getEstimatorPath(board))
    file.getParentFile().mkdirs()
    val fw = new FileWriter(file)
    fw.write(this.emitJson)
    fw.close()
  }
}

/** Companion object for [[CharacterizedBoard]]. */
object CharacterizedBoard extends Access with Parallel with WithTransform with WithJson with LazyLogging {

// scalastyle:off method.length
  /** Build a [[CharacterizedBoard]] instance for estimation.
   *
   *  @param board
   *    target board
   *  @param macros
   *    macros to characterize
   *  @param bitMap
   *    bitwidths map to use for the characterization
   *  @param path
   *    path of emission of sample for estimator building
   *  @param parallelism
   *    used to define the level of parallelism to use in the characterization process
   */
  def apply(
      board: FPGA,
      macros: MacroSeq,
      bitMap: Map[MacroBlock[_], List[Int]],
      path: Option[String] = None,
      parallelism: Option[Int] = None
  ): CharacterizedBoard = {
    logger.info(s"Building characterized library for board $board with macros $macros")

    def reportOperator(op: Operators.Operator, bits: Int): Observation = {
      logger.trace(s"Emitting $op operator [$bits]")
      val dir = path match {
        case Some(x) => s"$x/$op-$bits"
        case None    => s"$op-$bits"
      }
      logger.debug(s"Generating $dir")
      val annos = emitWithTransform(
          new EstimationWrapper(new BaseOperator(op, bits)),
          transforms = TransformSeq(new TopRegisterReport, new SynthesisExtractor),
          path = Some(dir),
          board = Some(board)
      )
      val metrics = getMetrics(annos)
      Observation(
          Left(op),
          Seq.empty,
          Seq(bits),
          Map(
              "lut"    -> metrics("realLUT"),
              "ff"     -> (0.0 max (metrics("realFF") - metrics("topReg"))),
              "dsp"    -> metrics("realDSP"),
              "memory" -> metrics("realMem"),
              "delay"  -> metrics("realDelay")
          )
      )
    }

    def reportMacro(
        mac: MacroBlock[_],
        params: Seq[Int]
    ): Observation = {
      val name     = mac.buildName(params)
      val maxValue = mac.getCapacity(board)
      val argDim   = mac.argDim
      logger.trace(s"Emitting $name operator [${params.mkString("-")}]")
      val dir = path match {
        case Some(x) => s"$x/$name"
        case None    => s"$name"
      }
      // don't run on heavy bit amount
      if (maxValue.isDefined && (params.map(log2Up(_)).reduce(_ + _) > maxValue.get)) { return EmptyObservation }
      logger.debug(s"Generating $dir")
      try {
        val annos = emitWithTransform(
            new EstimationWrapper(mac.factory.buildWith(params).asInstanceOf[chisel3.MultiIOModule]),
            transforms = TransformSeq(new TopRegisterReport, new SynthesisExtractor),
            path = Some(dir),
            board = Some(board)
        )
        val metrics = getMetrics(annos)
        val (realParams, realArgs) = mac.linearBitwidth match {
          case true  => (params.drop(argDim + 1), params.drop(1).take(argDim))
          case false => (params.drop(argDim), params.take(argDim))
        }
        Observation(
            Right(mac),
            realParams,
            realArgs,
            Map(
                "lut"    -> metrics("realLUT"),
                "ff"     -> (0.0 max (metrics("realFF") - metrics("topReg"))),
                "dsp"    -> metrics("realDSP"),
                "memory" -> metrics("realMem"),
                "delay"  -> metrics("realDelay")
            )
        )
      } catch {
        case e: Throwable =>
          logger.warn(s"Reporting of macro in dir $dir failed due to $e")
          EmptyObservation
      }
    }

    def buildParams(
        bitMap: Map[MacroBlock[_], List[Int]],
        operators: Seq[Operators.Operator],
        macros: MacroSeq
    ): (ParSeq[(Operators.Operator, Int)], ParSeq[(MacroBlock[_], Seq[Int])]) = {
      val operatorParams = buildParallelSet(
          for {
            op  <- operators
            bit <- bitMap(EmptyMacroBlock) //force default value retrieval
          } yield (op, bit),
          parallelism
      )

      val macroParams = buildParallelSet(
          for {
            ma <- macros.flatMap(ma => ma.paramValues.map(values => (ma -> values)))
            bit <- ma._1.linearBitwidth match {
              case true  => List(QeceContext.characterization.baseWidth)
              case false => bitMap(ma._1)
            }
          } yield {
            (ma._1 -> (Seq(bit) ++ ma._2))
          },
          parallelism
      )

      (operatorParams, macroParams)
    }

    def buildObservations(
        bitMap: Map[MacroBlock[_], List[Int]]
    ): (Seq[Observation], Seq[Observation]) = {
      val (operatorParams, macroParams) = QeceContext.characterization.characterizeRegisters match {
        case true  => buildParams(bitMap, Operators.basicOperators, macros)
        case false => buildParams(bitMap, Operators.basicOperators.filterNot(_ == Operators.REG), macros)
      }
      val operatorObservations = operatorParams.map { case (op, bit) => reportOperator(op, bit) }
      val macroObservations    = macroParams.map { case (mac, params) => reportMacro(mac, params) }
      (operatorObservations.seq.filter(_ != EmptyObservation), macroObservations.seq.filter(_ != EmptyObservation))
    }

    def middlePoints(bitMap: Map[MacroBlock[_], List[Int]]): Map[MacroBlock[_], List[Int]] = {
      bitMap.map { case (k, v) => (k -> (v.sliding(2).toList map { case List(x, y) => (x + y) / 2 })) }.toMap
    }

    // run synthesis on all bitwidth in the range, and every middle points
    val (operatorObs, macroObs) = buildObservations(bitMap)
    val (opControlSet, macControlSet) =
      buildObservations(middlePoints(bitMap).withDefaultValue(bitMap(EmptyMacroBlock))) // propagate default value

    // build estimators using the observations
    val operatorKeys = operatorObs.collect(obs => obs.kind match { case Left(x) => x }).toSet
    val macroKeys    = macroObs.collect(obs => obs.kind match { case Right(x) => x }).toSet

    val operatorSeq = operatorKeys.map(kind => CharacterizedOperator(kind, operatorObs)).toSeq
    val macroSeq    = macroKeys.map(kind => CharacterizedMacro(kind, macroObs)).toSeq

    // update fidelity thanks to control set
    operatorSeq.foreach(est => est.updateFidelity(opControlSet.filter(_.kind.left.getOrElse(0) == est.kind)))
    // no need to update fidelity on linear bitwidths
    // TODO: should update fidelity by varying on args instead of only bitwidth
    macroSeq
      .filter(!_.linear)
      .foreach(est => est.updateFidelity(macControlSet.filter(_.kind.right.getOrElse(0) == est.macroKind)))

    // store csv used to build operators
    val samples = "csv/samples"
    operatorKeys.map(kind =>
      Observation.toCSV(s"$samples/$kind.csv", operatorObs.filter(_.kind.left.getOrElse(0) == kind))
    )
    macroKeys.map(mac => Observation.toCSV(s"$samples/$mac.csv", macroObs.filter(_.kind.right.getOrElse(0) == mac)))
    val control = "csv/control"
    operatorKeys.map(kind =>
      Observation.toCSV(s"$control/$kind.csv", opControlSet.filter(_.kind.left.getOrElse(0) == kind))
    )
    macroKeys.map(mac =>
      Observation.toCSV(s"$control/$mac.csv", macControlSet.filter(_.kind.right.getOrElse(0) == mac))
    )

    new CharacterizedBoard(board, macroSeq, operatorSeq)
  }
// scalastyle:on method.length

  /** Build a [[CharacterizedBoard]] from a JSON string
   *
   *  @param json
   *    input string to decode
   */
  def decode(json: String): CharacterizedBoard = {
    io.circe.parser.decode[CharacterizedBoard](json) match {
      case Right(x) => x
      case Left(ex) => throw ex
    }
  }

  /** Build a [[CharacterizedBoard]] from a File
   *
   *  @param file
   *    File to decode
   */
  def decode(file: File): CharacterizedBoard = {
    io.circe.parser.decode[CharacterizedBoard](Source.fromFile(file).getLines.mkString("")) match {
      case Right(x) => x
      case Left(ex) => throw ex
    }
  }

  /** Build a [[CharacterizedBoard]] from a scala.io
   *
   *  @param file
   *    Source to decode
   */
  def decode(file: Source): CharacterizedBoard = {
    io.circe.parser.decode[CharacterizedBoard](file.getLines.mkString("")) match {
      case Right(x) => x
      case Left(ex) => throw ex
    }
  }
}
