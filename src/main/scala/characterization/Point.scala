/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization.estimators

/** Representation of a point in R^n
 *
 *  @param args
 *    coordinates of this point
 *  @param value
 *    value associated to these coordinates
 */
private[characterization] case class Point(args: Seq[Int], value: Double) {
  val dim = args.size
  def from(that: Seq[Int]): Double = {
    scala.math.sqrt((args zip that).map { case (x, y) => scala.math.pow(x - y, 2) }.reduce(_ + _))
  }
  def isUnder(that: Seq[Int]): Boolean = {
    (args zip that).map { case (x, y) => x < y }.reduce(_ & _)
  }
}
