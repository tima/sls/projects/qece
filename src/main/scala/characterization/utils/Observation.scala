/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization.utils

import qece.FileUtils

import qece.characterization.CharacterizedKind
import qece.estimation.utils.Operators

/** Class for representing synthesis results on either BasicOperator or Macros
 *
 *  @param kind
 *    kind of operator being synthetized
 *  @param params
 *    generation parameters for the operator
 *  @param args
 *    argument for estimation building
 *  @param values
 *    results of the synthesis, as a map
 */
case class Observation(
  kind: CharacterizedKind,
  params: Seq[Int],
  args: Seq[Int],
  values: Map[String, Double]
) {
  lazy val bitwidth = args.head
  lazy val argDim   = args.size

  /** Pretty print this observation
   *
   *  @param indent
   *    indentation level to be used
   */
  def prettyToString(indent: Int = 0): String = {
    val name = kind match {
      case Left(l)  => l
      case Right(r) => r
    }
    Seq(
        " " * indent + s"Observation ($name):",
        params.mkString(" " * (indent + 2) + "Parameters: [", ", ", "]"),
        args.mkString(" " * (indent + 2) + "Arguments: [", ", ", "]"),
        values.map { case (k, v) => s"($k -> $v)" }.mkString(" " * (indent + 2) + "Values: [", ", ", "]")
    ).mkString("\n")
  }

  /** Build a csv line with its header from this observation
   *
   *  @return
   *    headers, line and separator
   */
  def toCSV: (Seq[String], Seq[String]) = {
    val kindString = kind match {
      case Left(x)  => x.toString
      case Right(x) => x.toString
    }
    // only use first arg as bitwidth
    // TODO: replace by using argDim
    (
        Seq("kind", "params", "bitwidth") ++ values.keys,
        (Seq(kindString, params.mkString("-"), args.head.toString) ++ values.values.map(_.toString))
    )
  }
}

private[characterization] object EmptyObservation 
  extends Observation(Left(Operators.NOP), Seq.empty, Seq.empty, Map.empty)

/** Utils for [[Observation]]. */
private[characterization] object Observation {
  private val separator = "|"

  /** write [[Observation]] (s) to a csv file
   *
   *  @param fileName
   *    fileName to write to
   *  @todo
   *    check if parent dir exists, create if not
   *  @param obs
   *    [[Observation]] (s) to write to csv
   */
  def toCSV(fileName: String, obs: Seq[Observation]): Unit = {
    val csvs = obs.map(_.toCSV)
    assert(csvs.map(_._1).toSet.size == 1, "Headers should all be the same when building file")
    val content = Seq(csvs.map(_._1).head.mkString(separator), csvs.map(_._2).map(_.mkString(separator)).mkString("\n"))
      .mkString("\n")
    content.writeTo(fileName)
  }
}
