/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization.utils

import qece.characterization._
import qece.characterization.estimators._

import qece.estimation.utils.Operators
import qece.estimation.macros.MacroBlock

import qece.backend.FPGA

import io.circe._, io.circe.syntax._

/** Trait used to serialize and unserialize reports with manual derivation */
trait WithJson {

// -------- ENCODERS -----------------
//
  implicit protected val encodeCharacterizedBoard: Encoder[CharacterizedBoard] = new Encoder[CharacterizedBoard] {
    final def apply(a: CharacterizedBoard): Json =
      Json.obj(
          ("board", Json.fromString(a.board.name)),
          ("macros", a.macros.asJson),
          ("operators", a.operators.asJson)
      )
  }
  implicit protected val encodeCharacterizedMacro: Encoder[CharacterizedMacro] = new Encoder[CharacterizedMacro] {
    final def apply(a: CharacterizedMacro): Json =
      Json.obj(
          ("macroKind", Json.fromString(a.macroKind.className)),
          ("estimators", a.estimators.asJson),
          ("intervals", a.intervals.asJson)
      )
  }
  implicit protected val encodeCharacterizedOperator: Encoder[CharacterizedOperator] = new Encoder[CharacterizedOperator] {
    final def apply(a: CharacterizedOperator): Json =
      Json.obj(
          ("kind", Json.fromString(a.kind.name)),
          ("estimators", a.estimators.asJson),
          ("intervals", a.intervals.asJson)
      )
  }
  implicit protected val encodeRange: Encoder[Range] = new Encoder[Range] {
    final def apply(a: Range): Json =
      Json.obj(
          ("start", a.start.asJson),
          ("end", a.end.asJson),
          ("step", a.step.asJson)
      )
  }
  implicit protected val encodePoint: Encoder[Point] = new Encoder[Point] {
    final def apply(a: Point): Json =
      Json.obj(
          ("args", a.args.asJson),
          ("value", a.value.asJson)
      )
  }
  implicit protected val encodeGradient: Encoder[Gradient] = new Encoder[Gradient] {
    final def apply(a: Gradient): Json =
      Json.obj(
          ("points", a.points.asJson),
          ("linear", a.linear.asJson)
      )
  }
  implicit protected val encodeSimpleEstimator: Encoder[SimpleEstimator] = new Encoder[SimpleEstimator] {
    final def apply(a: SimpleEstimator): Json = {
      a match {
        case ge: GradientEstimator =>
          Json.obj(
              ("resource", Json.fromString(ge.resource)),
              ("fidelity", ge.fidelity.asJson),
              ("intervals", ge.intervals.asJson),
              ("gradient", ge.gradient.asJson)
          )
        case _ => throw new UnsupportedOperationException(s"Estimator with kind $a can not be serialized")
      }
    }
  }
  implicit protected val encodeParametrizedEstimator: Encoder[ParametrizedEstimator] = new Encoder[ParametrizedEstimator] {
    final def apply(a: ParametrizedEstimator): Json =
      Json.obj(
          ("resource", Json.fromString(a.resource)),
          ("fidelity", a.fidelity.asJson),
          ("intervals", a.intervals.asJson),
          ("estimators", a.estimators.asJson)
      )
  }

  implicit protected val encoder: Encoder[WithJson] = Encoder.instance {
    case cb: CharacterizedBoard    => encodeCharacterizedBoard.apply(cb)
    case cm: CharacterizedMacro    => encodeCharacterizedMacro.apply(cm)
    case co: CharacterizedOperator => encodeCharacterizedOperator.apply(co)
    case pe: ParametrizedEstimator => encodeParametrizedEstimator.apply(pe)
    case es: SimpleEstimator       => encodeSimpleEstimator.apply(es)
  }

// -------- DECODERS -----------------
  implicit protected def decodeRange: Decoder[Range] =
    new Decoder[Range] {
      final def apply(c: HCursor): Decoder.Result[Range] =
        for {
          start <- c.downField("start").as[Int]
          end   <- c.downField("end").as[Int]
          step  <- c.downField("step").as[Int]
        } yield {
          (start to end by step)
        }
    }
  implicit protected def decodePoint: Decoder[Point] =
    new Decoder[Point] {
      final def apply(c: HCursor): Decoder.Result[Point] =
        for {
          args  <- c.downField("args").as[Seq[Int]]
          value <- c.downField("value").as[Double]
        } yield {
          new Point(args, value)
        }
    }
  implicit protected def decodeGradient: Decoder[Gradient] =
    new Decoder[Gradient] {
      final def apply(c: HCursor): Decoder.Result[Gradient] =
        for {
          points <- c.downField("points").as[Seq[Point]]
          linear <- c.downField("linear").as[Boolean]
        } yield {
          new Gradient(points, linear)
        }
    }
  implicit protected def decodeSimpleEstimator: Decoder[SimpleEstimator] =
    new Decoder[SimpleEstimator] {
      final def apply(c: HCursor): Decoder.Result[SimpleEstimator] =
        for {
          resource  <- c.downField("resource").as[String]
          fidelity  <- c.downField("fidelity").as[Double]
          intervals <- c.downField("intervals").as[Seq[Range]]
          gradient  <- c.downField("gradient").as[Gradient]
        } yield {
          new GradientEstimator(resource, fidelity, intervals, gradient)
        }
    }
  implicit protected def decodeParametrizedEstimator: Decoder[ParametrizedEstimator] =
    new Decoder[ParametrizedEstimator] {
      final def apply(c: HCursor): Decoder.Result[ParametrizedEstimator] =
        for {
          resource   <- c.downField("resource").as[String]
          fidelity   <- c.downField("fidelity").as[Double]
          intervals  <- c.downField("intervals").as[Seq[Range]]
          estimators <- c.downField("estimators").as[Map[String, SimpleEstimator]]
        } yield {
          new ParametrizedEstimator(resource, fidelity, intervals, estimators)
        }
    }
  implicit protected def decodeCharacterizedMacro: Decoder[CharacterizedMacro] =
    new Decoder[CharacterizedMacro] {
      final def apply(c: HCursor): Decoder.Result[CharacterizedMacro] =
        for {
          macroKind  <- c.downField("macroKind").as[String]
          estimators <- c.downField("estimators").as[Seq[ParametrizedEstimator]]
          intervals  <- c.downField("intervals").as[Seq[Range]]
        } yield {
          new CharacterizedMacro(MacroBlock(macroKind), estimators, intervals)
        }
    }
  implicit protected def decodeCharacterizedOperator: Decoder[CharacterizedOperator] =
    new Decoder[CharacterizedOperator] {
      final def apply(c: HCursor): Decoder.Result[CharacterizedOperator] =
        for {
          kind       <- c.downField("kind").as[String]
          estimators <- c.downField("estimators").as[Seq[ParametrizedEstimator]]
          intervals  <- c.downField("intervals").as[Seq[Range]]
        } yield {
          new CharacterizedOperator(Operators.get(kind), estimators, intervals)
        }
    }
  implicit protected def decodeCharacterizedBoard: Decoder[CharacterizedBoard] =
    new Decoder[CharacterizedBoard] {
      final def apply(c: HCursor): Decoder.Result[CharacterizedBoard] =
        for {
          boardName <- c.downField("board").as[String]
          macros    <- c.downField("macros").as[Seq[CharacterizedMacro]]
          operators <- c.downField("operators").as[Seq[CharacterizedOperator]]
        } yield {
          new CharacterizedBoard(FPGA(boardName), macros, operators)
        }
    }

  private val customPrinter = Printer.spaces2.copy(dropNullValues = true)

  /** Emit a Json representation of this instance
   *  @return
   *    a Json string representing this instance
   */
  def emitJson: String = this.asJson.printWith(customPrinter)

  /** Pretty print the underlying construct.
   *
   *  @param indent
   *    indentation level to be used
   *  @return
   *    a String representation of this
   */
  def prettyToString(indent: Int = 0): String = " " * indent + this.toString

  def prettyToString: String = prettyToString()
}
