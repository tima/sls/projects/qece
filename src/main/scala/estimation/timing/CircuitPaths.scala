/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.timing

import qece.estimation.MetricMap

import firrtl.EmptyExpression
import firrtl.annotations.ReferenceTarget
import firrtl.ir.Expression

import java.io.File
import scala.io.Source

/** Representation of an estimated path between two endpoints
 *
 *  @param expr
 *    expression of this path (for printing purpose only)
 *  @param primitive
 *    operator used in this path (for printing purpose only)
 *  @param delay
 *    time of traversal of the path
 *  @param isMemoryAccess
 *    wether this path includes a memory access or not
 *  @param isInput
 *    wether this path access input ports or not
 */
private[timing] case class TimedPath(
    expr: Expression,
    primitive: ParametrizedPrimitive,
    delay: Double,
    paths: Seq[TimedPath] = Seq.empty,
    isMemoryAccess: Boolean = false,
    isInput: Boolean = false
) extends WithJson {

  /** Pretty print this [[TimedPath]]
   *
   *  @param indent
   *    indentation level to be used
   *  @return
   *    a String representation of this [[TimedPath]]
   */
  override def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + s"${expr.serialize}",
        " " * (indent + 2) +
          s"${primitive.prettyToString(0)} has delay ${delay}ns - mem: $isMemoryAccess / input: $isInput",
        paths.map(_.prettyToString(indent + 4)).mkString("")
    ).mkString("\n")
  }
}

/** Empty timed path, used to return errors without stoping the process. */
private[timing] object EmptyTimedPath extends TimedPath(EmptyExpression, Left(EmptyParametrizedOperator), 0.0)

private[timing] case class TimedConnection(ref: ReferenceTarget, path: TimedPath) extends WithJson {

  /** Pretty print this [[TimedConnection]]
   *
   *  @param indent
   *    indent level used for printing
   *  @return
   *    a String representing this [[Connection]]
   */
  override def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + "Target:",
        ref.prettyPrint(" " * (indent + 2)),
        " " * indent + "Paths:",
        path.prettyToString(indent + 2)
    ).mkString("\n")
  }
}

/** A serializable representation of a circuit and its endpoints, with associated paths.
 *
 *  @param circuit
 *    name of the circuit
 *  @param connections
 *    a sequence of all the connections found in the circuit
 */
private[estimation] case class CircuitPaths(circuit: String, connections: Seq[TimedConnection]) extends WithJson {

  /** Pretty print this [[CircuitPaths]]
   *
   *  @param indent
   *    indent level used for printing
   *  @return
   *    a String representing this [[CircuitPaths]]
   */
  override def prettyToString(indent: Int = 0): String = {
    Seq(" " * indent + circuit, connections.map(_.prettyToString(indent + 2)).mkString("\n")).mkString("\n")
  }

  /** Sort connections by longest delais
   *
   *  @return
   *    a new CircuitPaths with connections sorted by delay
   */
  def sortConnections: CircuitPaths = this.copy(connections = connections.sortWith(_.path.delay > _.path.delay))

  def summarize(indent: Int = 0, number: Int = 1): String = {
    this.sortConnections.connections
      .take(number)
      .map { co => " " * indent + co.prettyToString(indent + 2) }
      .mkString("\n")
  }

  def asMetricMap = {
    val delay = this.sortConnections.connections.head.path.delay
    MetricMap(
        "delay" -> delay,
        "freq"  -> 1.0 / delay
    )
  }
}

/** Companion object of [[CircuitPaths]].
 *
 *  Used to provide deserialization utilities.
 */
private[estimation] object CircuitPaths extends WithJson {

  /** Build a [[CircuitPaths]] by decoding a Json report file.
   *
   *  @param file
   *    json report file
   */
  def decode(file: File): CircuitPaths = {
    io.circe.parser.decode[CircuitPaths](Source.fromFile(file).getLines.mkString("")) match {
      case Right(s) => s
      case Left(ex) => throw ex
    }
  }

  /** Build a [[CircuitPaths]] by decoding a Json Source.
   *
   *  @param file
   *    json report file
   */
  def decode(file: Source): CircuitPaths = {
    io.circe.parser.decode[CircuitPaths](file.getLines.mkString("")) match {
      case Right(s) => s
      case Left(ex) => throw ex
    }
  }

  /** Build a [[CircuitPaths]] by decoding a Json report.
   *
   *  @param json
   *    report as String
   */
  def decode(json: String): CircuitPaths = {
    io.circe.parser.decode[CircuitPaths](json) match {
      case Right(s) => s
      case Left(ex) => throw ex
    }
  }
}
