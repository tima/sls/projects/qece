/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.timing

import firrtl.ir.{Expression, Reference}
import firrtl.annotations.{Target, ReferenceTarget}

import qece.estimation.utils.Operators
import qece.estimation.macros.{MacroBlock, ParametrizedMacroBlock, EmptyParametrizedMacroBlock}

import io.circe._, io.circe.syntax._

/** Trait used to serialize and unserialize reports with manual derivation, as automatic fail on some classes */
private[timing] trait WithJson {

// -------- ENCODERS -----------------

  implicit val encodeExpression: Encoder[Expression] = new Encoder[Expression] {
    final def apply(a: Expression): Json =
      Json.obj(
          ("expr", Json.fromString(a.serialize))
      )
  }
  implicit val encodeParametrizedOperator: Encoder[ParametrizedOperator] = new Encoder[ParametrizedOperator] {
    final def apply(a: ParametrizedOperator): Json =
      Json.obj(
          ("operator", Json.fromString(a.operator.name)),
          ("params", a.params.asJson)
      )
  }
  implicit val encodeParametrizedMacroBlock: Encoder[ParametrizedMacroBlock] = new Encoder[ParametrizedMacroBlock] {
    final def apply(a: ParametrizedMacroBlock): Json =
      Json.obj(
          ("macro", a.name.asJson),
          ("params", a.params.asJson)
      )
  }
  implicit val encodeParametrizedPrimitive: Encoder[ParametrizedPrimitive] = new Encoder[ParametrizedPrimitive] {
    final def apply(a: ParametrizedPrimitive): Json =
      Json.obj(
          ("operator", a.left.getOrElse(EmptyParametrizedOperator).asJson),
          ("macro", a.right.getOrElse(EmptyParametrizedMacroBlock).asJson)
      )
  }
  implicit val encodePath: Encoder[Path] = new Encoder[Path] {
    final def apply(a: Path): Json =
      Json.obj(
          ("module", a.module.asJson),
          ("expr", a.expr.asJson),
          ("primitive", a.primitive.asJson),
          ("level", Json.fromInt(a.level)),
          ("paths", Json.fromValues(a.paths.map(_.asJson))),
          ("isMemoryAccess", Json.fromBoolean(a.isMemoryAccess)),
          ("isInput", Json.fromBoolean(a.isInput)),
          ("toResolve", Json.fromBoolean(a.toResolve))
      )
  }
  implicit val encodeReferenceTarget: Encoder[ReferenceTarget] = new Encoder[ReferenceTarget] {
    final def apply(a: ReferenceTarget): Json =
      Json.obj(
          ("target", Json.fromString(a.serialize))
      )
  }
  implicit val encodeConnection: Encoder[Connection] = new Encoder[Connection] {
    final def apply(a: Connection): Json =
      Json.obj(
          ("ref", a.ref.asJson),
          ("path", a.path.asJson)
      )
  }
  implicit val encodeCircuitEndpoints: Encoder[CircuitEndpoints] = new Encoder[CircuitEndpoints] {
    final def apply(a: CircuitEndpoints): Json =
      Json.obj(
          ("circuit", a.circuit.asJson),
          ("connections", a.connections.asJson)
      )
  }
  implicit val encodeTimedPath: Encoder[TimedPath] = new Encoder[TimedPath] {
    final def apply(a: TimedPath): Json =
      Json.obj(
          ("expr", a.expr.asJson),
          ("primitive", a.primitive.asJson),
          ("delay", Json.fromDouble(a.delay).get),
          ("paths", Json.fromValues(a.paths.map(_.asJson))),
          ("isMemoryAccess", Json.fromBoolean(a.isMemoryAccess)),
          ("isInput", Json.fromBoolean(a.isInput))
      )
  }
  implicit val encodeTimedConnection: Encoder[TimedConnection] = new Encoder[TimedConnection] {
    final def apply(a: TimedConnection): Json =
      Json.obj(
          ("ref", a.ref.asJson),
          ("path", a.path.asJson)
      )
  }
  implicit val encodeCircuitPaths: Encoder[CircuitPaths] = new Encoder[CircuitPaths] {
    final def apply(a: CircuitPaths): Json =
      Json.obj(
          ("circuit", a.circuit.asJson),
          ("connections", a.connections.asJson)
      )
  }

  implicit val encoder: Encoder[WithJson] = Encoder.instance {
    case ci @ CircuitEndpoints(_, _)       => ci.asJson
    case co @ Connection(_, _)             => co.asJson
    case lp @ Path(_, _, _, _, _, _, _, _) => lp.asJson
    case po @ ParametrizedOperator(_, _)   => po.asJson
    case tp @ TimedPath(_, _, _, _, _, _)  => tp.asJson
    case tc @ TimedConnection(_, _)        => tc.asJson
    case cp @ CircuitPaths(_, _)           => cp.asJson
  }

// -------- DECODERS -----------------
  // dumb Expression decoder, as expression is not needed after serialization
  implicit val decodeExpression: Decoder[Expression] = new Decoder[Expression] {
    final def apply(c: HCursor): Decoder.Result[Expression] =
      for {
        expr <- c.downField("expr").as[String]
      } yield {
        new Reference(expr)
      }
  }
  implicit val decodeParametrizedOperator: Decoder[ParametrizedOperator] = new Decoder[ParametrizedOperator] {
    final def apply(c: HCursor): Decoder.Result[ParametrizedOperator] =
      for {
        operator <- c.downField("operator").as[String]
        params   <- c.downField("params").as[Seq[Int]]
      } yield {
        ParametrizedOperator(Operators.get(operator), params)
      }
  }
  implicit val decodeParametrizedMacroBlock: Decoder[ParametrizedMacroBlock] = new Decoder[ParametrizedMacroBlock] {
    final def apply(c: HCursor): Decoder.Result[ParametrizedMacroBlock] =
      for {
        macroName <- c.downField("macro").as[String]
        params    <- c.downField("params").as[Seq[Int]]
      } yield {
        ParametrizedMacroBlock(MacroBlock(macroName), params)
      }
  }
  implicit val decodeParametrizedPrimitive: Decoder[ParametrizedPrimitive] = new Decoder[ParametrizedPrimitive] {
    final def apply(c: HCursor): Decoder.Result[ParametrizedPrimitive] =
      for {
        operator <- c.downField("operator").as[ParametrizedOperator]
        mac      <- c.downField("macro").as[ParametrizedMacroBlock]
      } yield {
        (operator, mac) match {
          //TODO: proper exceptions
          case (EmptyParametrizedOperator, EmptyParametrizedMacroBlock) =>
            throw new UnsupportedOperationException(s"ParametrizedPrimitive can't be empty on both side")
          case (EmptyParametrizedOperator, x)   => Right(x)
          case (x, EmptyParametrizedMacroBlock) => Left(x)
          case (_, _) =>
            throw new UnsupportedOperationException(s"ParametrizedPrimitive can't be both Operator and MacroBlock")
        }
      }
  }
  implicit val decodePath: Decoder[Path] = new Decoder[Path] {
    final def apply(c: HCursor): Decoder.Result[Path] =
      for {
        module         <- c.downField("module").as[String]
        expr           <- c.downField("expr").as[Expression]
        primitive      <- c.downField("primitive").as[ParametrizedPrimitive]
        level          <- c.downField("level").as[Int]
        paths          <- c.downField("paths").as[Seq[Path]]
        isMemoryAccess <- c.downField("isMemoryAccess").as[Boolean]
        isInput        <- c.downField("isInput").as[Boolean]
        toResolve      <- c.downField("toResolve").as[Boolean]
      } yield {
        new Path(module, expr, primitive, level, paths, isMemoryAccess, isInput, toResolve)
      }
  }
  implicit val decodeReferenceTarget: Decoder[ReferenceTarget] = new Decoder[ReferenceTarget] {
    final def apply(c: HCursor): Decoder.Result[ReferenceTarget] =
      for {
        target <- c.downField("target").as[String]
      } yield {
        Target.deserialize(target).asInstanceOf[ReferenceTarget]
      }
  }
  implicit val decodeConnection: Decoder[Connection] = new Decoder[Connection] {
    final def apply(c: HCursor): Decoder.Result[Connection] =
      for {
        ref  <- c.downField("ref").as[ReferenceTarget]
        path <- c.downField("path").as[Path]
      } yield {
        Connection(ref, path)
      }
  }
  implicit val decodeCircuitEndpoints: Decoder[CircuitEndpoints] = new Decoder[CircuitEndpoints] {
    final def apply(c: HCursor): Decoder.Result[CircuitEndpoints] =
      for {
        circuit     <- c.downField("circuit").as[String]
        connections <- c.downField("connections").as[Seq[Connection]]
      } yield {
        new CircuitEndpoints(circuit, connections.toSeq)
      }
  }
  implicit val decodeTimedPath: Decoder[TimedPath] = new Decoder[TimedPath] {
    final def apply(c: HCursor): Decoder.Result[TimedPath] =
      for {
        expr           <- c.downField("expr").as[Expression]
        primitive      <- c.downField("primitive").as[ParametrizedPrimitive]
        delay          <- c.downField("delay").as[Double]
        paths          <- c.downField("paths").as[Seq[TimedPath]]
        isMemoryAccess <- c.downField("isMemoryAccess").as[Boolean]
        isInput        <- c.downField("isInput").as[Boolean]
      } yield {
        new TimedPath(expr, primitive, delay, paths, isMemoryAccess, isInput)
      }
  }
  implicit val decodeTimedConnection: Decoder[TimedConnection] = new Decoder[TimedConnection] {
    final def apply(c: HCursor): Decoder.Result[TimedConnection] =
      for {
        ref  <- c.downField("ref").as[ReferenceTarget]
        path <- c.downField("path").as[TimedPath]
      } yield {
        TimedConnection(ref, path)
      }
  }
  implicit val decodeCircuitPaths: Decoder[CircuitPaths] = new Decoder[CircuitPaths] {
    final def apply(c: HCursor): Decoder.Result[CircuitPaths] =
      for {
        circuit     <- c.downField("circuit").as[String]
        connections <- c.downField("connections").as[Seq[TimedConnection]]
      } yield {
        new CircuitPaths(circuit, connections.toSeq)
      }
  }

  private val customPrinter = Printer.spaces2.copy(dropNullValues = true)

  /** Emit a Json representation of this instance
   *  @return
   *    a Json string representing this instance
   */
  def emitJson: String = this.asJson.printWith(customPrinter)

  /** Pretty print the underlying construct.
   *
   *  @param indent
   *    indentation level to be used
   *  @return
   *    a String representation of this
   */
  def prettyToString(indent: Int = 0): String = " " * indent + this.toString
}
