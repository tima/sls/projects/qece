/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.timing.transforms

import qece.QeceContext

import qece.estimation.utils.{Operators, ReferenceTargetUtils}
import qece.estimation.timing.exceptions.ReferenceDuplicaFoundException

import qece.estimation.timing.{
  Connection,
  Path,
  ParametrizedOperator,
  CircuitEndpoints,
  EmittedEndpointReportAnnotation,
  EmittedEndpointReport
}

import qece.estimation.macros.{ReplaceInfo, ParametrizedMacroBlock}
import qece.estimation.macros.transforms.ReplaceMacro
import qece.estimation.transforms.{MetricTransform, SynchroLogging}
import qece.estimation.exceptions._

import scala.collection.mutable.{ListBuffer, HashMap}

import firrtl._
import firrtl.options.Dependency
import firrtl.ir._
import firrtl.annotations._
import firrtl.passes.MemPortUtils

/** Attempt to report resource usage estimation within the current circuit, with a hierarchical approach */
//TODO: For wires, should add a pass to resolve paths after everything else, due to potential affectation after use
private[qece] class EndpointReport extends MetricTransform {

  // do it right after macro replacement
  override val prerequisites = List(Dependency[ReplaceMacro])

  override val duration = QeceContext.timeout.endpointReport

  val warnLimit = 100

  // Output suffix for report emission
  val outputSuffix: String = ".endpoint.json"

  val generatedKeys = Seq(
      "#endpoint",
      "maxDepth"
  )

// scalastyle:off method.length
  /** run resource usage reporting for the current circuit
   *  @param circuit
   *    Circuit to be reported
   *  @param replaceInfos
   *    [[ReplaceInfo]] (s) to check macro
   */
  private def run(circuit: Circuit, replaceInfos: Seq[ReplaceInfo]): CircuitEndpoints = {

    // Circuit target building as basis of the whole target referential
    val ct = CircuitTarget(circuit.main)

    // inner endpoints to be instanciated
    val defEndpoints = new ListBuffer[ReferenceTarget]()

    // references to actually count as endpoints
    val pathEndpoints = new ListBuffer[ReferenceTarget]()

    // Report every path in every module recursively
    val defModules = new HashMap[ModuleTarget, HashMap[ReferenceTarget, Path]]

    // either run on chisel module, or evaluate extmodules
    def runDefModule(dm: DefModule): Unit = {
      dm match {
        case m: Module     => runModule(m)
        case em: ExtModule => runExtModule(em)
      }
    }

    def runExtModule(em: ExtModule): Unit = {
      val mt = ct.module(em.name)

      val inputs  = new ListBuffer[Port]()
      val outputs = new ListBuffer[Port]()

      // Don't support full ExtModule, as it is useless
      if (mt.module == mt.circuit) throw new UnsupportedOperationException(s"ExtModule ${em.name} can not be toplevel")

      def recordPort(port: Port): Unit = {
        port.direction match {
          case Input  => inputs += port
          case Output => outputs += port
        }
      }

      em.foreachPort(recordPort)
      val inputPaths = inputs.map(input =>
        mt.ref(input.name) -> Path(
            module = mt.module,
            expr = Reference(input.name, input.tpe, PortKind, SourceFlow),
            primitive = Left(ParametrizedOperator(Operators.UNRESOLVED, bitWidth(input.tpe))),
            level = 0,
            isInput = true,
            toResolve = true
        )
      )

      // build path for each replace info found
      def buildPaths(e: ReplaceInfo): HashMap[ReferenceTarget, Path] = {
        e.isCombi match {
          //build paths with macro kind from Inputs to outputs
          case true =>
            val outputPaths = outputs.map { output =>
              (mt.ref(output.name) -> Path(
                  mt.module,
                  Reference(output.name, output.tpe, PortKind, SourceFlow),
                  Right(ParametrizedMacroBlock(e.macroKind, e.params)),
                  level = 1,
                  paths = inputPaths.map(_._2),
                  isMemoryAccess = e.isMemory,
                  isInput = true,
                  toResolve = true
              ))
            }
            HashMap(outputPaths: _*)
          //build paths with macro kind from inputs to inner endpoint and from inner endpoint to outputs
          case false =>
            // inner endpoint definition
            val macroEndpoint = mt.ref("inner")
            defEndpoints += macroEndpoint
            // defEndpoints contains macroEndpoint match {
            //   case true =>
            //   case false => defEndpoints += macroEndpoint
            // }

            val kind = if (e.isMemory) MemKind else RegKind
            val innerPath = (macroEndpoint -> Path(
                mt.module,
                Reference(macroEndpoint.ref, outputs.head.tpe, kind),
                Right(ParametrizedMacroBlock(e.macroKind, e.params)),
                level = 1,
                paths = inputPaths.map(_._2),
                isMemoryAccess = e.isMemory,
                isInput = true,
                toResolve = true
            ))
            val outputPaths = outputs.map { output =>
              (mt.ref(output.name) -> Path(
                  mt.module,
                  Reference(output.name, output.tpe, PortKind),
                  Right(ParametrizedMacroBlock(e.macroKind, e.params)),
                  level = 0,
                  paths = Seq.empty,
                  isMemoryAccess = e.isMemory,
                  isInput = false
              ))
            }
            HashMap((outputPaths :+ innerPath): _*)
        }
      }

      replaceInfos.filter(_.defModule == em) match {
        case Seq() => throw new UnsupportedOperationException(s"Black boxes and other ExtModule are not supported yet")
        // case s      => defModules(ct.module(em.name)) = s.map(buildPaths).reduceOption(_++_).getOrElse(HashMap.empty)
        case s => defModules(ct.module(em.name)) = buildPaths(s.head)
      }
    }

    // recursively report pathes on modules
    def runModule(m: Module): Unit = {
      val mt = ct.module(m.name)

      val isMain = (mt.module == mt.circuit)

      val pathMap = new HashMap[ReferenceTarget, Path]()
      //TODO :USELESS ?
      val wires = new HashMap[ReferenceTarget, Path]()

      // Resolve every ref occurence in the current module pathMap with the newPath instance
      def resolve(ref: ReferenceTarget, newPath: Path): Unit = {
        // recursively resolve every paths and subpaths
        def resolve(p: Path): Path = {
          (p.toResolve, p.primitive) match {
            case (true, Left(operator)) =>
              operator.operator match {
                case Operators.UNRESOLVED =>
                  if (ReferenceTargetUtils.getReferenceTarget(mt, p.expr, List.empty) == Some(ref)) newPath else p
                case _ =>
                  // resolve recursively, and update this path inner values (level, isMemoryAccess, isInput)
                  val sub = p.paths.map(resolve)
                  p.copy(
                      paths = sub,
                      level = if (sub.isEmpty) 0 else sub.maxBy(_.level).level + 1,
                      isMemoryAccess = if (sub.isEmpty) p.isMemoryAccess else sub.map(_.isMemoryAccess).reduce(_ | _),
                      isInput = if (sub.isEmpty) p.isInput else sub.map(_.isInput).reduce(_ | _)
                  )
              }
            case (true, Right(_)) =>
              val sub = p.paths.map(resolve)
              p.copy(
                  paths = sub,
                  level = if (sub.isEmpty) 0 else sub.maxBy(_.level).level + 1,
                  isMemoryAccess = if (sub.isEmpty) p.isMemoryAccess else sub.map(_.isMemoryAccess).reduce(_ | _),
                  isInput = if (sub.isEmpty) p.isInput else sub.map(_.isInput).reduce(_ | _)
              )
            case (false, _) => p
          }
        }

        pathMap.transform({ (_, v) => resolve(v) })
      }

      // add a new path to the module pathMap
      def addPath(ref: ReferenceTarget, path: Path): Unit = {
        pathMap.contains(ref) match {
          case false => pathMap.update(ref, path)
          case true  => throw ReferenceDuplicaFoundException(s"Found multiple references ${ref} in module ${mt}")
        }
      }

      // replace every occurence of the path with a subfield Reference
      def instantiate(inst: ReferenceTarget, path: Path): Path = {
        def buildSubField(expr: Expression): Expression = {
          expr match {
            case r: Reference => SubField(Reference(inst.ref), r.name, r.tpe, r.flow)
            case s: SubField  => SubField(buildSubField(s.expr), s.name, s.tpe, s.flow)
            case _            => throw ParsingException(s"Unresolved operation must be reference, found ${expr}")
          }
        }
        path.primitive match {
          case Left(operator) =>
            operator.operator match {
              case Operators.UNRESOLVED => path.copy(expr = buildSubField(path.expr))
              case _                    => path.copy(paths = path.paths.map(instantiate(inst, _)))
            }
          case Right(_) => path.copy(paths = path.paths.map(instantiate(inst, _)))
        }
      }

      // record ports found in module, as they'll be used as references in further expressions
      def recordPort(port: Port): Unit = {
        val ((op, toResolve), isInput) = port.direction match {
          // record every input in pathMap as a Reference, either IO for toplevel, or unresolved for submodule
          // declaration
          case Input =>
            (
                if (isMain) {
                  (ParametrizedOperator(Operators.IO, bitWidth(port.tpe)), false)
                } else {
                  (ParametrizedOperator(Operators.UNRESOLVED, bitWidth(port.tpe)), true)
                },
                true
            )
          // record every output as References in the pathMap, in case it was use as a signal in other expressions
          case Output =>
            ((ParametrizedOperator(Operators.UNRESOLVED, bitWidth(port.tpe)), true), false)
        }
        addPath(
            mt.ref(port.name),
            Path(
                module = mt.module,
                expr = Reference(port.name, port.tpe, PortKind, SourceFlow),
                primitive = Left(op),
                level = 0,
                isInput = isInput,
                toResolve = toResolve
            )
        )
      }

      // record a DefMemory statement
      // Should not be used, since every memory primitive should be replaced by macroblock
      def addMemory(mem: DefMemory): Unit = {
        // retrieve every ports defined for this DefMemory
        val ports = MemPortUtils
          .memType(mem)
          .fields
          .map { x =>
            x.name -> (x.tpe match {
              case b: BundleType => b.fields
              case _             => throw ParsingException(s"Ports must be bundles, found ${x.tpe}")
            })
          }
          .toMap

        // inner endpoint definition
        val memEndpoint = mt.ref(mem.name).field("memory")

        // record every memory inputs
        val inputs = ListBuffer[Path]()

        // record every memory outputs along with their paths
        val outputs = ListBuffer[(ReferenceTarget, Path)]()

        def getExpression(ref: ReferenceTarget, tpe: Type, kind: Kind, flow: Flow): Expression = {
          def recurse(expr: Expression, fields: Seq[String]): Expression = {
            fields match {
              case Seq(s) => SubField(expr, s, tpe, flow)
              case s      => recurse(SubField(expr, s.head, tpe, flow), s.drop(1))
            }
          }
          ref.component match {
            case Seq() => Reference(ref.ref, tpe, kind, flow)
            case s =>
              recurse(
                  Reference(ref.ref, tpe, kind, flow),
                  s.map(_.asInstanceOf[firrtl.annotations.TargetToken.Field].value)
              )
          }
        }

        // record every port either as inputs of the mem endpoints, or as output reference
        ports.keys.foreach { portName =>
          ports(portName).foreach { f =>
            val ref = mt.ref(mem.name).field(portName).field(f.name)
            f.flip match {
              // record flipped Fields as possible references, as it will be refered as right hand
              case Flip =>
                outputs += (ref -> Path(
                    mt.module,
                    getExpression(memEndpoint, f.tpe, MemKind, SourceFlow),
                    Left(ParametrizedOperator(Operators.MEMORY, bitWidth(f.tpe))),
                    isMemoryAccess = true
                ))
              // record default Fields as subpaths of the memory endpoint
              case Default =>
                inputs += Path(
                    mt.module,
                    getExpression(memEndpoint, f.tpe, MemKind, SinkFlow),
                    Left(ParametrizedOperator(Operators.UNRESOLVED, bitWidth(f.tpe))),
                    toResolve = true
                )
              case x =>
                throw ParsingException(s"$x Orientation not found")
            }
          }
        }

        // add an inner endpoint, and add every input port as subpath of memEndpoint
        if (isMain) pathEndpoints += memEndpoint else defEndpoints += memEndpoint
        addPath(
            memEndpoint,
            Path(
                mt.module,
                Reference(memEndpoint.ref),
                Left(ParametrizedOperator(Operators.MEMORY, bitWidth(mem.dataType) * mem.depth)),
                level = inputs.toSeq.maxBy(_.level).level + 1,
                paths = inputs.toSeq,
                isMemoryAccess = inputs.toSeq.map(_.isMemoryAccess).reduce(_ | _),
                isInput = inputs.toSeq.map(_.isInput).reduce(_ | _),
                toResolve = inputs.toSeq.map(_.toResolve).reduce(_ | _)
            )
        )

        // add every output ports as ref
        outputs.foreach { case (ref, p) => addPath(ref, p) }

        // Check weither this memory config is supported
        (mem.readLatency, mem.writeLatency) match {
          case (0, 1) =>
          // both SyncReadMem and Mem resolve to (0, 1) case, so it'll be the only thing supported atm
          case (r, w) => logger.warn(s"[WARNING] Memory with $r cycle reads and $w cycle writes are not supported")
        }
      }

      // add an instance to the current module, using a DefInstance statement
      def addInstance(i: DefInstance): Unit = {
        val paths = defModules(ct.module(i.module))
        defEndpoints.foreach {
          case ref =>
            if (ct.module(ref.module) == ct.module(i.module)) {
              val subRef = ReferenceTargetUtils.getReferenceTarget(
                  mt.ref(i.name),
                  (ReferenceTargetUtils.getFields(ref) :+ ref.ref).toList
              )
              defEndpoints += subRef
              // If toplevel, record every endpoints to pathEndpoints rather than only to the defEndpoints map
              if (isMain) pathEndpoints += subRef
            }
        }
        paths.keys.foreach { key =>
          val myRef = ReferenceTargetUtils.getReferenceTarget(
              mt.ref(i.name),
              (ReferenceTargetUtils.getFields(key) :+ key.ref).toList
          )
          addPath(myRef, instantiate(mt.ref(i.name), paths(key)))
        }
      }

      // record a connect statement
      def addConnect(connect: Connect): Unit = {
        // add a location to pathMap, and to endpoints if toplevel
        def addLoc(loc: Expression, ref: ReferenceTarget): Unit = {
          loc match {
            // if reference, resolve path and record
            case r: Reference =>
              val p = Path(connect.expr, mt, pathMap)
              // Only report in main, add theoritical endpoints in submodule to defEndpoints
              r.kind match {
                case RegKind =>
                  if (isMain) { pathEndpoints += ref }
                  else { defEndpoints += ref }
                case PortKind =>
                  // resolve every port in case outputs are used as signal in the circuit
                  //TODO: CHECK IF USEFUL
                  resolve(ref, p)
                  // if loc is endpoint then it is an output port
                  if (isMain) { pathEndpoints += ref }
                case InstanceKind =>
                  //TODO: CHECK IF USEFUL
                  resolve(ref, p)
                case MemKind =>
                  //TODO: CHECK IF USEFUL
                  resolve(ref, p)
                case WireKind =>
                  // resolve every wire that might be used as ref in other paths
                  //TODO: CHECK IF USEFUL
                  resolve(ref, p)
                case NodeKind =>
                  resolve(ref, p)
                case _ =>
                  val str = s"Can't connect to Reference with kind ${r.kind} in ${loc.serialize} with ref ${ref}"
                  throw ParsingException(str)
              }
              pathMap.update(mt.ref(r.name), p)
            // if subfield, recursively resolve it
            case s: SubField => addLoc(s.expr, ref)
            case _ =>
              throw ParsingException(s"Loc ${connect.loc.serialize} should be either Reference or SubField")
          }
        }
        addLoc(connect.loc, ReferenceTargetUtils.getReferenceTarget(mt, connect.loc).get)
      }

      // retrieve a reference by deserializing its name
      def getReference(name: String, tpe: Type, kind: Kind): Expression = {
        def recurse(expr: Expression, fields: Seq[String]): SubField = {
          fields match {
            case Seq(s) => SubField(expr, s, tpe)
            case s      => recurse(SubField(expr, s.head, tpe), s.drop(1))
          }
        }
        name.split('.').toSeq match {
          case Seq(s) => Reference(s, tpe, kind)
          case s      => recurse(Reference(s.head, tpe, kind), s.drop(1))
        }
      }

      // run transform on every statement of the module
      def runStmt(stmt: Statement): Unit = {
        stmt match {
          // Add signal declaration to paths
          case w: DefWire =>
            val path = Path(
                module = mt.module,
                expr = getReference(w.name, w.tpe, WireKind),
                primitive = Left(ParametrizedOperator(Operators.UNRESOLVED, bitWidth(w.tpe))),
                level = 0,
                isInput = false,
                toResolve = true
            )
            val ref = ReferenceTargetUtils.deserialize(mt, w.name)
            addPath(ref, path)
            //TODO: Manage wire AND reg resolution after the first pass
            wires += (ref -> path)
          // On submodule instanciation, first record every endpoints to the current module, and then add every subpaths
          case i: DefInstance =>
            addInstance(i)
          // Instantiate memory component as references
          case mem: DefMemory =>
            addMemory(mem)
          // one of the terminal cases for path building
          case r: DefRegister =>
            addPath(
                mt.ref(r.name),
                Path(
                    mt.module,
                    Reference(r.name, r.tpe, RegKind),
                    Left(ParametrizedOperator(Operators.REG, bitWidth(r.tpe)))
                )
            )
          // record every node as paths to build endpoints
          case n: DefNode =>
            addPath(mt.ref(n.name), Path(n.value, mt, pathMap))
          // endpoints are always lefthand parts of a Connect statement
          case c: Connect =>
            addConnect(c)
          // block managed by foreachStmt method
          case _: Block  =>
          case EmptyStmt =>
          case _         => logger.warn(s"[WARNING] unknown statement -> ${stmt.getClass.getName}: " + stmt.serialize)
        }
        stmt.foreachStmt(runStmt)
      }
      // record every input port as nodes
      m.foreachPort(recordPort)
      // record every references as nodes
      m.foreachStmt(runStmt)
      defModules(ct.module(m.name)) = pathMap
    }
    circuit.modules.foreach(runDefModule)

    def checkForUnresolved(paths: Iterable[Path]): Unit = {
      val list = new scala.collection.mutable.ArrayBuffer[Path]()
      def check(path: Path): Unit = {
        path.primitive match {
          case Left(x) =>
            x.operator match {
              case Operators.UNRESOLVED => list += path
              case _                    =>
            }
          case Right(_) =>
        }
        path.paths.foreach(check)
      }
      paths.foreach(check)
      list.size match {
        case 0 =>
        case x =>
          val end = if (x > warnLimit) "  ..." else ""
          logger.warn(
              s"[WARNING] ${this.getClass.getName}:\n" +
                s"Found $x unresolved path(s):\n${list.take(warnLimit).map(_.prettyToString(2)).mkString("")}$end"
          )
      }
    }

    // get map from the topmodule, where every path is fully resolved to endpoint terminal cases
    val fullyResolved = defModules(ct.module(circuit.main))

    checkForUnresolved(fullyResolved.values)
    // serialize and return the endpoint summary
    val endpoints = pathEndpoints.map(ref => Connection(ref, fullyResolved(ref)))
    CircuitEndpoints(circuit.main, endpoints)
  }
// scalastyle:on method.length

  /** Execute the analysis transform on the circuit, and add an annotation to pass the report to further transforms.
   *  Also emits the report to the generation dir
   *  @param state
   *    circuit to apply the transform on
   *  @return
   *    the circuit, untouched as it is only a reporting transform
   */
  override def execute(state: CircuitState): CircuitState = {

    SynchroLogging.addTask("endpoint")
    mydebug(s"[ESTIMATION] Started endpoint report on ${getMetrics(state.annotations).toMap}")
    val (time, endpoints) = exec(Utils.time(run(getUpdatedCircuit(state), getReplaceInfos(state))))
    mydebug(s"[ESTIMATION] Completed endpoint report on ${getMetrics(state.annotations).toMap}")
    SynchroLogging.removeTask("endpoint")

    logger.trace(Seq(s"[$getTime] ${this.getClass.getSimpleName}:", endpoints.prettyToString()).mkString("\n"))

    state.copy(annotations =
      Seq(
          EmittedEndpointReportAnnotation(
              endpoints,
              EmittedEndpointReport(state.circuit.main, endpoints.emitJson, outputSuffix),
              time
          ),
          generateMetricAnnotation(endpoints.asMetricMap)
      ) ++ state.annotations
    )
  }

  // Deprecated
  override def emit(state: CircuitState, writer: java.io.Writer): Unit = {}
}
