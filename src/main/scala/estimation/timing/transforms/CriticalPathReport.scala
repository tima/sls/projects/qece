/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.timing.transforms

import qece.characterization.CharacterizedBoard

import qece.estimation.timing.{
  Path,
  TimedPath,
  TimedConnection,
  CircuitPaths,
  EmittedCriticalPathReport,
  EmittedCriticalPathReportAnnotation
}
import qece.estimation.exceptions._

import qece.estimation.utils.{Operators}
import qece.estimation.transforms.MetricTransform

import firrtl.{CircuitState, Utils}
import firrtl.options.Dependency
import firrtl.annotations.ReferenceTarget

/** Attempt to report resource usage estimation within the current circuit, with a hierarchical approach */
private[qece] class CriticalPathReport extends MetricTransform {

  // do it the closest from endpoint reporting
  override val prerequisites = List(Dependency[EndpointReport])

  // Output suffix for report emission
  val outputSuffix: String = ".paths.json"

  val generatedKeys = Seq(
      "delay",
      "freq"
  )

  private def reportPath(estimators: CharacterizedBoard, path: Path): TimedPath = {
    val subPaths = path.paths.map(reportPath(estimators, _))
    val opDelay =
      path.primitive match {
        case Left(operator) =>
          if (operator.operator == Operators.MEMORY) {
            throw UnexpectedPrimitiveException("Memory operator should be replaced by MemoryBlock macro")
          }
          if (operator.operator.isWire) { 0.0 }
          else {
            estimators.getCharacterizedOperator(operator.operator).getEstimator("delay").evaluate(operator.bitwidth)
          }
        case Right(macroBlock) =>
          estimators
            .getCharacterizedMacro(macroBlock.defMacro)
            .getEstimator("delay")
            .evaluate(macroBlock.paramString, macroBlock.args)
      }
    TimedPath(
        path.expr,
        primitive = path.primitive,
        delay = if (subPaths.isEmpty) opDelay else opDelay + subPaths.maxBy(_.delay).delay,
        paths = subPaths,
        isMemoryAccess = path.isMemoryAccess,
        isInput = path.isInput
    )
  }

  /** Execute the analysis transform on the circuit, and add an annotation to pass the report to further transforms.
   *  Also emits the report to the generation dir
   *  @param state
   *    circuit to apply the transform on
   *  @return
   *    the circuit, untouched as it is only a reporting transform
   */
  override def execute(state: CircuitState): CircuitState = {

    val (time, paths) = Utils.time({
      // Attempt to retrieve circuit endpoint from annotations
      val endpoints = getEndpoints(state)

      // Attempt to retrieve the estimator from configuration file, if it exists
      val estimators = getEstimators(state)

      // Retrieve path, compute delays, and sort paths by delay
      CircuitPaths(
          endpoints.circuit,
          endpoints.connections.map(co => co.ref -> reportPath(estimators, co.path)).map {
            case (k: ReferenceTarget, v: TimedPath) => TimedConnection(k, v)
          }
      ).sortConnections
    })

    logger.trace(Seq(s"[$getTime] ${this.getClass.getSimpleName}:", paths.prettyToString()).mkString("\n"))

    val report = EmittedCriticalPathReport(state.circuit.main, paths.emitJson, outputSuffix)
    state.copy(annotations =
      Seq(
          EmittedCriticalPathReportAnnotation(paths, report, time),
          generateMetricAnnotation(paths.asMetricMap)
      ) ++ state.annotations
    )
  }

  // Deprecated
  override def emit(state: CircuitState, writer: java.io.Writer): Unit = {}
}
