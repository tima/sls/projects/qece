/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.timing

import qece.estimation.annotations._

/** Class for endpoint reporting */
private[estimation] final case class EmittedEndpointReportAnnotation(
    endpoints: CircuitEndpoints,
    value: EmittedEndpointReport,
    override val duration: Double
) extends EmittedReportAnnotation[EmittedEndpointReport] with TimedAnnotation

/** Class for timing reporting */
private[estimation] final case class EmittedCriticalPathReportAnnotation(
    paths: CircuitPaths,
    value: EmittedCriticalPathReport,
    override val duration: Double
) extends EmittedReportAnnotation[EmittedCriticalPathReport] with TimedAnnotation

private[estimation] final case class EmittedEndpointReport(name: String, value: String, outputSuffix: String)     
  extends EmittedReport
private[estimation] final case class EmittedCriticalPathReport(name: String, value: String, outputSuffix: String)
  extends EmittedReport
