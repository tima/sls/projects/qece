/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.timing

import qece.estimation.exceptions.ParsingException
import qece.estimation.utils.{Operators, ReferenceTargetUtils}
import qece.estimation.MetricMap

import firrtl._
import firrtl.PrimOps._
import firrtl.ir._
import firrtl.annotations._

import scala.collection.mutable.Map

import java.io.File
import scala.io.Source

import logger.LazyLogging

/** A serializable representation of a circuit and its endpoints, with associated paths.
 *
 *  @param circuit
 *    name of the circuit
 *  @param connections
 *    a sequence of all the connections found in the circuit
 */
private[estimation] case class CircuitEndpoints(circuit: String, connections: Seq[Connection]) extends WithJson {

  /** Pretty print this [[CircuitEndpoints]]
   *
   *  @param indent
   *    indent level used for printing
   *  @return
   *    a String representing this [[CircuitEndpoints]]
   */
  override def prettyToString(indent: Int = 0): String = {
    Seq(" " * indent + circuit, connections.map(_.prettyToString(indent + 2)).mkString("\n")).mkString("\n")
  }

  def asMetricMap =
    MetricMap(
        "#endpoint" -> connections.size,
        "maxDepth"  -> connections.map(_.path.level).max
    )
}

/** Companion object of [[CircuitEndpoints]].
 *
 *  Used to provide deserialization utilities.
 */
private[timing] object CircuitEndpoints extends WithJson {

  /** Build a [[CircuitEndpoints]] by decoding a Json report file.
   *
   *  @param file
   *    json report file
   */
  def decode(file: File): CircuitEndpoints = {
    io.circe.parser.decode[CircuitEndpoints](Source.fromFile(file).getLines.mkString("")) match {
      case Right(s) => s
      case Left(ex) => throw ex
    }
  }

  /** Build a [[CircuitEndpoints]] by decoding a Json Source.
   *
   *  @param file
   *    json report file
   */
  def decode(file: Source): CircuitEndpoints = {
    io.circe.parser.decode[CircuitEndpoints](file.getLines.mkString("")) match {
      case Right(s) => s
      case Left(ex) => throw ex
    }
  }

  /** Build a [[CircuitEndpoints]] by decoding a Json report.
   *
   *  @param json
   *    report as String
   */
  def decode(json: String): CircuitEndpoints = {
    io.circe.parser.decode[CircuitEndpoints](json) match {
      case Right(s) => s
      case Left(ex) => throw ex
    }
  }
}

/** A representation of an endpoint, as a tuple of target and associated path.
 *
 *  @param ref
 *    endpoint target
 *  @param path
 *    associated logical path, from previous endpoints
 *  @todo
 *    TODO change path with paths, for multiple affectation resolution after estimation
 */
private[timing] case class Connection(ref: ReferenceTarget, path: Path) extends WithJson {

  /** Pretty print this [[Connection]]
   *
   *  @param indent
   *    indent level used for printing
   *  @return
   *    a String representing this [[Connection]]
   */
  override def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + "Target:",
        ref.prettyPrint(" " * (indent + 2)),
        " " * indent + "Paths:",
        path.prettyToString(indent + 2)
    ).mkString("\n")
  }
}

/** A representation of an [[qece.estimation.utils.Operators.Operator]] instance.
 *
 *  @param operator
 *    kind of operator
 *  @param params
 *    instanciation parameters (only bitWidth atm)
 */
private[timing] case class ParametrizedOperator(operator: Operators.Operator, params: Seq[Int]) extends WithJson {
  override def prettyToString(indent: Int = 0): String = " " * indent + s"${operator.name}[${params.mkString(", ")}]"

  lazy val bitwidth = params.head
}

private[timing] object ParametrizedOperator {
  def apply(operator: Operators.Operator, width: Int): ParametrizedOperator =
    new ParametrizedOperator(operator, Seq(width))
  def apply(operator: Operators.Operator, width: BigInt): ParametrizedOperator =
    new ParametrizedOperator(operator, Seq(width.toInt))
}

/** Empty [[ParametrizedOperator]]. */
private[timing] object EmptyParametrizedOperator extends ParametrizedOperator(Operators.NOP, Seq.empty)

/** A recursive representation of a logical path.
 *
 *  @param module
 *    module to display
 *  @param expr
 *    the expression associated to this path. WARNING: should be used only for display purposes
 *  @param primitive
 *    primitive used in the expression
 *  @param level
 *    level of operators of this path
 *  @param paths
 *    subpaths used in expr
 *  @param isMemoryAccess
 *    find endpoints with memory accesses
 *  @param isInput
 *    find input points
 *  @param toResolve
 *    is this path fully resolved yet ?
 */
private[timing] case class Path(
    module: String,
    expr: Expression,
    primitive: ParametrizedPrimitive,
    level: Int = 0,
    paths: Seq[Path] = Seq.empty,
    isMemoryAccess: Boolean = false,
    isInput: Boolean = false,
    toResolve: Boolean = false
) extends WithJson {

  /** Pretty print this [[Path]]
   *
   *  @param indent
   *    indent level used for printing
   *  @return
   *    a String representing this [[Path]]
   */
  override def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + s"${expr.serialize} [in $module] $toResolve",
        " " * (indent + 2) +
          s"${primitive.prettyToString(0)} ($level operation level(s)) - mem: $isMemoryAccess / IO: $isInput",
        paths.map(_.prettyToString(indent + 4)).mkString("")
    ).mkString("\n")
  }
}

/** Empty logical path, used to return errors without stoping the process. */
// object EmptyPath extends Path("", EmptyExpression, Left(EmptyParametrizedOperator))
private[timing] class EmptyPath(module: String, expr: Expression) extends Path(module, expr, Left(EmptyParametrizedOperator))

/** Companion object for [[Path]].
 *
 *  Used to build paths recursively from expressions found in FIRRTL.
 */
private[timing] object Path extends LazyLogging {

  private def getKind(s: SubField): Kind = {
    s.expr match {
      case ns: SubField => getKind(ns)
      case r: Reference => r.kind
      case _            => throw ParsingException(s"SubField expression must be either Reference or SubField")
    }
  }

// scalastyle:off method.length
  /** Build a [[Path]] for an expression
   *
   *  @param expr
   *    expression used as basis for the [[Path]] building
   *  @param mt
   *    module in which the expression was found, used as a basis for ReferenceTarget  building
   *  @param nodes
   *    a map of every ReferenceTarget already registered in the current module, to resolve references in the IR
   *  @return
   *    a [[Path]] recursively representing the logic levels of the expression
   */
  def apply(expr: Expression, mt: ModuleTarget, nodes: Map[ReferenceTarget, Path]): Path = {
    expr match {
      case d: DoPrim =>
        val op = d.op match {
          case PrimOps.Pad  => ParametrizedOperator(Operators.PAD, bitWidth(d.tpe))
          case PrimOps.Shl  => ParametrizedOperator(Operators.SSHIFT, bitWidth(d.tpe))
          case PrimOps.Shr  => ParametrizedOperator(Operators.SSHIFT, bitWidth(d.tpe))
          case PrimOps.Cat  => ParametrizedOperator(Operators.CAT, bitWidth(d.tpe))
          case PrimOps.Head => ParametrizedOperator(Operators.SELECT, bitWidth(d.tpe))
          case PrimOps.Tail => ParametrizedOperator(Operators.SELECT, bitWidth(d.tpe))
          case PrimOps.Bits => ParametrizedOperator(Operators.SELECT, bitWidth(d.tpe))
          case Dshlw        => ParametrizedOperator(Operators.DSHIFT, bitWidth(d.tpe))
          case PrimOps.Dshl => ParametrizedOperator(Operators.DSHIFT, bitWidth(d.tpe))
          case PrimOps.Dshr => ParametrizedOperator(Operators.DSHIFT, bitWidth(d.tpe))
          case Addw         => ParametrizedOperator(Operators.ADD, bitWidth(d.tpe))
          case Subw         => ParametrizedOperator(Operators.ADD, bitWidth(d.tpe))
          case PrimOps.Add  => ParametrizedOperator(Operators.ADD, bitWidth(d.tpe))
          case PrimOps.Sub  => ParametrizedOperator(Operators.ADD, bitWidth(d.tpe))
          case PrimOps.Mul  => ParametrizedOperator(Operators.MULT, bitWidth(d.tpe))
          case PrimOps.And  => ParametrizedOperator(Operators.BIN, bitWidth(d.tpe))
          case PrimOps.Or   => ParametrizedOperator(Operators.BIN, bitWidth(d.tpe))
          case PrimOps.Xor  => ParametrizedOperator(Operators.BIN, bitWidth(d.tpe))
          case PrimOps.Not  => ParametrizedOperator(Operators.BIN, bitWidth(d.tpe))
          case PrimOps.Eq   => ParametrizedOperator(Operators.COMP, bitWidth(d.args.head.tpe))
          case PrimOps.Neq  => ParametrizedOperator(Operators.COMP, bitWidth(d.args.head.tpe))
          case PrimOps.Leq  => ParametrizedOperator(Operators.COMP, bitWidth(d.args.head.tpe))
          case PrimOps.Geq  => ParametrizedOperator(Operators.COMP, bitWidth(d.args.head.tpe))
          case PrimOps.Lt   => ParametrizedOperator(Operators.COMP, bitWidth(d.args.head.tpe))
          case PrimOps.Gt   => ParametrizedOperator(Operators.COMP, bitWidth(d.args.head.tpe))
          case op =>
            logger.warn(s"[TODO] PrimOp -> ${op.getClass.getName}: " + expr.serialize); EmptyParametrizedOperator
        }
        val subpaths = d.args.map(Path(_, mt, nodes))
        Path(
            mt.module,
            d,
            Left(op),
            subpaths.maxBy(_.level).level + 1,
            subpaths,
            subpaths.map(_.isMemoryAccess).reduce(_ | _),
            subpaths.map(_.isInput).reduce(_ | _),
            subpaths.map(_.toResolve).reduce(_ | _)
        )

      case mux: Mux =>
        val subpaths = Seq(Path(mux.cond, mt, nodes), Path(mux.tval, mt, nodes), Path(mux.fval, mt, nodes))
        Path(
            mt.module,
            mux,
            Left(ParametrizedOperator(Operators.MUX, bitWidth(mux.tpe))),
            subpaths.maxBy(_.level).level + 1,
            subpaths,
            subpaths.map(_.isMemoryAccess).reduce(_ | _),
            subpaths.map(_.isInput).reduce(_ | _),
            subpaths.map(_.toResolve).reduce(_ | _)
        )

      case r: Reference =>
        r.kind match {
          // case where reference register was already solved, thus was already updated in nodes
          // Reference of RegKind can not be real paths
          case RegKind =>
            Path(mt.module, r, Left(ParametrizedOperator(Operators.REG, bitWidth(r.tpe))), 0, Seq.empty, false, false)
          case _ =>
            val myRef = ReferenceTargetUtils.getReferenceTarget(mt, r, List.empty).get
            nodes.contains(myRef) match {
              case true => nodes(myRef)
              case false =>
                logger.warn(s"[WARNING] did not found Reference ${myRef} in known references")
                new EmptyPath(mt.module, r)
            }
        }

      case s: SubField =>
        getKind(s) match {
          // case where reference register was already solved, thus was already updated in nodes
          // Reference of RegKind can not be real paths
          case RegKind =>
            Path(mt.module, s, Left(ParametrizedOperator(Operators.REG, bitWidth(s.tpe))), 0, Seq.empty, false, false)
          case _ =>
            val myRef = ReferenceTargetUtils.getReferenceTarget(mt, s.expr, s.name :: Nil).get
            nodes.contains(myRef) match {
              case true => nodes(myRef)
              case false =>
                logger.warn(s"[WARNING] did not found SubField ${myRef} in known references")
                new EmptyPath(mt.module, s)
            }
        }
      case u: UIntLiteral => Path(mt.module, u, Left(ParametrizedOperator(Operators.CONST, bitWidth(u.tpe))))
      case s: SIntLiteral => Path(mt.module, s, Left(ParametrizedOperator(Operators.CONST, bitWidth(s.tpe))))
      case _ =>
        logger.warn(s"[TODO] Expression -> ${expr.getClass.getName}: " + expr.serialize)
        new EmptyPath(mt.module, EmptyExpression)
    }
  }
// scalastyle:on method.length

  /** Build a [[Path]] from an expression only, assuming no ReferenceTarget has been found yet
   *
   *  @param expr
   *    expression to build the [[Path]] with
   *  @param m
   *    module as basis for the ReferenceTarget
   *  @return
   *    a [[Path]] associated to the expr
   */
  def apply(expr: Expression, m: ModuleTarget): Path = Path(expr, m, Map[ReferenceTarget, Path]())
}
