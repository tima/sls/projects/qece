/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.hinters.transforms

import firrtl.CircuitState

import qece.estimation.hinters.{WithHinters, HintersAnnotation}
import qece.estimation.transforms.MetricTransform

/** Retrieve all previously generated hinters, and display them */
class DisplayHinters extends MetricTransform with WithHinters {

  override val generatedKeys = Seq.empty

  val outputSuffix = ".hints"

  // TODO: do it at the very end of transforms

  /** Display the hinters.
   *  @param state
   *    circuit to apply the transform on
   *  @return
   *    the circuit, untouched as it is only a display transform
   *  @todo
   *    manage dependancy correctly
   */
  override def execute(state: CircuitState): CircuitState = {
    println(prettyHints)
    state.copy(annotations = Seq(HintersAnnotation(hints)) ++ state.annotations)
  }

  /** Deprecated */
  override def emit(state: CircuitState, writer: java.io.Writer): Unit = {}
}
