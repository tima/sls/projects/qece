/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.hinters

/** Singleton object used to store hinters through transforms
 *
 *  @todo
 *    TODO: Make it a private singleton object, as it should not be accessed from anything else than the WithHinters
 *    trait
 */
private[qece] object HinterStore {
  private[this]  val store = new scala.collection.mutable.ArrayBuffer[String]()

  /** Add an entry to the hinter store
   *
   *  It should not be accessed directly
   *
   *  @param msg
   *    string to add
   */
  def addEntry(msg: String) = store += msg

  /** Retrieve all entries from the store
   *
   *  It should no be accessed directly
   */
  def getEntries = store
}

/** Trait for adding hinters during the transforms */
private[qece] trait WithHinters {

  /** add an hinter, with the implementing class as a reference for tracing purposer
   *
   *  @param msg
   *    message to add as hinter
   */
  def addHinter(msg: String) = HinterStore.addEntry(s"${this.getClass.getSimpleName}: $msg")

  /** retrieve all hinters from store */
  def hints: Seq[String] = HinterStore.getEntries

  /** Pretty print the content of the hinter store as a String
   *
   *  @param indent
   *    indentation level used by this printer
   */
  def prettyHints(indent: Int = 0): String = {
    def id(i: Int) = " " * i
    HinterStore.getEntries match {
      case Seq() => "Found no hinters"
      case x =>
        Seq(
            id(indent) + "Found hinters:",
            x.map(s => id(indent + 2) + s"*\t ${s.replace("\n", s"\n${id(indent + 2)}\t")}").mkString("\n")
        ).mkString("\n")
    }
  }

  /** Utility for printing */
  def prettyHints: String = prettyHints()
}
