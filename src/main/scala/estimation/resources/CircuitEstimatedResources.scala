/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources

import qece.backend.FPGA
import qece.estimation.MetricMap

import io.circe.generic.auto._
import scala.io.Source
import java.io.File

/** Json compatible circuit description for primitive reporting
 *  @param circuit
 *    name of the circuit - it is the name of the toplevel module
 *  @param module
 *    toplevel module
 */
private[estimation] case class CircuitEstimatedResources(
  circuit: String,
  module: ModuleEstimatedResources
) extends AutoJson {

  /** Pretty print this Circuit */
  override def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + s"Resource report for circuit $circuit:",
        " " * (indent + 2) + s"Toplevel ${module.name}:",
        module.prettyToString(indent + 2)
    ).mkString("\n")
  }

  def summarize(indent: Int = 0): String = {
    Seq(
        " " * (indent + 2) + s"lut: ${module.lut}",
        " " * (indent + 2) + s"ff: ${module.ff}",
        " " * (indent + 2) + s"dsp: ${module.dsp}",
        " " * (indent + 2) + s"memory: ${module.memory}"
    ).mkString("\n")
  }

  def asMetricMap(board: FPGA) =
    MetricMap(
        "lut"     -> module.lut,
        "ff"      -> module.ff,
        "dsp"     -> module.dsp,
        "memory"  -> module.memory,
        "%lut"    -> module.lut.toDouble / board.maxResources.lut,
        "%ff"     -> module.ff.toDouble / board.maxResources.ff,
        "%dsp"    -> module.dsp.toDouble / board.maxResources.dsp,
        "%memory" -> module.memory.toDouble / board.maxResources.memory
    )
}

/** Json object counting the instances of a given module for primitive reporting
 *  @param count
 *    number of instances for the given module
 *  @param module
 *    module being counter
 */
case class ModuleEstimatedResourcesWithCount(count: Int, module: ModuleEstimatedResources) extends AutoJson {

  /** Pretty print this CountingModule
   *
   *  @param indent
   *    indentation level
   */
  override def prettyToString(indent: Int = 0): String = {
    Seq(" " * indent + s"$count module(s) ${module.name}:", module.prettyToString(indent + 2)).mkString("\n")
  }
}

/** Json compatible module description for primitive reporting
 *  @param name
 *    name of the module
 *  @param modules
 *    Seq of [[ModuleEstimatedResourcesWithCount]], counting instances of submodules in this module
 *  @param resources
 *    summary of resources in this module, as seen from a hiearchical point of view
 */
case class ModuleEstimatedResources(
    name: String,
    modules: Seq[ModuleEstimatedResourcesWithCount],
    lut: BigInt,
    ff: BigInt,
    dsp: BigInt,
    memory: BigInt
) extends AutoJson {

  /** Pretty print this Module
   *
   *  @param indent
   *    indentation level
   */
  override def prettyToString(indent: Int = 0): String = {
    Seq(
        modules.map(_.prettyToString(indent + 2)).mkString("\n"),
        " " * indent + s"lut: $lut",
        " " * indent + s"ff: $ff",
        " " * indent + s"dsp: $dsp",
        " " * indent + s"memory: $memory"
    ).mkString("\n")
  }
}

/** Empty [[ModuleEstimatedResourcesWithCount]], when module got no submodule */
private[estimation] object NoModuleEstimatedResourcesWithCount
  extends ModuleEstimatedResourcesWithCount(0, EmptyModuleEstimatedResources)

/** Empty [[ModuleEstimatedResources]] */
private[estimation] object EmptyModuleEstimatedResources extends ModuleEstimatedResources("", Seq(), 0, 0, 0, 0)

private[estimation] object CircuitEstimatedResources {

  /** Build a [[CircuitEstimatedResources]] by decoding a Json report file
   *  @param file
   *    json report file
   */
  def decode(file: File): CircuitEstimatedResources = {
    io.circe.parser.decode[CircuitEstimatedResources](Source.fromFile(file).getLines.mkString("")) match {
      case Right(s) => s
      case Left(ex) => throw ex
    }
  }

  /** Build a [[CircuitEstimatedResources]] by decoding a Json report
   *  @param json
   *    report as String
   */
  def decode(json: String): CircuitEstimatedResources = {
    io.circe.parser.decode[CircuitEstimatedResources](json) match {
      case Right(s) => s
      case Left(ex) => throw ex
    }
  }
}
