/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources

import qece.CustomLogging

import qece.estimation.utils.Operators
import qece.estimation.macros.ParametrizedMacroBlock
import qece.estimation.resources.exceptions._

import firrtl._
import firrtl.ir._

import scala.collection.mutable.HashMap

/** Resource storage using [[Store]] instances for the different resources in the module
 *  @param name
 *    name of the Module
 */
private[resources] class ModuleResourceStore(
    val name: String,
    moduleStore: Map[String, ModuleResourceStore] = Map.empty,
    macroList: Map[String, ParametrizedMacroBlock] = Map.empty
) extends CustomLogging {

  // mapping (operator -> store)
  private val stores: Map[Operators.Operator, BitOpStore] = Operators.withCost.map(x => x -> new BitOpStore()).toMap
  private val macroStore: HashMap[ParametrizedMacroBlock, (Int, Int)] = HashMap(
      macroList.values.map(v => (v -> (0, 0))).toSeq: _*
  )

  // Particular store for memories
  private val memStore = new MemoryStore()

  // Couting submodule instances
  private val submoduleStore     = new HashMap[String, Int]()
  private var submodulesIncluded = false

  /** Go through submodules to increment total resource counters */
  def updateSubmodules(): Unit = {
    if (!submodulesIncluded) {
      submodulesIncluded = true
      submoduleStore.foreach {
        case (module, count) => {
          moduleStore.contains(module) match {
            case true =>
              for ((name, st) <- stores) {
                st += (moduleStore(module).stores(name)) * count
              }
              for ((kind, st) <- moduleStore(module).macroStore) {
                macroStore(kind) = ((macroStore(kind)._1 + st._1 * count, macroStore(kind)._2))
              }
              memStore += (moduleStore(module).memStore) * count
            case false => logger.warn(s"[WARNING] Unable to retrieve usage for module $module")
          }
        }
      }
    }
  }

  /** Add a memory instance to this store
   *
   *  @param m
   *    DefMemory to be added
   */
  def addMemory(m: DefMemory): Unit = {
    logger.debug(s"Adding memory:\n${m.serialize}")
    memStore.addLocal(MemoryType(m.dataType, m.depth, m.writeLatency, m.readLatency))
  }

  /** Add a module instance to this store
   *  @param i
   *    DefInstance to be added
   */
  def addInstance(i: DefInstance): Unit = {
    logger.debug(s"Adding instance:\n${i.serialize}")
    macroList contains i.module match {
      // add a macro
      case true =>
        val ref = macroList(i.module)
        macroStore(ref) = ((macroStore(ref)._1 + 1, macroStore(ref)._2 + 1))
      // add a simple submodule
      case false =>
        submoduleStore contains i.module match {
          case true  => submoduleStore(i.module) += 1
          case false => submoduleStore += ((i.module, 1))
        }
    }
  }

  /** Add a register to this store
   *  @param r
   *    DefRegister to be added
   */
  def addRegister(r: DefRegister): Unit = {
    logger.debug(s"Adding Register:\n${r.serialize}")
    stores(Operators.REG).addLocal(bitWidth(r.tpe))
  }

  /** Add an expression to this store, by matching on its underlying type
   *  @param e
   *    Expression to be added
   */
  def addExpression(e: Expression): Unit = {
    logger.debug(s"Adding expression:\n${e.serialize}")
    e match {
      case d: DoPrim =>
        val (op, tpe) = Operators.fromDoPrim(d)
        if (Operators.withCost contains op) stores(op).addLocal(bitWidth(tpe))
        d.args.foreach(addExpression(_))
      case m: Mux => stores(Operators.MUX).addLocal(bitWidth(m.tpe))
      // useless ; to remove once TODO is removed
      case _: Reference   =>
      case _: SubField    =>
      case _: UIntLiteral =>
      case _: SIntLiteral =>
      case _              => logger.warn(s"[TODO] Expression -> ${e.getClass.getName}: " + e.serialize)
    }
  }

  /** Return a [[qece.estimation.resources.ModulePrimitives]] representation of this module, recursively including every
   *  submodule, and reporting resources used.
   */
  def getModulePrimitives: ModulePrimitives = {
    val submoduleVec = submoduleStore
      .map {
        case (mod, count) =>
          ModulePrimitivesWithCount(
              count,
              moduleStore.get(mod) match {
                case Some(m) => m.getModulePrimitives
                case None    => throw ModuleNotFoundException(s"No submodule was found with name $mod")
              }
          )
        case _ => NoModulePrimitivesWithCount
      }
      .filter(_.module != EmptyModulePrimitives)
      .filter(_.count > 0)
      .toSeq
    val macroVec = macroStore.map {
      case (kind, (nt, nl)) => MacroValue(kind, nt, nl)
    }.toSeq
    val resourceVec = stores
      .map {
        case (name, store) =>
          if (!store.empty) { store.getPrimitives(name) }
          else { EmptyPrimitives }
      }
      .filter(x => !x.values.isEmpty)
      .toSeq ++ Seq(memStore.getPrimitives(Operators.MEMORY)).filter(x => !x.values.isEmpty)
    ModulePrimitives(name, submoduleVec, macroVec, resourceVec)
  }
}
