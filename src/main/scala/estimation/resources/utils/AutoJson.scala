/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources

import qece.estimation.macros.{ParametrizedMacroBlock, MacroBlock}

import cats.syntax.functor._
import io.circe._, io.circe.generic.auto._, io.circe.syntax._

/** Trait used to serialize and unserialize reports with automatic derivation */
private[resources] trait AutoJson {

  // Explicit encoder for Macro, as they are serialized in a custom way
  implicit val encodeParametrizedMacro: Encoder[ParametrizedMacroBlock] = new Encoder[ParametrizedMacroBlock] {
    final def apply(a: ParametrizedMacroBlock): Json =
      Json.obj(
          ("name", Json.fromString(a.name)),
          ("params", a.params.asJson)
      )
  }

// -------- ENCODERS -----------------
  implicit val encoder: Encoder[AutoJson] = Encoder.instance {
    case circuit @ CircuitPrimitives(_, _)                  => circuit.asJson
    case countMod @ ModulePrimitivesWithCount(_, _)         => countMod.asJson
    case mod @ ModulePrimitives(_, _, _, _)                 => mod.asJson
    case res @ Primitives(_, _)                             => res.asJson
    case value @ PrimitiveValue(_, _, _)                    => value.asJson
    case mv @ MacroValue(_, _, _)                           => mv.asJson
    case circuit @ CircuitEstimatedResources(_, _)          => circuit.asJson
    case countMod @ ModuleEstimatedResourcesWithCount(_, _) => countMod.asJson
    case mod @ ModuleEstimatedResources(_, _, _, _, _, _)   => mod.asJson
  }

// -------- DECODERS -----------------

  // Explicit decoder for Macro
  implicit val decodeParametrizedMacro: Decoder[ParametrizedMacroBlock] = new Decoder[ParametrizedMacroBlock] {
    final def apply(c: HCursor): Decoder.Result[ParametrizedMacroBlock] =
      for {
        name   <- c.downField("name").as[String]
        params <- c.downField("params").as[Seq[Int]]
      } yield {
        ParametrizedMacroBlock(MacroBlock(name), params)
      }
  }

  implicit val decoder: Decoder[AutoJson] =
    List[Decoder[AutoJson]](
        Decoder[CircuitPrimitives].widen,
        Decoder[ModulePrimitivesWithCount].widen,
        Decoder[ModulePrimitives].widen,
        Decoder[Primitives].widen,
        Decoder[PrimitiveValue].widen,
        Decoder[MacroValue].widen,
        Decoder[CircuitEstimatedResources].widen,
        Decoder[ModuleEstimatedResourcesWithCount].widen,
        Decoder[ModuleEstimatedResources].widen
    ).reduceLeft(_ or _)

  private val customPrinter = Printer.spaces2.copy(dropNullValues = true)

  /** Emit a Json representation of this instance
   *  @return
   *    a Json string representing this instance
   */
  def emitJson: String = this.asJson.printWith(customPrinter)

  /** Pretty print the underlying construct.
   *
   *  @param indent
   *    indentation level to be used
   *  @return
   *    a String representation of this
   */
  def prettyToString(indent: Int = 0): String = " " * indent + this.toString
}
