/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources

import qece.estimation.utils.Operators

import firrtl.ir._

import scala.collection.mutable.HashMap

/** A [[Store]] instance for operators where only bitwidth matters */
private[resources] class BitOpStore() extends Store[BigInt]

/** Memory type used as index in [[MemoryStore]]
 *  @param tpe
 *    Chisel underlying type of memory elements
 *  @param depth
 *    depth of the memory banks
 *  @param readLatency
 *    read latency of the memory banks
 *  @param writeLatency
 *    write latency of the memory banks
 */
private[resources] case class MemoryType(tpe: Type, depth: BigInt, readLatency: BigInt, writeLatency: BigInt)

/** A [[Store]] instance for memory kind resources */
private[resources] class MemoryStore() extends Store[MemoryType]

/** A store asbtract class that will be used throughout a module hierarchy to count every instance of the given resource
 *  @tparam T
 *    the type of object that will be used as index in the Store
 */
abstract class Store[T <: Object] {
  val store = new HashMap[T, (BigInt, BigInt)]() // (total, local)

  /** Append a local entity to the store, thus to the local and total entity counters
   *  @param index
   *    the index where to insert the entity
   */
  def addLocal(index: T): Unit = {
    if (store.contains(index)) {
      store(index) = (store(index)._1 + 1, store(index)._2 + 1)
    } else {
      store += ((index, (1, 1)))
    }
  }

  /** Append all entities found in submodule in the store, thus only to the total entity counter
   *  @param index
   *    the index where to insert the entity
   */
  def addSub(index: T, value: BigInt): Unit = {
    if (store.contains(index)) {
      store(index) = (store(index)._1 + value, store(index)._2)
    } else {
      store += ((index, (value, 0)))
    }
  }

  /** Append all entities found in another store
   *  @param that
   *    the other store being appended
   */
  def +=(that: Store[T]): Unit = {
    for ((i, (tot, _)) <- that.store) {
      addSub(i, tot)
    }
  }

  /** Return a copy of this store with both local and total counters multiplied by the given factor
   *  @param count
   *    the replication factor
   *  @return
   *    a modified copy of the store
   */
  def *(count: Int): Store[T] = {
    val n: Store[T] = this.getClass.getDeclaredConstructor().newInstance()
    for ((i, (tot, loc)) <- this.store) {
      n.store += ((i, (tot * count, loc * count)))
    }
    n
  }

  /** Check wether the store is empty */
  def empty: Boolean = store.isEmpty

  /** Create a [[qece.estimation.resources.Primitives]] to serialize this Store
   *  @param kind
   *    Type of element being stored here
   *  @return
   *    a [[qece.estimation.resources.Primitives]] representation of this store
   *
   *  @todo
   *    TODO replace toString by a real serialization
   */
  def getPrimitives(kind: Operators.Operator): Primitives = {
    Primitives(
        kind,
        store.map {
          case (b, (nbT, nbL)) => PrimitiveValue(b.toString, nbT, nbL)
        }.toSeq
    )
  }

  override def toString: String = store.map { case (k, v) => s"$k -> $v" }.mkString("[", ", ", "]")
}
