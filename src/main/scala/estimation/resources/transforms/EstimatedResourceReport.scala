/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources.transforms

import qece.QeceContext

import qece.characterization.{CharacterizedBoard, UnvalidEstimationException}

import qece.estimation.utils.Operators
import qece.estimation.resources.{
  ModuleEstimatedResourcesWithCount,
  ModuleEstimatedResources,
  CircuitEstimatedResources,
  ModulePrimitives,
  EmittedResourceReportAnnotation,
  EmittedResourceReport
}

import qece.estimation.transforms.{MetricTransform, SynchroLogging}

import firrtl.{CircuitState, Utils}
import firrtl.options.Dependency

import scala.math._

/** Attempt to report primitive usage estimation within the current circuit, with a hierarchical approach */
//TODO: generate metrics (as of the resource list)
private[qece] class EstimatedResourceReport extends MetricTransform {

  // do it the closest from primitive emission
  override val prerequisites = List(Dependency[PrimitiveReport])

  override val duration = QeceContext.timeout.estimateResource

  val outputSuffix: String = ".resource.json"

  val generatedKeys = Seq(
      "lut",
      "ff",
      "dsp",
      "memory",
      "%lut",
      "%ff",
      "%dsp",
      "%memory"
  )

// scalastyle:off method.length
  /** Recursively run estimators on every resources of every modules
   *
   *  @param estimators
   *    estimators for the target board
   *  @param module
   *    module currently being reported
   *  @param count
   *    number of module instances being reported
   */
  private def reportModule(
      estimators: CharacterizedBoard,
      module: ModulePrimitives,
      count: Int = 1
  ): ModuleEstimatedResourcesWithCount = {
    def merge(m1: Map[String, BigInt], m2: Map[String, BigInt]): Map[String, BigInt] = {
      (m1.keySet ++ m2.keySet).map(key => key -> (m1.getOrElse(key, BigInt(0)) + m2.getOrElse(key, BigInt(0)))).toMap
    }
    // little trick for when memory blocks are not used
    def parseMemoryType(str: String): (Int, Int) =
      (str.split("Width\\(")(1).split("\\)")(0).toInt, str.split(",")(1).toInt)

    val lateModules = module.modules.map(mod => reportModule(estimators, mod.module, mod.count))
    val operatorResources = module.primitives
      .map { primitive =>
        primitive.kind match {
          case Operators.MEMORY =>
            // throw UnexpectedPrimitiveException(
            //    s"Found MEMORY operator, which should be replaced by MemoryBlock macro"
            //  )
            // can estimate memory without MemoryBlock replacement, but will not be accurate
            Seq(
                "memory" ->
                  primitive.values
                    .map { value =>
                      val (width, depth) = parseMemoryType(value.key)
                      (width * depth * value.total) / 1024
                    }
                    .reduce(_ + _)
            ).toMap.asInstanceOf[Map[String, BigInt]]
          case Operators.REG =>
            Seq(
                "ff" ->
                  primitive.values
                    .map(value => value.key.toInt * value.total)
                    .reduce(_ + _)
            ).toMap.asInstanceOf[Map[String, BigInt]]
          case x =>
            val estimator = estimators.getCharacterizedOperator(x)
            estimator.estimators
              .filter(generatedKeys contains _.resource)
              .map { est =>
                try {
                  est.resource -> BigInt(
                      ceil(
                          (primitive.values
                            .map { value => est.evaluate(value.key.toInt) * value.total.toDouble })
                            .reduce(_ + _)
                      ).toInt
                  )
                } catch {
                  case e: UnvalidEstimationException =>
                    throw UnvalidEstimationException(
                        s"Failed to estimate resource ${est.resource} on operator ${x.caption}:\n\t" +
                          e.getMessage()
                    )
                  case e: Throwable => throw e
                }
              }
              .toMap
              .asInstanceOf[Map[String, BigInt]]
        }
      }
      .reduceOption(merge)
      .getOrElse(Map.empty)
    val macroResources = module.macros
      .map { pm =>
        estimators
          .getCharacterizedMacro(pm.mac.defMacro)
          .estimators
          .filter(generatedKeys contains _.resource)
          .map { est =>
            try {
              est.resource ->
                BigInt(ceil(pm.total.doubleValue * pm.mac.factor * est.evaluate(pm.mac.paramString, pm.mac.args)).toInt)
            } catch {
              case e: UnvalidEstimationException =>
                throw UnvalidEstimationException(
                    s"Failed to estimate resource ${est.resource} on macro ${pm.mac.defMacro}:\n" +
                      e.getMessage()
                )
              case e: Throwable => throw e
            }
          }
          .toMap
          .asInstanceOf[Map[String, BigInt]]
      }
      .reduceOption(merge)
      .getOrElse(Map.empty)
    val resources = merge(operatorResources, macroResources)
    ModuleEstimatedResourcesWithCount(
        count,
        ModuleEstimatedResources(
            module.name,
            lateModules,
            //TODO: use MetricMap
            resources.getOrElse("lut", 0),
            resources.getOrElse("ff", 0),
            resources.getOrElse("dsp", 0),
            resources.getOrElse("memory", 0)
        )
    )
  }
// scalastyle:on method.length

  /** Execute the technological mapping transform on the circuit.
   *
   *  @param state
   *    circuit to apply the transform on
   *  @return
   *    the circuit, untouched as it is only a reporting transform
   */
  override def execute(state: CircuitState): CircuitState = {

    // Attempt to retrieve circuit primitive from annotations
    val jsCircuit = getPrimitives(state)

    // Attempt to retrieve estimators from annotations and resources
    val estimators = getEstimators(state)

    SynchroLogging.addTask("resource estimation")
    logStart(state)
    // Attempt to estimate resources hierarchicaly in modules
    val (time, circuit) =
      exec(Utils.time(CircuitEstimatedResources(jsCircuit.circuit, reportModule(estimators, jsCircuit.module).module)))

    logEnd(state)
    SynchroLogging.removeTask("resource estimation")

    logger.trace(Seq(s"[$getTime] ${this.getClass.getSimpleName}:", circuit.prettyToString()).mkString("\n"))

    // add to annotations for further transforms, and emit to file
    state.copy(annotations =
      Seq(
          EmittedResourceReportAnnotation(
              circuit,
              EmittedResourceReport(state.circuit.main, circuit.emitJson, outputSuffix),
              time
          ),
          generateMetricAnnotation(circuit.asMetricMap(getBoard(state).get))
      ) ++ state.annotations
    )
  }

  // Deprecated
  override def emit(state: CircuitState, writer: java.io.Writer): Unit = {}
}
