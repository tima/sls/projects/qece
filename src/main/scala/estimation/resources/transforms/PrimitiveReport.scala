/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources.transforms

import qece.QeceContext

import qece.estimation.resources.{
  CircuitPrimitives,
  ModuleResourceStore,
  EmittedPrimitiveReportAnnotation,
  EmittedPrimitiveReport
}

import qece.estimation.resources.exceptions._

import qece.estimation.macros.{ReplaceInfo, ParametrizedMacroBlock}
import qece.estimation.macros.transforms.ReplaceMacro

import qece.estimation.transforms.{MetricTransform, SynchroLogging}

import firrtl.{CircuitState, Utils}
import firrtl.options.Dependency
import firrtl.ir._

import scala.collection.mutable.LinkedHashMap

/** Attempt to report primitive usage estimation within the current circuit, with a hierarchical approach */
//TODO: use MetricTransform to generate primitive metric reporting (#op and macro ?) ?
private[qece] class PrimitiveReport extends MetricTransform {

  // do it right after macro replacement
  override val prerequisites = List(Dependency[ReplaceMacro])

  override val generatedKeys = Seq.empty

  override val duration = QeceContext.timeout.primitiveReport

  val outputSuffix: String = ".primitive.json"

  val moduleStore = new LinkedHashMap[String, ModuleResourceStore]()
  val macroStore  = new LinkedHashMap[String, ParametrizedMacroBlock]()

// scalastyle:off method.length
  /** run resource usage reporting for the current circuit
   *  @param circuit
   *    Circuit to be reported
   */
  private def run(circuit: Circuit, replaceInfos: Seq[ReplaceInfo]): Unit = {

    // Run reporting on every DefModule of the circuit
    def runModule(m: Module): Unit = {
      logger.debug(s"Running on module:\n${m.serialize}")
      val store = new ModuleResourceStore(m.name, moduleStore.toMap, macroStore.toMap)

      // Run report on every Statement of the current module
      def runStatements(stmt: Statement): Unit = {
        logger.debug(s"Running on statement:\n${stmt.serialize}")
        stmt match {
          case r: DefRegister => store.addRegister(r)
          case n: DefNode     => store.addExpression(n.value)
          case m: DefMemory   => store.addMemory(m)
          case i: DefInstance => store.addInstance(i)
          // nothing to do (should be removed once TODO print becomes useless)
          case w: DefWire => logger.debug(s"Wire:\n${w.serialize}")
          case b: Block =>
            logger.debug(s"Block:\n${b.serialize}") //stmt.foreachStmt does the trick , no need to iterate on stmts
          case c: Connect =>
            logger.debug(s"Connect:\n${c.serialize}")
            store.addExpression(c.loc)
            store.addExpression(c.expr)
          case EmptyStmt => logger.debug("Empty statement")
          case _         => logger.warn(s"[TODO] Statement -> ${stmt.getClass.getName}: " + stmt.serialize)
        }
        stmt.foreachStmt(runStatements)
      }
      m.foreachStmt(runStatements)
      store.updateSubmodules
      moduleStore += ((m.name, store))
    }

    def runExtModule(em: ExtModule): Unit = {
      replaceInfos.filter(_.defModule == em) match {
        case Seq() => throw new UnsupportedOperationException(s"Black boxes and other ExtModule are not supported yet")
        case s: Seq[ReplaceInfo] =>
          macroStore ++= s.map(e => ((e.defModule.name, ParametrizedMacroBlock(e.macroKind, e.params))))
      }
    }

    def runDefModule(dm: DefModule): Unit = {
      dm match {
        case m: Module    => runModule(m)
        case e: ExtModule => runExtModule(e)
      }
    }

    circuit.modules.foreach(runDefModule)
  }
// scalastyle:on method.length

  /** Execute the analysis transform on the circuit, and add an annotation to pass the report to further transforms.
   *  Also emits the report to the generation dir
   *  @param state
   *    circuit to apply the transform on
   *  @return
   *    the circuit, untouched as it is only a reporting transform
   */
  override def execute(state: CircuitState): CircuitState = {

    SynchroLogging.addTask("primitives")
    mydebug(s"[ESTIMATION] Started primitive estimation on ${getMetrics(state.annotations).toMap}")
    val (time, circuit) = exec(Utils.time({
      // run reporting on circuit
      run(getUpdatedCircuit(state), getReplaceInfos(state))

      // build a serializable representation from the built report
      CircuitPrimitives(
          state.circuit.main,
          moduleStore.get(state.circuit.main) match {
            case Some(m) => m.getModulePrimitives
            case _       => throw ModuleNotFoundException(s"Can't find top module with name ${state.circuit.main}")
          }
      )
    }))

    logger.trace(Seq(s"[$getTime] ${this.getClass.getSimpleName}:", circuit.prettyToString()).mkString("\n"))
    mydebug(s"[ESTIMATION] Completed primitive estimation on ${getMetrics(state.annotations).toMap}")
    SynchroLogging.removeTask("primitives")

    // add to annotations for further transforms, and emit to file
    state.copy(annotations =
      Seq(
          EmittedPrimitiveReportAnnotation(
              circuit,
              EmittedPrimitiveReport(state.circuit.main, circuit.emitJson, outputSuffix),
              time
          )
      ) ++ state.annotations
    )
  }

  // Deprecated
  override def emit(state: CircuitState, writer: java.io.Writer): Unit = {}
}
