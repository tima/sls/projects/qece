/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources.transforms

import qece.estimation.transforms.MetricTransform
import qece.estimation.MetricMap

import firrtl.{CircuitState, bitWidth}
import firrtl.ir._
import firrtl.options.Dependency

/** Report registers used in TopLevel only
 *
 *  Used to retrieve registers used in [[qece.characterization.components.EstimationWrapper]]
 */
private[qece] class TopRegisterReport extends MetricTransform {

  // do it right after macro replacement
  override val prerequisites = List(Dependency[firrtl.VerilogEmitter])

  val outputSuffix: String = ".wrapper.json"

  val generatedKeys = Seq("topReg")

  def run(circuit: Circuit): BigInt = {
    var registers = BigInt(0)
    def runMain(m: DefModule): Unit = {
      if (circuit.main == m.name) { m.foreachStmt(runStmt) }
    }
    def runStmt(stmt: Statement): Unit = {
      stmt match {
        case r: DefRegister => registers += bitWidth(r.tpe)
        case _              =>
      }
      stmt.foreachStmt(runStmt)
    }
    circuit.foreachModule(runMain)
    registers
  }

  /** Execute the analysis transform on the circuit, and add an annotation to pass the report to further transforms.
   *  Also emits the report to the generation dir
   *  @param state
   *    circuit to apply the transform on
   *  @return
   *    the circuit, untouched as it is only a reporting transform
   */
  override def execute(state: CircuitState): CircuitState = {

    val reg = exec(run(state.circuit))

    // add to annotations for further transforms, and emit to file
    state.copy(annotations = Seq(generateMetricAnnotation(MetricMap(generatedKeys.head -> reg))) ++ state.annotations)
  }

  // Deprecated
  override def emit(state: CircuitState, writer: java.io.Writer): Unit = {}
}
