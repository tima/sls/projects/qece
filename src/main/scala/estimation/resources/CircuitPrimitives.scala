/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources

import qece.estimation.utils.Operators

import qece.estimation.macros.{ParametrizedMacroBlock, EmptyParametrizedMacroBlock, MacroBlock}
import qece.estimation.resources.exceptions._

import io.circe.generic.auto._
import scala.io.Source
import java.io.File

/** Json compatible circuit description for primitive reporting
 *  @param circuit
 *    name of the circuit - it is the name of the toplevel module
 *  @param module
 *    toplevel module
 */
private[estimation] case class CircuitPrimitives(circuit: String, module: ModulePrimitives) extends AutoJson {

  /** Pretty print this Circuit */
  override def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + s"Primitive report for circuit $circuit:",
        " " * indent + s"Toplevel ${module.name}:",
        module.prettyToString(indent + 2)
    ).mkString("\n")
  }
}

/** Json compatible object counting the instances of a given module for primitive reporting
 *  @param count
 *    number of instances for the given module
 *  @param module
 *    module being counted
 */
private[estimation] case class ModulePrimitivesWithCount(count: Int, module: ModulePrimitives) extends AutoJson {

  /** Pretty print this CountingModule
   *
   *  @param indent
   *    indentation level
   */
  override def prettyToString(indent: Int = 0): String = {
    Seq(" " * indent + s"$count module(s) ${module.name}:", module.prettyToString(indent + 2)).mkString("\n")
  }
}

/** Json compatible module description for primitive reporting
 *  @param name
 *    name of the module
 *  @param modules
 *    [[ModulePrimitivesWithCount]] (s), counting instances of submodules in this module
 *  @param macros
 *    [[MacroValue]] (s), counting instances of macros in this module
 *  @param primitives
 *    summary of primitives in this module, as seen from a hiearchical point of view
 */
private[estimation] case class ModulePrimitives(
    name: String,
    modules: Seq[ModulePrimitivesWithCount],
    macros: Seq[MacroValue],
    primitives: Seq[Primitives]
) extends AutoJson {

  /** Get a [[Primitives]] from its kind It throws an exception if multiple primitives were found with the same kind
   *  (construct exception)
   *  @param kind
   *    kind of the resource
   */
  def getPrimitives(kind: Operators.Operator): Primitives = {
    primitives.filter(_.kind == kind) match {
      case Seq(s) => s
      case Seq()  => EmptyPrimitives
      case _      => throw ResourceDuplicaFoundException(s"Found to many primitives with kind $kind in module $name")
    }
  }

  /** Get a [[MacroValue]] from the inner macro kind It throws an exception if multiple primitives were found with the
   *  same kind (construct exception)
   *  @param macroKind
   *    kind of the macro
   */
  def getMacro(macroKind: MacroBlock[_]): Seq[MacroValue] = {
    macros.filter(_.mac.defMacro == macroKind)
  }

  /** Retrieve a [[ModulePrimitives]] instance from its name
   *
   *  @param module
   *    name of the module being searched
   */
  def getModulePrimitives(module: String): ModulePrimitives = {
    modules.filter(_.module.name == module) match {
      case Seq()  => throw ModuleNotFoundException(s"Did not found module $module in module $name")
      case Seq(s) => s.module
      case _      => throw ResourceDuplicaFoundException(s"Found too many modules with name $module in module $name")
    }
  }

  /** Pretty print this Module
   *
   *  @param indent
   *    indentation level
   */
  override def prettyToString(indent: Int = 0): String = {
    val newIndent = indent + 2
    Seq(
        modules.map(_.prettyToString(newIndent)).mkString("\n"),
        macros.map(_.prettyToString(newIndent)).mkString("\n"),
        primitives.map(_.prettyToString(newIndent)).mkString("\n")
    ).mkString("\n")
  }
}

/** Json compatible object counting the instances of a given macro for primitive reporting
 *  @param mac
 *    macro being reported
 *  @param total
 *    total amount of this macro
 *  @param local
 *    local amount of this macro
 */
private[estimation] case class MacroValue(
  mac: ParametrizedMacroBlock,
  total: BigInt,
  local: BigInt
) extends AutoJson {
  override def prettyToString(indent: Int = 0): String = " " * indent + s"Macro: $mac $total ($local)"
}

/** Json compatible resource description for primitive reporting
 *  @param kind
 *    kind of operator
 *  @param values
 *    record of this kind of operators, identified by a particular key (e.g. bitwidth of operators)
 */
case class Primitives(kind: Operators.Operator, values: Seq[PrimitiveValue]) extends AutoJson {

  /** Pretty print this Module
   *
   *  @param indent
   *    indentation level
   */
  override def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + s"kind: $kind",
        values.map(value => " " * (indent + 2) + s"${value.key}: ${value.total} (${value.local})").mkString("\n")
    ).mkString("\n")
  }
}

/** Json compatible value description for primitive reporting
 *  @param key
 *    key identifying the operator value in a Seq of values
 *  @param total
 *    global count for this kind of resource
 *  @param local
 *    local count for this kind of resource
 */
private[estimation] case class PrimitiveValue(key: String, total: BigInt, local: BigInt) extends AutoJson

/** Empty [[ModulePrimitivesWithCount]], when module got no submodule */
private[estimation] object NoModulePrimitivesWithCount extends ModulePrimitivesWithCount(0, EmptyModulePrimitives)

/** Empty [[ModulePrimitives]] */
private[estimation] object EmptyModulePrimitives extends ModulePrimitives("", Seq.empty, Seq.empty, Seq.empty)

/** Empty [[Primitives]] */
private[estimation] object EmptyPrimitives extends Primitives(Operators.NOP, Seq.empty)

private[estimation] object EmptyMacroValue extends MacroValue(EmptyParametrizedMacroBlock, 0, 0)

private[estimation] object EmptyCircuitPrimitives extends CircuitPrimitives("", EmptyModulePrimitives)

/** Empty [[PrimitiveValue]] */
private[estimation] object EmptyPrimitiveValue extends PrimitiveValue("", 0, 0)

private[estimation] object CircuitPrimitives extends AutoJson {

  /** Build a [[CircuitPrimitives]] by decoding a Json report file
   *  @param file
   *    json report file
   */
  def decode(file: File): CircuitPrimitives = {
    io.circe.parser.decode[CircuitPrimitives](Source.fromFile(file).getLines.mkString("")) match {
      case Right(s) => s
      case Left(ex) => throw ex
    }
  }

  /** Build a [[CircuitPrimitives]] by decoding a Json report
   *  @param json
   *    report as String
   */
  def decode(json: String): CircuitPrimitives = {
    io.circe.parser.decode[CircuitPrimitives](json) match {
      case Right(s) => s
      case Left(ex) => throw ex
    }
  }
}
