/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.qor

import qece.estimation.MetricMap
import qece.estimation.transforms.{TimedSimulationAnnotation, SimulationBasedGeneration}

import chisel3.MultiIOModule

import firrtl.{AnnotationSeq, Utils}

/** Add a simulation based error metric generator.
 *
 *  @param tester
 *    provide QoRTester
 */
private[qece] class EmpiricalErrorEstimation[T <: MultiIOModule](
    val testerGenerator: MetricMap => ((T) => ErrorFeaturedPeekPokeTester[T])
) extends ErrorEstimation
    with SimulationBasedGeneration {

  override def execute(annotations: AnnotationSeq, m: => MultiIOModule): AnnotationSeq = {
    val (time, error) = Utils.time(runSimulation(testerGenerator(getMetrics(annotations)))(m))
    Seq(
        generateMetricAnnotation(MetricMap(errorKey -> error)),
        TimedSimulationAnnotation(time)
    ) ++ annotations
  }
}

/** Companion object of [[EmpiricalErrorEstimation]]. */
private[qece] object EmpiricalErrorEstimation {
  def apply[T <: MultiIOModule](
      testerGenerator: MetricMap => ((T) => ErrorFeaturedPeekPokeTester[T])
  ): EmpiricalErrorEstimation[T] = new EmpiricalErrorEstimation[T](testerGenerator)
}
