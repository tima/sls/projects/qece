/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.qor

import qece.estimation.{MetricMap, Metric}
import qece.estimation.transforms.MetricExtraction

import firrtl.AnnotationSeq

/** Add an analytical error metric to generated metrics.
 *
 *  @param func
 *    function to generate the theoretical quality of result that the kernel should achieve
 */
private[qece] class AnalyticalErrorEstimation(func: MetricMap => Metric) 
  extends ErrorEstimation with MetricExtraction {
  override def run(annos: AnnotationSeq): AnnotationSeq = {
    annos :+ generateMetricAnnotation(MetricMap(errorKey -> func(getMetrics(annos))))
  }
}

/** Companion object of [[AnalyticalErrorEstimation]]. */
private[qece] object AnalyticalErrorEstimation {
  def apply(func: MetricMap => Metric): AnalyticalErrorEstimation =
    new AnalyticalErrorEstimation(func)
}
