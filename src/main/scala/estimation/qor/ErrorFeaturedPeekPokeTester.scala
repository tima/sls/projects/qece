/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.qor

import chisel3.iotesters.PeekPokeTester

import chisel3.MultiIOModule

import qece.estimation.qor.exceptions.UndefinedError

private[qor] trait WithError {
  var error: Option[Double] = None

  def setError(newError: Double): Unit = this.error = Some(newError)

  def getError: Double =
    error match {
      case None =>
        throw UndefinedError(s"Error for ${this.getClass.getSimpleName} is undefined")
      case Some(x) => x
    }
}

/** PeekPokeTester enhanced with error generating helpers
 *  @param mut
 *    module under test
 */
abstract class ErrorFeaturedPeekPokeTester[T <: MultiIOModule](
    mut: T
) extends PeekPokeTester(mut)
    with WithError {

  /** Used to suppress useless logging => but if done, signal is not poked */
  //override def pokeFixedPoint(signal: FixedPoint, value: Double): Unit = {
  //  try {
  //    super.pokeFixedPoint(signal, value)
  //  } catch {
  //    case e: java.lang.IllegalArgumentException =>
  //    case t: Throwable => throw t
  //  }
  //}

  /** Set the error value to nRMSE of estimations
   *
   *  @param estimations
   *    estimation values
   *  @param references
   *    reference value
   */
  def setNormalizedRootMeanSquareError(estimations: Array[Double], references: Array[Double]): Unit =
    setError(StatisticHelper.normalizedRootMeanSquareError(estimations, references).abs)

}
