/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.qor

/** Utility object for statistics. */
object StatisticHelper {

  /** Get mean and std values for a serie
   *
   *  @param serie
   *    serie to analyze
   */
  def getMeanAndStd(serie: Array[Double]): (Double, Double) = {
    val N     = serie.size
    val mean  = serie.sum / N
    var total = 0.0
    for (x <- serie) {
      total += math.pow(x - mean, 2)
    }
    (mean, math.sqrt(total / N))
  }

  /** Compute root-mean-square error for a given serie
   *
   *  @param samples
   *    serie to analyze
   *  @param references
   *    reference values to use as oracle
   */
  def rootMeanSquareError(samples: Array[Double], references: Array[Double]): Double = {
    require(samples.size == references.size, s"Can not compute RMSE on vectors of different sizes")
    math.sqrt((samples zip references).map { case (x, y) => math.pow(x - y, 2) }.sum / samples.size)
  }

  /** Compute normalised root-mean-square error for a given serie
   *
   *  @param samples
   *    serie to analyze
   *  @param references
   *    reference values to use as oracle
   */
  def normalizedRootMeanSquareError(samples: Array[Double], references: Array[Double]): Double =
    rootMeanSquareError(samples, references) / (samples.sum / samples.size)
}
