/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.qor

import org.apache.commons.math3.distribution.{NormalDistribution => ND}

/** Utility object for normal distribution. */
class NormalDistribution(mean: Double, std: Double) {
  // N(mean, std)
  private val nd = new ND(mean, std)

  private def roundAt(p: Int)(n: Double) = {
    val s = math pow (10, p)
    (math round n * s) / s
  }

  private def getPrecision(step: Double) = {
    step.toString.size - 2
  }

  /** Return zalpha from N(0, 1), to build confidence interval
   *
   *  @param confidence
   *    confidence needed for the interval (e.g. 99%)
   *  @param step
   *    precision of zalpha
   */
  def getZAlpha(confidence: Double, step: Double = 0.01): Double = {
    require(0.5 <= confidence && confidence <= 1.0, "Confidence should be included in [0.5; 1[")
    val value = confidence + (1 - confidence) / 2
    var x     = 0.0
    while (nd.cumulativeProbability(x) < value) x += step
    roundAt(getPrecision(step))(x)
  }

  /** Get a sample following N(mean, std). */
  def sample: Double = {
    // avoid NaN returning here
    val s = nd.sample()
    if (s.isNaN) sample else s
  }

  /** Get an iterator over a N(mean, std). */
  def iterator: Iterator[Double] = Iterator.continually(sample)

  /** Build a sequence of samples following N(mean, std). To use over iterator to produce deterministic tests
   *
   *  @param n
   *    number of samples to produce
   */
  def seq(n: Int): Seq[Double] = (0 until n).map(_ => this.sample)
}

object StandardNormalDistribution extends NormalDistribution(0.0, 1.0)
