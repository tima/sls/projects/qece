/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.graph

import qece.estimation.utils.Operators

import firrtl.annotations.ReferenceTarget
import firrtl.ir.{DefMemory, Type}

/** Representing a memory node
 *
 *  @param dataIn
 *    data written
 *  @param addrIn
 *    addr for read
 *  @param dataOut
 *    data read
 *  @param addrOut
 *    addr for write
 *  @param wen
 *    write enable
 *  @param ren
 *    read enable
 */
private[qece] class MemoryNode(
    node: DefMemory,
    ref: ReferenceTarget,
    tpe: Type,
    val depth: BigInt,
    var addrIn: SignalNode,
    var dataIn: SignalNode,
    var wen: SignalNode,
    var addrOut: SignalNode,
    var dataOut: SignalNode,
    var ren: SignalNode
) extends OperatorNode(node, ref, Operators.MEMORY, tpe, Seq(dataIn, addrIn, addrOut, wen, ren), Seq(dataOut)) {

  private[qece] val mem = node.name

  override def prettyToString(indent: Int): String = {
    Seq(
        " " * indent + s"$ref ($operator [${tpe.serialize}] with depth ${depth})",
        " " * (indent + 2) + "sources:",
        " " * (indent + 4) + s"addrIn: $addrIn",
        " " * (indent + 4) + s"dataIn: $dataIn",
        " " * (indent + 4) + s"wen: $wen",
        " " * (indent + 4) + s"addrOut: $addrOut",
        " " * (indent + 4) + s"ren: $ren",
        " " * (indent + 2) + "sinks:",
        " " * (indent + 4) + s"dataOut: $dataOut"
    ).mkString("\n")
  }

  private[qece] def updateSignal(target: ReferenceTarget, signal: SignalNode): Unit = {
    if (target == addrIn.ref) {
      sources = (sources diff Seq(addrIn)) :+ signal
      addrIn = signal
    }
    if (target == dataIn.ref) {
      sources = (sources diff Seq(dataIn)) :+ signal
      dataIn = signal
    }
    if (target == wen.ref) {
      sources = (sources diff Seq(wen)) :+ signal
      wen = signal
    }
    if (target == addrOut.ref) {
      sources = (sources diff Seq(addrOut)) :+ signal
      addrOut = signal
    }
    if (target == dataOut.ref) {
      sinks = (sinks diff Seq(dataOut)) :+ signal
      dataOut = signal
    }
    if (target == ren.ref) {
      sources = (sources diff Seq(ren)) :+ signal
      ren = signal
    }
    assert(Seq(dataIn, addrIn, addrOut, wen, ren).toSet sameElements sources.toSet, "Sources should match")
    assert(Seq(dataOut).toSet sameElements sinks.toSet, "Sinks should match")
  }
}
