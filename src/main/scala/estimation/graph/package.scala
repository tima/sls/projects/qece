/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation

import qece.estimation.utils.Operators

import firrtl.ir.{UIntLiteral, SIntLiteral}

import logger.LazyLogging

package object graph {
  private[estimation] implicit class SeqSignalNode(s: Seq[SignalNode]) extends LazyLogging {

    /** Perform a real distinct operation on SignalNode(s), detecting dependancy on equality with literal
     *
     *  Used for mux condition identification TODO: properly absorb padding => maybe in pattern detection instead of
     *  here
     */
    def deepDistinct: Seq[SignalNode] = {
      def isLiteral(s: SignalNode): Boolean = {
        s.node match {
          case _: UIntLiteral => true
          case _: SIntLiteral => true
          case _              => logger.info(s"${s.node} not recognized as literal"); false
        }
      }
      def deepFind(s: SignalNode): SignalNode = {
        s.source match {
          case Some(opNode) =>
            opNode.operator match {
              case Operators.PAD =>
                //update bitwidth when padding
                deepFind(opNode.sources.head).updateType(opNode.tpe)
              case Operators.COMP =>
                (isLiteral(opNode.sources.head), isLiteral(opNode.sources(1))) match {
                  // constant condition will be skipped at synthesis time anyway, if not sooner
                  case (true, true)   => DontCareNode
                  case (true, false)  => deepFind(opNode.sources(1))
                  case (false, true)  => deepFind(opNode.sources.head)
                  case (false, false) => s
                }
              case _ => s
            }
          case _ => s
        }
      }
      // TODO: use a better way to absorb padding
      s.map(deepFind).filter(_ != DontCareNode).distinct
    }
  }
}
