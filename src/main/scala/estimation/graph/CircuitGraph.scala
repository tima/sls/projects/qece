/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.graph

import qece.estimation.utils.{Operators, Namespace, ReferenceTargetUtils}

import firrtl._
import firrtl.ir._
import firrtl.annotations.{ModuleTarget, ReferenceTarget}
import firrtl.analyses.ConnectionGraph
import firrtl.passes.MemPortUtils

/** Class representing a Circuit, with all module(s) as [[ModuleGraph]] (s)
 *
 *  @param circuit
 *    name of the toplevel used
 *  @param modules
 *    all defined modules of the circuit as [[ModuleGraph]] (s)
 */
private[qece] case class CircuitGraph(circuit: String, modules: Seq[ModuleGraph]) {

  /** Pretty print this [[CircuitGraph]] as a String
   *
   *  @param indent
   *    indentation level to be used
   */
  def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + s"CircuitGraph for circuit $circuit:",
        modules.map(_.prettyToString(indent + 2)).mkString("\n\n")
    ).mkString("\n")
  }

  /** Utils for printing. */
  def prettyToString: String = prettyToString()

  /** Run a transform on every module, and return an update [[CircuitGraph]].
   *
   *  @param transform
   *    transform function to be runned on every module
   *  @return
   *    a new [[CircuitGraph]] after the transform have been runned
   */
  def mapModules(transform: ModuleGraph => ModuleGraph): CircuitGraph = {
    this.copy(modules = modules.map(transform))
  }

  /** Retrieve a [[ModuleGraph]]
   *
   *  @param mt
   *    ModuleTarget of the module
   */
  def getModule(mt: ModuleTarget): Option[ModuleGraph] = {
    val mods = modules.filter(_.module == mt)
    assert(mods.size < 2, s"Found multiple modules ${mt} in graph ${circuit}")
    mods.headOption
  }

  /** Retrieve a [[ModuleGraph]]
   *
   *  @param m
   *    FIRRTL definition node for this module
   */
  def getModule(m: DefModule): Option[ModuleGraph] = getModule(ModuleTarget(circuit, m.name))
}

/** Companion object for [[CircuitGraph]] class. */
object CircuitGraph {

// scalastyle:off method.length
  /** Build a whole [[CircuitGraph]] from a Circuit
   *
   *  @param circuit
   *    circuit to be representated as graph
   */
  def apply(circuit: Circuit): CircuitGraph = {
    def getReferenceTarget(mt: ModuleTarget, expr: Expression): ReferenceTarget = {
      expr match {
        case r: Reference => mt.ref(r.name)
        case s: SubField  => getReferenceTarget(mt, s.expr).field(s.name)
      }
    }
    val look = ConnectionGraph(circuit).irLookup

    val nodes     = new scala.collection.mutable.HashMap[ReferenceTarget, OperatorNode]()
    val memNodes  = new scala.collection.mutable.HashMap[ReferenceTarget, MemoryNode]()
    val signals   = new scala.collection.mutable.HashMap[ReferenceTarget, SignalNode]()
    val operators = new scala.collection.mutable.HashMap[Operators.Operator, Seq[ReferenceTarget]]()

    def appendOperator(op: Operators.Operator, node: OperatorNode): Unit = {
      operators contains op match {
        case true  => operators(op) = operators(op) :+ node.ref
        case false => operators(op) = Seq(node.ref)
      }
    }

    def addMemoryNode(
        node: DefMemory,
        ref: ReferenceTarget,
        tpe: Type,
        depth: BigInt,
        addrIn: SignalNode,
        dataIn: SignalNode,
        wen: SignalNode,
        addrOut: SignalNode,
        dataOut: SignalNode,
        ren: SignalNode
    ): MemoryNode = {
      val opNode = nodes contains ref match {
        case false => new MemoryNode(node, ref, tpe, depth, addrIn, dataIn, wen, addrOut, dataOut, ren)
        case true  => throw GraphException(s"MemoryNode $ref was already found in graph: can't be added")
      }
      val (sources, sinks) = (Seq(addrIn, dataIn, wen, addrOut, ren), Seq(dataOut))
      sources.map(s => updateSignalSinks(s.ref, opNode))
      sinks.map(s => updateSignalSources(s.ref, opNode))
      nodes(ref) = opNode
      memNodes(ref) = opNode
      appendOperator(opNode.operator, opNode)
      opNode
    }

    def addOperatorNode(
        node: FirrtlNode,
        ref: ReferenceTarget,
        operator: Operators.Operator,
        tpe: Type,
        sources: Seq[ReferenceTarget],
        sinks: Seq[ReferenceTarget]
    ): OperatorNode = {
      val opNode = nodes contains ref match {
        case false => OperatorNode(node, ref, operator, tpe)
        case true  => throw GraphException(s"OperatorNode $ref was already found in graph: can't be added")
      }
      sources.map(s => updateSignalSinks(s, opNode)).foreach(sig => opNode.addSource(sig))
      sinks.map(s => updateSignalSources(s, opNode)).foreach(sig => opNode.addSink(sig))
      nodes(ref) = opNode
      appendOperator(operator, opNode)
      opNode
    }

    def addSignalNode(node: SignalNode): SignalNode = {
      signals contains node.ref match {
        case true  => throw GraphException(s"Reference ${node.ref} was already found in graph")
        case false => signals(node.ref) = node
      }
      node
    }

    def getType(ref: ReferenceTarget): Type = {
      try {
        look.tpe(ref)
      } catch {
        case _: IllegalArgumentException => UnknownType
        case x: Throwable                => throw x
      }
    }

    def getKind(ref: ReferenceTarget): Kind = {
      try {
        look.kind(ref)
      } catch {
        case _: IllegalArgumentException => UnknownKind
        case x: Throwable                => throw x
      }
    }

    def updateSignalSinks(ref: ReferenceTarget, sink: OperatorNode): SignalNode = {
      val signal = signals contains ref match {
        case false =>
          new SignalNode(
              Reference(
                  ReferenceTargetUtils.getSerialized(ref),
                  getType(ref),
                  getKind(ref)
              ),
              ref,
              getType(ref),
              None,
              Seq(sink)
          )
        case true =>
          val sig = signals(ref)
          sig.addSink(sink)
          sig
      }
      signals(ref) = signal
      signal
    }

    def updateSignalSources(ref: ReferenceTarget, source: OperatorNode): SignalNode = {
      val signal = signals contains ref match {
        case false =>
          new SignalNode(Reference(ref.ref, getType(ref), getKind(ref)), ref, getType(ref), Some(source), Seq.empty)
        case true =>
          val sig = signals(ref)
          sig.addSource(source)
          sig
      }
      signals(ref) = signal
      signal
    }

    def recordModule(m: DefModule): Unit = {
      val mt = ModuleTarget(circuit.main, m.name)
      // used to label every PrimOp operation
      val operatorCounter = new scala.collection.mutable.HashMap[PrimOp, Int]()
      def getOperatorIndex(op: PrimOp): Int = {
        val index = operatorCounter contains op match {
          case false => 0
          case true  => operatorCounter(op) + 1
        }
        operatorCounter(op) = index
        index
      }
      val muxNs   = Namespace("mux")
      val uintNs  = Namespace("uint")
      val sintNs  = Namespace("sint")
      val connect = Namespace("connect")

      def getRegisterOut(name: String): ReferenceTarget = mt.ref(s"${name}")
      def getRegisterIn(name: String): ReferenceTarget  = mt.ref(s"${name}_in")

      def getExpr(expr: Expression): Either[ReferenceTarget, OperatorNode] = {
        def addOpWithArgs(
            node: FirrtlNode,
            ref: ReferenceTarget,
            op: Operators.Operator,
            tpe: Type,
            args: Seq[Expression]
        ): OperatorNode = {
          val registers = new scala.collection.mutable.ArrayBuffer[(Int, OperatorNode)]()
          val consts    = new scala.collection.mutable.ArrayBuffer[(Int, SignalNode)]()
          val sources = args.zipWithIndex.map {
            case (a, i) =>
              a match {
                case r: Reference =>
                  r.kind match {
                    case RegKind =>
                      val ref = mt.ref(r.name)
                      registers += (i -> nodes(ref))
                      ref
                    case _ => getReferenceTarget(mt, r)
                  }
                case sub: SubField => getReferenceTarget(mt, sub)
                case u: UIntLiteral =>
                  val ref = mt.ref(uintNs.get(u.width.toString))
                  consts += (i -> new SignalNode(u, ref, UIntType(u.width), None, Seq.empty))
                  ref
                case s: SIntLiteral =>
                  val ref = mt.ref(sintNs.get(s.width.toString))
                  consts += (i -> new SignalNode(s, ref, SIntType(s.width), None, Seq.empty))
                  ref
                case d: DoPrim =>
                  getExpr(d) match {
                    case Left(x) => x
                    case Right(x) =>
                      val ref   = mt.ref(connect.get)
                      val inner = addSignalNode(new SignalNode(d, ref, x.tpe, Some(x), Seq.empty))
                      x.addSink(inner)
                      ref
                  }
                case arg => throw GraphException(s"Unrecognized argument $arg in DoPrim $args")
              }
          }
          //TODO: Need to preserve order in sources => this implementation could fail somehow
          // How to properly preserve order ?
          val realSources = sources diff registers.map(_._2.ref) diff consts.map(_._2.ref)
          val opNode      = addOperatorNode(node, ref, op, tpe, realSources, Seq.empty)
          registers.foreach {
            case (i, n) =>
              n.sinks match {
                case Seq(out) =>
                  out.addSink(opNode)
                  opNode.insertSource(out, i)
                case _ => throw GraphException(s"Register node $n should have one and only one output signal")
              }
          }
          consts.foreach {
            case (i, s) =>
              s.addSink(opNode)
              opNode.insertSource(s, i)
          }
          opNode
        }
        expr match {
          case d: DoPrim =>
            val ref       = mt.ref(s"${d.op.serialize}#${getOperatorIndex(d.op)}")
            val (op, tpe) = Operators.fromDoPrim(d)
            Right(addOpWithArgs(d, ref, op, tpe, d.args))
          case m: Mux =>
            val ref = mt.ref(muxNs.get)
            Right(addOpWithArgs(m, ref, Operators.MUX, m.tpe, Seq(m.cond, m.tval, m.fval)))
          case r: Reference =>
            val ref = mt.ref(r.name)
            (nodes.get(ref), r.kind) match {
              case (Some(node), _) => Right(node)
              case (_, RegKind)    => throw GraphException(s"Did not found register $ref in scope")
              case (_, _)          => Left(ref)
            }
          case s: SubField =>
            val ref = getReferenceTarget(mt, s)
            nodes.get(ref) match {
              case Some(node) => Right(node)
              case _          => Left(ref)
            }
          case u: UIntLiteral => Right(OperatorNode(u, mt.ref(u.value.toString), Operators.CONST, u.tpe))
          case s: SIntLiteral => Right(OperatorNode(s, mt.ref(s.value.toString), Operators.CONST, s.tpe))
          case _              => throw GraphException(s"expression ${expr.serialize} not found")
        }
      }
      def recordConnect(c: Connect): Unit = {
        (getExpr(c.loc), getExpr(c.expr)) match {
          case (Right(loc), Right(expr)) =>
            //only legit if loc is a register, with only one source signal
            (loc.operator, loc.sources) match {
              case (Operators.REG, Seq(in)) =>
                expr.removeSinks
                expr.addSink(in)
                in.addSource(expr)
              case (_, _) =>
                throw GraphException(
                    s"Can't map OperatorNode on OperatorNode if location is not a register with only one source signal"
                )
            }
          case (Right(loc), Left(expr)) =>
            (loc.operator, loc.sources) match {
              case (Operators.REG, Seq(_)) =>
                loc.removeSources
                signals contains expr match {
                  case false => throw GraphException(s"Did not found signal $expr in scope")
                  case true =>
                    signals(expr).addSink(loc)
                    loc.addSource(signals(expr))
                }
              case (_, _) =>
                throw GraphException(
                    s"Can't map Reference on OperatorNode if location is not a register with only one source signal"
                )
            }
          case (Left(loc), Right(expr)) =>
            expr.removeSinks
            signals.get(loc) match {
              case None => throw GraphException(s"Did not found signal $loc in scope")
              case Some(signal) =>
                signal.addSource(expr)
                expr.addSink(signal)
            }
          // use only for memory instanciation
          case (Left(loc), Left(expr)) =>
            (isMemPort(loc), isMemPort(expr)) match {
              case (true, true) => throw new UnsupportedOperationException(s"Binding memory to memory not managed yet")
              case (true, false) =>
                signals contains expr match {
                  case true =>
                    val opRef = mt.ref(loc.ref)
                    memNodes(opRef).updateSignal(loc, signals(expr))
                    signals(expr).addSink(memNodes(opRef))
                  case false => throw GraphException(s"Signal $expr was not found in scope")
                }
              case (false, _) => // submodule instanciation
            }
        }
      }
      def recordRegister(r: DefRegister): Unit = {
        val regIn  = getRegisterIn(r.name)
        val regOut = getRegisterOut(r.name)
        val regRef = mt.ref(r.name)
        addSignalNode(SignalNode(Reference(r.name, r.tpe, RegKind, SourceFlow), regOut, r.tpe))
        addSignalNode(SignalNode(DefWire(NoInfo, r.name, r.tpe), regIn, r.tpe))
        addOperatorNode(r, regRef, Operators.REG, r.tpe, Seq(regIn), Seq(regOut))
      }
      def recordNode(n: DefNode): Unit = {
        val ref    = mt.ref(n.name)
        val signal = addSignalNode(SignalNode(n, ref, n.value.tpe))
        getExpr(n.value) match {
          case Right(opNode) =>
            opNode.addSink(signal)
            signal.addSource(opNode)
          // add a simple connection operator to preserve the graph integrity
          case Left(inRef) =>
            addOperatorNode(n, mt.ref(connect.get), Operators.CONNECT, n.value.tpe, Seq(inRef), Seq(signal.ref))
        }
      }
      def recordPort(port: Port): Unit = {
        val ref = mt.ref(port.name)
        addSignalNode(SignalNode(port, ref, port.tpe))
      }
      def recordInstance(i: DefInstance): Unit = {
        i.tpe match {
          case b: BundleType =>
            b.fields.foreach { f =>
              val ref = mt.ref(i.name).field(f.name)
              addSignalNode(SignalNode(ref, f.tpe))
            }
          case _ => throw GraphException(s"Instance $i was not of type BundleType")
        }
      }
      def isMemPort(ref: ReferenceTarget): Boolean = memNodes contains mt.ref(ref.ref)

      //TODO: manage mem with multiple readers and writers (used in FIR)
      def recordMemory(mem: DefMemory): Unit = {
        // simple case, only manage SyncReadMem primitive, as it is the only one currently implemented
        assert(mem.readers.size == 1, s"Memory must have one and only one reader, found ${mem.readers.size}")
        assert(mem.writers.size == 1, s"Memory must have one and only one writer, found ${mem.writers.size}")
        assert(mem.readwriters.size == 0, s"Memory must have zero readwriter, found ${mem.readwriters.size}")
        assert(mem.writeLatency == 1, s"Memory must have 1 write latency, ${mem.writeLatency}")
        assert(mem.readLatency == 0, s"Memory must have 0 read latency, ${mem.readLatency}")

        val memRef = mt.ref(mem.name)

        val ports = MemPortUtils
          .memType(mem)
          .fields
          .map { x =>
            x.name -> (x.tpe match {
              case b: BundleType => b.fields
              case _             => throw GraphException(s"Ports must be bundles, found ${x.tpe}")
            })
          }
          .toMap

        val reader = mem.readers.head
        val writer = mem.writers.head

        def getField(fields: Seq[Field], name: String): Field = {
          fields.filter(_.name == name).toList match {
            case xs :: Nil => xs
            case _         => throw GraphException(s"Found multiple ports with name $name in $fields")
          }
        }

        def getSignalNode(port: String, fieldName: String, isInput: Boolean): SignalNode = {
          val f = getField(ports(port), fieldName)
          isInput match {
            case true  => assert(f.flip == Default, s"Field $fieldName in port $port should be input")
            case false => assert(f.flip == Flip, s"Field $fieldName in port $port should be output")
          }
          val ref  = mt.ref(mem.name).field(port).field(f.name)
          val node = SubField(SubField(Reference(mem.name, mem.dataType, MemKind), port), f.name, f.tpe)
          new SignalNode(node, ref, f.tpe, None, Seq.empty, true)
        }

        // TODO: better handling ?
        val dataIn = getSignalNode(writer, "data", true)
        val addrIn = getSignalNode(writer, "addr", true)
        val wen    = getSignalNode(writer, "en", true)

        val dataOut = getSignalNode(reader, "data", false)
        val addrOut = getSignalNode(reader, "addr", true)
        val ren     = getSignalNode(reader, "en", true)

        // TODO: useless for now, but add it as reference for connection
        val mask = getSignalNode(writer, "mask", true)

        Seq(dataIn, addrIn, wen, mask, dataOut, addrOut, ren).foreach(sig => addSignalNode(sig))

        addMemoryNode(mem, memRef, mem.dataType, mem.depth, addrIn, dataIn, wen, addrOut, dataOut, ren)
      }
      def recordStmt(stmt: Statement): Unit = {
        stmt match {
          case c: Connect     => recordConnect(c)
          case i: DefInstance => recordInstance(i)
          case r: DefRegister => recordRegister(r)
          case n: DefNode     => recordNode(n)
          case m: DefMemory   => recordMemory(m)
          case _: Block       =>
          case EmptyStmt      =>
          case _ =>
            throw new UnsupportedOperationException(s"Statement ${stmt.serialize} is not supported in graph building")
        }
        stmt.foreachStmt(recordStmt)
      }
      m.foreachPort(recordPort)
      m.foreachStmt(recordStmt)
    }

    // utility function to only retrieve nodes from a given module in graph construction
    def filterMap[T](mt: ModuleTarget, map: Map[ReferenceTarget, T]): Map[ReferenceTarget, T] = {
      map.filter(_._1.module == mt.module)
    }

    // register every nodes
    circuit.modules.foreach(recordModule)

    CircuitGraph(
        circuit.main,
        circuit.modules.map { mod =>
          val mt = ModuleTarget(circuit.main, mod.name)
          //only retrieve input ports which are defined as inputs signals to operators
          val ports = look.ports(mt).filter(p => (look.flow(p) == SourceFlow) && (signals.contains(p))).map(signals(_))
          // retrieve all module operator nodes
          val modNodes = filterMap(mt, nodes.toMap)
          // retrieve operator map for traversal
          val modOperators = operators.map { case (op, seq) => op -> seq.filter(_.module == mt.module) }.toMap
          // build new graph
          new ModuleGraph(mt, ports, modNodes, modOperators)
        }
    )
  }
// scalastyle:on method.length
}
