/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.graph

import scala.collection._

import qece.estimation.utils.Operators

/** Class utils for graph traversal functions
 *
 *  @param toRemove
 *    nodes marked to be removed after traversal
 *  @param details
 *    details about the nodes
 *  @param sourceNodes
 *    source nodes found
 *  @param lastAdded
 *    last node added in the graph
 */
private[qece] class GraphUtils(
    val toRemove: mutable.ArrayBuffer[OperatorNode],
    val details: mutable.ArrayBuffer[(Int, Int)],
    val sourceNodes: mutable.ArrayBuffer[OperatorNode],
    var lastAdded: OperatorNode
) {

  private def getWidth(node: OperatorNode): Int = node.sources.map(_.width.toInt).reduceOption(_ max _).getOrElse(0)

  private def isLastAdded(node: OperatorNode): Boolean = lastAdded == node

  private def addNode(node: OperatorNode): OperatorNode = {
    // Case where a pattern include multiple time the same kind of operator.
    // To avoid counting nodes more than once in the pattern, only update the found nodes when nodes is new
    if (!isLastAdded(node)) {
      lastAdded = node
      toRemove.append(node)
    }
    node
  }

  /** Find and remove all [[OperatorNode]] driven by the origin node through non costing nodes, except when reaching a
   *  module boundary.
   */
  private def removeWire(
      node: OperatorNode,
      found: Seq[OperatorNode] = Seq.empty,
      backward: Boolean = false
  ): (OperatorNode, Seq[OperatorNode]) = {
    if (backward) {
      (node.operator.isWire, node.sources.size) match {
        case (true, 0)  => (node, found)
        case (false, _) => (node, found)
        case (true, _) =>
          node.sources.head.source match {
            case Some(s) => removeWire(s, found :+ node, true)
            case None    => (node, found :+ node)
          }
      }
    } else {
      (node.isSinglePath, node.operator.isWireR, node.drivesOutput) match {
        case (true, true, false) =>
          removeWire(node.next, found :+ node)
        case (_, true, true) =>
          (node, found :+ node)
        case (_, _, _) =>
          (node, found)
      }
    }
  }

  def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + s"GraphUtils:",
        " " * (indent + 2) + s"To remove:\n${toRemove.map(_.prettyToString(indent + 4)).mkString("\n")}",
        " " * (indent + 2) + s"Sources:\n${sourceNodes.map(_.prettyToString(indent + 4)).mkString("\n")}",
        " " * (indent + 2) + s"Last added:\n${lastAdded.prettyToString(indent + 4)}"
    ).mkString("\n")
  }
// scalastyle:off method.length
  // TODO: Add maxOp to stop exploration when found to many operators
  def findOperatorGroup(
      operator: Operators.Operator,
      excludeInputIndices: Seq[Int] = Seq.empty
  )(src: OperatorNode): OperatorNode = {
    def findSources(src: OperatorNode): Seq[OperatorNode] = {
      src.sources.zipWithIndex
        .filter { case (_, i) => !(excludeInputIndices contains i) }
        .map(_._1)
        .filter(_.source != None)
        .map(_.source.get)
    }
    def addLocal(node: OperatorNode, seq: Seq[OperatorNode]): Seq[OperatorNode] = {
      isLastAdded(node) match {
        case false => seq :+ node
        case true  => seq
      }
    }
    def expand(
        node: OperatorNode,
        found: Seq[OperatorNode],
        onlyBackward: Boolean = false
    ): (OperatorNode, Seq[OperatorNode]) = {
      //TODO: add remove wire backward ?
      val newFound = findSources(node).map(expand(_, Seq.empty, true)).flatMap(_._2) ++ found
      (node.operator == operator, node.isSinglePath, onlyBackward) match {
        case (true, true, false) =>
          val (n, f) = expand(node.next, addLocal(node, newFound))
          removeWire(n, f)
        case (true, true, true) => (node, addLocal(node, newFound))
        case (true, false, _)   => (node, addLocal(node, newFound))
        case (false, true, _)   => removeWire(node, newFound)
        case (false, false, _)  => (node, newFound)
      }
    }
    val (trueSrc, initSeq) = removeWire(src)
    val (_, found)         = expand(trueSrc, initSeq)

    // update nodes to remove from the initial graph
    found.distinct.foreach(addNode)

    // update details about the nodes found
    details += (found.filter(_.operator == operator).size -> getWidth(src))

    sourceNodes += src
    lastAdded
  }
// scalastyle:on method.length

  /** Traverse a graph in an only forward fashion from an entry [[OperatorNode]] while a next node exist and the current
   *  node is either of the searched type, or a wire kind
   *
   *  While traversing, update the toRemove and details collection with informations found
   *
   *  @param operator
   *    kind of operators being searched by this function
   *  @param max
   *    max number of operators of the searched kind to find
   *
   *  @param src
   *    entry [[OperatorNode]] to use for the traversal
   *
   *  @return
   *    the last node in the pattern.
   */
  def findOperatorSequence(operator: Operators.Operator, max: Int)(src: OperatorNode): OperatorNode = {
    def addLocal(node: OperatorNode, seq: Seq[OperatorNode]): Seq[OperatorNode] = {
      isLastAdded(node) match {
        case false => seq :+ node
        case true  => seq
      }
    }
    def unroll(node: OperatorNode, found: Seq[OperatorNode]): (OperatorNode, Seq[OperatorNode]) = {
      (node.operator == operator, found.size < max, node.isSinglePath) match {
        case (true, true, true) =>
          val (n, f) = unroll(node.next, addLocal(node, found))
          removeWire(n, f)
        case (true, true, false) => (node, addLocal(node, found))
        case (_, _, true)        => removeWire(node, found)
        case (_, _, false)       => (node, found)
      }
    }
    val (trueSrc, initSeq) = removeWire(src)
    val (next, found)      = unroll(trueSrc, initSeq)

    // update nodes to remove from the initial graph
    found.foreach(addNode)

    // update details about the nodes found
    details += (found.filter(_.operator == operator).size -> getWidth(src))

    sourceNodes += src
    next
  }
}

/** Companion object for [[GraphUtils]]. */
private[qece] object GraphUtils {

  /** Build an empty [[GraphUtils]]. */
  def apply(): GraphUtils =
    new GraphUtils(
        mutable.ArrayBuffer[OperatorNode](),
        mutable.ArrayBuffer[(Int, Int)](),
        mutable.ArrayBuffer[OperatorNode](),
        EmptyOperatorNode
    )
}
