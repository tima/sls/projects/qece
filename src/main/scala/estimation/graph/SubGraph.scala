/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.graph

import firrtl.ir.{Port, NoInfo, Input, Output, FirrtlNode, EmptyStmt}
import firrtl.annotations.{CircuitTarget, ModuleTarget}

/** Class representing a SubGraph as a subset of [[OperatorNode]] (s) with input and output [[SignalNode]] (s)
 *
 *  @param mt
 *    module target for this subgraph
 *  @param inputs
 *    input [[SignalNode]] (s) of the [[SubGraph]]
 *  @param outputs
 *    output [[SignalNode]] (s) of the [[SubGraph]]
 *  @param nodes
 *    [[OperatorNode]] (s) of the [[SubGraph]]
 */
private[qece] case class SubGraph(
  mt: ModuleTarget,
  inputs: Seq[SignalNode],
  outputs: Seq[SignalNode],
  nodes: Seq[OperatorNode]
) {

  /** Pretty print this [[SubGraph]]  to a String
   *
   *  @param indent
   *    indentation level to be used
   */
  def prettyToString(indent: Int = 0): String = {
    def id(i: Int) = " " * i
    Seq(
        id(indent) + s"SubGraph in module ${mt.serialize}:",
        Seq(id(indent + 2) + "inputs:", inputs.map(_.prettyToString(indent + 4)).mkString("\n")).mkString("\n"),
        Seq(id(indent + 2) + "outputs:", outputs.map(_.prettyToString(indent + 4)).mkString("\n")).mkString("\n"),
        Seq(id(indent + 2) + "nodes:", nodes.map(_.prettyToString(indent + 4)).mkString("\n")).mkString("\n")
    ).mkString("\n")
  }

  /** Utils for printing
   *
   *  Equivalent to prettyToString(0)
   */
  def prettyToString: String = prettyToString()

  /** Map inputs with their respective names.
   *
   *  @param inputNames
   *    names to map on
   */
  def mapInputs(inputNames: Seq[String], default: SignalNode = DontCareNode): Seq[(Port, SignalNode)] = {
    // small trick to adjust dimensions
    (inputNames
      .zipAll(inputs, "", default))
      .map { case (n, i) => Port(NoInfo, n, Input, i.tpe) -> i }
      .filter(_._1.name != "")
      .filter(_._2 != DontCareNode)
  }

  /** Map outputs with their respective names.
   *
   *  @param outputNames
   *    names to map on
   */
  def mapOutputs(outputNames: Seq[String], default: SignalNode = DontCareNode): Seq[(Port, SignalNode)] = {
    // small trick to adjust dimensions
    (outputNames
      .zipAll(outputs, "", default))
      .map { case (n, o) => Port(NoInfo, n, Output, o.tpe) -> o }
      .filter(_._1.name != "")
      .filter(_._2 != DontCareNode)
  }

  /** Retrieve nodes from this subgraph. */
  def getFirrtlNodes: Seq[FirrtlNode] = {
    val inside = nodes.flatMap(n => n.sinks ++ n.sources).distinct
    (nodes.map(_.node) ++ (inside diff inputs diff outputs).map(_.node)).filter(_ != EmptyStmt).distinct
  }
}

/** Companion object of the [[SubGraph]] class. */
private[qece] object SubGraph {

  /** Create a [[SubGraph]]  from [[OperatorNode]] (s)
   *
   *  @param nodes
   *    [[OperatorNode]] (s) of the subgraph
   *  @param inputs
   *    [[SignalNode]] (s) inputs of the subgraph
   *  @param outputs
   *    [[SignalNode]] (s) outputs of the subgraph
   */
  def apply(nodes: Seq[OperatorNode], inputs: Seq[SignalNode], outputs: Seq[SignalNode]): SubGraph =
    SubGraph(nodes.head.ref.moduleTarget, inputs, outputs, nodes)
}

private[qece] object EmptySubGraph extends SubGraph(CircuitTarget("").module(""), Seq.empty, Seq.empty, Seq.empty)
