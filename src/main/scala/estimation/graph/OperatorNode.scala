/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.graph

import qece.estimation.utils.Operators

import firrtl.annotations.{ModuleTarget, ReferenceTarget}
import firrtl.ir.{FirrtlNode, Type, UnknownType, EmptyStmt}

/** Graph node used to store an Operator.
 *
 *  Variables are used to allow graph building with references
 *
 *  @param node
 *    Pointer to the original operation node in the circuit
 *  @param ref
 *    ReferenceTarget used to identify this operator
 *  @param operator
 *    kind of operator
 *  @param tpe
 *    type of the operator
 *  @param sources
 *    [[SignalNode]] (s) driving this operator
 *  @param sinks
 *    [[SignalNode]] (s) driven by this operator
 *  @todo
 *    TODO: create case classes from [[OperatorNode]], after building from circuit ?
 */
private[qece] class OperatorNode(
    val node: FirrtlNode,
    val ref: ReferenceTarget,
    val operator: Operators.Operator,
    val tpe: Type,
    var sources: Seq[SignalNode],
    var sinks: Seq[SignalNode]
) {

  /** Pretty print this [[OperatorNode]] to a String
   *
   *  @param indent
   *    indentation level to be used
   */
  def prettyToString(indent: Int = 0): String = {
    def id(i: Int) = " " * i
    Seq(
        s"${id(indent)}$ref ($operator [${tpe.serialize}])",
        id(indent + 2) + "sources:",
        sources.map(_.prettyToString(indent + 4)).mkString("\n"),
        id(indent + 2) + "sinks:",
        sinks.map(_.prettyToString(indent + 4)).mkString("\n")
    ).mkString("\n")
  }

  /** Add a new [[SignalNode]] as source of this [[OperatorNode]]
   *
   *  @param signal
   *    [[SignalNode]] to be added
   */
  def addSource(signal: SignalNode): Unit = this.sources = this.sources :+ signal

  /** Insert a new [[SignalNode]] as source of this [[OperatorNode]], at a given position
   *
   *  @param signal
   *    [[SignalNode]] to be added
   *  @param index
   *    index to insert the source
   */
  def insertSource(signal: SignalNode, index: Int): Unit = {
    this.sources = this.sources.dropRight(this.sources.size - index) ++ Seq(signal) ++ this.sources.drop(index)
  }

  /** Remove all sources signals from this [[OperatorNode]] */
  def removeSources: Unit = {
    this.sources.foreach(_.removeSink(this))
    this.sources = Seq.empty
  }

  /** Add a new [[SignalNode]] as sink of this [[OperatorNode]]
   *
   *  @param signal
   *    [[SignalNode]] to be added
   */
  def addSink(signal: SignalNode): Unit = this.sinks = this.sinks :+ signal

  /** Remove all sinks signals from this [[OperatorNode]] */
  def removeSinks: Unit = {
    this.sinks.foreach(_.removeSource)
    this.sinks = Seq.empty
  }

  /** Retrieve bitwidth from this [[OperatorNode]]. */
  def width = firrtl.bitWidth(tpe)

  /** Retrieve all [[OperatorNode]](s) driven by this [[OperatorNode]] output. */
  def nexts: Seq[OperatorNode] = sinks.flatMap(_.sinks)

  /** Return the following [[OperatorNode]], if unique */
  def next: OperatorNode = {
    assert(isSinglePath, s"Node $ref is not a single path, can not return next node")
    this.nexts.head
  }

  /** Does this [[OperatorNode]] drives any [[OperatorNode]] ? */
  def hasNexts: Boolean = this.nexts.size != 0

  /** Does this [[OperatorNode]] output drive one and only one [[OperatorNode]] ? */
  def isSinglePath: Boolean = {
    (this.sinks.size == 1) && (this.sinks.head.isSinglePath)
  }

  /** Determine if this [[OperatorNode]] only drives an output signal or not. */
  def drivesOutput: Boolean = {
    (this.sinks.size == 1) && (this.sinks.head.isOutput)
  }

  /** Retrieve all previous [[OperatorNode]] from this [[OperatorNode]]. */
  def prevs: Seq[OperatorNode] = sources.map(_.source).filter(_ != None).map(_.get)

  /** Return the previous [[OperatorNode]], if unique */
  def prev: OperatorNode = {
    assert(prevs.size == 1, s"Node $ref is not a single path, can not return previous node")
    this.prevs.head
  }

  /** Does this [[OperatorNode]] have input(s) signal(s) ? */
  def hasPrevs: Boolean = this.prevs.size != 0

  def connect(subGraph: SubGraph): Unit = {
    assert(this.sources == Seq.empty, s"Can't connect node $this which already got sources")
    assert(this.sinks == Seq.empty, s"Can't connect node $this which already got sinks")

    subGraph.inputs.foreach { i =>
      // retrieve all nodes driven by this input in the subGraph, to replace it
      val nodes = i.sinks.filter(op => subGraph.nodes contains op)
      i.sinks = (i.sinks diff nodes) :+ this
    }
    subGraph.outputs.foreach(o => o.source = Some(this))
    this.sinks = subGraph.outputs
    this.sources = subGraph.inputs
  }

  override def toString = this.ref.serialize
}

/** Companion object for [[OperatorNode]] class. */
private[qece] object OperatorNode {

  /** Build a non connected [[OperatorNode]].
   *
   *  @param node
   *    Pointer to the original operator in the circuit
   *  @param ref
   *    ReferenceTarget of the operator
   *  @param operator
   *    Kind of operator
   *  @param tpe
   *    Type  of the operator
   */
  def apply(node: FirrtlNode, ref: ReferenceTarget, operator: Operators.Operator, tpe: Type): OperatorNode =
    new OperatorNode(node, ref, operator, tpe, Seq.empty, Seq.empty)

}

/** Empty operator node. */
private[qece] object EmptyOperatorNode
    extends OperatorNode(EmptyStmt, ModuleTarget("", "").ref("nop"), Operators.NOP, UnknownType, Seq.empty, Seq.empty)
