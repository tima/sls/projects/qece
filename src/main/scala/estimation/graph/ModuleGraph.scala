/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.graph

import qece.estimation.utils.Operators

import firrtl.annotations.{ModuleTarget, ReferenceTarget}

/** Class representing a Module with a graph of its operators
 *
 *  @param module
 *    ModuleTarget referencing this module
 *  @param inputs
 *    input [[SignalNode]] (s) of this module
 *  @param nodes
 *    [[OperatorNode]] (s) declared in this module
 *  @param operators
 *    operator map used to retrieve [[OperatorNode]] (s) from their kind
 */
private[qece] case class ModuleGraph(
    module: ModuleTarget,
    inputs: Seq[SignalNode],
    nodes: Map[ReferenceTarget, OperatorNode],
    operators: Map[Operators.Operator, Seq[ReferenceTarget]]
) {

  /** Pretty print this [[ModuleGraph]]  to a String
   *
   *  @param indent
   *    indentation level to be used
   */
  def prettyToString(indent: Int = 0): String = {
    Seq(
        " " * indent + s"Module[${module}]",
        inputs.map(_.prettyToString(indent + 4)).mkString(" " * (indent + 2) + "Inputs:\n", "\n", "\n"),
        nodes
          .map {
            case (_, node) => node.prettyToString(indent + 4)
          }
          .mkString(" " * (indent + 2) + "Operators:\n", "\n\n", "\n")
    ).mkString("\n")
  }

  /** Utils for printing. */
  def prettyToString: String = prettyToString(0)

  /** Retrieve all [[OperatorNode]] (s) for a certain kind of operator
   *
   *  @param kind
   *    kind of operators being searched
   */
  def getOperators(kind: Operators.Operator): Seq[OperatorNode] = {
    operators.filter(_._1 == kind).flatMap { case (_, seq) => seq.map(ref => getNode(ref).get).toSeq }.toSeq
  }

  /** Retrieve an [[OperatorNode]] from the module declaration
   *
   *  @param ref
   *    ReferenceTarget of the [[OperatorNode]]
   */
  def getNode(ref: ReferenceTarget): Option[OperatorNode] = nodes.get(ref)

  def replace(sub: SubGraph, to: OperatorNode): ModuleGraph = {
    to.connect(sub)
    val newNodes     = (nodes -- sub.nodes.map(_.ref)) + (to.ref -> to)
    val tmpOperators = new scala.collection.mutable.HashMap[Operators.Operator, Seq[ReferenceTarget]]()
    tmpOperators ++= (operators.map {
      case (op, refs) => op -> refs.filter(ref => !(sub.nodes contains ref))
    })
    tmpOperators contains to.operator match {
      case true  => tmpOperators(to.operator) = tmpOperators(to.operator) ++ Seq(to.ref)
      case false => tmpOperators(to.operator) = Seq(to.ref)
    }
    this.copy(nodes = newNodes, operators = tmpOperators.toMap)
  }
}
