/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.graph

import qece.estimation.utils.ReferenceTargetUtils

import firrtl._
import firrtl.ir._
import firrtl.PrimOps.{Pad, Bits}
import firrtl.annotations.{CircuitTarget, ReferenceTarget}

private[qece] object DontCareNode
    extends SignalNode(EmptyStmt, CircuitTarget("").module("").ref("DontCare"), UnknownType, None, Seq.empty)

/** SignalNode to zero
 *  @todo
 *    TODO proper reference and type management
 */
private[qece] object ZeroNode
    extends SignalNode(
        UIntLiteral(0, IntWidth(1)),
        CircuitTarget("").module("").ref("0"),
        UIntType(IntWidth(1)),
        None,
        Seq.empty
    )

/** Graph node used to store a Signal.
 *
 *  Variables are used to allow graph building with references
 *
 *  @param node
 *    Pointer to the original signal node in the circuit
 *  @param ref
 *    ReferenceTarget used to identify this signal
 *  @param tpe
 *    type of the signal
 *  @param sources
 *    [[OperatorNode]] driving this signal
 *  @param sinks
 *    [[OperatorNode]] (s) driven by this signal
 *  @todo
 *    TODO: create case classes from [[SignalNode]], after building from circuit ?
 */
private[qece] class SignalNode(
    val node: FirrtlNode,
    val ref: ReferenceTarget,
    var tpe: Type,
    var source: Option[OperatorNode],
    var sinks: Seq[OperatorNode],
    val isMemPort: Boolean = false
) {

  val name = ReferenceTargetUtils.getSerialized(ref)

  override def toString = this.ref.serialize

  /** Print this [[SignalNode]] to a String.
   *
   *  @param indent
   *    indentation level to be used
   */
  def prettyToString(indent: Int = 0): String = " " * indent + s"${ref.serialize} [${tpe.serialize}]"

  /** Retrieve bitwidth from this [[SignalNode]]. */
  def width = bitWidth(tpe)

  /** Set a new [[OperatorNode]] source to this [[SignalNode]]
   *
   *  @param node
   *    [[OperatorNode]] to be added
   */
  def addSource(node: OperatorNode): Unit = this.source = Some(node)

  /** Unset the source of this [[SignalNode]]. */
  def removeSource: Unit = this.source = None

  /** Update type of signal. */
  def updateType(tpe: Type) = {
    this.tpe = tpe
    this
  }

  /** Append a new [[OperatorNode]] sink to this [[SignalNode]]
   *
   *  @param node
   *    [[OperatorNode]] to be added
   */
  def addSink(node: OperatorNode): Unit = {
    this.sinks = this.sinks :+ node
  }

  /** Determine if this signal drives multiple operators or not. */
  def isSinglePath: Boolean = (sinks.size == 1)

  /** Determine if this signal is a module output or not. */
  def isOutput: Boolean =
    node match {
      case Port(_, _, d, _) => (d == Output) && !isMemPort
      // case Port(_, _, d, _) => (d == Output)
      case _ => false
    }

  /** Determine if this signal is a module input or not. */
  def isInput: Boolean =
    node match {
      case Port(_, _, d, _) => (d == Input) && !isMemPort
      case _                => false
    }

  /** Remove an [[OperatorNode]]  from this [[SignalNode]].
   *
   *  @param node
   *    [[OperatorNode]] to remove, if found
   */
  def removeSink(node: OperatorNode): Unit = {
    this.sinks = this.sinks contains node match {
      case false => this.sinks
      case true  => this.sinks diff Seq(node)
    }
  }
}

/** Companion object for [[SignalNode]] class. */
object SignalNode {

  /** Build a non connected [[SignalNode]].
   *
   *  @param node
   *    Pointer to the signal node in the circuit
   *  @param ref
   *    ReferenceTarget of the signal
   *  @param tpe
   *    Type of the signal
   */
  def apply(node: FirrtlNode, ref: ReferenceTarget, tpe: Type): SignalNode =
    new SignalNode(node, ref, tpe, None, Seq.empty)

  def apply(ref: ReferenceTarget, tpe: Type, kind: Kind = UnknownKind): SignalNode =
    new SignalNode(Reference(ReferenceTargetUtils.getSerialized(ref), tpe, kind), ref, tpe, None, Seq.empty)

  def instanciate(i: DefInstance, node: FirrtlNode, flow: Flow): FirrtlNode = {
    node match {
      case p: Port => SubField(Reference(i.name, p.tpe, InstanceKind), p.name, p.tpe, flow)
      case _       => throw new UnsupportedOperationException(s"Can't instantiate $node to $i")
    }
  }

// scalastyle:off method.length
  private def buildConnect(loc: FirrtlNode, expr: FirrtlNode, isInput: Boolean): Connect = {
    def getZero(tpe: Type): Literal = {
      tpe match {
        // assume UInt 0 on UnknownType, as it results in a Zero vector
        case UnknownType => UIntLiteral(0, UnknownWidth)
        case UIntType(w) => UIntLiteral(0, w)
        case _           => throw new UnsupportedOperationException(s"Can't work on type $tpe")
      }
    }
    val myLoc = loc match {
      case p: Port      => Reference(p.name, p.tpe, if (isInput) InstanceKind else PortKind, SinkFlow)
      case d: DefNode   => Reference(d.name, d.value.tpe, NodeKind, SinkFlow)
      case w: DefWire   => Reference(w.name, w.tpe, WireKind, SinkFlow)
      case r: Reference => r //r.copy(kind = WireKind)
      case s: SubField  => s
      case d: DoPrim =>
        d.op match {
          // case where a padded signal is assigned
          case Pad =>
            val padding = (scala.math.log(d.consts.head.doubleValue) / scala.math.log(2)).ceil.toInt
            d.args.head.mapType(x =>
              x match {
                case u: UIntType => new UIntType(IntWidth(bitWidth(u) + padding))
                case x =>
                  throw new UnsupportedOperationException(s"Pad: Can't map type on type $x")
              }
            )
          // case where subsignal is assigned
          case Bits =>
            val (high, low) = (d.consts(0), d.consts(1))
            d.args.head.mapType(x =>
              x match {
                case _: UIntType => new UIntType(IntWidth(high - low + 1))
                case x =>
                  throw new UnsupportedOperationException(s"Bits: Can't map type on type $x")
              }
            )
          case _ =>
            throw new UnsupportedOperationException(s"DoPrim ${loc.serialize} not supported as left hand member")
        }
      case _ =>
        throw new UnsupportedOperationException(s"${loc.serialize} not supported as left hand member")
    }
    val myExpr = expr match {
      case p: Port        => Reference(p.name, p.tpe, if (isInput) PortKind else InstanceKind, SourceFlow)
      case d: DefNode     => Reference(d.name, d.value.tpe, NodeKind, SourceFlow)
      case w: DefWire     => Reference(w.name, w.tpe, WireKind, SourceFlow)
      case r: Reference   => r
      case s: SubField    => s
      case EmptyStmt      => getZero(myLoc.tpe)
      case u: UIntLiteral => u
      case s: SIntLiteral => s
      //TODO: should not happen !
      case d: DoPrim => d.args.head
      case _ =>
        throw new UnsupportedOperationException(s"${expr.serialize} not supported as right hand member")
    }
    Connect(NoInfo, myLoc, myExpr)
// scalastyle:on method.length
  }

  def connectIn(loc: FirrtlNode, expr: FirrtlNode): Connect  = buildConnect(loc, expr, true)
  def connectOut(loc: FirrtlNode, expr: FirrtlNode): Connect = buildConnect(loc, expr, false)

}
