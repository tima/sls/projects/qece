/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation

import qece.estimation.exceptions.{UndefinedMetricException, MultipleMetricDefinitionException}

import scala.collection.mutable.ArrayBuffer

/** Class to represent a metric value.
 *  For now, only using a Double value, but it could evolve.
 *
 *  @param value
 *    value associated to this metric
 */
case class Metric(value: Double) {
  override def toString: String = value.toString
  def toBool                    = (value == 1.0)
  def <=(that: Any): Boolean =
    that match {
      case d: Double => value <= d
      case _         => throw new UnsupportedOperationException(s"Can't compare metric to $that")
    }

}

/** Companion object of [[Metric]]. */
object Metric {
  def apply(b: Boolean): Metric = Metric(if (b) 1.0 else 0.0)
}

/** Metric used for annotation.
 *  Useful to discriminate real metric and temporal metrics.
 *
 *  @param value
 *    associated value
 */
class AnnotationMetric(value: Double) extends Metric(value)

/** Store all the metrics of a given design in a named map */
class MetricMap(private[qece] var underlying: Map[String, Metric]) {

  /** Cast this [[MetricMap]] to its underlying representation. */
  lazy private[qece] val toMap: Map[String, Metric] = underlying.toMap
  lazy private[qece] val keys                       = underlying.keySet

  private[qece] def checkMultipleDefinition(keySet: Set[String]): Unit = {
    val duplicates = ArrayBuffer[String]()
    for (key <- this.underlying.keySet) {
      keySet contains key match {
        case true => duplicates += key
        case _    =>
      }
    }
    duplicates.size match {
      case 0 =>
      case _ =>
        throw MultipleMetricDefinitionException(
            s"Found multiple metric definition for keys ${duplicates.mkString("[", ", ", "]")}"
        )
    }
  }

  /** Check if this [[MetricMap]] defines every needed metrics
   *
   *  @param transform
   *    class invoking this method, for accurate exception throwing
   *  @param keys
   *    key(s) to be searched in this [[MetricMap]]
   */
  private[qece] def checkDefinition(transform: Class[_], keys: Seq[String]): Unit = {
    val missingKeys = keys
      .map {
        case k =>
          underlying.keySet contains k match {
            case true  => ""
            case false => k
          }
      }
      .filter(_ != "")
    missingKeys.size match {
      case 0 =>
      case _ =>
        throw UndefinedMetricException(
            s"Transform ${transform.getSimpleName} does not define metric(s) with name(s):\n" +
              missingKeys.mkString(", ")
        )
    }
  }

  private[qece] def filterNot(keys: Iterable[String]): MetricMap = new MetricMap(this.underlying -- keys)

  /** Return this [[MetricMap]] keys as a csv compliant header, for serialization purposes
   *
   *  @param preHead
   *    additionnal fields to put before the metrics keys
   *  @param separator
   *    separator character for the generated csv
   */
  private[qece] def getCsvHeader(preHead: Seq[String] = Seq.empty, separator: String): String = {
    (preHead ++ underlying.keySet).reduce((x, y) => s"$x$separator$y")
  }

  /** Return this [[MetricMap]] as a csv compliant entry, for serialization purposes
   *
   *  @param infos
   *    additionnal informations to put before the metrics
   *  @param separator
   *    separator character for the generated csv
   */
  private[qece] def asCsvEntry(infos: Seq[String] = Seq.empty, separator: String): String = {
    (infos ++ underlying.values.map(_.toString)).reduce((x, y) => s"$x$separator$y")
  }

  /** Remove all the metrics that are not of type [[AnnotationMetric]], i.e. the timing annotations. */
  private[qece] def removeAnnotations = underlying.filterNot(_._2.isInstanceOf[AnnotationMetric])

  private[qece] def canEqual(that: Any) = that.isInstanceOf[MetricMap]

  /** Check is this [[MetricMap]] contains a given key.
   *
   *  @param key
   *    name of the metric to check
   */
  def contains(key: String): Boolean = underlying.keySet contains key

  /** Merge two [[MetricMap]] into one
   *
   *  @param that
   *    other [[MetricMap]] to merge
   */
  @throws(classOf[MultipleMetricDefinitionException])
  def ++(that: MetricMap): MetricMap = {
    checkMultipleDefinition(that.underlying.keySet)
    new MetricMap((this.underlying.toSeq ++ that.underlying.toSeq).toMap)
  }

  /** Add a named [[Metric]] to a [[MetricMap]], returning an updated version
   *
   *  @param that
   *    tuple including metric name and value
   *  @return
   *    new [[MetricMap]] containing the value
   */
  def ++(that: (String, Metric)): MetricMap = new MetricMap((this.underlying.toSeq :+ (that._1 -> that._2)).toMap)

  /** Merge a [[MetricMap]] with this [[MetricMap]].
   *  
   *  If two metrics with the same name are found, keep the one in the merged map (latest).
   *
   *  @param that
   *    other [[MetricMap]] to merge
   */
  def ++&(that: MetricMap): MetricMap = {
    that ++ this.filterNot(that.underlying.keys)
  }

  /** Merge a [[MetricMap]] with this [[MetricMap]] in place.
   *
   *  @param that
   *    other [[MetricMap]] to merge in
   */
  def ++=(that: MetricMap): Unit = underlying = (this ++ that).underlying

  /** Pretty print this [[MetricMap]] to a string
   *
   *  @param indent
   *    indentation level to be used
   */
  def prettyToString(indent: Int = 0): String = {
    underlying
      .map {
        case (k, v) =>
          " " * indent + s"$k:\t\t$v"
      }
      .mkString("\n")
  }

  /** Accessor to metrics by name
   *
   *  @param name
   *    name of the metric being accessed
   */
  def apply(name: String): Double = {
    underlying.keySet contains name match {
      case true  => underlying(name).value
      case false => throw UndefinedMetricException(s"Did not found any metric with name $name")
    }
  }

  /** Define the error metric. */
  def error: Double = {
    underlying.keySet contains MetricMap.errorKey match {
      case true  => underlying(MetricMap.errorKey).value
      case false => throw UndefinedMetricException(s"Did not found any error metric")
    }
  }

  override def toString: String = prettyToString()

  override def equals(that: Any): Boolean =
    // Comparison of metrics should not rely on annotation timing informations.
    that match {
      case that: MetricMap =>
        that.canEqual(this) && this.removeAnnotations == that.removeAnnotations
      case _ => false
    }
}

object MetricMap {
  private[qece] val empty: MetricMap = new MetricMap(Map.empty)
  private[qece] val errorKey = "__error__"

  def apply(ms: (String, Metric)*): MetricMap = new MetricMap(ms.toList.toMap)
}
