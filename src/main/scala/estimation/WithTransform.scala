/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation

import qece.{QeceConfig, QeceContext, CustomLogging}

import qece.estimation.transforms._
import qece.estimation.annotations._
import qece.estimation.utils.{Access, WithTimeout}
import qece.estimation.macros.MacroSeq

import qece.exploration.utils.Constructor
import qece.exploration.strategies.utils.Point

import qece.backend.FPGA

import chisel3.MultiIOModule
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

import firrtl.stage.RunFirrtlTransformAnnotation
import firrtl.AnnotationSeq

import scala.concurrent._
import scala.reflect.runtime.universe.TypeTag

import logger.LogLevelAnnotation

/** Trait for verilog generation after running specified transforms. */
trait WithTransform extends CustomLogging with Access with WithTimeout {

  override private[qece] val duration = QeceContext.timeout.totalEstimation

  /** Run transforms/metric extractors over a Chisel module, with Chisel generation if useful
   *
   *  @param m
   *    chisel module (pass-by-name)
   *  @param initialMap
   *    metric map already existing before this estimation run. WARNING: should include parameters
   *  @param args
   *    arguments of the scala main, used by ChiselStage
   *  @param transforms
   *    Seq of transform to run before emission
   *  @param macros
   *    if specified, additional macros to use for detection (plus the ones found in the board library)
   *  @param path
   *    if specified, emit to target-dir/path
   *  @param board
   *    if specified, target fpga. Needed for estimation transforms
   *  @return
   *    Generated annotations
   */
  private[qece] def emitWithTransform(
      m: => MultiIOModule,
      initialMap: MetricMap = MetricMap.empty,
      args: Array[String] = Array("--target-dir", QeceContext.emission.basePath),
      transforms: TransformSeq = TransformSeq.default,
      macros: MacroSeq = MacroSeq.empty,
      path: Option[String] = None,
      board: Option[FPGA] = None
  ): AnnotationSeq = {
    def appendDir(post: String): Array[String] = {
      val targetKey = "--target-dir"
      args contains targetKey match {
        case true =>
          val index = args.indexOf(targetKey) + 1
          args.updated(index, s"${args(index)}/$post")
        case false => args ++ Array(targetKey, post)
      }
    }

    mydebug(s"[PREELAB] Running ${transforms.preElaboration} on module with metrics ${initialMap.toMap}")
    // propagate existing metrics, including parameters, and extract all pre elaboration metrics
    val preElabAnnotations = transforms.preElaboration
      .foldLeft(AnnotationSeq(Seq(MetricMapAnnotation(initialMap)))) { case (annotations, ext) => ext.run(annotations) }

    mydebug(s"[SIMU] Running ${transforms.simulations} on module with metrics ${initialMap.toMap}")
    val postSimuAnnotations = transforms.requireSimulation match {
      case true =>
        transforms.simulations.foldLeft(preElabAnnotations) { case (annotations, ext) => ext.execute(annotations, m) }
      case false => preElabAnnotations
    }

    // build real annotation set for elaboration
    val elabAnnotations =
      transforms.transforms.map(tfs => RunFirrtlTransformAnnotation(tfs)) ++ (board match {
        case Some(b) => Seq(BoardAnnotation(b))
        case None    => Seq()
      }) ++ Seq(MacroAnnotation(macros)) ++ postSimuAnnotations
    val ar = path match {
      case Some(s) => appendDir(s)
      case _       => args
    }

    mydebug(s"[FIRRTL] Running ${transforms.transforms} on module with metrics ${initialMap.toMap}")
    // don't run elaboration if no transform or post elaboration extractor was found
    transforms.requireElaboration match {
      case true =>
        // run metric extraction transforms, among other chisel generating transforms
        val generatedAnnos = (new ChiselStage).execute(
            Array("-X", "verilog") ++ ar,
            // remove all useless "Elaborating design" prints
            ChiselGeneratorAnnotation(() => m) +: 
            LogLevelAnnotation(QeceContext.logger.chiselLogLevel) +: 
            elabAnnotations
        )
        mydebug(s"[POSTELAB] Running ${transforms.postElaboration} on module with metrics ${initialMap.toMap}")
        // extract post elaboration metrics
        transforms.postElaboration
          .foldLeft(generatedAnnos) { case (annotations, ext) => ext.run(annotations) }
      case false => postSimuAnnotations
    }
  }

  /** Retrieve emission directory from arguments. */
  final protected def getEmissionDirectory(args: Array[String]): String = {
    val target = "--target-dir"
    args contains target match {
      case true  => args(args.indexOf("--target-dir") + 1)
      case false => QeceContext.emission.basePath
    }
  }

  /** Method to (recursively) handle elaboration exception. */
  private def manageException(e: Throwable, constructor: String, params: Seq[Int]): Unit = {
    def printStackTrace(e: Throwable): Unit = {
      logger.error(s"\t${e.getLocalizedMessage()}")
      logger.error(
          e.getStackTrace.take(e.getStackTrace.size min QeceContext.logger.stackDepth).mkString("\t\t", "\n\t\t", "")
      )
    }

    e match {
      // from chisel 3.4.3, StageError are replace by directly returning the IllegalArgumentException
      case e: java.lang.IllegalArgumentException =>
        timeError(
            s"[SKIPPING] Elaboration on top $constructor with params ${params.mkString("[", ", ", "]")} failed:"
        )
        printStackTrace(e)
      case e: firrtl.options.StageError =>
        timeError(
            s"[SKIPPING] Elaboration on top $constructor with params ${params.mkString("[", ", ", "]")} failed:"
        )
        printStackTrace(e)
      case e: qece.characterization.UnvalidEstimationException =>
        timeError(
            s"[SKIPPING] Unvalid estimation on top $constructor with params ${params.mkString("[", ", ", "]")}:"
        )
        printStackTrace(e)
      case e: qece.estimation.exceptions.EstimationFailedException =>
        timeError(
            s"[SKIPPING] Estimation on top $constructor with params ${params.mkString("[", ", ", "]")} failed:"
        )
        printStackTrace(e)
      case e: TimeoutException =>
        timeError(
            s"[SKIPPING] Timeout occured on top $constructor with params ${params.mkString("[", ", ", "]")}"
        )
        printStackTrace(e)
      case e: qece.estimation.exceptions.PostElabEstimationFailedException =>
        timeError(
            s"[SKIPPING] Post elaboration estimation on top $constructor " +
              s"with params ${params.mkString("[", ", ", "]")} failed:"
        )
        printStackTrace(e)
      case e: java.lang.reflect.InvocationTargetException =>
        manageException(e.getCause(), constructor, params)
      case e: Throwable =>
        timeError(
            "[SKIPPING] " +
              s"Unexpected exception happened on top $constructor with param ${params.mkString("[", ", ", "]")}:"
        )
        printStackTrace(e)
    }
  }

  /** Run estimation transforms.
   *
   *  @param T
   *    Type of the module being explored
   *  @param transforms
   *    estimation transforms to run
   *  @param point
   *    point to run estimation on
   *  @return
   *    estimation as a [[MetricMap]]
   */
  private def estimate[T: TypeTag](
      transforms: TransformSeq,
  )(point: Point): MetricMap = {
    val board       = FPGA.default
    val path        = QeceContext.emission.targetPath
    val args        = Array("--target-dir", QeceContext.emission.basePath)
    val constructor = new Constructor[T]
    // rebuild params from metrics as dimension reduction may have removed some params
    val params      = constructor.paramNames.map(n => point.metrics(n).toInt) 
    val emissionDir = s"$path/${constructor}_${params.mkString("-")}"
    mydebug(s"[ESTIMATION] Running on module with params ${params}")
    try {
      getMetrics(
          emitWithTransform(
              m = constructor.buildWith(params).asInstanceOf[MultiIOModule],
              initialMap = point.metrics,
              transforms = transforms,
              args = args,
              path = Some(emissionDir),
              board = Some(board)
          )
      )
    } catch {
      case e: Throwable =>
        manageException(e, constructor.toString, params)
        MetricMap.empty
    }
  }

  /** Run estimation transforms.
   *
   *  @tparam T
   *    Type of the module being explored
   *  @param transforms
   *    estimation transforms to run
   *  @param config
   *    configuration to use in the estimation process
   *  @param point
   *    point to run estimation on
   *  @return
   *    the result of the estimation(s) as a [[MetricMap]]
   */
  def estimate[T: TypeTag](
    transforms: TransformSeq,
    config: QeceConfig = QeceContext.value
  )(point: Point): MetricMap = QeceContext.withValue(config){ estimate[T](transforms)(point) }
}
