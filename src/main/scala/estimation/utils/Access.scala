/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.utils

import qece.estimation._
import qece.estimation.annotations._
import qece.estimation.macros._
import qece.estimation.timing._
import qece.estimation.resources._
import qece.estimation.hinters._
import qece.estimation.exceptions._

import qece.backend.FPGA
import qece.characterization.CharacterizedBoard

import java.io.File
import scala.io.Source

import firrtl.{AnnotationSeq, CircuitState, EmittedVerilogCircuitAnnotation}
import firrtl.ir.{Circuit, ExtModule}
import firrtl.options.{TargetDirAnnotation, OutputAnnotationFileAnnotation}

import firrtl.stage.FirrtlCircuitAnnotation

/** Trait used to access the resource system, both for write and reads */
//TODO: Refactor file in corresponding packages
//TODO: configure library file path
trait Access {

  /** Retrieve local resource directory as String path */
  private[qece] def getResourceDir: String = s"src/main/resources"

  private[qece] def getEstimatorPath(board: FPGA): String = s"$getResourceDir/${getEstimatorFile(board)}"

  //TODO: configure library file path
  private[qece] def getEstimatorFile(board: FPGA): String = s"lib/${board.getClass.getSimpleName}.json"

  /** Attempt to retrieve a resource at the given path.
   *
   *  It first attempts to locally retrieve the resource, then from the [[qece]] package embedded resources
   *
   *  @param path
   *    target path
   *  @return
   *    a Source content if the resource was found, None elsehow
   */
  private[qece] def getResource(path: String): Option[Source] = {
    val local = new File(s"$getResourceDir/$path")
    local.exists match {
      // attempt to retrieve local resource first
      case true => Some(Source.fromFile(local))
      // otherwise, attempt to retrieve from resource
      case false =>
        try {
          Source.fromInputStream(this.getClass.getClassLoader.getResourceAsStream(path)) match {
            case x if !x.isEmpty => Some(x)
            case _               => None
          }
        } catch {
          // resource was not found
          case e: Throwable => println(e); None
        }
    }
  }

  /** Get a macro updated Circuit from CircuitState annotations, if available. If not, retrieve the original circuit.
   *
   *  Used to run transform on a previously updated circuit, without changing emitted circuit (i.e. macro block
   *  detection before estimations)
   *  @param state
   *    state of circuit the transform is running on
   *  @return
   *    the updated circuit if found, the original one elsehow
   */
  private[qece] def getUpdatedCircuit(state: CircuitState): Circuit = {
    getUpdatedCircuit(state.annotations) match {
      case Some(c) => c
      case _       => state.circuit
    }
  }

  /** Get a Circuit from an AnnotationSeq, if found.
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @return
   *    the updated circuit if found
   */
  private[qece] def getUpdatedCircuit(annos: AnnotationSeq): Option[Circuit] = {
    annos.filter(_.isInstanceOf[MacroReplacedCircuitAnnotation]).collectFirst {
      case a: MacroReplacedCircuitAnnotation => a.circuit
    }
  }

  /** Get [[qece.estimation.macros.ReplaceInfo]] (s) from a CircuitState
   *
   *  @param state
   *    CircuitState to search in
   *  @return
   *    the replaced macros
   */
  private[qece] def getReplaceInfos(state: CircuitState): Seq[ReplaceInfo] = getReplaceInfos(state.annotations)

  /** Get [[qece.estimation.macros.ReplaceInfo]] (s) from an AnnotationSeq
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @return
   *    the replaced macros
   */
  private[qece] def getReplaceInfos(annos: AnnotationSeq): Seq[ReplaceInfo] = {
    annos.filter(_.isInstanceOf[MacroReplacedCircuitAnnotation]).collectFirst {
      case a: MacroReplacedCircuitAnnotation => a.infos
    } match {
      case Some(c) => c
      case _       => Seq.empty
    }
  }

  /** Get ExtModule (s) from a CircuitState
   *
   *  @param state
   *    CircuitState to search in
   *  @return
   *    the replaced macros
   */
  private[qece] def getMacroExtModules(state: CircuitState): Seq[ExtModule] = getMacroExtModules(state.annotations)

  /** Get ExtModule (s) from an AnnotationSeq
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @return
   *    the replaced macros
   */
  private[qece] def getMacroExtModules(annos: AnnotationSeq): Seq[ExtModule] = getReplaceInfos(annos).map(_.defModule)

  /* Attempt to retrieve the estimator from configuration file, if it exists
   *
   * @param board target board */
  private[qece] def getEstimatorsFromBoard(board: FPGA): CharacterizedBoard = {
    this.getResource(this.getEstimatorFile(board)) match {
      case Some(b) => CharacterizedBoard.decode(b)
      case _ =>
        throw ConfigurationNotFoundException(
            s"${this.getClass.getSimpleName}:\n" +
              s"\tNo estimator configuration file was found for board ${board.getClass.getSimpleName}.\n" +
              s"\tPlease build a ${CharacterizedBoard.getClass.getName} to generate it"
        )
    }
  }

  /** Get an estimator with respect to the target [[qece.backend.FPGA]] target, if found.
   *
   *  @param state
   *    state of the circuit the transform is running on
   *  @return
   *    an estimator set as a [[qece.characterization.CharacterizedBoard]] instance
   */
  private[qece] def getEstimators(state: CircuitState): CharacterizedBoard = {
    getBoard(state) match {
      case Some(b) => getEstimatorsFromBoard(b)
      case _       => throw BoardNotFoundException(s"${this.getClass.getName}: No board was found for this transform")
    }
  }

  /** Attempt to retrieve a [[qece.estimation.macros.MacroSeq]] from a circuit, through the boards estimators
   *
   *  @param state
   *    circuit to check in
   */
  private[qece] def getMacroSeq(state: CircuitState): MacroSeq = {
    getBoard(state) match {
      case Some(a) => getEstimatorsFromBoard(a).macroSeq
      case _       => MacroSeq.empty
    }
  }

  /** Retrieve additional macros to be replaced from annotations
   *
   *  Mainly used for test purposes
   *
   *  @param state
   *    circuit to look for annotations
   */
  private[qece] def getAdditionalMacros(state: CircuitState): MacroSeq = {
    state.annotations
      .filter(_.isInstanceOf[MacroAnnotation])
      .collectFirst {
        case a: MacroAnnotation => a.macros
      }
      .getOrElse(MacroSeq.empty)
  }

  /** Retrieve the target [[qece.backend.FPGA]] from a CircuitState, if found.
   *
   *  @param state
   *    state of the circuit the transform is running on
   *  @return
   *    an [[qece.backend.FPGA]] if found
   */
  private[qece] def getBoard(state: CircuitState): Option[FPGA] = getBoard(state.annotations)

  /** Retrieve the target [[qece.backend.FPGA]] from an AnnotationSeq, if found.
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @return
   *    an [[qece.backend.FPGA]] if found
   */
  private[qece] def getBoard(annos: AnnotationSeq): Option[FPGA] = {
    annos.filter(_.isInstanceOf[BoardAnnotation]).collectFirst {
      case a: BoardAnnotation => a.board
    }
  }

  /** Retrieve the generated hinters from an AnnotationSeq, if found.
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @return
   *    a Seq of hinters, if found
   */
  private[qece] def getHinters(annos: AnnotationSeq): Option[Seq[String]] = {
    annos.filter(_.isInstanceOf[HintersAnnotation]).collectFirst {
      case a: HintersAnnotation => a.hinters
    }
  }

  /** Get primitive report from a previous transform, if found.
   *
   *  @param state
   *    state of the circuit the transform is running on
   *  @return
   *    the primitive report if found
   */
  private[qece] def getPrimitives(state: CircuitState): CircuitPrimitives = getPrimitives(state.annotations)

  /** Get primitive report from a previous transform, if found.
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @return
   *    the primitive report if found
   */
  private[qece] def getPrimitives(annos: AnnotationSeq): CircuitPrimitives = {
    annos.filter(_.isInstanceOf[EmittedPrimitiveReportAnnotation]).collectFirst {
      case a: EmittedPrimitiveReportAnnotation => a.primitives
    } match {
      case Some(a) => a
      case _ =>
        throw PrimitiveReportNotFoundException(
            s"${this.getClass.getName}: No EmittedPrimitiveReportAnnotation found for this transform"
        )
    }
  }

  /** Get resource report from a previous transform, if found.
   *
   *  @param state
   *    state of the circuit the transform is running on
   *  @return
   *    the resource report if found
   */
  private[qece] def getResources(state: CircuitState): CircuitEstimatedResources = getResources(state.annotations)

  /** Get resource report from a previous transform, if found.
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @return
   *    the resource report if found
   */
  private[qece] def getResources(annos: AnnotationSeq): CircuitEstimatedResources = {
    annos.filter(_.isInstanceOf[EmittedResourceReportAnnotation]).collectFirst {
      case a: EmittedResourceReportAnnotation => a.resources
    } match {
      case Some(a) => a
      case _ =>
        throw ResourceReportNotFoundException(
            s"${this.getClass.getName}: No EmittedResouceReportAnnotation found for this transform"
        )
    }
  }

  /** Get endpoint report from a previous transform, if found.
   *
   *  @param state
   *    state of the circuit the transform is running on
   *  @return
   *    the endpoint report if found
   */
  private[qece] def getEndpoints(state: CircuitState): CircuitEndpoints = getEndpoints(state.annotations)

  /** Get endpoint report from a AnnotationSeq, if found.
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @return
   *    the endpoint report if found
   */
  private[qece] def getEndpoints(annos: AnnotationSeq): CircuitEndpoints = {
    annos.filter(_.isInstanceOf[EmittedEndpointReportAnnotation]).collectFirst {
      case a: EmittedEndpointReportAnnotation => a.endpoints
    } match {
      case Some(a) => a
      case _ =>
        throw EndpointReportNotFoundException(
            s"${this.getClass.getName}: No EmittedEndpointReportAnnotation found for this transform"
        )
    }
  }

  /** Get path report from a previous transform, if found.
   *
   *  @param state
   *    state of the circuit the transform is running on
   *  @return
   *    the path report if found
   */
  private[qece] def getPaths(state: CircuitState): CircuitPaths = getPaths(state.annotations)

  /** Get endpoint report from a AnnotationSeq, if found.
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @return
   *    the path report if found
   */
  private[qece] def getPaths(annos: AnnotationSeq): CircuitPaths = {
    annos.filter(_.isInstanceOf[EmittedCriticalPathReportAnnotation]).collectFirst {
      case a: EmittedCriticalPathReportAnnotation => a.paths
    } match {
      case Some(a) => a
      case _ =>
        throw CriticalPathReportNotFoundException(
            s"${this.getClass.getName}: No EmittedCriticalPathReportAnnotation found for this transform"
        )
    }
  }

  /** Get circuit from a AnnotationSeq, if found.
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @return
   *    the circuit if found
   */
  private[qece] def getCircuit(annos: AnnotationSeq): Circuit = {
    annos.filter(_.isInstanceOf[FirrtlCircuitAnnotation]).collectFirst {
      case a: FirrtlCircuitAnnotation => a.circuit
    } match {
      case Some(a) => a
      case _ =>
        throw CircuitNotFoundException(s"${this.getClass.getName}: No FirrtlCircuitAnnotation found for this transform")
    }
  }

  private[qece] def getTime = qece.Utils.getTime

  /** Retrieve the target directory from an AnnotationSeq
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @todo
   *    TODO add proper exception
   */
  private[qece] def getTargetDir(annos: AnnotationSeq): String = {
    annos.filter(_.isInstanceOf[TargetDirAnnotation]).collectFirst {
      case a: TargetDirAnnotation => a.directory
    } match {
      case Some(a) => a
      case _       => throw new UnsupportedOperationException(s"Did not found target directory")
    }
  }

  private[qece] def getTargetDir(state: CircuitState): String = getTargetDir(state.annotations)

  /** Retrieve the target filename from an AnnotationSeq
   *
   *  @param annos
   *    AnnotationSeq to search in
   *  @todo
   *    TODO add proper exception
   */
  private[qece] def getTargetFile(annos: AnnotationSeq): String = {
    annos.filter(_.isInstanceOf[OutputAnnotationFileAnnotation]).collectFirst {
      case a: OutputAnnotationFileAnnotation => a.file
    } match {
      case Some(a) => a
      case _       => throw new UnsupportedOperationException(s"Did not found any file name")
    }
  }

  private[qece] def getTargetFile(state: CircuitState): String = getTargetFile(state.annotations)

  private[qece] def getProfiling(annos: AnnotationSeq): MetricMap = {
    MetricMap(
        annos
          .filter(_.isInstanceOf[TimedAnnotation])
          .map(_.asInstanceOf[TimedAnnotation])
          .map(x => (x.getClass.getSimpleName -> new AnnotationMetric(x.duration))): _*
    )
  }

  private[qece] def getEmittedVerilog(annos: AnnotationSeq): String = {
    annos.filter(_.isInstanceOf[EmittedVerilogCircuitAnnotation]).collectFirst {
      case a: EmittedVerilogCircuitAnnotation => a.value
    } match {
      case Some(c) => c.value
      case _       => throw new UnsupportedOperationException(s"Can't get verilog")
    }
  }

  private[qece] def getMetrics(annos: AnnotationSeq): MetricMap = {
    annos
      .filter(_.isInstanceOf[MetricMapAnnotation])
      .collect {
        case a: MetricMapAnnotation => a.metrics
      }
      .reduceOption(_ ++& _)
      .getOrElse(MetricMap.empty) ++& getProfiling(annos)
  }
}
