/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.utils

import qece.QeceContext
import qece.estimation.transforms.SynchroLogger

import scala.concurrent._
import scala.concurrent.duration._

trait WithTimeout {
  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.global
  // basic duration is Infinite
  private[qece] val duration: String = "Inf"
  private[qece] val syncLogger       = new SynchroLogger("x")

  /** Enable to define timeout on some transforms, if needed */
  def exec[T](block: => T): T = {
    try {
      QeceContext.logger.enableSyncDebug match {
        case false =>
          Await.result(Future(block), Duration(duration))
        case true =>
          syncLogger.addTask(this.getClass.getSimpleName)
          val tmp = Await.result(Future(block), Duration(duration))
          syncLogger.removeTask(this.getClass.getSimpleName)
          tmp
      }
    } catch {
      case _: TimeoutException =>
        syncLogger.removeTask(this.getClass.getSimpleName)
        throw new TimeoutException(
            s"${this.getClass.getSimpleName} timed out after ${Duration(duration).toString}"
        )
      case e: java.util.concurrent.ExecutionException =>
        syncLogger.removeTask(this.getClass.getSimpleName)
        throw e.getCause()
    }
  }
}
