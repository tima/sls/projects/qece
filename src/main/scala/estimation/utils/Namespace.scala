/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.utils

case class Namespace(prefix: String) {
  private[qece] val counters = new scala.collection.mutable.HashMap[String, Int]()
  private[qece] var counter  = 0

  private[qece] def get: String = {
    val result = s"${prefix}_$counter"
    counter += 1
    result
  }

  private[qece] def get(second: String): String = {
    val value = counters contains second match {
      case true =>
        val tmp = counters(second)
        counters(second) = tmp + 1
        tmp
      case false =>
        counters(second) = 0
        0
    }
    s"${prefix}_${second}_$value"
  }
}
