/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.utils

import firrtl.annotations.{ModuleTarget, ReferenceTarget, TargetToken}
import firrtl.annotations.TargetToken.Field
import firrtl.ir.{Expression, Reference, SubField}

/** Utils to manipulate ReferenceTarget. */
object ReferenceTargetUtils {

  /** Retrieve a reference target from an expression
   *
   *  @param m
   *    ModuleTarget to base the reference in
   *  @param expr
   *    Expression to use for building
   *  @param sub
   *    subfields to add
   */
  private[qece] def getReferenceTarget(
      m: ModuleTarget,
      expr: Expression,
      sub: List[String] = List.empty
  ): Option[ReferenceTarget] = {
    def getRef(expr: Expression, l: List[String]): Option[(String, List[String])] = {
      expr match {
        case r: Reference => Some((r.name -> l))
        case s: SubField  => getRef(s.expr, l :+ s.name)
        case _            => None
      }
    }
    def buildReferenceTarget(ref: String, sub: List[String]): ReferenceTarget = {
      sub match {
        case h :: t => buildReferenceTarget(ref, t).field(h)
        case Nil    => m.ref(ref)
      }
    }
    getRef(expr, sub) match {
      case Some((ref, newSub)) => Some(buildReferenceTarget(ref, newSub))
      case _                   => None
    }
    // val (ref, newSub) = getRef(expr, sub)
    // buildReferenceTarget(ref, newSub)
  }

  /** Retrieve a ReferenceTarget recursively by adding subfields
   *
   *  @param ref
   *    base ReferenceTarget
   *  @param fields
   *    fields to add to the target
   */
  private[qece] def getReferenceTarget(ref: ReferenceTarget, fields: List[String]): ReferenceTarget = {
    fields match {
      case h :: t => getReferenceTarget(ref, t).field(h)
      case Nil    => ref
    }
  }

  /** Retrieve all fields of a ReferenceTarget as a seq of Strings
   *
   *  @param ref
   *    ReferenceTarget
   */
  private[qece] def getFields(ref: ReferenceTarget): Seq[String] = {
    ref.component.map(_.asInstanceOf[Field].value).reverse
  }

  /** Serialize a ReferenceTarget to a string, with only reference
   *
   *  @param ref
   *    Reference to serialize
   */
  private[qece] def getSerialized(ref: ReferenceTarget): String = {
    def accessField(component: TargetToken): String = {
      component match {
        case f: Field => f.value
        case _        => throw new UnsupportedOperationException(s"Can't access field on component $component")
      }
    }
    ref.component match {
      case Seq() => ref.ref
      case s     => s"${ref.ref}.${s.map(accessField).mkString(".")}"
    }
  }

  /** Build a ReferenceTarget from a ModuleTarget and a serialized target name
   *
   *  @param mt
   *    ModuleTarget to use
   *  @param name
   *    Serialized name of the ReferenceTarget to build, as of the meaning of the [[getSerialized]] method
   */
  private[qece] def deserialize(mt: ModuleTarget, name: String): ReferenceTarget = {
    def recurse(ref: ReferenceTarget, fields: Seq[String]): ReferenceTarget = {
      fields match {
        case Seq(s) => ref.field(s)
        case s      => recurse(ref.field(s.head), s.drop(1))
      }
    }
    name.split('.').toSeq match {
      case Seq(s) => mt.ref(s)
      case s      => recurse(mt.ref(s.head), s.drop(1))
    }
  }
}
