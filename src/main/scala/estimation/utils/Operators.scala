/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.utils

import qece.estimation.exceptions.OperatorNotFoundException

import firrtl.ir._
import firrtl.{PrimOps, bitWidth}
import firrtl.{Dshlw, Addw, Subw}

/** Operators utils. */
object Operators {

  /** Kind of operators used
   *
   *  Should use case object for every case, except macro definition
   *
   *  @param name
   *    name of the operator kind
   *  @param caption
   *    caption to print
   */
  sealed abstract class Operator(val name: String, val caption: String) {
    override def toString = name

    def isWire   = noCost contains this
    def isWireR  = wireR contains this
    def isCostly = withCost contains this
  }

  case object ADD    extends Operator("add", "Adder")
  case object REG    extends Operator("reg", "Register")
  case object MULT   extends Operator("mult", "Multiplier")
  case object BIN    extends Operator("binOp", "Binary Operator")
  case object MUX    extends Operator("mux", "Multiplexer")
  case object COMP   extends Operator("comp", "Comparaison Operator")
  case object DSHIFT extends Operator("dshift", "Dynamic Shift")

  // No cost operators
  case object SSHIFT  extends Operator("sshift", "Static Shift")
  case object SELECT  extends Operator("select", "Selection")
  case object PAD     extends Operator("pad", "Padding")
  case object CAT     extends Operator("cat", "Concatenation")
  case object CONVERT extends Operator("convert", "Convertion")

  // Memory
  case object MEMORY extends Operator("memory", "Memory")

  // Operators used for transforms
  // Empty operator
  case object NOP extends Operator("nop", "NOP")

  case object MACRO extends Operator("macro", "Macro")

  case object CONNECT    extends Operator("connect", "Connect")
  case object IO         extends Operator("io", "IO")
  case object CONST      extends Operator("const", "Const")
  case object UNRESOLVED extends Operator("unresolved", "Unresolved")

  /** Define all costly operator kinds known */
  val withCost = Seq(ADD, REG, MULT, BIN, MUX, COMP, DSHIFT, MEMORY)

  /** Base operators to characterize */
  val basicOperators = withCost.filter(_ != MEMORY)

  /** Define all non costly operator kinds known */
  val noCost = Seq(SSHIFT, SELECT, PAD, CAT, IO, CONST, CONVERT, CONNECT)

  val wireR = Seq(SELECT, CONNECT)

  /** Retrieve an operator from its name, if known */
  def get(name: String): Operator = {
    name match {
      case "add"        => ADD
      case "reg"        => REG
      case "mult"       => MULT
      case "binOp"      => BIN
      case "mux"        => MUX
      case "comp"       => COMP
      case "dshift"     => DSHIFT
      case "sshift"     => SSHIFT
      case "select"     => SELECT
      case "cat"        => CAT
      case "nop"        => NOP
      case "pad"        => PAD
      case "memory"     => MEMORY
      case "io"         => IO
      case "const"      => CONST
      case "connect"    => CONNECT
      case "unresolved" => UNRESOLVED
      case "macro"      => MACRO
      case _            => throw OperatorNotFoundException(s"Operator with name $name is unknown")
    }
  }

  def maxType(args: Seq[Expression]): Type = {
    args.sortWith((x, y) => bitWidth(x.tpe) > bitWidth(y.tpe)).head.tpe
  }

// scalastyle:off method.length
  /** Build an [[Operator]] from a DoPrim expression, along with the dataType
   *
   *  @param d
   *    DoPrim representation of this operator
   *  @todo
   *    TODO: dynamic shift : use control signal instead ?
   */
  def fromDoPrim(d: DoPrim): (Operator, Type) = {
    d.op match {
      // conversions
      case PrimOps.AsUInt => (CONVERT, UIntType(IntWidth(bitWidth(d.args.head.tpe))))
      case PrimOps.AsSInt => (CONVERT, SIntType(IntWidth(bitWidth(d.args.head.tpe))))
      // no inherent cost (static padding)
      case PrimOps.Pad => (PAD, d.tpe)
      // no inherent cost (static shift)
      case PrimOps.Shl => (SSHIFT, d.tpe)
      case PrimOps.Shr => (SSHIFT, d.tpe)
      // no inherent cost (static concatenation)
      case PrimOps.Cat => (CAT, d.tpe)
      // no inherent cost (static sub assignment)
      case PrimOps.Head => (SELECT, d.tpe)
      case PrimOps.Tail => (SELECT, d.tpe)
      case PrimOps.Bits => (SELECT, d.tpe)
      case Dshlw        => (DSHIFT, d.args(0).tpe)
      case PrimOps.Dshl => (DSHIFT, d.args(0).tpe)
      case PrimOps.Dshr => (DSHIFT, d.args(0).tpe)
      case Addw         => (ADD, maxType(d.args))
      case Subw         => (ADD, maxType(d.args))
      case PrimOps.Add  => (ADD, maxType(d.args))
      case PrimOps.Sub  => (ADD, maxType(d.args))
      case PrimOps.Mul  => (MULT, maxType(d.args))
      case PrimOps.And  => (BIN, maxType(d.args))
      case PrimOps.Or   => (BIN, maxType(d.args))
      case PrimOps.Xor  => (BIN, maxType(d.args))
      case PrimOps.Not  => (BIN, maxType(d.args))
      case PrimOps.Eq   => (COMP, maxType(d.args))
      case PrimOps.Neq  => (COMP, maxType(d.args))
      case PrimOps.Leq  => (COMP, maxType(d.args))
      case PrimOps.Geq  => (COMP, maxType(d.args))
      case PrimOps.Lt   => (COMP, maxType(d.args))
      case PrimOps.Gt   => (COMP, maxType(d.args))
      case op           => throw OperatorNotFoundException(s"[TODO] PrimOp -> ${op.getClass.getName}: " + d.serialize)
    }
  }
// scalastyle:on method.length
}
