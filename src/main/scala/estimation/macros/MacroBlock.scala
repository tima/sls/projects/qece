/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros

import scala.reflect.runtime.universe.TypeTag

import qece.backend.FPGA

import qece.exploration.utils.Constructor

import qece.estimation.graph.ModuleGraph
import qece.estimation.macros.exceptions._
import qece.estimation.macros.components._

import chisel3.MultiIOModule

/** Class for AnnotatedModule implementing Product trait for reflectivity.
 *
 *  @param bitWidth
 *    bitWidth will be used for characterization, it MUST BE the first argument of the constructor
 */
abstract class AnnotatedModule(val bitWidth: Int) extends MultiIOModule with Product

/** Useful for returning an [[EmptyMacroBlock]] object. */
class EmptyAnnotatedModule extends AnnotatedModule(0) {
  def canEqual(that: Any): Boolean = false
  def productArity: Int            = 0
  def productElement(n: Int): Any  = Unit
}

/** Used for compilation. */
object EmptyMacroBlock
    extends MacroBlock[EmptyAnnotatedModule](
        argDim = 0,
        linearBitwidth = false,
        getCapacity = { _ => None }
    ) {
  def findAndReplacePattern(graph: ModuleGraph): (ModuleGraph, Seq[ReplaceInfo]) = (graph -> Seq.empty)
}

/** [[MacroBlock]] utility for listing all available macros, for deserialization purpose. */
object MacroBlock {
  private val list = Map(
      (MacUnit).className         -> MacUnit,
      (MemoryBlock).className     -> MemoryBlock,
      (MacroMux).className        -> MacroMux,
      (EmptyMacroBlock).className -> EmptyMacroBlock
  )
  def apply(name: String): MacroBlock[_] = {
    list.get(name) match {
      case Some(x) => x
      case _       => throw UnknownMacroException(s"Unknown macro $name")
    }
  }
}

/** Meta module which allows retrieving parameter annotations to build design space.
 *
 *  HasPattern trait is used to specify that [[MacroBlock]] must implement a pattern matching function
 *
 *  @param argDim
 *    number of arguments used in estimation
 *  @param firstArg
 *    first arg used for estimation - to be used to remove bitwidth from estimation
 *  @param getCapacity
 *    get maximum bitwidth usable in macroblock, as a power of two used for memory characterization limitation
 */
abstract class MacroBlock[T: TypeTag](
    val argDim: Int,
    val linearBitwidth: Boolean,
    val getCapacity: (FPGA => Option[Int])
) extends Constructor
    with CanReplacePattern
