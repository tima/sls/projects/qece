/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros.components

import qece.estimation.utils._
import qece.estimation.graph._
import qece.estimation.macros.{MacroBlock, EmptyAnnotatedModule, ReplaceInfo}
import qece.estimation.hinters.WithHinters

import firrtl.ir.{NoInfo, ExtModule, IntParam, DefInstance}
import firrtl.{SourceFlow, SinkFlow, bitWidth}

import scala.collection._

// TODO: add a characterization utility with method based on width input of LUT, instead of synthesis result.
// Still use result on Bin basic operator generation for timing
// need to experiment on chisel generation before
/** Define pattern replacement methods as defined in MacroBlock class through HasPattern trait */
private[qece] object LutBlock
    extends MacroBlock[EmptyAnnotatedModule](
        argDim = 1,
        linearBitwidth = false,
        getCapacity = { _ => None }
    )
    with WithHinters {
// scalastyle:off method.length
  def findAndReplacePattern(graph: ModuleGraph): (ModuleGraph, Seq[ReplaceInfo]) = {
    val ns = Namespace("lut")
    def getToRemove(from: OperatorNode): (SubGraph, OperatorNode, ReplaceInfo) = {
      val utils = GraphUtils()
      // first input is condition, can't use it to chain muxes
      val last = utils.findOperatorGroup(Operators.BIN)(from)
      // mux groups detected are fully combinatorial, thus we can detect inputs this way as we won't have loops in it
      val signals =
        utils.toRemove
          .map(_.sources.zipWithIndex.filter { case (_, i) => i != 0 }.map(_._1))
          .reduce(_ ++ _)
          .diff(utils.toRemove.map(_.sinks).reduce(_ ++ _))
      val signalWidth = signals.map(x => bitWidth(x.tpe)).reduce(_ + _)
      val subGraph    = SubGraph(utils.toRemove, signals, last.sinks)

      // name IO
      val inputs =
        subGraph.mapInputs(signals.zipWithIndex.map { case (_, i) => s"in_$i" })
      val outputs = subGraph.mapOutputs(Seq("out"))

      // define mac parameters from GraphUtils for instanciation
      val params = List(
          IntParam("bitwidth", signalWidth)
      )

      val defModuleName = buildName(params.map(_.value))
      val defModule = ExtModule(
          NoInfo,
          defModuleName,
          (inputs ++ outputs).map(_._1).to[Vector],
          defModuleName,
          params
      )

      val instModule = DefInstance(NoInfo, ns.get, defModuleName, last.tpe)
      val opNode = OperatorNode(
          instModule,
          firrtl.annotations.ModuleTarget("", from.ref.module).ref(instModule.name),
          Operators.MACRO,
          last.tpe
      )
      // build connects
      val connectIn = inputs.map {
        case (n, i) => SignalNode.connectIn(SignalNode.instanciate(instModule, n, SinkFlow), i.node)
      }
      val connectOut = outputs.map {
        case (n, o) => SignalNode.connectOut(o.node, SignalNode.instanciate(instModule, n, SourceFlow))
      }
      val info = ReplaceInfo(
          this,
          from.ref.module,
          defModule,
          instModule,
          true,
          false,
          connectIn ++ connectOut,
          Seq.empty,
          Map.empty,
          subGraph.getFirrtlNodes
      )
      (subGraph, opNode, info)
    }

    // search and replace muxes by mux blocks, for compact mux groups
    graph
      .getOperators(Operators.BIN)
      .foldLeft((graph, Seq[ReplaceInfo]())) {
        case ((myGraph, repInfos), node) =>
          repInfos.containNode(node) match {
            // replace only if original node was not already absorbed in another muxblock
            case true => (myGraph, repInfos)
            case false =>
              val (sub, op, repInfo) = getToRemove(node)
              (myGraph.replace(sub, op), repInfos :+ repInfo)
          }
      }
  }
// scalastyle:on method.length
}
