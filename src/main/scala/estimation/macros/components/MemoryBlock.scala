/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros.components

import qece.QeceContext

import qece.exploration.annotations._

import qece.estimation.utils.{Operators, Namespace, FirrtlUtils}
import qece.estimation.hinters.WithHinters

import qece.estimation.graph.{
  ModuleGraph,
  OperatorNode,
  MemoryNode,
  SignalNode,
  SubGraph,
  GraphUtils,
  EmptyOperatorNode,
  ZeroNode
}

import qece.estimation.macros.{MacroBlock, AnnotatedModule, ReplaceInfo}
import qece.estimation.macros.exceptions._

import chisel3._
import chisel3.util._

import firrtl.ir.{NoInfo, ExtModule, IntParam, DefInstance, DefWire, FirrtlNode, SubField, Reference, DefRegister}
import firrtl.{SourceFlow, SinkFlow, InstanceKind, Kind, WireKind}

import scala.collection._

/** Chisel representaiton of a Memory macro
 *
 *  @param elemWidth
 *    width of elements used in this memory block
 *  @param nAddr
 *    number of available addresses in this memory
 *  @param nReg
 *    number of register to be absorbed in this memory block
 */
private[qece] case class BankedMemory(
    elemWidth: Int,
    @pow2(0, QeceContext.characterization.maxMemoryBlockAddr) nAddr: Int,
    @linear(0, 1) nReg: Int
) extends AnnotatedModule(elemWidth) {

  val addrIn = IO(Input(UInt(log2Up(nAddr).W)))
  val dataIn = IO(Input(UInt(elemWidth.W)))
  val wen    = IO(Input(Bool()))

  val addrOut = IO(Input(UInt(log2Up(nAddr).W)))
  val dataOut = IO(Output(UInt(elemWidth.W)))
  val ren     = IO(Input(Bool()))

  val memory = SyncReadMem(nAddr, UInt(elemWidth.W))

  when(wen) { memory.write(addrIn, dataIn) }
  dataOut := memory.read(addrOut, ren)
}

/** Define pattern replacement methods as defined in MacroBlock class through HasPattern trait */
object MemoryBlock
    extends MacroBlock[BankedMemory](
        argDim = 2,
        linearBitwidth = false,
        getCapacity = { board => Some(board.maxMemory) }
    )
    with WithHinters {

// scalastyle:off method.length
  def findAndReplacePattern(graph: ModuleGraph): (ModuleGraph, Seq[ReplaceInfo]) = {
    val ns = Namespace("memory")
    def getToRemove(from: OperatorNode): (SubGraph, OperatorNode, ReplaceInfo) = {
      // cast the operator node to a memory node, to access fields
      val realNode   = from.asInstanceOf[MemoryNode]
      val changeKind = new scala.collection.mutable.HashMap[String, Kind]()

      def isSubField(node: FirrtlNode): Boolean = {
        node match {
          case s: SubField  => isSubField(s.expr)
          case r: Reference => r.name == realNode.mem
          case _            => false
        }
      }

      // absorb pipelining registers to macro, if possible
      val addrOutReg = realNode.addrOut.source match {
        case Some(s) =>
          s.operator match {
            case Operators.REG =>
              // removing registers, thus marking the reference as WireKind instead of RegKind
              changeKind += (s.node.asInstanceOf[DefRegister].name -> WireKind)
              Some(s)
            case _ => None
          }
        case None => throw NoneSourceException(s"Did not found addrOut source")
      }

      // absorb pipelining registers to macro, if possible
      val renReg = realNode.ren.source match {
        case Some(s) =>
          s.operator match {
            case Operators.REG =>
              // removing registers, thus marking the reference as WireKind instead of RegKind
              changeKind += (s.node.asInstanceOf[DefRegister].name -> WireKind)
              Some(s)
            case _ => None
          }
        case None => throw NoneSourceException(s"Did not found ren source")
      }

      // patterns to recognize
      val patterns = Seq((Operators.MEMORY -> 1), (Operators.REG -> getMax("nReg")))

      // instanciate a new GraphUtils to record nodes to remove
      val utils = GraphUtils()

      // traverse graph to fill GraphUtils
      val func = patterns.map { case (op, max) => utils.findOperatorSequence(op, max)(_) }
      func.foldLeft(from)((node, func) => func(node))
      val pattern = (patterns.map(_._1) zip utils.details).map { case (op, detail) => (op -> detail) }

      // Retrieve io of the macro, to build the subgraph
      val subIn = Seq(realNode.addrIn, realNode.dataIn, realNode.wen) ++ (addrOutReg match {
        case Some(s) => s.sources
        case None    => Seq(realNode.addrOut)
      }) ++ (renReg match {
        case Some(s) => s.sources
        case None    => Seq(realNode.ren)
      })
      val last = utils.lastAdded
      val subGraph = SubGraph(
          Seq(addrOutReg.getOrElse(EmptyOperatorNode), renReg.getOrElse(EmptyOperatorNode))
            .filter(_ != EmptyOperatorNode) ++ utils.toRemove,
          subIn,
          last.sinks
      )

      // name IO
      val inputs  = subGraph.mapInputs(Seq("addrIn", "dataIn", "wen", "addrOut", "ren"), ZeroNode)
      val outputs = subGraph.mapOutputs(Seq("dataOut"))

      // define mac parameters from GraphUtils for instanciation
      val params = List(
          IntParam("elemWidth", pattern.head._2._2),
          IntParam("nAddr", realNode.depth),
          IntParam("nReg", pattern.last._2._1)
      )

      // build ExtModule to replace macro
      val defModuleName = buildName(params.map(_.value))
      val defModule = ExtModule(
          NoInfo,
          defModuleName,
          (inputs ++ outputs).map(_._1).to[Vector],
          defModuleName,
          params
      )

      // build DefInstance to replace macro
      val instModule = DefInstance(NoInfo, ns.get, defModuleName, last.tpe)

      // build OperatorNode to replace subgraph in graph
      val opNode = OperatorNode(instModule, subGraph.mt.ref(instModule.name), Operators.MACRO, last.tpe)

      // build connects
      val connectIn = inputs.map {
        case (n, i) =>
          SignalNode.connectIn(
              SignalNode.instanciate(instModule, n, SinkFlow),
              FirrtlUtils.changeReferenceKind(i.node, InstanceKind)
          )
      }
      val connectOut = outputs.map {
        case (n, o) =>
          SignalNode.connectOut(
              FirrtlUtils.changeReferenceKind(o.node, InstanceKind),
              SignalNode.instanciate(instModule, n, SourceFlow)
          )
      }

      // identify signals to declare
      val defWires = (inputs ++ outputs).filter { case (_, signal) => isSubField(signal.node) }.map {
        case (_, signal) => DefWire(NoInfo, signal.name, signal.tpe)
      }

      // build ReplaceInfo for circuit updating
      // TODO: when register detected, specify that out -> reg might be the critical path
      // TODO: change true to pattern.last._2._1 == 0
      val info = ReplaceInfo(
          this,
          from.ref.module,
          defModule,
          instModule,
          false,
          true,
          connectIn ++ connectOut,
          defWires,
          changeKind.toMap,
          subGraph.getFirrtlNodes
      )

      (subGraph, opNode, info)
    }
    // search and replace every memory by a memory block
    graph
      .getOperators(Operators.MEMORY)
      .foldLeft((graph, Seq[ReplaceInfo]())) {
        case ((myGraph, repInfos), node) =>
          val (sub, op, repInfo) = getToRemove(node)
          (myGraph.replace(sub, op), repInfos :+ repInfo)
      }
  }
// scalastyle:on method.length
}
