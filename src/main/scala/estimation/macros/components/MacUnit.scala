/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros.components

import qece.QeceContext

import qece.exploration.annotations._

import qece.estimation.utils.{Operators, Namespace}
import qece.estimation.hinters.WithHinters

import qece.estimation.graph.{ModuleGraph, OperatorNode, SignalNode, SubGraph, GraphUtils, ZeroNode}
import qece.estimation.macros.{MacroBlock, AnnotatedModule, ReplaceInfo}

import chisel3._

import firrtl.ir.{NoInfo, ExtModule, IntParam, DefInstance}
import firrtl.{SourceFlow, SinkFlow}

import scala.collection._

/** Chisel representation of a Mac unit
 *
 *  @param inWidth
 *    input width of the operators
 *  @param outFactor
 *    output width
 *  @param nbRegMul
 *    number of register between mult and add operation
 *  @param add
 *    number of add operator
 *  @param nbRegAdd
 *    number of register after add
 */
private[qece] case class Mac(
    inWidth: Int,
    @linear(1, 2) outFactor: Int,
    @linear(0, QeceContext.characterization.maxDspRegister) nbRegMul: Int,
    @linear(0, 1) add: Int,
    @linear(0, 1) nbRegAdd: Int
) extends AnnotatedModule(inWidth) {
  val op1 = IO(Input(UInt(inWidth.W)))
  val op2 = IO(Input(UInt(inWidth.W)))
  val op3 = IO(Input(UInt(inWidth.W)))
  val res = IO(Output(UInt((inWidth * outFactor).W)))

  val mulRes = if (nbRegMul > 0) Reg(Vec(nbRegMul, UInt())) else Wire(Vec(1, UInt()))

  mulRes(0) := op1 * op2
  for (i <- 0 until nbRegMul - 1) {
    mulRes(i + 1) := mulRes(i)
  }

  if (add > 0) {
    val addRes = if (nbRegAdd > 0) Reg(Vec(nbRegAdd, UInt())) else Wire(Vec(1, UInt()))
    addRes(0) := op3 + mulRes(0 max (nbRegMul - 1))
    for (i <- 0 until nbRegAdd - 1) {
      addRes(i + 1) := addRes(i)
    }
    res := addRes(0 max (nbRegAdd - 1))
  } else {
    res := mulRes(0 max (nbRegMul - 1))
  }
}

/** Define pattern replacement methods as defined in MacroBlock class through HasPattern trait */
object MacUnit
    extends MacroBlock[Mac](
        argDim = 1,
        linearBitwidth = false,
        getCapacity = { _ => None }
    )
    with WithHinters {
// scalastyle:off method.length
  def findAndReplacePattern(graph: ModuleGraph): (ModuleGraph, Seq[ReplaceInfo]) = {
    val ns = Namespace("mac")
    def getToRemove(from: OperatorNode): (SubGraph, OperatorNode, ReplaceInfo) = {
      // patterns to recognize
      val patterns = Seq(
          (Operators.MULT -> 1),
          (Operators.REG  -> getMax("nbRegMul")),
          (Operators.ADD  -> getMax("add")),
          (Operators.REG  -> getMax("nbRegAdd"))
      )

      // instanciate a new GraphUtils to record nodes to remove
      val utils = GraphUtils()

      // traverse graph to fill GraphUtils
      val func = patterns.map { case (op, max) => utils.findOperatorSequence(op, max)(_) }
      func.foldLeft(from)((node, func) => func(node))
      val pattern = (patterns.map(_._1) zip utils.details).map { case (op, detail) => (op -> detail) }

      // Retrieve io of the macro, to build the subgraph
      val index   = if (utils.sourceNodes(2) == utils.sourceNodes(1)) 0 else 1
      val withAdd = pattern(2)._2._1 != 0
      val subIn =
        if (withAdd) {
          utils.sourceNodes.head.sources ++ utils.sourceNodes(2).sources diff utils.sourceNodes(index).sinks
        } else {
          utils.sourceNodes.head.sources
        }
      val last     = utils.lastAdded
      val subGraph = SubGraph(utils.toRemove, subIn, last.sinks)

      // name IO
      val inputs  = subGraph.mapInputs(Seq("op1", "op2", "op3"), ZeroNode)
      val outputs = subGraph.mapOutputs(Seq("out"))

      val outFactor = last.width.toInt / pattern.head._2._2

      // define mac parameters from GraphUtils for instanciation
      val params = List(
          IntParam("inWidth", pattern.head._2._2),
          IntParam("outFactor", outFactor),
          IntParam("nbRegMul", pattern(1)._2._1),
          IntParam("add", pattern(2)._2._1),
          IntParam("nbRegAdd", pattern(3)._2._1)
      )

      // build ExtModule to replace macro
      val defModuleName = buildName(params.map(_.value))
      val defModule = ExtModule(
          NoInfo,
          defModuleName,
          (inputs ++ outputs).map(_._1).to[Vector],
          defModuleName,
          params
      )

      // build DefInstance to replace macro
      val instModule = DefInstance(NoInfo, ns.get, defModuleName, last.tpe)

      // build OperatorNode to replace subgraph in graph
      val opNode = OperatorNode(instModule, subGraph.mt.ref(instModule.name), Operators.MACRO, last.tpe)

      // build connects
      val connectIn = inputs.map {
        case (n, i) => SignalNode.connectIn(SignalNode.instanciate(instModule, n, SinkFlow), i.node)
      }
      val connectOut = outputs.map {
        case (n, o) => SignalNode.connectOut(o.node, SignalNode.instanciate(instModule, n, SourceFlow))
      }

      // build ReplaceInfo for circuit updating
      val info = ReplaceInfo(
          this,
          from.ref.module,
          defModule,
          instModule,
          ((pattern(1)._2._1 == 0) && (pattern(3)._2._1 == 0)),
          false,
          connectIn ++ connectOut,
          Seq.empty,
          Map.empty,
          subGraph.getFirrtlNodes
      )

      (subGraph, opNode, info)
    }

    // search and replace every mult by a macunit, replacing every mult when found
    graph
      .getOperators(Operators.MULT)
      .foldLeft((graph, Seq[ReplaceInfo]())) {
        case ((myGraph, repInfos), node) =>
          val (sub, op, repInfo) = getToRemove(node)
          (myGraph.replace(sub, op), repInfos :+ repInfo)
      }
  }
// scalastyle:on method.length
}
