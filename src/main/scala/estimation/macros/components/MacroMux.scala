/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros.components

import qece.QeceContext

import qece.exploration.annotations._

import qece.estimation.utils._
import qece.estimation.graph._
import qece.estimation.macros.{MacroBlock, AnnotatedModule, ReplaceInfo}
import qece.estimation.hinters.WithHinters

import chisel3._

import firrtl.ir.{NoInfo, ExtModule, IntParam, DefInstance}
import firrtl.{SourceFlow, SinkFlow, bitWidth}

import scala.collection._
import math._

/** Chisel representation of a binary multiplexor tree
 *
 *  @param bitwidth
 *    input width of the operators
 *  @param nbInput
 *    number of distinct inputs for this mux
 *  @param nbCond
 *    number of distinct conditions
 *  @deprecated
 *    should not be used, does not converge atm
 */
private[qece] case class MuxModule(
    bitwidth: Int,
    @pow2(1, QeceContext.characterization.maxMuxBlockInput) nbInput: Int,
    @pow2(0, QeceContext.characterization.maxMuxBlockInput) nbCond: Int
) extends AnnotatedModule(bitwidth) {
  require(nbCond >= (log(nbInput) / log(2)).ceil.toInt, s"Condition bits ($nbCond) must be greater than log2($nbInput)")
  require(nbCond <= nbInput, s"Condition bits ($nbCond) must be lower than $nbInput")
  val inVec   = IO(Input(Vec(nbInput, UInt(bitWidth.W))))
  val condVec = IO(Input(Vec(nbCond, Bool())))
  val result  = IO(Output(UInt(bitWidth.W)))

// scalastyle:off method.length
  def muxTree[T <: Data](inputs: IndexedSeq[T], conds: IndexedSeq[Bool]): T = {
    val inputSize = inputs.size
    val condSize  = conds.size
    assert(
        condSize >= (log(inputSize) / log(2)).ceil.toInt,
        s"Condition bits ($condSize) must be greater than log2($inputSize)"
    )
    assert(condSize <= inputSize, s"Condition bits ($condSize) must be lower than $inputSize")
    if (1 << condSize == inputSize) {
      // logarithmic case: we got n inputs for log2(n) conditions
      inputSize match {
        case 1 => inputs.head
        case 2 => Mux(conds.last, inputs.head, inputs.last)
        case _ =>
          Mux(
              conds.last,
              muxTree(inputs.take(inputSize / 2), conds.dropRight(1)),
              muxTree(inputs.takeRight(inputSize / 2), conds.dropRight(1))
          )
      }
    } else if (condSize == inputSize) {
      //TODO manage one hot encoding, but it can not be implemented using 2v1 muxes
      // one hot encoding: we got n conditions for n inputs
      // instead, drop a condition
      muxTree(inputs, conds.dropRight(1))
    } else {
      // supra logarithmic case: we got more than log2(n) conditions for n inputs, but less than n
      val rightCondSize = max((log((inputSize / 2.0).ceil) / log(2)).ceil.toInt, condSize / 2)
      val leftCondSize  = max((log((inputSize / 2.0).floor) / log(2)).ceil.toInt, condSize / 2)
      Mux(
          conds.last,
          muxTree(inputs.take((inputSize / 2.0).floor.toInt), conds.dropRight(1).take(leftCondSize)),
          muxTree(inputs.takeRight((inputSize / 2.0).ceil.toInt), conds.dropRight(1).takeRight(rightCondSize))
      )
    }
  }
// scalastyle:on method.length

  result := muxTree(inVec, condVec)
}

/** Define pattern replacement methods as defined in MacroBlock class through HasPattern trait */
object MacroMux
    extends MacroBlock[MuxModule](
        argDim = 2,
        linearBitwidth = true,
        getCapacity = { _ => None }
    )
    with WithHinters {
// scalastyle:off method.length
  def findAndReplacePattern(graph: ModuleGraph): (ModuleGraph, Seq[ReplaceInfo]) = {
    val ns = Namespace("muxBlock")
    def getToRemove(from: OperatorNode): (SubGraph, OperatorNode, ReplaceInfo) = {
      val utils = GraphUtils()
      // first input is condition, can't use it to chain muxes
      // TODO: limit number of inputs found to max size of MuxModule, to allow composition instead of triggering
      // exception, and allow a better estimation
      val last = utils.findOperatorGroup(Operators.MUX, Seq(0))(from)
      val conds =
        utils.toRemove
          .flatMap {
            case node =>
              node.sources.zipWithIndex.filter {
                case (_, i) => (node.operator == Operators.MUX) && (i == 0)
              }
          }
          .map(_._1)
          .deepDistinct
      // utils.toRemove.filter(_.operator == Operators.MUX)
      //   .map(_.sources.zipWithIndex.filter{case (_, i) => i == 0}.map(_._1)).reduce(_ ++ _).deepDistinct
      // mux groups detected are fully combinatorial, thus we can detect inputs this way as we won't have loops in it
      val signals =
        utils.toRemove
          .map {
            case node =>
              node.sources.zipWithIndex
                .filter {
                  case (_, i) => (node.operator != Operators.MUX) || (i != 0)
                }
                .map(_._1)
          }
          .reduce(_ ++ _)
          .diff(utils.toRemove.map(_.sinks).reduce(_ ++ _))
      val condWidth = conds.map(x => bitWidth(x.tpe)).reduce(_ + _)

      // assert((condWidth <= signals.size) && (condWidth >= (log(signals.size)/log(2)).ceil.toInt),
      //   s"Found $condWidth condition bits for ${signals.size} inputs")

      val name        = ns.get
      val signalWidth = signals.map(x => bitWidth(x.tpe)).reduce(_ max _)
      val subGraph =
        SubGraph(utils.toRemove, conds ++ signals, last.sinks)

      // name IO
      val inputs =
        subGraph.mapInputs(
            conds.zipWithIndex.map { case (_, i) => s"cond_$i" } ++
              signals.zipWithIndex.map { case (_, i) => s"in_$i" }
        )
      val outputs = subGraph.mapOutputs(Seq("out"))

      // define mac parameters from GraphUtils for instanciation
      val params = List(
          IntParam("bitwidth", signalWidth),
          IntParam("nbInput", signals.size),
          IntParam("nbCond", condWidth)
      )

      val defModuleName = buildName(params.map(_.value))
      val defModule = ExtModule(
          NoInfo,
          defModuleName,
          (inputs ++ outputs).map(_._1).to[Vector],
          defModuleName,
          params
      )

      val instModule = DefInstance(NoInfo, name, defModuleName, last.tpe)
      val opNode = OperatorNode(
          instModule,
          firrtl.annotations.ModuleTarget("", from.ref.module).ref(instModule.name),
          Operators.MACRO,
          last.tpe
      )

      // build connects
      val connectIn = inputs.map {
        case (n, i) => SignalNode.connectIn(SignalNode.instanciate(instModule, n, SinkFlow), i.node)
      }
      val connectOut = outputs.map {
        case (n, o) => SignalNode.connectOut(o.node, SignalNode.instanciate(instModule, n, SourceFlow))
      }
      val info = ReplaceInfo(
          this,
          from.ref.module,
          defModule,
          instModule,
          true,
          false,
          connectIn ++ connectOut,
          Seq.empty,
          Map.empty,
          subGraph.getFirrtlNodes
      )

      (subGraph, opNode, info)
    }

    // search and replace muxes by mux blocks, for compact mux groups
    graph
      .getOperators(Operators.MUX)
      .foldLeft((graph, Seq[ReplaceInfo]())) {
        case ((myGraph, repInfos), node) =>
          repInfos.containNode(node) match {
            // replace only if original node was not already absorbed in another muxblock
            case true => (myGraph, repInfos)
            case false =>
              val (sub, op, repInfo) = getToRemove(node)
              (myGraph.replace(sub, op), repInfos :+ repInfo)
          }
      }
  }
// scalastyle:on method.length
}
