/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros

import qece.estimation.macros.exceptions.ModuleAlreadyDefinedException
import qece.estimation.utils.FirrtlUtils

import firrtl.ir._
import firrtl.{EmptyExpression, Kind}

import logger.LazyLogging

private[macros] object EmptyReplaceInfo
    extends ReplaceInfo(
        EmptyMacroBlock,
        "",
        ExtModule(NoInfo, "", Seq.empty, "", Seq.empty),
        DefInstance(NoInfo, "", ""),
        true,
        false,
        Seq.empty,
        Seq.empty,
        Map.empty,
        Seq.empty
    )

/** Case class to store informations about replacing multiple FirrtlNode by an ExtModule instance.
 *
 *  Used to replace found macros in ExtModule.
 *
 *  @param macroKind
 *    kind of macro being replaced
 *  @param module
 *    name of the module to replace in
 *  @param defModule
 *    definition of the module
 *  @param defInstance
 *    instance of the module
 *  @param isCombi
 *    define if the circuit being replaced is combinatorial or not
 *  @param isMemory
 *    define if the circuit being replaced contains a memory access or not
 *  @param toConnect
 *    connections to add to the new module
 *  @param defWires
 *    signals to add as declaration (used to list memory ports when replacing)
 *  @param changeKind
 *    list of nodes which kind should be changed to the specified one
 *  @param toRemove
 *    nodes to remove from the initial circuit
 */
private[estimation] case class ReplaceInfo(
    macroKind: MacroBlock[_],
    module: String,
    defModule: ExtModule,
    defInstance: DefInstance,
    isCombi: Boolean,
    isMemory: Boolean,
    toConnect: Seq[Connect],
    defWires: Seq[DefWire],
    changeKind: Map[String, Kind],
    toRemove: Seq[FirrtlNode]
) extends LazyLogging {
  // build effective nodes to remove, adding to the ones already marked the references they might be used as
  private val toRemoveWithReferences = (toRemove.map { node =>
    node match {
      // case r: DefRegister => Reference(r.name, r.tpe, RegKind, SourceFlow)
      // case w: DefWire     => Reference(w.name, w.tpe, WireKind, SourceFlow)
      case _ => EmptyExpression
    }
  } ++ toRemove).filter(_ != EmptyExpression)
  // build map from parameters to allow easy access to parameters by name
  val intParams = defModule.params
    .filter(_.isInstanceOf[IntParam])
    .map(_.asInstanceOf[IntParam])
    .map(p => p.name -> p.value.toInt)
    .toMap
  // build parameter set in the right order
  val params = defModule.params.filter(_.isInstanceOf[IntParam]).map(_.asInstanceOf[IntParam]).map(_.value.toInt)

  /** Display this [[ReplaceInfo]] instance. */
  def serialize = {
    val combiString = isCombi match {
      case true  => "(combinatorial)"
      case false => "(sequential)"
    }
    val memoryString = isMemory match {
      case true  => " [Memory]"
      case false => ""
    }
    Seq(
        s"Found macro of kind $macroKind in module $module",
        s"Adding ${defModule.name} $combiString$memoryString defined as:",
        defModule.serialize,
        s"\nReplacing nodes:",
        toRemoveWithReferences.map(n => s"\t${n.serialize}").mkString("\n"),
        s"\nwith ${defInstance.serialize}",
        s"\nadding the following connections:",
        toConnect.map(c => s"\t${c.serialize}").mkString("\n"),
        ""
    ).mkString("\n")
  }

  /** Retrieve an Int Param from it's name in the new ExtModule
   *
   *  @param s
   *    name of the parameters being searched
   *  @return
   *    the value of the parameter if found, 0 elsehow
   */
  def getParam(s: String): Int = {
    intParams.getOrElse(s, 0)
  }

// scalastyle:off method.length
  /** Update the input circuit according to the informations contained in this [[ReplaceInfo]]
   *
   *  Delete all nodes to be deleted, add the ExtModule if not already defined, a new instanciation of it, and the
   *  connections associated
   *
   *  @param circuit
   *    circuit to be updated
   *  @param macros
   *    macros names found, to know if an already existing module is coming from a previous macro replacement, or is
   *    really an ExtModule (as of the meaning of black boxes)
   *  @return
   *    the updated circuit
   */
  def update(circuit: Circuit, macros: Seq[String]): Circuit = {
    def replaceInModule(d: DefModule): DefModule = {
      // remove statements that are marked to remove (either in subGraph found, or references to nodes in it)
      def removeStmt(stmt: Statement): Statement = {
        (stmt, toRemove contains stmt) match {
          case (c: Connect, _) =>
            (toRemoveWithReferences contains c.loc, toRemoveWithReferences contains c.expr) match {
              case (false, false) => stmt.mapStmt(removeStmt)
              case (_, _)         => logger.info(s"Removing node ${stmt}"); EmptyStmt
            }
          case (d: DefNode, _) =>
            toRemoveWithReferences contains d.value match {
              case true  => logger.info(s"Removing node ${stmt.serialize}"); EmptyStmt
              case false => stmt.mapStmt(removeStmt)
            }
          case (_, true)  => logger.info(s"Removing node ${stmt.serialize}"); EmptyStmt
          case (_, false) => stmt.mapStmt(removeStmt)
        }
      }

      def getString(s: Expression): String = {
        s match {
          case r: Reference => r.name
          case s: SubField  => s"${getString(s.expr)}.${s.name}"
          case _            => throw new UnsupportedOperationException(s"Can't get String on $s")
        }
      }
      def updateKinds(stmt: Statement): Statement = {
        stmt match {
          case c: Connect =>
            val loc = c.loc match {
              case r: Reference =>
                changeKind.get(getString(r)) match {
                  case Some(kind) => r.copy(kind = kind)
                  case _          => r
                }
              case s: SubField =>
                changeKind.get(getString(s)) match {
                  case Some(kind) => FirrtlUtils.changeReferenceKind(s, kind).asInstanceOf[SubField]
                  case _          => s
                }
              case x => x
            }
            val expr = c.expr match {
              case r: Reference =>
                changeKind.get(getString(r)) match {
                  case Some(kind) => r.copy(kind = kind)
                  case _          => r
                }
              case s: SubField =>
                changeKind.get(getString(s)) match {
                  case Some(kind) => FirrtlUtils.changeReferenceKind(s, kind).asInstanceOf[SubField]
                  case _          => s
                }
              case x => x
            }
            c.copy(loc = loc, expr = expr)
          case d: DefNode =>
            d.copy(value = d.value match {
              case r: Reference =>
                changeKind.get(getString(r)) match {
                  case Some(kind) => r.copy(kind = kind)
                  case _          => r
                }
              case s: SubField =>
                changeKind.get(getString(s)) match {
                  case Some(kind) => FirrtlUtils.changeReferenceKind(s, kind).asInstanceOf[SubField]
                  case _          => s
                }
              case x => x
            })
          case _ => stmt.mapStmt(updateKinds)
        }
      }

      d match {
        case mod: Module =>
          mod.copy(body = mod.body.mapStmt(removeStmt) match {
            // care to order when adding connections
            case b: Block =>
              if (mod.name == module) {
                b.copy(
                    stmts = Seq(defInstance) ++ defWires ++ b.stmts ++ toConnect
                ).mapStmt(updateKinds)
              } else { b }
            case x => x
          })
        // don't touch to ExtModules definitions
        case _ => d
      }
    }
    // only add the ExtModule if it was not already defined, and if it does not colide with a "real" ExtModule
    circuit
      .copy(modules = (circuit.modules.map(_.name) contains defModule.name) match {
        case true => circuit.modules
        case false =>
          if (macros contains defModule.name) { Seq(defModule) ++ circuit.modules }
          else {
            throw ModuleAlreadyDefinedException(s"Macro $defModule colides with existing module name ${defModule.name}")
          }
      })
      .mapModule(replaceInModule)
  }
// scalastyle:on method.length
}
