/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros

import qece.estimation.macros.components._

/** Define which macros should be searched for while running transforms. */
private[qece] class MacroSeq(private[qece] val underlying: List[MacroBlock[_]]) {
  def +:(that: MacroBlock[_]): MacroSeq      = MacroSeq(this.underlying ++ List(that))
  def ++(that: MacroSeq): MacroSeq           = MacroSeq(this.underlying ++ that.underlying)
  def ++(that: Seq[MacroBlock[_]]): MacroSeq = MacroSeq(this.underlying ++ that)
  def toSeq: Seq[CanReplacePattern]          = underlying.toSeq
  override def toString: String              = underlying.mkString("[", ", ", "]")

  def isEmpty: Boolean = underlying.size == 0
}

/** Companion object for [[MacroSeq]]. */
private[qece] object MacroSeq {
  def apply(xs: Seq[MacroBlock[_]]): MacroSeq = new MacroSeq(xs.toList)
  def apply(x: MacroBlock[_]): MacroSeq       = new MacroSeq(List(x))

  // TODO: manage in QeceContext
  val default    = MacroSeq(List(MacUnit, MemoryBlock, MacroMux))
  val noMuxBlock = MacroSeq(List(MacUnit, MemoryBlock))
  val onlyMemory = MacroSeq(List(MemoryBlock))
  val empty      = MacroSeq(List())
}
