/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros

/** Parametrize a MacroBlock
 *
 *  @param defMacro
 *    kind of macro
 *  @param params
 *    parameters for this macro
 *  @todo
 *    absorb in ReplaceInfo (see issue)
 */
private[estimation] case class ParametrizedMacroBlock(defMacro: MacroBlock[_], params: Seq[Int]) {
  val name = defMacro.className
  def prettyToString(indent: Int = 0): String = {
    " " * indent + this.toString
  }
  lazy val argDim   = defMacro.argDim
  lazy val linear   = defMacro.linearBitwidth
  lazy val bitWidth = params.head
  lazy val (factor, args, paramString) = linear match {
    case true  => (bitWidth, params.drop(1).take(argDim), params.drop(argDim + 1).mkString("-"))
    case false => (1, params.take(argDim), params.drop(argDim).mkString("-"))
  }

  override def toString: String = s"${name}${args.mkString("(", ", ", ")")} ${params.mkString("[", ", ", "]")}"
}

private[estimation] object EmptyParametrizedMacroBlock extends ParametrizedMacroBlock(EmptyMacroBlock, Seq.empty)
