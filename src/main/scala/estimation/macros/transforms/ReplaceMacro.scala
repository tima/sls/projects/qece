/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros.transforms

import qece.QeceContext

import qece.estimation.graph.CircuitGraph
import qece.estimation.macros._
import qece.estimation.hinters.WithHinters
import qece.estimation.hinters.transforms.DisplayHinters
import qece.estimation.transforms.{MetricTransform, SynchroLogging}

import firrtl.{Emitter, CircuitState}
import firrtl.options.Dependency

import firrtl.ir._
import firrtl._

/** Attempt to report resource usage estimation within the current circuit, with a hierarchical approach */
//TODO: remove Emitter trait -> exception raised at runtime without it...
private[estimation] class ReplaceMacro extends MetricTransform with Emitter with WithHinters {

  // do it the closest from endpoint reporting
  override val prerequisites          = List(Dependency[firrtl.VerilogEmitter])
  override val optionalPrerequisiteOf = List(Dependency[DisplayHinters])

  override val outputSuffix = ".macro.json"
  override val duration     = QeceContext.timeout.replaceMacro

  override val generatedKeys = Seq.empty

  // build a graph with operation as nodes and signals as nodes, search for patterns in it and replace in final circuit
  private def replaceMacros(circuit: Circuit, macros: MacroSeq): (Circuit, Seq[ReplaceInfo]) =
    macros.isEmpty match {
      case true => (circuit, Seq.empty)
      case false =>
        val (_, infos) = macros.foldLeft(CircuitGraph(circuit), Seq[ReplaceInfo]()) { (c, ma) =>
          val infos = new scala.collection.mutable.ArrayBuffer[ReplaceInfo]()
          (
              c._1.mapModules { mod =>
                val result = ma.findAndReplacePattern(mod)
                infos ++= result._2
                // propagate updated graph to following macros
                result._1
              },
              c._2 ++ infos
          )
        }
        (infos.foldLeft(circuit) { (c, i) => i.update(c, infos.map(_.defModule.name)) }, infos)
    }

  /** Execute the analysis transform on the circuit, and add an annotation to pass the report to further transforms.
   *  Also emits the report to the generation dir
   *  @param state
   *    circuit to apply the transform on
   *  @return
   *    the circuit, untouched as it is only a reporting transform
   */
  override def execute(state: CircuitState): CircuitState = {

    SynchroLogging.addTask("replace macro")
    val (time, (newCircuit, infos)) =
      exec(Utils.time(replaceMacros(state.circuit, getMacroSeq(state) ++ getAdditionalMacros(state))))
    SynchroLogging.removeTask("replace macro")

    logger.trace(s"[${getTime}] ${this.getClass.getSimpleName}:")
    logger.trace(s"Replaced following macros:\n ${infos.map(_.serialize).mkString("\n")}")

    state.copy(
        annotations = Seq(MacroReplacedCircuitAnnotation(newCircuit, infos, time)) ++ state.annotations
    )
  }

  // Deprecated
  override def emit(state: CircuitState, writer: java.io.Writer): Unit = {}
}
