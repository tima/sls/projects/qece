/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.annotations

import qece.backend.FPGA

import qece.estimation.MetricMap
import qece.estimation.macros.MacroSeq

import firrtl._
import firrtl.annotations._
import firrtl.options.CustomFileEmission
import firrtl.options.Viewer.view
import firrtl.stage.{FirrtlFileAnnotation, FirrtlOptions}

/** Abstract class used for emission Refers to firrtl EmittedComponent (sealed trait) */
abstract class EmittedReport {
  def name: String
  def value: String
  def outputSuffix: String
}

/** Trait for Annotations containing emitted reports */
trait EmittedAnnotation[T <: EmittedReport] extends NoTargetAnnotation with CustomFileEmission {
  val value: T

  override protected def baseFileName(annotations: AnnotationSeq): String = {
    view[FirrtlOptions](annotations).outputFileName.getOrElse(value.name)
  }

  override protected val suffix: Option[String] = Some(value.outputSuffix)

  override def replacements(file: java.io.File): AnnotationSeq = Seq(FirrtlFileAnnotation(file.toString))
}

trait TimedAnnotation {
  val duration: Double
}

/** Trait used to add emitted reports as Annotation */
trait EmittedReportAnnotation[T <: EmittedReport] extends EmittedAnnotation[T] {
  override def getBytes = value.value.getBytes
}

/** Class used to pass the target to transforms. */
final case class BoardAnnotation(board: FPGA) extends NoTargetAnnotation

/** Class used to allow printing in transforms. TODO: remove ? */
final case class PrintReportAnnotation(allow: Boolean = false) extends NoTargetAnnotation

/** Class used to forcing macro detection and replacement.
 *
 *  Mostly used for tests
 */
final case class MacroAnnotation(macros: MacroSeq = MacroSeq.empty) extends NoTargetAnnotation

/** Class used to report metric. */
final case class MetricMapAnnotation(metrics: MetricMap = MetricMap.empty) extends NoTargetAnnotation
