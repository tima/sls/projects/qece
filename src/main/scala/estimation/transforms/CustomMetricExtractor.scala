/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.transforms

import qece.estimation.{MetricMap, Metric}

import firrtl.AnnotationSeq

/** Wrapper to add custom metrics in the flow, outside of emission process
 *
 *  @param funcs
 *    function to generate each metric to add
 */
abstract class CustomMetricExtraction(funcs: Seq[(String, MetricMap => Metric)]) 
  extends MetricExtraction {
  private[qece] override val generatedKeys = funcs.map(_._1).toSeq

  private[qece] override def run(annos: AnnotationSeq): AnnotationSeq = {
    annos :+ generateMetricAnnotation(
        funcs.foldLeft(getMetrics(annos)) { case (m, (s, f)) => m ++& MetricMap(s -> f(m)) }
    )
  }
}

/** Run custom metric extraction before emission process. */
class PreCustomTransform(funcs: Seq[(String, MetricMap => Metric)])
    extends CustomMetricExtraction(funcs)
    with PreElaboration

/** Companion object of [[PreCustomTransform]]. */
object PreCustomTransform {
  def apply(funcs: (String, MetricMap => Metric)*): PreCustomTransform =
    new PreCustomTransform(funcs.toSeq)
}

/** Run custom metric extraction after emission process. */
class PostCustomTransform(funcs: Seq[(String, MetricMap => Metric)])
    extends CustomMetricExtraction(funcs)
    with PostElaboration

/** Companion object of [[PostCustomTransform]]. */
object PostCustomTransform {
  def apply(funcs: (String, MetricMap => Metric)*): PostCustomTransform =
    new PostCustomTransform(funcs.toSeq)
}
