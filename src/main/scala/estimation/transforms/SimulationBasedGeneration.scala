/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.transforms

import qece.{QeceContext, CustomLogging}

import qece.estimation.Metric
import qece.estimation.utils.Access
import qece.estimation.annotations.TimedAnnotation
import qece.estimation.qor.ErrorFeaturedPeekPokeTester

import chisel3.MultiIOModule

import firrtl.AnnotationSeq
import firrtl.annotations.NoTargetAnnotation

/** Used to propagate time profiling of simulation. */
final case class TimedSimulationAnnotation(val duration: Double) 
  extends NoTargetAnnotation with TimedAnnotation

/** Simulation trait for generating metrics through simulation circuit. */
trait SimulationBasedGeneration 
  extends MetricGeneration with CustomLogging with Access with UseSimulation {

  val backend = QeceContext.simulation.backend

  def execute(metrics: AnnotationSeq, m: => MultiIOModule): AnnotationSeq

  def runSimulation[T <: MultiIOModule](
      tester: (T) => ErrorFeaturedPeekPokeTester[T]
  )(m: => MultiIOModule): Metric = {
    logger.info(s"Using $backend in ${this.getClass.getSimpleName}")
    var error: Double = Double.NaN
    chisel3.iotesters.Driver.execute(
        Array(
            "--backend-name",
            backend,
            "--target-dir",
            s"test_run_dir/simu_qor"
        ),
        () => m
    ) { c =>
      val result = tester(c.asInstanceOf[T]); error = result.getError; result
    }
    Metric(error)
  }
}
