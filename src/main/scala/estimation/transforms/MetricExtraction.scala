/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.transforms

import qece.CustomLogging

import qece.estimation.utils.Access

import firrtl.AnnotationSeq

/** Run metric extraction, by operating on already existing metrics */
trait MetricExtraction extends MetricGeneration with CustomLogging with Access {

  /** run the transform on an AnnotationSeq
   *
   *  @param annos
   *    annotations, which may be pre or post emission process
   *  @return
   *    an AnnotationSeq with a new [[qece.estimation.MetricMap]] in it
   */
  private[qece] def run(annos: AnnotationSeq): AnnotationSeq

  private[qece] def logStart(annos: AnnotationSeq): Unit = {
    mytrace(
        s"Running ${this.getClass.getSimpleName} in ${getTargetDir(annos)}"
    )
  }

  private[qece] def logEnd(annos: AnnotationSeq): Unit = {
    mytrace(
        s"${this.getClass.getSimpleName} completed in ${getTargetDir(annos)}"
    )
  }
}
