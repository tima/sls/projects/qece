/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.transforms

import qece.{QeceContext, CustomLogging}

import java.util.concurrent.Semaphore

private[qece] class SynchroLogger(str: String) extends CustomLogging {
  private[this] val semaphore = new Semaphore(1)

  private[this] val running = new scala.collection.mutable.HashMap[String, Int]()

  private[this] def addEntry(n: String): Unit = {
    running contains n match {
      case true  => running(n) = running(n) + 1
      case false => running += (n -> 1)
    }
    print(s"Added $n to ${running.map { case (k, v) => s"$v $k" }.mkString("[", ", ", "]")}")
  }

  private[this] def removeEntry(n: String): Unit = {
    print(s"Removing $n from ${running.map { case (k, v) => s"$v $k" }.mkString("[", ", ", "]")}", 1)
    running(n) = running(n) - 1
  }

  private[qece] def print(s: String, rm: Int = 0): Unit =
    if (QeceContext.logger.enableSyncDebug) println(str * 3 + s"[${running.values.sum - rm}]" + str * 3 + s" $s")

  private[qece] def addTask(n: String): Unit = {
    if (QeceContext.logger.enableSyncDebug) {
      semaphore.acquire
      addEntry(n)
      semaphore.release
    }
  }

  private[qece] def removeTask(n: String): Unit = {
    if (QeceContext.logger.enableSyncDebug) {
      semaphore.acquire
      removeEntry(n)
      semaphore.release
    }
  }
}

private[qece] object SynchroLogging extends SynchroLogger("-")
