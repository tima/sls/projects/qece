/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.transforms

import qece.estimation.{Metric, MetricMap}
import qece.estimation.qor.ErrorFeaturedPeekPokeTester

import qece.estimation.macros.transforms.ReplaceMacro
import qece.estimation.resources.transforms.EstimatedResourceReport
import qece.estimation.qor.EmpiricalErrorEstimation
import qece.estimation.timing.transforms.CriticalPathReport
import qece.estimation.hinters.transforms.DisplayHinters
import qece.estimation.backend.SynthesisExtractor

import chisel3.MultiIOModule

/** A class to represent a set of transforms to run sequentially.
 *  
 *  @param underlying
 *    sequence of transforms to run
 */
class TransformSeq(private[qece] val underlying: List[MetricGeneration]) {
  lazy val toSeq: Seq[MetricGeneration] = underlying.toSeq

  /** All the metric transforms. */
  lazy val transforms: Seq[MetricTransform] =
    underlying.toSeq.filter(_.isInstanceOf[MetricTransform]).map(_.asInstanceOf[MetricTransform])
  /** All the simulation relative transforms. */
  lazy val simulations: Seq[SimulationBasedGeneration] =
    underlying.toSeq
      .filter(_.isInstanceOf[SimulationBasedGeneration])
      .map(_.asInstanceOf[SimulationBasedGeneration])

  /** All the metric extraction transforms. */
  lazy val extractors: Seq[MetricExtraction] =
    underlying.toSeq.filter(_.isInstanceOf[MetricExtraction]).map(_.asInstanceOf[MetricExtraction])

  /** All the transforms to run prior to elaboration. */
  lazy val preElaboration: Seq[MetricExtraction]  = extractors.filter(_.isInstanceOf[PreElaboration])

  /** All the transforms to run prior after elaboration. */
  lazy val postElaboration: Seq[MetricExtraction] = extractors.filter(_.isInstanceOf[PostElaboration])

  /** Check if this seq of transforms require elaboration to be run. */
  lazy val requireElaboration: Boolean = (transforms ++ postElaboration).size != 0

  /** Check if this seq of transforms simulation(s) to be run. */
  lazy val requireSimulation: Boolean  = simulations.size != 0

  def serialize: String         = underlying.map(_.getClass.getSimpleName).mkString("[", ", ", "]")

  /** Create a new [[TransformSeq]] by merging the content of this one with another.
   *
   *  @param ts
   *    other [[TransformSeq]] to merge with
   */
  def ++(ts: TransformSeq)      = new TransformSeq(underlying ++ ts.underlying)

  override def toString: String = this.serialize
}

/** Companion object for [[TransformSeq]]. */
object TransformSeq {
  /** Create a [[TransformSeq]] from a sequence of [[MetricGeneration]].
   *
   *  @param xs
   *    seq of [[MetricGeneration]](s) to use
   */
  def apply(xs: MetricGeneration*): TransformSeq = new TransformSeq(xs.toList)

  /** wrapper for ReplaceMacro transform. */
  val macros: TransformSeq    = TransformSeq(new ReplaceMacro)

  /** wrapper for EstimatedResourceReport transform. */
  val resources: TransformSeq = TransformSeq(new EstimatedResourceReport)

  /** wrapper for CriticalPathReport transform. */
  val timing: TransformSeq    = TransformSeq(new CriticalPathReport)

  /** wrapper for DisplayHinters transform. */
  val hinters: TransformSeq   = TransformSeq(new DisplayHinters)

  /** wrapper for SynthesisExtractor transform. */
  val synthesis: TransformSeq = TransformSeq(new SynthesisExtractor)

  /** default seq of transforms to run.
   *
   *  @todo manage in QeceContext
   */
  val default: TransformSeq = resources

  /** empty seq of transforms. */
  val empty: TransformSeq   = new TransformSeq(List.empty)

  /** define a set of transforms to run before elaboration, using custom functions to generate them from a 
   *  [[MetricMap]].
   *
   *  @param funcs
   *    list of functions to run sequentially, along with the name associated with each newly created metric
   *  @return
   *    a sequential [[TransformSeq]] containing each function
   */
  def preElab(funcs: (String, MetricMap => Metric)*)  = TransformSeq(new PreCustomTransform(funcs.toSeq))

  /** define a set of transforms to run after elaboration, using custom functions to generate them from a [[MetricMap]].
   *
   *  @param funcs
   *    list of functions to run sequentially, along with the name associated with each newly created metric
   *  @return
   *    a sequential [[TransformSeq]] containing each function
   */
  def postElab(funcs: (String, MetricMap => Metric)*) = TransformSeq(new PostCustomTransform(funcs.toSeq))

  /** define a simulation based [[TransformSeq]].
   *
   *  @tparam T
   *    type of the module to be simulated
   *  @param testerGenerator
   *    function which associate a [[MetricMap]] with the result of a tester
   */
  def simulation[T <: MultiIOModule](testerGenerator: MetricMap => ((T) => ErrorFeaturedPeekPokeTester[T])) =
    TransformSeq(new EmpiricalErrorEstimation(testerGenerator))
}
