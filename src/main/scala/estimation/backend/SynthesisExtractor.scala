/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.backend

import qece.QeceContext

import qece.estimation.MetricMap
import qece.estimation.transforms.{PostElaboration, MetricExtraction, SynchroLogging}
import qece.estimation.exceptions.BoardNotFoundException

import firrtl.{AnnotationSeq, Utils}

/** Basic extraction to perform synthesis on a design after generation, and extract metrics. */
class SynthesisExtractor extends MetricExtraction with PostElaboration {

  override val generatedKeys = Seq(
      "realLUT",
      "realFF",
      "realDSP",
      "realMem",
      "%realLUT",
      "%realFF",
      "%realDSP",
      "%realMem",
      "realFreq",
      "realDelay"
  )

  override val duration = QeceContext.timeout.synthesisTransform

  override def run(annos: AnnotationSeq): AnnotationSeq = {
    val board = getBoard(annos) match {
      case Some(x) => x
      case None =>
        throw BoardNotFoundException(s"Did not found any board for transform ${this.getClass.getSimpleName}")
    }
    val top  = getTargetFile(annos)
    val path = getTargetDir(annos)

    SynchroLogging.addTask("synthesis")
    logStart(annos)
    try {
      val (time, (resources, timing)) = exec(Utils.time(board.reportSynthesis(path, top)))
      val metrics = MetricMap(
          "realLUT"   -> resources.lut,
          "realFF"    -> resources.ff,
          "realDSP"   -> resources.dsp,
          "realMem"   -> resources.memory,
          "%realLUT"  -> resources.lut.toDouble / board.maxResources.lut,
          "%realFF"   -> resources.ff.toDouble / board.maxResources.ff,
          "%realDSP"  -> resources.dsp.toDouble / board.maxResources.dsp,
          "%realMem"  -> resources.memory.toDouble / board.maxResources.memory,
          "realFreq"  -> timing.frequency,
          "realDelay" -> timing.delay
      )
      logEnd(annos)
      SynchroLogging.removeTask("synthesis")
      Seq(
          generateMetricAnnotation(metrics),
          TimedSynthesisAnnotation(time)
      ) ++ annos
    } catch {
      case e: qece.backend.exceptions.SynthesisFailedException =>
        SynchroLogging.removeTask("synthesis")
        throw qece.estimation.exceptions.PostElabEstimationFailedException(
            s"Synthesis failed for file $path/$top.v due to\n\t${e.getLocalizedMessage()}"
        )
      case e: Throwable =>
        SynchroLogging.removeTask("synthesis")
        throw e
    }
  }
}
