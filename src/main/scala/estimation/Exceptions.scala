/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.exceptions

final case class BoardNotFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class ManyBoardFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class PrimitiveReportNotFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class ManyPrimitiveReportFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class ResourceReportNotFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class CriticalPathReportNotFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class EndpointReportNotFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class ManyEndpointReportFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class ConfigurationNotFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class ParsingException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class TransformNotFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class CircuitNotFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class OperatorNotFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class KeyNotFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class ManyKeyFoundException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class UnexpectedPrimitiveException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class MultipleMetricDefinitionException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class UndefinedMetricException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class EstimationFailedException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class PostElabEstimationFailedException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)

final case class SimulationFailedException(
    private val message: String = "",
    private val cause: Throwable = None.orNull
) extends Exception(message, cause)
