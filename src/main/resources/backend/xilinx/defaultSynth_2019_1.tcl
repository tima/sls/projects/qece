create_project -force synth PROJECT_PATH -part PART;
set_property simulator_language Verilog [current_project];
# too high fanout may result in non ending syntheses
set_property STEPS.SYNTH_DESIGN.ARGS.FANOUT_LIMIT 100 [get_runs synth_1];
add_files -norecurse ./TOP_MODULE;
add_files -fileset constrs_1 -norecurse ./CONSTRAINT;
update_compile_order -fileset sources_1;

#define max thread number to one, as parallelism will be controled from scala
set_param general.maxThreads NB_THREAD;

launch_runs synth_1 -jobs NB_THREAD;

# waiting for synthesis to complete
wait_on_run synth_1;

# timing summary
open_run synth_1;
report_timing -file TIMING_REPORT;
