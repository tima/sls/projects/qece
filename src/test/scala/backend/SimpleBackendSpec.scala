/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.backend

import qece.tags.{Long, Synthesis}

import chisel3.iotesters.ChiselFlatSpec
import chisel3.stage.ChiselStage
import chisel3._

import logger.{LogLevelAnnotation, LogLevel}

class SimpleBackendSpec extends ChiselFlatSpec {
  class SequentialOp(bitwidth: Int, nbOp: Int = 1) extends Module {
    val io = IO(new Bundle {
      val op1    = Input(Vec(nbOp, UInt(bitwidth.W)))
      val op2    = Input(Vec(nbOp, UInt(bitwidth.W)))
      val result = Output(Vec(nbOp, UInt(bitwidth.W)))
    })

    /** Buffer inputs and output to allow timing report on vivado (not available for combinatorial circuits) */
    val buffOp1    = Reg(Vec(nbOp, UInt(bitwidth.W)))
    val buffOp2    = Reg(Vec(nbOp, UInt(bitwidth.W)))
    val buffResult = Reg(Vec(nbOp, UInt(bitwidth.W)))

    for (i <- 0 until nbOp) {
      buffOp1(i) := io.op1(i)
      buffOp2(i) := io.op2(i)
      io.result(i) := buffResult(i)
      buffResult(i) := buffOp1(i) * buffOp2(i)
    }
  }

  class CombiOp(bitwidth: Int) extends MultiIOModule {
    val op1 = IO(Input(UInt(bitwidth.W)))
    val op2 = IO(Input(UInt(bitwidth.W)))
    val out = IO(Output(UInt(bitwidth.W)))

    out := op1 + op2
  }

  val board  = FPGA.default
  val runDir = "test_run_dir"
  def reportSynthesis(dir: String, name: String, m: => RawModule) = {
    println(s"Running synthesis in $dir")
    (new ChiselStage).emitVerilog(m, Array("--target-dir", dir), Seq(LogLevelAnnotation(LogLevel.Error)))
    board.reportSynthesis(dir, name)
  }

  val nbOp   = 2
  val nbBits = 8
  "Vivado" should s"be used to synthesize and report $nbOp operators on $nbBits bits" taggedAs (Long, Synthesis) in {
    val dir    = s"$runDir/$nbOp-SequentialOp-$nbBits"
    val (r, t) = reportSynthesis(dir, "SequentialOp", new SequentialOp(nbBits, nbOp))
    board.clear(dir)
    r.lut should equal(68)
    r.ff should equal(48)
    r.dsp should equal(0)
    r.memory should equal(0)
    t.delay should be > (0.0)
  }

  "Combinatorial circuit timing" should s"be properly estimated on $nbBits bits" taggedAs Long in {
    val dir    = s"$runDir/CombiOp-$nbBits"
    val (r, t) = reportSynthesis(dir, "CombiOp", new CombiOp(nbBits))
    board.clear(dir)
    r.lut should equal(nbBits)
    r.ff should equal(0)
    r.dsp should equal(0)
    r.memory should equal(0)
    t.delay should be > (0.0)
  }
}
