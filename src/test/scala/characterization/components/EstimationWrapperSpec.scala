/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization.components

import qece.tags.Resources

import qece.estimation.MetricMap
import qece.estimation.utils.{Operators, TransformFlatSpec}

import qece.estimation.resources.transforms.TopRegisterReport

class EstimationWrapperSpec extends TransformFlatSpec {

  def report(kind: Operators.Operator, bitwidth: Int, nbOp: Int = 1): MetricMap = {
    getMetrics(
        emitWithTransform(
            new EstimationWrapper(new BaseOperator(kind, bitwidth, nbOp)),
            args = args,
            transforms = Seq(new TopRegisterReport),
            path = Some("estimationWrapper")
        )
    )
  }

  "Simple operator wrapping" should "properly be reported" taggedAs Resources in {
    val width   = 8
    val nbOp    = 8
    val metrics = report(Operators.ADD, width, nbOp)
    metrics("topReg") should be(3 * width * nbOp)
  }
}
