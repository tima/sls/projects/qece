/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization.components

import qece.tags.Resources

import qece.estimation.utils.{Operators, TransformFlatSpec}

import qece.estimation.resources.transforms.PrimitiveReport

import qece.estimation.resources.Primitives

class BaseOperatorGenSpec extends TransformFlatSpec {

  val widths         = List(8, 256)
  def log2Up(x: Int) = scala.math.ceil(scala.math.log(x) / scala.math.log(2.0)).toInt

  def report(kind: Operators.Operator, bitwidth: Int, nbOp: Int = 1): Primitives = {
    val args = Array("--target-dir", targetDir)
    val annos = emitWithTransform(
        new BaseOperator(kind, bitwidth, nbOp),
        args = args,
        transforms = Seq(new PrimitiveReport),
        path = Some(kind.name)
    )
    getPrimitives(annos).module.getPrimitives(kind)
  }

  "Adders" should "properly be reported" taggedAs Resources in {
    for (width <- widths) {
      val primitives = report(Operators.ADD, width, 8)
      primitives.values.map(_.total).reduceOption(_ + _).getOrElse(0) should equal(8)
      primitives.values.foreach(_.key should equal(s"$width"))
    }
  }
  "Multipliers" should "properly be reported, as macro are not infered here" taggedAs Resources in {
    for (width <- widths) {
      val primitives = report(Operators.MULT, width, 8)
      primitives.values.map(_.total).reduceOption(_ + _).getOrElse(0) should equal(8)
      primitives.values.foreach(_.key should equal(s"$width"))
    }
  }
  "Binary operators" should "properly be reported" taggedAs Resources in {
    for (width <- widths) {
      val primitives = report(Operators.BIN, width, 8)
      primitives.values.map(_.total).reduceOption(_ + _).getOrElse(0) should equal(8)
      primitives.values.foreach(_.key should equal(s"$width"))
    }
  }
  "Multiplexers" should "properly be reported" taggedAs Resources in {
    for (width <- widths) {
      val primitives = report(Operators.MUX, width, 8)
      primitives.values.map(_.total).reduceOption(_ + _).getOrElse(0) should equal(8)
      primitives.values.foreach(_.key should equal(s"$width"))
    }
  }
  "Comparators" should "properly be reported" taggedAs Resources in {
    for (width <- widths) {
      val primitives = report(Operators.COMP, width, 8)
      primitives.values.map(_.total).reduceOption(_ + _).getOrElse(0) should equal(8)
      primitives.values.foreach(_.key should equal(s"$width"))
    }
  }
  "Dynamic shifters" should "properly be reported" taggedAs Resources in {
    for (width <- widths) {
      val primitives = report(Operators.DSHIFT, width, 8)
      primitives.values.map(_.total).reduceOption(_ + _).getOrElse(0) should equal(8)
      //TODO fix Dshift width
      primitives.values.foreach(_.key should equal(s"${2 max width}"))
    }
  }
  "Registers" should "properly be reported" taggedAs Resources in {
    for (width <- widths) {
      val primitives = report(Operators.REG, width, 8)
      primitives.values.map(_.total).reduceOption(_ + _).getOrElse(0) should equal(8)
      primitives.values.foreach(_.key should equal(s"$width"))
    }
  }
}
