/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.characterization

import qece.backend.Zedboard
import qece.estimation.macros.components.MacUnit
// import qece.estimation.macros.{MacroSeq, MacroBlock}
// import qece.estimation.macros.components.{MacUnit, MemoryBlock}

import qece.estimation.utils.{TransformFlatSpec, Operators}
import qece.tags.{Estimation, Resources}
// import qece.tags.{Long, Synthesis, Estimation, Resources}

import scala.math._

class CharacterizationSpec extends TransformFlatSpec {

  "Estimators for Zedboard" should "be accurate enough" taggedAs (Resources, Estimation) in {
    val characterizedBoard = getEstimatorsFromBoard(new Zedboard)
    characterizedBoard.macros.foreach { mac =>
      mac.estimators.foreach(est => est.fidelity should be > (0.69))
    }
    characterizedBoard.operators.foreach { op =>
      op.estimators.foreach(est => est.fidelity should be > (0.8))
    }
  }

  "Characterized macros" should "be quite near the reality" taggedAs (Resources, Estimation) in {
    val characterizedBoard = getEstimatorsFromBoard(new Zedboard)
    val estimators         = characterizedBoard.getCharacterizedMacro(MacUnit)
    val config = List(List(1, 0, 0, 0), List(1, 1, 0, 0), List(2, 0, 0, 0), List(2, 1, 0, 0), List(1, 3, 1, 1)).map(
        _.mkString("-")
    )

    ceil(estimators.getEstimator("dsp").evaluate(config(0), 32)).toInt should equal(3)
    ceil(estimators.getEstimator("lut").evaluate(config(0), 32)).toInt should equal(15)
    ceil(estimators.getEstimator("ff").evaluate(config(0), 32)).toInt should equal(0)
    ceil(estimators.getEstimator("dsp").evaluate(config(0), 64)).toInt should equal(10)
    ceil(estimators.getEstimator("lut").evaluate(config(0), 64)).toInt should equal(31)
    ceil(estimators.getEstimator("ff").evaluate(config(0), 64)).toInt should equal(0)

    ceil(estimators.getEstimator("dsp").evaluate(config(1), 32)).toInt should equal(3)
    ceil(estimators.getEstimator("lut").evaluate(config(1), 32)).toInt should equal(15)
    ceil(estimators.getEstimator("ff").evaluate(config(1), 32)).toInt should equal(0)
    ceil(estimators.getEstimator("dsp").evaluate(config(1), 64)).toInt should equal(10)
    ceil(estimators.getEstimator("lut").evaluate(config(1), 64)).toInt should equal(31)
    ceil(estimators.getEstimator("ff").evaluate(config(1), 64)).toInt should equal(0)

    ceil(estimators.getEstimator("dsp").evaluate(config(2), 32)).toInt should equal(4)
    ceil(estimators.getEstimator("lut").evaluate(config(2), 32)).toInt should equal(47)
    ceil(estimators.getEstimator("ff").evaluate(config(2), 32)).toInt should equal(0)
    ceil(estimators.getEstimator("dsp").evaluate(config(2), 64)).toInt should equal(16)
    ceil(estimators.getEstimator("lut").evaluate(config(2), 64)).toInt should equal(161)
    ceil(estimators.getEstimator("ff").evaluate(config(2), 64)).toInt should equal(0)

    ceil(estimators.getEstimator("dsp").evaluate(config(3), 32)).toInt should equal(4)
    ceil(estimators.getEstimator("lut").evaluate(config(3), 32)).toInt should equal(47)
    ceil(estimators.getEstimator("ff").evaluate(config(3), 32)).toInt should equal(0)
    ceil(estimators.getEstimator("dsp").evaluate(config(3), 64)).toInt should equal(16)
    ceil(estimators.getEstimator("lut").evaluate(config(3), 64)).toInt should equal(161)
    ceil(estimators.getEstimator("ff").evaluate(config(3), 64)).toInt should equal(0)

    ceil(estimators.getEstimator("dsp").evaluate(config(4), 32)).toInt should equal(3)
    ceil(estimators.getEstimator("lut").evaluate(config(4), 32)).toInt should equal(32)
    ceil(estimators.getEstimator("ff").evaluate(config(4), 32)).toInt should equal(2)
    ceil(estimators.getEstimator("dsp").evaluate(config(4), 64)).toInt should equal(10)
    ceil(estimators.getEstimator("lut").evaluate(config(4), 64)).toInt should equal(108)
    ceil(estimators.getEstimator("ff").evaluate(config(4), 64)).toInt should equal(17)
  }

  "Characterized operators" should "be quite near the reality" taggedAs (Resources, Estimation) in {
    val characterizedBoard = getEstimatorsFromBoard(new Zedboard)
    val mult               = characterizedBoard.getCharacterizedOperator(Operators.MULT)

    ceil(mult.getEstimator("lut").evaluate(32)).toInt should equal(47)
    ceil(mult.getEstimator("ff").evaluate(32)).toInt should equal(0)
    ceil(mult.getEstimator("dsp").evaluate(32)).toInt should equal(4)
    ceil(mult.getEstimator("memory").evaluate(32)).toInt should equal(0)
  }
}
