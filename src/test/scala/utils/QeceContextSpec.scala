/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.utils

import qece._

class QeceContextSpec extends CustomFlatSpec {
  "QeceContext" should "work correctly to pass configuration easily" in {

    def check(defaultCost: Double, numThread: Int) = {
      QeceContext.core.defaultCost should be (defaultCost)
      QeceContext.parallel.numThread should be (numThread)
    }

    QeceContext.withValue(
      QeceContext.copy(core = _.copy(defaultCost = 5.0), parallel = _.copy(numThread = 16))
    ){
      check(5.0, 16)
    }

    QeceContext.withValue(core = _.copy(defaultCost = 2.0), parallel = _.copy(numThread = 8)){
      check(2.0, 8)
    }

    val config = QeceContext.value

    config.funcCopy(parallel = _.copy(numThread = 4)).parallel.numThread should be (4)
  }
}
