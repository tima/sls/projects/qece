/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.qor

import qece.exploration.utils.Explorable
import qece.exploration.annotations._

import qece.estimation.{MetricMap, WithTransform}
import qece.estimation.transforms.TransformSeq
import qece.estimation.utils.{Access, TransformFlatSpec}

import chisel3._
import chisel3.experimental.FixedPoint

import scala.collection.mutable.ArrayBuffer

import qece.tags._

class SimpleFixedPointModule(
    @linear(18) @qualityOfResult width: Int,
    @linear(5, 10) @qualityOfResult binaryPoint: Int,
    @pow2(1, 3) nElem: Int
) extends Explorable {
  val tpe = FixedPoint(width.W, binaryPoint.BP)
  val op1 = IO(Input(Vec(nElem, tpe)))
  val op2 = IO(Input(Vec(nElem, tpe)))
  val out = IO(Output(tpe))

  out := VecInit((op1 zip op2).map { case (x, y) => x * y }).reduceTree(_ + _)
}

class SimpleFixedPointTester(
    nElem: Int,
    nbSimu: Int = 10,
    distribution: NormalDistribution = StandardNormalDistribution
)(m: SimpleFixedPointModule)
    extends ErrorFeaturedPeekPokeTester(m) {
  val workload = distribution.iterator

  def goldenReference(samples: Array[(Double, Double)]): Double = samples.map { case (x, y) => x * y }.reduce(_ + _)

  def computeHardware(samples: Array[(Double, Double)]): Double = {
    step(1)
    val (op1, op2) = samples.unzip
    for (i <- 0 until nElem) {
      pokeFixedPoint(m.op1(i), op1(i))
      pokeFixedPoint(m.op2(i), op2(i))
    }
    peekFixedPoint(m.out)
  }

  val results = new ArrayBuffer[(Double, Double)]()

  (0 until nbSimu).foreach { _ =>
    val samples = Array.tabulate(nElem)(_ => (workload.next -> workload.next))
    results.append((computeHardware(samples) -> goldenReference(samples)))
  }

  val (hw, sw) = results.toArray.unzip
  setNormalizedRootMeanSquareError(hw, sw)
}

object SimpleFixedPointHelper extends WithTransform with Access {
  def directCall(width: Int, bp: Int, nElem: Int, nbSimu: Int, distribution: NormalDistribution): Double = {
    var tmp: Double = 0.0
    chisel3.iotesters.Driver.execute(
        Array(
            "--backend-name",
            "treadle",
            "--target-dir",
            "test_run_dir/test_simpleFixedPoint",
            "--top-name",
            "SimpleFixedPointModule"
        ),
        () => new SimpleFixedPointModule(width, bp, nElem)
    ) { c =>
      val result = new SimpleFixedPointTester(nElem, nbSimu, distribution)(c)
      tmp = result.getError
      result
    }
    tmp
  }

  def apiCall(width: Int, bp: Int, nElem: Int, nbSimu: Int, distribution: NormalDistribution): Double = {
    getMetrics(
        emitWithTransform(
            m = new SimpleFixedPointModule(width, bp, nElem),
            initialMap = MetricMap("nElem" -> nElem),
            args = Array.empty,
            transforms = TransformSeq.simulation(m =>
              (c => new SimpleFixedPointTester(m("nElem").toInt, nbSimu, distribution)(c))
            ),
            path = Some("test_SimpleFixedPoint")
        )
    ).error
  }
}

class EmpiricalErrorEstimationSpec extends TransformFlatSpec {

  "Direct empirical qor estimation" should "be able to estimate qor over a simple kernel" taggedAs (QoR) in {
    SimpleFixedPointHelper.directCall(32, 16, 16, 100, new NormalDistribution(16, 3)) should be < (0.01)
    SimpleFixedPointHelper.directCall(64, 16, 32, 100, new NormalDistribution(128, 3)) should be < (0.01)
    SimpleFixedPointHelper.directCall(32, 16, 16, 100, new NormalDistribution(256, 3)) should be > (0.01)
    SimpleFixedPointHelper.directCall(32, 12, 32, 100, new NormalDistribution(64, 3)) should be < (0.01)
  }

  "Empirical qor estimation through API call " should "be able to estimate qor over a simple kernel" taggedAs (QoR) in {
    SimpleFixedPointHelper.apiCall(32, 16, 16, 100, new NormalDistribution(16, 3)) should be < (0.01)
    SimpleFixedPointHelper.apiCall(32, 16, 16, 100, new NormalDistribution(128, 3)) should be > (0.01)
  }
}
