/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.qor

import qece.estimation.utils.TransformFlatSpec

class StandardNormalDistributionSpec extends TransformFlatSpec {

  "ZAlpha for 0.99 confidence interval" should "be 2.58" in {
    StandardNormalDistribution.getZAlpha(0.99) should be(2.58)
  }

  "ZAlpha for 0.98 confidence interval" should "be 2.33" in {
    StandardNormalDistribution.getZAlpha(0.98) should be(2.33)
  }

  "ZAlpha for 0.95 confidence interval" should "be 1.96" in {
    StandardNormalDistribution.getZAlpha(0.95) should be(1.96)
  }

  "ZAlpha for 0.9 confidence interval" should "be 1.65" in {
    StandardNormalDistribution.getZAlpha(0.9) should be(1.65)
  }

  "ZAlpha for 0.8 confidence interval" should "be 1.29" in {
    StandardNormalDistribution.getZAlpha(0.8) should be(1.29)
  }
}
