/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros.components

import qece.tags.MacroSpec

import qece.estimation.utils.{Operators, TransformFlatSpec}
import qece.estimation.resources.{CircuitPrimitives, EmptyCircuitPrimitives}
import qece.estimation.transforms.TransformSeq
import qece.estimation.macros._

import qece.estimation.resources.transforms.PrimitiveReport

import chisel3._

class MacroMuxSpec extends TransformFlatSpec {

  private def getEmitted(m: => MultiIOModule): (CircuitPrimitives, String) = {
    try {
      val annos = emitWithTransform(
          m,
          args = args,
          transforms = TransformSeq(new PrimitiveReport),
          path = Some("macroMux")
      )
      (getPrimitives(annos), getEmittedVerilog(annos))
    } catch {
      case _: java.lang.IllegalArgumentException => (EmptyCircuitPrimitives, "")
      case _: firrtl.options.StageError          => (EmptyCircuitPrimitives, "")
      case _: java.lang.AssertionError           => (EmptyCircuitPrimitives, "")
      case e: Throwable                          => throw e
    }
  }

  def parseVerilog(code: String): (Seq[String], Seq[String]) = {
    val conditions = new scala.collection.mutable.ArrayBuffer[String]()
    val inputs     = new scala.collection.mutable.ArrayBuffer[String]()
    code.split("\n").filter(x => (x contains "wire") || (x contains "assign")).map {
      case l =>
        val splitted = l.split("=").drop(1).head.split("\\?")
        conditions += splitted.head
        inputs ++= splitted(1).split("\\;").head.split("\\:")
    }
    (conditions.toSeq.distinct, inputs.toSeq.filter(x => !(x contains "result")))
  }

  "MacroMux blocks" should "properly generate Mux trees" taggedAs MacroSpec in {
    val bitwidth    = 32
    val nbInputList = List(8, 9, 10)
    val nbCondList  = Seq(3, 4, 7, 9)
    for (nbCond <- nbCondList; nbInput <- nbInputList) {
      val (primitives, circuit) = getEmitted(new MuxModule(bitwidth, nbInput, nbCond))
      val (cond, inputs)        = parseVerilog(circuit)
      if (circuit != "") {
        if (nbInput == nbCond) {
          assert(
              cond.size == nbCond - 1,
              s"Found ${cond.size} conds while looking for ${nbCond - 1} in [$bitwidth $nbInput $nbCond]"
          )
        } else {
          assert(
              cond.size == nbCond,
              s"Found ${cond.size} conds while looking for $nbCond in [$bitwidth $nbInput $nbCond]"
          )
        }
        assert(
            inputs.size == nbInput,
            s"Found ${inputs.size} inputs while looking for $nbInput in [$bitwidth $nbInput $nbCond]"
        )
        primitives.module
          .getPrimitives(Operators.MUX)
          .values
          .map(_.total)
          .reduceOption(_ + _)
          .getOrElse(0) should be(nbInput - 1)
      }
    }
  }

}
