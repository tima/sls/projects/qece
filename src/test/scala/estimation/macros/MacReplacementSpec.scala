/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros.components

import qece.estimation.utils.{Operators, TransformFlatSpec}
import qece.estimation.resources.CircuitPrimitives
import qece.estimation.transforms._
import qece.estimation.macros._

import qece.estimation.macros.transforms.ReplaceMacro
import qece.estimation.resources.transforms.PrimitiveReport
import qece.estimation.timing.transforms.EndpointReport

import chisel3._

import qece.tags.MacroSpec

class MacReplacementSpec extends TransformFlatSpec {
  class Combi(width: Int) extends MultiIOModule {
    val io = IO(new Bundle {
      val op1 = Input(UInt(width.W))
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    io.out := (io.op1 * io.op1) + io.op2
  }

  //TODO: test with Reg and Wire
  class MacWithTree(width: Int, combi: Boolean = false) extends MultiIOModule {
    val op0 = IO(Input(UInt(width.W)))
    val op1 = IO(Input(UInt(width.W)))
    val op2 = IO(Input(UInt(width.W)))
    val op3 = IO(Input(UInt(width.W)))
    val op4 = IO(Input(UInt(width.W)))
    val op5 = IO(Input(UInt(width.W)))
    val op6 = IO(Input(UInt(width.W)))
    val op7 = IO(Input(UInt(width.W)))
    val res = IO(Output(UInt()))

    val reg0 = if (combi) Wire(UInt()) else Reg(UInt())
    val reg1 = if (combi) Wire(UInt()) else Reg(UInt())
    val reg2 = if (combi) Wire(UInt()) else Reg(UInt())
    val reg3 = if (combi) Wire(UInt()) else Reg(UInt())

    reg0 := op0 * op1
    reg1 := op2 * op3
    reg2 := op4 * op5
    reg3 := op6 * op7

    val add0 = if (combi) Wire(UInt()) else Reg(UInt())
    val add1 = if (combi) Wire(UInt()) else Reg(UInt())

    add0 := reg0 + reg1
    add1 := reg2 + reg3

    res := add0 + add1
  }

  class First(width: Int) extends MultiIOModule {
    val io = IO(new Bundle {
      val op1 = Input(UInt(width.W))
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    val mulReg = RegNext(io.op1 * io.op1)
    io.out := mulReg + io.op1
  }

  class Second(width: Int) extends MultiIOModule {
    val io = IO(new Bundle {
      val op1 = Input(UInt(width.W))
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    val tmp = RegNext((io.op1 * io.op2) + io.op1)
    io.out := tmp
  }

  class Third(width: Int) extends MultiIOModule {
    val io = IO(new Bundle {
      val op1 = Input(UInt(width.W))
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    val mulReg1 = RegNext(io.op1 * io.op1)
    val mulReg2 = RegNext(mulReg1)
    val mulReg3 = RegNext(mulReg2)
    val add     = RegNext(io.op2 + mulReg3)
    io.out := add
  }

  class MultiMac(width: Int) extends MultiIOModule {
    val io = IO(new Bundle {
      val op1  = Input(UInt(width.W))
      val op2  = Input(UInt(width.W))
      val out1 = Output(UInt(width.W))
      val out2 = Output(UInt(width.W))
    })

    val mulReg1 = RegNext(io.op1 * io.op1)
    val mulReg2 = RegNext(mulReg1)
    val mulReg3 = RegNext(mulReg2)
    val add     = RegNext(io.op2 + mulReg3)
    io.out2 := add

    io.out1 := (io.op1 * io.op2) + io.op1
  }

  class MultiSinks(width: Int) extends MultiIOModule {
    val io = IO(new Bundle {
      val op1 = Input(UInt(width.W))
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    val mulReg = RegNext(io.op1 * io.op2)
    io.out := mulReg + mulReg
  }

  class InnerOp(width: Int) extends MultiIOModule {
    val op1 = IO(Input(UInt(width.W)))
    val op2 = IO(Input(UInt(width.W)))
    val out = IO(Output(UInt(width.W)))

    out := op1 * op2
  }
  class InnerModule(width: Int) extends MultiIOModule {
    val in   = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.op2 := in
    out := op.out
    outR := RegNext(op.out * in)
  }

  class OtherModule(width: Int) extends MultiIOModule {
    val in   = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.op2 := in
    out := op.out
    outR := op.out * in
  }

  class Top(nbInner: Int = 1, width: Int = 5) extends MultiIOModule {
    val in   = IO(Input(Vec(nbInner * 2 - 1, UInt(width.W))))
    val out  = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))
    val outR = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))

    val inner = Array.fill(nbInner)(Module(new InnerModule(width)))
    for (i <- 0 until nbInner) {
      inner(i).in := in(i)
      out(i) := inner(i).out
      outR(i) := RegNext(inner(i).outR)
    }
    val other = Array.fill(nbInner - 1)(Module(new OtherModule(width)))
    for (i <- 0 until nbInner - 1) {
      other(i).in := in(i + nbInner)
      out(i + nbInner) := other(i).out
      outR(i + nbInner) := RegNext(other(i).outR)
    }
  }

  private def getMacros(m: => MultiIOModule): (Seq[ReplaceInfo], CircuitPrimitives) = {
    val annos = emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq(new ReplaceMacro, new PrimitiveReport),
        path = Some("macroMac"),
        macros = MacroSeq(MacUnit)
    )
    (getReplaceInfos(annos), getPrimitives(annos))
  }

  // run endpoint report to display warning on unconnected paths
  private def runEndpointReport(m: => MultiIOModule): Unit = {
    emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq(new ReplaceMacro, new EndpointReport),
        path = Some("macroMacEndpoints"),
        macros = MacroSeq(MacUnit)
    )
  }

  private def checkMult(primitives: CircuitPrimitives): Unit = {
    primitives.module.getPrimitives(Operators.MULT).values.foreach { resource =>
      withClue(s"found multiplications in ${primitives.circuit}: ") { resource.total should equal(0) }
    }
  }

  "Macro replacement for combinatorial Mac units" should "properly report MAC units" taggedAs MacroSpec in {
    val (macros, primitives) = getMacros(new Combi(32))
    macros.size should equal(1)
    val first = macros.head
    first.isCombi should be(true)
    first.getParam("inWidth") should equal(32)
    first.getParam("outFactor") should equal(1)
    first.getParam("nbRegMul") should equal(0)
    first.getParam("add") should equal(1)
    first.getParam("nbRegAdd") should equal(0)
    first.toConnect.size should equal(4)
    first.toRemove.size should equal(7)
    checkMult(primitives)
  }

  "Macro replacement for multiplication only" should "properly report MAC units" taggedAs MacroSpec in {
    val (macros, primitives) = getMacros(new InnerOp(32))
    macros.size should equal(1)
    val first = macros.head
    first.isCombi should be(true)
    first.getParam("inWidth") should equal(32)
    first.getParam("outFactor") should equal(1)
    first.getParam("nbRegMul") should equal(0)
    first.getParam("add") should equal(0)
    first.getParam("nbRegAdd") should equal(0)
    first.toConnect.size should equal(4)
    first.toRemove.size should equal(3)
    checkMult(primitives)
  }

  "Macro replacement for complex Mac units" should "properly report MAC unit" taggedAs MacroSpec in {
    val (firstMacro, firstPrimitives) = getMacros(new First(32))
    firstMacro.size should equal(1)
    firstMacro.head.isCombi should be(false)
    firstMacro.head.getParam("inWidth") should equal(32)
    firstMacro.head.getParam("outFactor") should equal(1)
    firstMacro.head.getParam("nbRegMul") should equal(1)
    firstMacro.head.getParam("add") should equal(1)
    firstMacro.head.getParam("nbRegAdd") should equal(0)
    firstMacro.head.toConnect.size should equal(4)
    firstMacro.head.toRemove.size should be(9)
    checkMult(firstPrimitives)
    val (secondMacro, secondPrimitives) = getMacros(new Second(32))
    secondMacro.size should equal(1)
    secondMacro.head.isCombi should be(false)
    secondMacro.head.getParam("inWidth") should equal(32)
    secondMacro.head.getParam("outFactor") should equal(1)
    secondMacro.head.getParam("nbRegMul") should equal(0)
    secondMacro.head.getParam("add") should equal(1)
    secondMacro.head.getParam("nbRegAdd") should equal(1)
    secondMacro.head.toConnect.size should equal(4)
    secondMacro.head.toRemove.size should be(9)
    checkMult(secondPrimitives)
    val (thirdMacro, thirdPrimitives) = getMacros(new Third(32))
    thirdMacro.size should equal(1)
    thirdMacro.head.isCombi should be(false)
    thirdMacro.head.getParam("inWidth") should equal(32)
    thirdMacro.head.getParam("outFactor") should equal(1)
    thirdMacro.head.getParam("nbRegMul") should equal(3)
    thirdMacro.head.getParam("add") should equal(1)
    thirdMacro.head.getParam("nbRegAdd") should equal(1)
    thirdMacro.head.toConnect.size should equal(4)
    thirdMacro.head.toRemove.size should equal(14)
    checkMult(thirdPrimitives)
  }

  "Macro replacement with multiple Mac units in the same module" should
    "properly report MAC unit" taggedAs MacroSpec in {
    val (macros, primitives) = getMacros(new MultiMac(32))
    macros.size should equal(2)
    macros(0).isCombi should be(true)
    macros(0).getParam("inWidth") should equal(32)
    macros(0).getParam("outFactor") should equal(1)
    macros(0).getParam("nbRegMul") should equal(0)
    macros(0).getParam("add") should equal(1)
    macros(0).getParam("nbRegAdd") should equal(0)
    macros(0).toConnect.size should equal(4)
    macros(0).toRemove.size should equal(7)
    macros(1).isCombi should be(false)
    macros(1).getParam("inWidth") should equal(32)
    macros(1).getParam("outFactor") should equal(1)
    macros(1).getParam("nbRegMul") should equal(3)
    macros(1).getParam("add") should equal(1)
    macros(1).getParam("nbRegAdd") should equal(1)
    macros(1).toConnect.size should equal(4)
    macros(1).toRemove.size should equal(14)
    checkMult(primitives)
  }

  "Macro replacement for mac trees" should "properly report MAC units" taggedAs MacroSpec in {
    val (macs, primitives) = getMacros(new MacWithTree(32))
    macs.size should equal(4)
    macs.foreach(_.isCombi should be(false))
    val macWithAdd = macs.filter(_.getParam("add") == 1)
    val macNoAdd   = macs.filter(_.getParam("add") == 0)

    macWithAdd.size should equal(2)
    macNoAdd.size should equal(2)

    macWithAdd.foreach { mac =>
      mac.getParam("inWidth") should equal(32)
      mac.getParam("outFactor") should equal(2)
      mac.getParam("nbRegMul") should equal(1)
      mac.getParam("add") should equal(1)
      mac.getParam("nbRegAdd") should equal(1)
      mac.toConnect.size should equal(4)
    }

    macNoAdd.foreach { mac =>
      mac.getParam("inWidth") should equal(32)
      mac.getParam("outFactor") should equal(2)
      mac.getParam("nbRegMul") should equal(1)
      mac.getParam("add") should equal(0)
      mac.getParam("nbRegAdd") should equal(0)
      mac.toConnect.size should equal(4)
    }
    // check no unresolved path remains by running endpoint reporting
    runEndpointReport(new MacWithTree(32))
    checkMult(primitives)
  }

  "Macro replacement for multiple sinks Mac units" should "not report MAC units" taggedAs MacroSpec in {
    val (multi, primitives) = getMacros(new MultiSinks(32))
    multi.size should equal(1)
    multi.head.isCombi should be(false)
    multi.head.getParam("inWidth") should equal(32)
    multi.head.getParam("outFactor") should equal(2)
    multi.head.getParam("nbRegMul") should equal(1)
    multi.head.getParam("add") should equal(0)
    multi.head.getParam("nbRegAdd") should equal(0)
    multi.head.toConnect.size should equal(4)
    multi.head.toRemove.size should equal(3)
    checkMult(primitives)
  }

  "Macro replacement in complex, hierarchical modules" should "properly report MAC units" taggedAs MacroSpec in {
    val nbInner              = 9
    val (macros, primitives) = getMacros(new Top(nbInner, 32))
    macros.size should equal(3)
    val mainMacs = primitives.module.getMacro(MacUnit)
    val noReg    = mainMacs.filter(_.mac.params == List(32, 1, 0, 0, 0)).head
    noReg.total should equal(nbInner + (nbInner - 1) * 2)
    noReg.local should equal(0)
    val reg = mainMacs.filter(_.mac.params == List(32, 1, 1, 0, 0)).head
    reg.total should equal(nbInner)
    reg.local should equal(0)
    val innerMacs  = primitives.module.getModulePrimitives("InnerModule").getMacro(MacUnit)
    val innerNoReg = innerMacs.filter(_.mac.params == List(32, 1, 0, 0, 0)).head
    innerNoReg.total should equal(1)
    innerNoReg.local should equal(0)
    val innerReg = innerMacs.filter(_.mac.params == List(32, 1, 1, 0, 0)).head
    innerReg.total should equal(1)
    innerReg.local should equal(1)
    checkMult(primitives)
  }
}
