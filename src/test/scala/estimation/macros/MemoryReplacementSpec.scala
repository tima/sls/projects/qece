/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros.components

import qece.tags.MacroSpec

import qece.estimation.utils.{Operators, TransformFlatSpec}
import qece.estimation.resources.CircuitPrimitives
import qece.estimation.transforms.TransformSeq
import qece.estimation.macros._

import qece.estimation.macros.transforms.ReplaceMacro
import qece.estimation.resources.transforms.PrimitiveReport

import chisel3._
import chisel3.util._

class MemoryReplacementSpec extends TransformFlatSpec {

  class Combi(nBank: Int, nAddr: Int, elemWidth: Int) extends MultiIOModule {
    val bankIn  = IO(Input(UInt(log2Up(nBank).W)))
    val bankOut = IO(Input(UInt(log2Up(nBank).W)))
    val addrIn  = IO(Input(UInt(log2Up(nAddr).W)))
    val addrOut = IO(Input(UInt(log2Up(nAddr).W)))
    val dataIn  = IO(Input(UInt(elemWidth.W)))
    val dataOut = IO(Output(UInt(elemWidth.W)))

    val memory = Array.fill(nBank)(SyncReadMem(nAddr, UInt(elemWidth.W)))

    dataOut := DontCare
    for (i <- 0 until nBank) {
      val bank = memory(i)
      when(bankOut === i.U) { dataOut := bank.read(addrOut, bankOut === i.U) }
      when(bankIn === i.U) { bank.write(addrIn, dataIn * i.U) }
    }
  }

  class WithReg(nBank: Int, nAddr: Int, elemWidth: Int) extends MultiIOModule {
    val bankIn  = IO(Input(UInt(log2Up(nBank).W)))
    val bankOut = IO(Input(UInt(log2Up(nBank).W)))
    val addrIn  = IO(Input(UInt(log2Up(nAddr).W)))
    val addrOut = IO(Input(UInt(log2Up(nAddr).W)))
    val dataIn  = IO(Input(UInt(elemWidth.W)))
    val dataOut = IO(Output(UInt(elemWidth.W)))

    val memory = Array.fill(nBank)(SyncReadMem(nAddr, UInt(elemWidth.W)))

    val outBuffer = Reg(Vec(nBank, UInt(elemWidth.W)))

    dataOut := outBuffer(bankOut)

    for (i <- 0 until nBank) {
      val bank = memory(i)
      outBuffer(i.U) := bank.read(addrOut, bankOut === i.U)
      when(bankIn === i.U) { bank.write(addrIn, dataIn * i.U) }
    }
  }

  private def getMacros(m: => MultiIOModule): (Seq[ReplaceInfo], CircuitPrimitives) = {
    val annos = emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq(new ReplaceMacro, new PrimitiveReport),
        path = Some("macroMemory"),
        macros = MacroSeq(List(MacUnit, MemoryBlock))
    )
    (getReplaceInfos(annos), getPrimitives(annos))
  }

  private def checkMemory(primitives: CircuitPrimitives): Unit = {
    primitives.module.getPrimitives(Operators.MEMORY).values.foreach { resource =>
      withClue(s"found memory in ${primitives.circuit}: ") { resource.total should equal(0) }
    }
  }

  "Macro replacement for combinatorial memory blocks" should "properly report MemoryBlock" taggedAs MacroSpec in {
    val width                = 32
    val nbMem                = 2
    val nAddr                = 32
    val (macros, primitives) = getMacros(new Combi(nbMem, nAddr, width))
    macros.size should equal(nbMem * 2)
    primitives.module.getMacro(MemoryBlock).foreach { primitive =>
      primitive.mac.params should be(List(width, nAddr, 0))
    }
    val memories = macros.filter(_.macroKind == MemoryBlock)
    memories.size should equal(nbMem)
    memories.foreach { memory =>
      memory.isCombi should be(false)
      memory.toConnect.size should equal(6)
      memory.toRemove.size should be > (4)
    }
    memories(0).toRemove.size should be(7)
    memories(1).toRemove.size should be(5)
    checkMemory(primitives)
  }

  "Macro replacement for registered memory blocks" should "properly report MemoryBlock" taggedAs MacroSpec in {
    val width                = 32
    val nbMem                = 2
    val nAddr                = 32
    val (macros, primitives) = getMacros(new WithReg(nbMem, nAddr, width))
    macros.size should equal(nbMem * 2)
    primitives.module.getMacro(MemoryBlock).foreach { primitive =>
      primitive.mac.params should be(List(width, nAddr, 1))
    }
    val memories = macros.filter(_.macroKind == MemoryBlock)
    memories.size should equal(nbMem)
    memories.foreach { memory =>
      memory.isCombi should be(false)
      memory.toConnect.size should equal(6)
      memory.toRemove.size should be > (1)
    }
    memories(0).toRemove.size should be(7)
    memories(1).toRemove.size should be(7)
    checkMemory(primitives)
  }
}
