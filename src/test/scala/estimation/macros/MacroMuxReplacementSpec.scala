/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.macros.components

import qece.tags.MacroSpec

import qece.estimation.utils.{Operators, TransformFlatSpec}
import qece.estimation.resources.CircuitPrimitives
import qece.estimation.transforms._
import qece.estimation.macros._

import qece.estimation.macros.transforms.ReplaceMacro
import qece.estimation.resources.transforms.PrimitiveReport
import qece.estimation.timing.transforms.EndpointReport

import chisel3._

import math._

class MacroMuxReplacementSpec extends TransformFlatSpec {

  class SimpleSelectMux(bitWidth: Int, nbInput: Int) extends MultiIOModule {
    val inputs = IO(Input(Vec(nbInput, UInt(bitWidth.W))))
    val conds  = IO(Input(UInt((log(bitWidth) / log(2.0)).ceil.toInt.W)))
    val output = IO(Output(UInt(bitWidth.W)))

    output := inputs(conds)
  }

  private def getMacros(m: => MultiIOModule): (Seq[ReplaceInfo], CircuitPrimitives) = {
    val annos = emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq(new ReplaceMacro, new PrimitiveReport),
        macros = MacroSeq(MacroMux),
        path = Some("macroMuxReplacement")
    )
    (getReplaceInfos(annos), getPrimitives(annos))
  }

  // run endpoint report to display warning on unconnected paths
  private def runEndpointReport(m: => MultiIOModule): Unit = {
    emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq(new ReplaceMacro, new EndpointReport),
        path = Some("macroMuxEndpoints")
    )
  }

  private def checkMux(primitives: CircuitPrimitives): Unit = {
    primitives.module.getPrimitives(Operators.MUX).values.foreach { resource =>
      withClue(s"found multiplexers in ${primitives.circuit}: ") { resource.total should equal(0) }
    }
  }

  "Mux replacement for simple dynamic select" should "properly find and replace MacroMux" taggedAs MacroSpec in {
    val bitwidth             = 32
    val nbInput              = 140
    val (macros, primitives) = getMacros(new SimpleSelectMux(bitwidth, nbInput))
    macros.size should equal(1)
    val first = macros.head
    first.isCombi should be(true)
    first.getParam("bitwidth") should equal(bitwidth)
    first.getParam("nbInput") should equal(nbInput)
    first.getParam("nbCond") should equal((log(nbInput) / log(2)).ceil.toInt)
    runEndpointReport(new SimpleSelectMux(bitwidth, nbInput))
    checkMux(primitives)
  }

  "Mux replacement for simple mux trees" should "properly find and replace MacroMux" taggedAs MacroSpec in {
    val bitwidth             = 55
    val nbInput              = 24
    val nbCond               = 9
    val (macros, primitives) = getMacros(new MuxModule(bitwidth, nbInput, nbCond))
    macros.size should equal(1)
    val first = macros.head
    first.isCombi should be(true)
    first.getParam("bitwidth") should equal(bitwidth)
    first.getParam("nbInput") should equal(nbInput)
    first.getParam("nbCond") should equal(nbCond)
    runEndpointReport(new MuxModule(bitwidth, nbInput, nbCond))
    checkMux(primitives)
  }

}
