/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.backend

import qece.backend.Zedboard
import qece.estimation.MetricMap
import qece.estimation.utils.{TransformFlatSpec, Access}
import qece.estimation.transforms.TransformSeq

import qece.tags.{Synthesis, Long}

import chisel3._

class SynthesisTransformSpec extends TransformFlatSpec with Access {

  class SequentialOp(bitwidth: Int, nbOp: Int = 1) extends Module {
    val io = IO(new Bundle {
      val op1    = Input(Vec(nbOp, UInt(bitwidth.W)))
      val op2    = Input(Vec(nbOp, UInt(bitwidth.W)))
      val result = Output(Vec(nbOp, UInt(bitwidth.W)))
    })

    /** Buffer inputs and output to allow timing report on vivado (not available for combinatorial circuits) */
    val buffOp1    = Reg(Vec(nbOp, UInt(bitwidth.W)))
    val buffOp2    = Reg(Vec(nbOp, UInt(bitwidth.W)))
    val buffResult = Reg(Vec(nbOp, UInt(bitwidth.W)))

    for (i <- 0 until nbOp) {
      buffOp1(i) := io.op1(i)
      buffOp2(i) := io.op2(i)
      io.result(i) := buffResult(i)
      buffResult(i) := buffOp1(i) * buffOp2(i)
    }
  }

  class CombiOp(bitwidth: Int) extends MultiIOModule {
    val op1 = IO(Input(UInt(bitwidth.W)))
    val op2 = IO(Input(UInt(bitwidth.W)))
    val out = IO(Output(UInt(bitwidth.W)))

    out := op1 + op2
  }

  def report(m: => MultiIOModule, p: String): MetricMap = {
    val annos = emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq(new SynthesisExtractor),
        path = Some(s"synthesis$p"),
        board = Some(new Zedboard)
    )
    getMetrics(annos)
  }

  "Sequential operators" should "properly be synthetized" taggedAs (Synthesis, Long) in {
    val metrics = report(new SequentialOp(8, 3), "Sequential")
    metrics should not be (Map.empty)
    metrics("realFF") should be(72.0)
    metrics("%realLUT") should be > (0.0019)
    metrics("%realLUT") should be < (0.0020)
  }

  "Combinatorial operators" should "properly be synthetized" taggedAs (Synthesis, Long) in {
    val metrics = report(new CombiOp(12), "Combi")
    metrics should not be (Map.empty)
  }
}
