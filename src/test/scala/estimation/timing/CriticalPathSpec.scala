/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.timing

import chisel3._
import chisel3.util._

import qece.backend.Zedboard

import qece.estimation.utils.TransformFlatSpec
import qece.estimation.transforms.TransformSeq
import qece.estimation.MetricMap

import qece.tags.{Estimation, Timing, Deprecated}

class CriticalPathSpec extends TransformFlatSpec {
  class InnerOp(width: Int) extends MultiIOModule {
    val op1 = IO(Input(UInt(width.W)))
    val io = IO(new Bundle {
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    io.out := op1 + io.op2
  }
  class InnerModule(width: Int) extends MultiIOModule {
    val in   = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.io.op2 := in
    out := op.io.out
    outR := RegNext(op.io.out + outR + in)
  }
  class OtherModule(width: Int) extends MultiIOModule {
    val in   = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.io.op2 := in
    out := op.io.out
    outR := RegNext(op.io.out) * in
  }

  class Top(nbInner: Int = 1, width: Int = 5) extends MultiIOModule {
    val in   = IO(Flipped(Decoupled(Vec(nbInner * 2 - 1, UInt(width.W)))))
    val out  = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))
    val outR = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))

    in.ready := true.B
    val inner = Array.fill(nbInner)(Module(new InnerModule(width)))
    for (i <- 0 until nbInner) {
      inner(i).in := in.bits(i)
      out(i) := inner(i).out
      outR(i) := RegNext(inner(i).outR)
    }
    val other = Array.fill(nbInner - 1)(Module(new OtherModule(width)))
    for (i <- 0 until nbInner - 1) {
      other(i).in := in.bits(i + nbInner)
      out(i + nbInner) := other(i).out
      outR(i + nbInner) := RegNext(other(i).outR)
    }
  }

  private def report(m: => MultiIOModule): MetricMap = {
    val annos = emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq.timing,
        path = Some("path"),
        board = Some(new Zedboard)
    )
    getMetrics(annos)
  }

  "Combinatorial paths" should "be properly estimated" taggedAs (Estimation, Timing, Deprecated) in {
    val metrics = report(new InnerOp(17))
    metrics("#endpoint") should equal(1)
    metrics("delay") should be < (2.5)
    metrics("delay") should be > (2.4)
    assert(false, "Estimators should be checked")
  }

  "Simple operator" should "be properly estimated" taggedAs (Estimation, Timing, Deprecated) in {
    val metrics = report(new InnerModule(17))
    metrics("#endpoint") should equal(3)
    metrics("delay") should be < (7.5)
    metrics("delay") should be > (7.4)
    assert(false, "Estimators should be checked")
  }

  "Complex, hierarchical operator" should "be properly estimated" taggedAs (Estimation, Timing, Deprecated) in {
    val metrics = report(new Top(6, 32))
    metrics("#endpoint") should equal((6 - 1) * 8 + 4 + 1)
    metrics("delay") should be < (8.8)
    metrics("delay") should be > (8.7)
    assert(false, "Estimators should be checked")
  }
}
