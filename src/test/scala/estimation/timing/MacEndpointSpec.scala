/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.timing

import chisel3._
import chisel3.util._

import qece.estimation.MetricMap
import qece.estimation.utils.TransformFlatSpec
import qece.estimation.transforms.TransformSeq

import qece.estimation.resources.transforms.PrimitiveReport
import qece.estimation.timing.transforms.EndpointReport

import qece.estimation.macros.MacroSeq
import qece.estimation.macros.components.MacUnit

import qece.tags.{Timing, Deprecated}

class MacEndpointSpec extends TransformFlatSpec {
  class Sequential(width: Int) extends MultiIOModule {
    val op1 = IO(Input(UInt(width.W)))
    val io = IO(new Bundle {
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    io.out := RegNext(op1 * io.op2)
  }
  class Combi(width: Int) extends MultiIOModule {
    val op1 = IO(Input(UInt(width.W)))
    val io = IO(new Bundle {
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    io.out := op1 * io.op2
  }
  class InnerModule(width: Int) extends MultiIOModule {
    val in   = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new Sequential(width))
    op.op1 := in
    op.io.op2 := in
    out := op.io.out
    outR := RegNext(op.io.out + outR + in)
  }
  class OtherModule(width: Int) extends MultiIOModule {
    val op1  = IO(Input(UInt(width.W)))
    val op2  = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new Combi(width))
    op.op1 := op1
    op.io.op2 := op1
    out := op.io.out
    outR := RegNext(op.io.out) * op2
  }

  class Top(nbInner: Int = 1, width: Int = 5) extends MultiIOModule {
    val in   = IO(Flipped(Decoupled(Vec(nbInner * 2 - 1, UInt(width.W)))))
    val out  = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))
    val outR = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))

    in.ready := true.B
    val inner = Array.fill(nbInner)(Module(new InnerModule(width)))
    for (i <- 0 until nbInner) {
      inner(i).in := in.bits(i)
      out(i) := inner(i).out
      outR(i) := RegNext(inner(i).outR)
    }
    val other = Array.fill(nbInner - 1)(Module(new OtherModule(width)))
    for (i <- 0 until nbInner - 1) {
      other(i).op1 := in.bits(i + nbInner)
      other(i).op2 := in.bits(i + nbInner)
      out(i + nbInner) := other(i).out
      outR(i + nbInner) := RegNext(other(i).outR)
    }
  }

  private def report(m: => MultiIOModule): MetricMap = {
    val annos = emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq(new PrimitiveReport, new EndpointReport),
        path = Some("macEndpoints"),
        macros = MacroSeq(MacUnit)
    )
    getMetrics(annos)
  }

  "Combinatorial paths" should "contains only one endpoint, i.e. output" taggedAs (Timing, Deprecated) in {
    val metrics = report(new Combi(17))
    metrics("#endpoint") should equal(1)
    metrics("maxDepth") should equal(1)
  }

  "Sequential paths" should "contains 2 endpoints (1 output, 1 sequential mac)" taggedAs (Timing, Deprecated) in {
    val metrics = report(new Sequential(17))
    metrics("#endpoint") should equal(2)
    metrics("maxDepth") should equal(1)
  }

  "Simple combinatorial operator" should "contains 3 endpoints (2 output, 1 reg)" taggedAs (Timing, Deprecated) in {
    val metrics = report(new OtherModule(17))
    metrics("#endpoint") should equal(3)
    metrics("maxDepth") should equal(1)
  }

  "Simple sequential operator" should
    "contains 4 endpoints (2 output, 1 reg, 1 sequential mac)" taggedAs (Timing, Deprecated) in {
    val metrics = report(new InnerModule(17))
    metrics("#endpoint") should equal(4)
    metrics("maxDepth") should equal(4)
  }

  "Complex, hierarchical operator" should
    "contains (n * 4) + ((n-1) * 3) + (2 * n - 1) + 1 endpoints (ready)" taggedAs (Timing, Deprecated) in {
    val n       = 6
    val metrics = report(new Top(n, 32))
    metrics("#endpoint") should equal((2 * n - 1) + (n * 4) + ((n - 1) * 3) + 1)
    metrics("maxDepth") should equal(4)
  }
}
