/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.timing

import qece.estimation.utils.TransformFlatSpec
import qece.estimation.transforms.TransformSeq
import qece.estimation.timing.transforms.EndpointReport

import chisel3._

import qece.tags.{Timing, Deprecated}

class MemoryEndpointSpec extends TransformFlatSpec {
  class InnerOp(width: Int) extends MultiIOModule {
    val op1 = IO(Input(UInt(width.W)))
    val io = IO(new Bundle {
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    io.out := op1 + io.op2
  }
  class InnerModule(nMem: Int, nElem: Int, width: Int) extends MultiIOModule {
    val in     = IO(Input(UInt(width.W)))
    val out    = IO(Output(UInt(width.W)))
    val outMem = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.io.op2 := in
    out := op.io.out

    val memory = Array.fill(nMem)(SyncReadMem(nElem, UInt(width.W)))
    for (i <- 0 until nMem) {
      memory(i).write(in, out)
    }
    outMem := memory.map(_.read(out)).reduce(_ + _)
  }
  class OtherModule(nMem: Int, nElem: Int, width: Int) extends MultiIOModule {
    val in     = IO(Input(UInt(width.W)))
    val out    = IO(Output(UInt(width.W)))
    val outMem = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.io.op2 := in
    out := op.io.out

    val memory = Array.fill(nMem)(Mem(nElem, UInt(width.W)))
    for (i <- 0 until nMem) {
      memory(i).write(in, out)
    }
    outMem := memory.map(_.read(out)).reduce(_ + _)
  }

  class Top(nMem: Int = 3, nElem: Int = 5, nbInner: Int = 1, width: Int = 5) extends MultiIOModule {
    val in   = IO(Input(Vec(nbInner * 2 - 1, UInt(width.W))))
    val out  = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))
    val outR = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))

    val inner = Array.fill(nbInner)(Module(new InnerModule(nMem, nElem, width)))
    for (i <- 0 until nbInner) {
      inner(i).in := in(i)
      out(i) := inner(i).out
      outR(i) := RegNext(inner(i).outMem)
    }
    val other = Array.fill(nbInner - 1)(Module(new OtherModule(nMem, nElem, width)))
    for (i <- 0 until nbInner - 1) {
      other(i).in := in(i + nbInner)
      out(i + nbInner) := other(i).out
      outR(i + nbInner) := RegNext(other(i).outMem)
    }
  }

  private def report(m: => MultiIOModule): Int = {
    val annos =
      emitWithTransform(m, args = args, transforms = TransformSeq(new EndpointReport), path = Some("memEndpoints"))
    getMetrics(annos)("#endpoint").toInt
  }

  "Combinatorial paths" should "contains only one endpoint, i.e. output" taggedAs (Timing, Deprecated) in {
    report(new InnerOp(17)) should equal(1)
  }

  "InnerModule" should
    "contains 2 + 5*2 endpoints (2 output, 5 memories with 2 endpoints " +
      "- mem and pipelined reading addr)" taggedAs (Timing, Deprecated) in {
    report(new InnerModule(5, 4, 17)) should equal(2 + 5 * 2)
  }

  "OtherModule" should "contains 2 + 4 endpoints (2 output, 4 mem)" taggedAs (Timing, Deprecated) in {
    report(new OtherModule(4, 4, 17)) should equal(2 + 4)
  }

  "Complex, hierarchical operator" should
    "contains ((6*2-1) * 2) (outputs) + (6*4*2) (innerModules) " +
      "+ (5*4) (otherModules) + (6*2-1) (reg) endpoints" taggedAs (Timing, Deprecated) in {
    report(new Top(4, 10, 6, 32)) should equal((6 * 2 - 1) * 2 + 6 * 4 * 2 + 5 * 4 + (6 * 2 - 1))
  }
}
