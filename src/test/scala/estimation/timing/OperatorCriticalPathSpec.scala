/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.timing

import qece.characterization.components.BaseOperator
import qece.backend.Zedboard

import qece.estimation.MetricMap
import qece.estimation.utils.{Operators, TransformFlatSpec}
import qece.estimation.transforms.TransformSeq

import qece.tags.{Estimation, Timing, Deprecated}

class OperatorCriticalPathSpec extends TransformFlatSpec {

  def report(kind: Operators.Operator, bitWidth: Int = 1): MetricMap = {
    getMetrics(
        emitWithTransform(
            new BaseOperator(kind, bitWidth),
            args = args,
            transforms = TransformSeq.timing,
            path = Some(kind.name),
            board = Some(new Zedboard)
        )
    )
  }

  "Adders" should "properly be estimated" taggedAs (Estimation, Timing, Deprecated) in {
    val metrics = report(Operators.ADD, 32)
    metrics("#endpoint") should equal(1)
    metrics("delay") should be < (3.0)
    metrics("delay") should be > (2.9)
    assert(false, "Check estimators")
  }
  "Multipliers" should "properly be estimated" taggedAs (Estimation, Timing, Deprecated) in {
    val metrics = report(Operators.MULT, 16)
    metrics("#endpoint") should equal(1)
    metrics("delay") should be < (1.8)
    metrics("delay") should be > (1.7)
    assert(false, "Check estimators")
  }
  "Binary operators" should "properly be estimated" taggedAs (Estimation, Timing, Deprecated) in {
    val metrics = report(Operators.BIN, 32)
    metrics("#endpoint") should equal(1)
    metrics("delay") should be < (1.4)
    metrics("delay") should be > (1.3)
    assert(false, "Check estimators")
  }
  "Multiplexers" should "properly be estimated" taggedAs (Estimation, Timing, Deprecated) in {
    val metrics = report(Operators.MUX, 32)
    metrics("#endpoint") should equal(1)
    metrics("delay") should be < (1.9)
    metrics("delay") should be > (1.6)
    assert(false, "Check estimators")
  }
  "Comparators" should "properly be estimated" taggedAs (Estimation, Timing, Deprecated) in {
    val metrics = report(Operators.COMP, 32)
    metrics("#endpoint") should equal(1)
    metrics("delay") should be < (2.9)
    metrics("delay") should be > (2.8)
    assert(false, "Check estimators")
  }
  "Dynamic shifters" should "properly be estimated" taggedAs (Estimation, Timing, Deprecated) in {
    val metrics = report(Operators.DSHIFT, 32)
    metrics("#endpoint") should equal(1)
    metrics("delay") should be < (1.7)
    metrics("delay") should be > (1.6)
    assert(false, "Check estimators")
  }
}
