/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.graph

import qece.tags.GraphSpec

import qece.estimation.utils.{Operators, TransformFlatSpec}

import chisel3._
import chisel3.stage.{ChiselGeneratorAnnotation, ChiselStage}
import chisel3.util._

import firrtl.annotations.ModuleTarget

import logger.{LogLevelAnnotation, LogLevel}

class CircuitGraphSpec extends TransformFlatSpec {
  class InnerOp(width: Int) extends MultiIOModule {
    val op1 = IO(Input(UInt(width.W)))
    val io = IO(new Bundle {
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    io.out := op1 + io.op2
  }
  class InnerModule(width: Int) extends MultiIOModule {
    val in   = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.io.op2 := in
    out := op.io.out
    outR := RegNext(op.io.out + outR + in)
  }
  class OtherModule(width: Int) extends MultiIOModule {
    val in   = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.io.op2 := in
    out := op.io.out
    outR := RegNext(op.io.out) * in
  }

  class Top(nbInner: Int = 1, width: Int = 5) extends MultiIOModule {
    val in   = IO(Flipped(Decoupled(Vec(nbInner * 2 - 1, UInt(width.W)))))
    val out  = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))
    val outR = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))

    in.ready := true.B
    val inner = Array.fill(nbInner)(Module(new InnerModule(width)))
    for (i <- 0 until nbInner) {
      inner(i).in := in.bits(i)
      out(i) := inner(i).out
      outR(i) := RegNext(inner(i).outR)
    }
    val other = Array.fill(nbInner - 1)(Module(new OtherModule(width)))
    for (i <- 0 until nbInner - 1) {
      other(i).in := in.bits(i + nbInner)
      out(i + nbInner) := other(i).out
      outR(i + nbInner) := RegNext(other(i).outR)
    }
  }

  private def getGraph(m: => RawModule): CircuitGraph = {
    // trick used to cast ChiselGeneratorAnnotation to AnnotationSeq
    val circuit = getCircuit(
        (new ChiselStage).execute(
            Array("-X", "verilog", "--target-dir", s"$targetDir/lowGraph"),
            Seq(ChiselGeneratorAnnotation(() => m), LogLevelAnnotation(LogLevel.Error))
        )
    )
    CircuitGraph(circuit)
  }

  "Combinatorial CircuitGraph" should "be properly build" taggedAs GraphSpec in {
    val module = getGraph(new InnerOp(5)).modules.head
    module.getOperators(Operators.ADD).size should equal(1)
    module.getOperators(Operators.ADD).head.next.operator should be(Operators.SELECT)
    module.getOperators(Operators.ADD).head.next.hasNexts should be(false)
    module.getOperators(Operators.MULT).size should equal(0)
    module.getOperators(Operators.REG).size should equal(0)
  }

  "Simple operator CircuitGraph" should "be properly built" taggedAs GraphSpec in {
    val graph       = getGraph(new InnerModule(17))
    val innerModule = graph.getModule(ModuleTarget("InnerModule", "InnerModule")).get
    val innerOp     = graph.getModule(ModuleTarget("InnerModule", "InnerOp")).get
    innerOp.getOperators(Operators.REG).size should equal(0)
    innerOp.getOperators(Operators.ADD).size should equal(1)
    innerOp.getOperators(Operators.ADD).foreach { node =>
      node.sources.size should equal(2)
    }
    innerOp.getOperators(Operators.ADD).head.next.operator should be(Operators.SELECT)
    innerModule.getOperators(Operators.REG).size should equal(1)
    innerModule.getOperators(Operators.REG).foreach { reg =>
      reg.sources.size should equal(1)
      reg.sinks.size should equal(1)
    }
    innerModule.getOperators(Operators.REG).head.next.operator should be(Operators.ADD)
  }

  "Complex operator CircuitGraph" should "be properly built" taggedAs GraphSpec in {
    val graph = getGraph(new Top(6, 32))
    val top   = graph.getModule(ModuleTarget("Top", "Top")).get
    val other = graph.getModule(ModuleTarget("Top", "OtherModule")).get
    top.getOperators(Operators.ADD).size should equal(0)
    top.getOperators(Operators.MULT).size should equal(0)
    top.getOperators(Operators.REG).size should equal(6 + 5)
    other.getOperators(Operators.MULT).size should equal(1)
    other.getOperators(Operators.MULT).foreach { node =>
      node.sources.size should equal(2)
    }
  }

  "CircuitGraph" should "build not fail on unknown modules" taggedAs GraphSpec in {
    val graph = getGraph(new Top(6, 32)).getModule(ModuleTarget("Top", "Unknown"))
    graph should be(None)
  }
}
