/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.graph

import qece.tags.GraphSpec

import qece.estimation.utils.{Operators, TransformFlatSpec}

import chisel3._
import chisel3.stage.{ChiselGeneratorAnnotation, ChiselStage}
import chisel3.util._

import firrtl.annotations.ModuleTarget

import logger.{LogLevelAnnotation, LogLevel}

class MemoryGraphSpec extends TransformFlatSpec {
  class InnerOp(width: Int) extends MultiIOModule {
    val op1 = IO(Input(UInt(width.W)))
    val io = IO(new Bundle {
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    io.out := op1 + io.op2
  }
  class InnerModule(nMem: Int, nElem: Int, width: Int) extends MultiIOModule {
    val in     = IO(Input(UInt(width.W)))
    val out    = IO(Output(UInt(width.W)))
    val outMem = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.io.op2 := in
    out := op.io.out

    val memory = Array.fill(nMem)(SyncReadMem(nElem, UInt(width.W)))
    for (i <- 0 until nMem) {
      memory(i).write(in, out)
    }
    outMem := memory.map(_.read(out)).reduce(_ + _)
  }
  class Top(nbInner: Int, nMem: Int, nAddr: Int, width: Int) extends MultiIOModule {
    val in  = IO(Flipped(Decoupled(Vec(nbInner, UInt(width.W)))))
    val out = IO(Output(Vec(nbInner, UInt(width.W))))

    val reg = Array.fill(nbInner)(Reg(UInt(width.W)))

    in.ready := true.B
    val inner = Array.fill(nbInner)(Module(new InnerModule(nMem, nAddr, width)))
    for (i <- 0 until nbInner) {
      inner(i).in := in.bits(i)
      reg(i) := inner(i).outMem
      out(i) := reg(i)
    }
  }

  private def getGraph(m: => RawModule): CircuitGraph = {
    // trick used to cast ChiselGeneratorAnnotation to AnnotationSeq
    val circuit = getCircuit(
        (new ChiselStage).execute(
            Array("-X", "verilog", "--target-dir", s"$targetDir/memLowGraph"),
            Seq(ChiselGeneratorAnnotation(() => m), LogLevelAnnotation(LogLevel.Error))
        )
    )
    CircuitGraph(circuit)
  }

  "Simple memory CircuitGraph" should "be properly built" taggedAs GraphSpec in {
    val graph       = getGraph(new InnerModule(5, 4, 17))
    val innerModule = graph.getModule(ModuleTarget("InnerModule", "InnerModule")).get
    innerModule.getOperators(Operators.MEMORY).size should equal(5)
    innerModule.getOperators(Operators.MEMORY).foreach { mem =>
      mem.sources.size should equal(5)
      mem.sinks.size should equal(1)
    }
  }

  "Hierarchical memory CircuitGraph" should "be properly built" taggedAs GraphSpec in {
    val graph = getGraph(new Top(12, 5, 4, 17))
    val top   = graph.getModule(ModuleTarget("Top", "Top")).get
    top.getOperators(Operators.MEMORY).size should equal(0)
    val innerModule = graph.getModule(ModuleTarget("Top", "InnerModule")).get
    innerModule.getOperators(Operators.MEMORY).size should equal(5)
    innerModule.getOperators(Operators.MEMORY).foreach { mem =>
      mem.sources.size should equal(5)
      mem.sinks.size should equal(1)
    }
  }
}
