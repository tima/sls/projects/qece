/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.transforms

import qece.estimation.{Metric, MetricMap}
import qece.estimation.utils.TransformFlatSpec

import chisel3._

class PreCustomTransformSpec extends TransformFlatSpec {
  class MyModule(nbIter: Int, bitWidth: Int) extends MultiIOModule {
    val in  = IO(Input(UInt(bitWidth.W)))
    val out = IO(Output(UInt()))

    val regVec = Reg(Vec(nbIter, UInt(bitWidth.W)))

    regVec(0) := in
    for (i <- 1 until nbIter) {
      regVec(i) := regVec(i - 1) + in
    }
    out := regVec(nbIter - 1)
  }

  private def run(params: Map[String, Int]): MetricMap = {
    getMetrics(
        emitWithTransform(
            m = new MyModule(params.values.head, params.values.tail.head),
            initialMap = new MetricMap(params.map { case (k, v) => k -> Metric(v) }),
            args = args,
            transforms = TransformSeq.preElab("latency" -> (m => m("nbIter")), "inv" -> (m => 1 / m("latency"))),
            path = Some("customMetric")
        )
    )
  }

  "PreCustomTransformSpec" should "work properly" in {
    val nbIter  = 3
    val metrics = run(Map("nbIter" -> nbIter, "bitWidth" -> 16))
    metrics("latency") should equal(nbIter)
    metrics("inv") should equal(1.0 / nbIter)
  }
}
