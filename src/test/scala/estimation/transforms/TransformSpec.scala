/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.transforms

import qece.backend.Zedboard

import qece.estimation.utils.TransformFlatSpec
import qece.estimation.macros.ReplaceInfo
import qece.estimation.MetricMap

import chisel3._

import qece.tags.{Resources, Estimation}

class TransformSpec extends TransformFlatSpec {
  class Combi(width: Int) extends MultiIOModule {
    val io = IO(new Bundle {
      val op1 = Input(UInt(width.W))
      val op2 = Input(UInt(width.W))
      val out = Output(UInt(width.W))
    })

    io.out := (io.op1 * io.op1) + io.op2
  }

  private def run(m: => MultiIOModule): (Seq[ReplaceInfo], MetricMap) = {
    val annos = emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq.default,
        path = Some("transforms"),
        board = Some(new Zedboard)
    )
    (getReplaceInfos(annos), getMetrics(annos))
  }

  "TransformSpec" should "properly report resources" taggedAs (Resources, Estimation) in {
    val (infos, metrics) = run(new Combi(32))
    infos.size should be(1)
    val mac = infos.head
    mac.isCombi should be(true)
    mac.getParam("inWidth") should equal(32)
    mac.getParam("outFactor") should equal(1)
    mac.getParam("nbRegMul") should equal(0)
    mac.getParam("add") should equal(1)
    mac.getParam("nbRegAdd") should equal(0)
    mac.toConnect.size should equal(4)
    mac.toRemove.size should equal(7)
    metrics("lut") should be(47)
    metrics("ff") should be(0)
    metrics("dsp") should be(3)
    metrics("memory") should be(0)
  }
}
