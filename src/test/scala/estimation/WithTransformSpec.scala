/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation

import qece.QeceContext

import qece.estimation.utils.TransformFlatSpec
import qece.estimation.transforms.TransformSeq
import qece.tags.{Estimation, Long, Synthesis, Resources}

import qece.exploration.annotations.dummy
import qece.exploration.utils.Explorable
import qece.exploration.strategies.utils.Point

import scala.reflect.runtime.universe.TypeTag

import chisel3._

class Combi(
    @dummy width: Int
) extends Explorable {
  val io = IO(new Bundle {
    val op1 = Input(UInt(width.W))
    val op2 = Input(UInt(width.W))
    val out = Output(UInt(width.W))
  })

  io.out := (io.op1 * io.op1) + io.op2
}

class WithTransformSpec extends TransformFlatSpec {

  private def run[T: TypeTag](tfs: TransformSeq, width: Int): MetricMap = {
    estimate[T](
        transforms = tfs,
        config = QeceContext.copy(
          backend = _.copy(targetBoard = "Zedboard"),
          emission = _.copy(targetPath = "withTransform")
        )
    )(Point("width" -> width))
  }

  "WithTransform on estimated resources " should
    "properly report used metrics with default estimators" taggedAs (Estimation, Resources) in {
    val metrics = run[Combi](TransformSeq.default, 32)
    metrics("lut") should be(47.0)
    metrics("ff") should be(0.0)
    metrics("dsp") should be(3.0)
    metrics("memory") should be(0.0)
  }

  "WithTransform with synthesis" should
    "properly report used metrics" taggedAs (Long, Estimation, Resources, Synthesis) in {
    val metrics = run[Combi](TransformSeq.synthesis ++ TransformSeq.default, 32)
    metrics("realLUT") should be(47.0)
    metrics("realFF") should be(0.0)
    metrics("realDSP") should be(3.0)
    metrics("realMem") should be(0.0)
    metrics("realDelay") should be < (14.5)
    metrics("realDelay") should be > (14.4)
    metrics("lut") should be(47.0)
    metrics("ff") should be(0.0)
    metrics("dsp") should be(3.0)
    metrics("memory") should be(0.0)
  }
}
