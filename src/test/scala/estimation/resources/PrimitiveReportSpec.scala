/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources

import qece.tags.Resources

import qece.estimation.utils.{Operators, TransformFlatSpec}

import qece.estimation.transforms.TransformSeq
import qece.estimation.resources.transforms.PrimitiveReport

import qece.estimation.macros.components.MacUnit
import qece.backend.{FPGA, Zedboard}

import chisel3._

class PrimitiveReportSpec extends TransformFlatSpec {
  class InnerOp(width: Int) extends MultiIOModule {
    val op1 = IO(Input(UInt(width.W)))
    val op2 = IO(Input(UInt(width.W)))
    val out = IO(Output(UInt(width.W)))

    out := op1 + op2
  }
  class InnerModule(width: Int) extends MultiIOModule {
    val in   = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.op2 := in
    out := op.out
    outR := RegNext(op.out) + in
  }
  class OtherModule(width: Int) extends MultiIOModule {
    val in   = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.op2 := in
    out := op.out
    outR := RegNext(op.out) * in
  }

  class Top(nbInner: Int = 1, width: Int = 5) extends MultiIOModule {
    val in   = IO(Input(Vec(nbInner * 2 - 1, UInt(width.W))))
    val out  = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))
    val outR = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))

    val inner = Array.fill(nbInner)(Module(new InnerModule(width)))
    for (i <- 0 until nbInner) {
      inner(i).in := in(i)
      out(i) := inner(i).out
      outR(i) := RegNext(inner(i).outR)
    }
    val other = Array.fill(nbInner - 1)(Module(new OtherModule(width)))
    for (i <- 0 until nbInner - 1) {
      other(i).in := in(i + nbInner)
      out(i + nbInner) := other(i).out
      outR(i + nbInner) := RegNext(other(i).outR * in(i))
    }
  }

  def report(m: => MultiIOModule, board: Option[FPGA] = None): ModulePrimitives = {
    val annos = emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq(new PrimitiveReport),
        path = Some("early"),
        board = board
    )
    getPrimitives(annos).module
  }

  "Adders report" should "be accurate" taggedAs Resources in {
    val top = report(new Top(9, 32))
    top.getPrimitives(Operators.ADD).values.map(_.total).reduceOption(_ + _).getOrElse(-1) should equal(26)
  }

  "Multipliers report" should "be accurate" taggedAs Resources in {
    val top = report(new Top(9, 32))
    top.getPrimitives(Operators.MULT).values.map(_.total).reduceOption(_ + _).getOrElse(-1) should equal(16)
  }

  "Registers report" should "be accurate" taggedAs Resources in {
    val top = report(new Top(9, 32))
    top.getPrimitives(Operators.REG).values.map(_.total).reduceOption(_ + _).getOrElse(-1) should equal(34)
  }

  "MAC report" should "be accurate" taggedAs Resources in {
    val top   = report(new Top(9, 32), Some(new Zedboard))
    val macs  = top.getMacro(MacUnit)
    val noReg = macs.filter(_.mac.params == List(32, 1, 0, 0, 0)).head
    noReg.mac.params should equal(List(32, 1, 0, 0, 0))
    noReg.total should equal(8)
    noReg.local should equal(0)
    val reg = macs.filter(_.mac.params == List(32, 1, 1, 0, 0)).head
    reg.mac.params should equal(List(32, 1, 1, 0, 0))
    reg.total should equal(8)
    reg.local should equal(8)
    top.getPrimitives(Operators.MULT).values.map(_.total).reduceOption(_ + _).getOrElse(0) should equal(0)
  }
}
