/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources

import qece.backend.Zedboard
import qece.characterization.components.BaseMemory

import qece.estimation.utils.{TransformFlatSpec, Operators}
import qece.estimation.transforms.TransformSeq
import qece.estimation.macros.components.MemoryBlock
import qece.estimation.MetricMap

import qece.tags.{Estimation, Resources, Deprecated}

class EstimatedMemoryReportSpec extends TransformFlatSpec {

  def report(nBank: Int, nElem: Int, elemW: Int): (ModulePrimitives, MetricMap) = {
    val annos = emitWithTransform(
        new BaseMemory(nBank, nElem, elemW),
        args = args,
        transforms = TransformSeq.resources,
        path = Some("memEstimation"),
        board = Some(new Zedboard)
    )
    (getPrimitives(annos).module, getMetrics(annos))
  }

  "Memory resources" should "be properly reported" taggedAs (Estimation, Resources) in {
    {
      val (primitives, metrics) = report(6, 32, 32)
      primitives.getPrimitives(Operators.REG).values should be(Seq.empty)
      primitives.getMacro(MemoryBlock).size should be(1)
      val mem = primitives.getMacro(MemoryBlock).head
      mem.mac.params should be(Seq(32, 32, 0))
      mem.total should be(6)
      metrics("lut") should be(60.0)
      metrics("ff") should be(0.0)
      metrics("dsp") should be(0.0)
      metrics("memory") should be(108.0)
    }

    {
      val (primitives, metrics) = report(32, 256, 80)
      primitives.getPrimitives(Operators.REG).values should be(Seq.empty)
      primitives.getMacro(MemoryBlock).size should be(1)
      val mem = primitives.getMacro(MemoryBlock).head
      mem.mac.params should be(Seq(80, 256, 0))
      mem.total should be(32)
      metrics("lut") should be(512.0)
      metrics("ff") should be(0.0)
      metrics("dsp") should be(0.0)
      metrics("memory") should be(1008.0)
    }

    {
      val (primitives, metrics) = report(32, 120, 80)
      primitives.getPrimitives(Operators.REG).values should be(Seq.empty)
      primitives.getMacro(MemoryBlock).size should be(1)
      val mem = primitives.getMacro(MemoryBlock).head
      mem.mac.params should be(Seq(80, 120, 0))
      mem.total should be(32)
      metrics("lut") should be(448.0)
      metrics("ff") should be(0.0)
      metrics("dsp") should be(0.0)
      metrics("memory") should be(998.0)
    }
  }

  // Used to test memory estimation without macro replacement
  "Memory resources without macroblock" should "be properly reported" taggedAs (Deprecated, Estimation, Resources) in {
    {
      val (_, metrics) = report(32, 256, 80)
      metrics("lut") should be(128.0)
      metrics("ff") should be(288.0)
      metrics("dsp") should be(0.0)
      metrics("memory") should be(640.0)
    }
  }
}
