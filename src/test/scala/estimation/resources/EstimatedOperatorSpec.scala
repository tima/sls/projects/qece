/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources

import qece.characterization.components.BaseOperator

import qece.backend.Zedboard
import qece.estimation.utils.{Operators, TransformFlatSpec, Access}
import qece.estimation.transforms.TransformSeq

import qece.tags.{Resources, Estimation}

class EstimatedOperatorSpec extends TransformFlatSpec with Access {

  def report(kind: Operators.Operator, bitWidth: Int = 1): (BigInt, BigInt, BigInt, BigInt) = {
    val annos = emitWithTransform(
        new BaseOperator(kind, bitWidth),
        args = args,
        transforms = TransformSeq.resources,
        path = Some(kind.name),
        board = Some(new Zedboard)
    )
    val top = getResources(annos).module
    (top.lut, top.ff, top.dsp, top.memory)
  }

  "Adders" should "properly be reported" taggedAs (Estimation, Resources) in {
    report(Operators.ADD, 32) should be(32, 0, 0, 0)
  }
  "Multipliers" should "properly be reported" taggedAs (Estimation, Resources) in {
    report(Operators.MULT, 16) should be(0, 0, 1, 0)
  }
  "Binary operators" should "properly be reported" taggedAs (Estimation, Resources) in {
    report(Operators.BIN, 32) should be(32, 0, 0, 0)
  }
  "Multiplexers" should "properly be reported" taggedAs (Estimation, Resources) in {
    report(Operators.MUX, 32) should be(32, 0, 0, 0)
  }
  "Comparators" should "properly be reported" taggedAs (Estimation, Resources) in {
    report(Operators.COMP, 32) should be(16, 0, 0, 0)
  }
  "Dynamic shifters" should "properly be reported" taggedAs (Estimation, Resources) in {
    report(Operators.DSHIFT, 32) should be(17, 0, 0, 0)
  }
}
