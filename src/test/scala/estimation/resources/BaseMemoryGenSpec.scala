/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources

import qece.tags.Resources

import qece.characterization.components.BaseMemory
import qece.estimation.utils.Operators

import qece.estimation.utils.TransformFlatSpec
import qece.estimation.transforms.TransformSeq

import qece.estimation.resources.transforms.PrimitiveReport

class BaseMemoryGenSpec extends TransformFlatSpec {

  def report(nBank: Int, nElem: Int, elemW: Int): BigInt = {
    val annos = emitWithTransform(
        new BaseMemory(nBank, nElem, elemW),
        args = args,
        transforms = TransformSeq(new PrimitiveReport),
        path = Some("mem")
    )
    getPrimitives(annos).module.getPrimitives(Operators.MEMORY).values.map(_.total).reduce(_ + _)
  }

  "Memory" should "properly be reported" taggedAs Resources in {
    report(2, 4, 32) should equal(2)
  }
}
