/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources

import qece.backend.{FPGA, Zedboard}

import chisel3._

import qece.estimation.MetricMap
import qece.estimation.utils.TransformFlatSpec
import qece.estimation.transforms.TransformSeq

import qece.tags.{Estimation, Resources}

class EstimatedResourceReportSpec extends TransformFlatSpec {
  class InnerOp(width: Int) extends MultiIOModule {
    val op1 = IO(Input(UInt(width.W)))
    val op2 = IO(Input(UInt(width.W)))
    val out = IO(Output(UInt(width.W)))

    out := op1 + op2
  }
  class InnerModule(width: Int) extends MultiIOModule {
    val in   = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.op2 := in
    out := op.out
    outR := RegNext(op.out) + in
  }
  class OtherModule(width: Int) extends MultiIOModule {
    val in   = IO(Input(UInt(width.W)))
    val out  = IO(Output(UInt(width.W)))
    val outR = IO(Output(UInt(width.W)))

    val op = Module(new InnerOp(width))
    op.op1 := in
    op.op2 := in
    out := op.out
    outR := RegNext(op.out) * in
  }

  class Top(nbInner: Int = 1, width: Int = 5) extends MultiIOModule {
    val in   = IO(Input(Vec(nbInner * 2 - 1, UInt(width.W))))
    val out  = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))
    val outR = IO(Output(Vec(nbInner * 2 - 1, UInt(width.W))))

    val inner = Array.fill(nbInner)(Module(new InnerModule(width)))
    for (i <- 0 until nbInner) {
      inner(i).in := in(i)
      out(i) := inner(i).out
      outR(i) := RegNext(inner(i).outR)
    }
    val other = Array.fill(nbInner - 1)(Module(new OtherModule(width)))
    for (i <- 0 until nbInner - 1) {
      other(i).in := in(i + nbInner)
      out(i + nbInner) := other(i).out
      outR(i + nbInner) := RegNext(other(i).outR)
    }
  }

  def report(m: => MultiIOModule, board: FPGA): MetricMap = {
    val annos = emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq.resources,
        path = Some("estimation"),
        board = Some(board)
    )
    getResources(annos).asMetricMap(board)
  }

  "Resources" should "be properly reported" taggedAs (Estimation, Resources) in {
    val inner   = 16
    val circuit = report(new Top(inner, 32), new Zedboard)

    circuit("lut") should be(1729.0)
    circuit("ff") should be(1984.0)
    circuit("dsp") should be((inner - 1) * 3)
    circuit("memory") should be(0)
    circuit("%lut") should be(0.0325)
    circuit("%ff") should be > (0.01864)
    circuit("%ff") should be < (0.01865)
    circuit("%dsp") should be > (0.204)
    circuit("%dsp") should be < (0.205)
    circuit("%memory") should be(0)
  }
}
