/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.estimation.resources

import qece.backend.{FPGA, Zedboard}

import chisel3._
import chisel3.experimental._

import qece.estimation.MetricMap
import qece.estimation.utils.TransformFlatSpec
import qece.estimation.transforms.TransformSeq

import qece.tags.{Estimation, Resources}

class EstimatedFixedPointSpec extends TransformFlatSpec {

  class InnerOp(w: Int, bp: Int) extends MultiIOModule {
    require(bp < w, s"binary point $bp can't be greater than total width $w")
    val op1 = IO(Input(FixedPoint(w.W, bp.BP)))
    val op2 = IO(Input(FixedPoint(w.W, bp.BP)))
    val op3 = IO(Input(FixedPoint(w.W, (bp / 2).BP)))
    val out = IO(Output(FixedPoint(w.W, bp.BP)))

    out := (op1 * op2) + op3
  }

  def report(m: => MultiIOModule, board: FPGA): MetricMap = {
    val annos = emitWithTransform(
        m,
        args = args,
        transforms = TransformSeq.resources,
        path = Some("fixedPointEstimation"),
        board = Some(board)
    )
    getResources(annos).asMetricMap(board)
  }

  "Resources" should "be properly reported" taggedAs (Estimation, Resources) in {
    val circuit = report(new InnerOp(32, 16), new Zedboard)

    circuit("lut") should be(79.0)
    circuit("ff") should be(0.0)
    circuit("dsp") should be(4.0)
    circuit("memory") should be(0)
    circuit("%lut") should be > (0.0014)
    circuit("%lut") should be < (0.0015)
    circuit("%ff") should be(0.0)
    circuit("%dsp") should be > (0.018)
    circuit("%dsp") should be < (0.019)
    circuit("%memory") should be(0)
  }
}
