/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.utils

import qece.exploration.annotations._

import chisel3._

/** Dumb module for testing. */
class MyModule(
    @linear(0, 3) nElem: Int,
    @qualityOfResult @ImpactMetric("test") @pow2(5, 7) bitWidth: Int,
    @qualityOfResult @enum(17, 34, 50) const: Int,
    @pow2(2, 6) nConst: Int
) extends Explorable {
  val op1      = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val op2      = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val out      = IO(Output(UInt()))
  val outConst = IO(Output(Vec(nConst, UInt())))

  out := VecInit((op1 zip op2).map { case (x, y) => x * y }).reduceTree(_ + _)
  outConst := VecInit.tabulate(nConst)(_ => const.U)
}

class ReflectSpec extends ExplorationFlatSpec {
  "Reflect" should "work correctly to inspect annotations" in {
    val const  = new Constructor[MyModule]
    val params = const.paramMap
    val qor    = const.factory.getImpactingParams(new qualityOfResult)
    val test   = const.factory.getImpactingParams(new ImpactMetric("test"))
    params("nElem") should be(List(0, 1, 2, 3))
    params("bitWidth") should be(List(32, 64, 128))
    params("const") should be(List(17, 34, 50))
    qor should be(List("bitWidth", "const"))
    test should be(List("bitWidth"))
  }
}
