package qece.exploration

import qece.QeceContext

package object strategies {
  val testConfig = QeceContext.copy(emission = _.copy(basePath = "test_run_dir"), parallel = _.copy(numThread = 4))
}
