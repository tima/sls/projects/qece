/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies

import qece.QeceContext

import qece.exploration.annotations._

import qece.estimation.transforms.TransformSeq

import qece.exploration.utils.{ExplorationFlatSpec, Explorable}
import qece.exploration.strategies.utils.Params

import qece.tags.{Exploration, Synthesis, Long, Resources}

import chisel3._

class SimpleModule(
    @pow2(0, 5) bitWidth: Int,
    @linear(1, 5) nElem: Int
) extends Explorable {
  val op1 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val op2 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val res = IO(Output(UInt(bitWidth.W)))

  val tmp = Wire(Vec(nElem, UInt()))
  tmp := (op1 zip op2).map { case (x, y) => x * y }
  res := tmp.reduceTree((x, y) => x + y, x => x)
}


class ExhaustiveExplorationSpec extends ExplorationFlatSpec {

  "CSV generation" should "properly be done" taggedAs (Exploration, Resources) in {
    val path    = "csvGeneration"
    val genPath = s"${QeceContext.emission.basePath}/$path/gen.csv"
    val builder = StrategyBuilder(testConfig).withPath(path)
    val strat = builder.buildStrategy(
        builder.sort[SimpleModule](TransformSeq.resources, m => m("lut") + m("nElem"), (_ < _)),
        builder.sort[SimpleModule](TransformSeq.empty, _("ff"), (_ > _))
    )
    strat.explore[SimpleModule]
    strat.writeBack(genPath)
    new java.io.File(genPath).exists should be(true)
  }

  "Simple module" should "properly be explored" taggedAs (Exploration, Resources) in {
    val strat = StrategyBuilder(testConfig).withPath("baseExploration")
    val results = strat.explore[SimpleModule](
        strat.sort[SimpleModule](TransformSeq.resources, m => m("lut") + m("nElem"), (_ < _))
    )
    results.head.cost should be(1.0)
    results.last.params should be(Params(Seq("bitWidth" -> 32, "nElem" -> 5)))
  }

  "Synthesis exploration" should "be possible" taggedAs (Exploration, Long, Synthesis, Resources) in {
    val strat = StrategyBuilder(testConfig).withPath("baseExplorationSynth")
    val results = strat.explore[SimpleModule](
        strat.sort[SimpleModule](
            TransformSeq.synthesis ++ TransformSeq.resources,
            _("realLUT"),
            (_ < _)
        )
    )
    results.head.cost should be(0.0)
    results.last.params should be(Params(Seq("bitWidth" -> 8, "nElem" -> 5)))
  }
}
