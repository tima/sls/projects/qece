/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies

import qece.exploration.annotations._

import qece.estimation.MetricMap
import qece.estimation.transforms.TransformSeq

import qece.exploration.utils.{ExplorationFlatSpec, Explorable}
import qece.exploration.strategies.utils.Point

import chisel3._

import qece.tags.{Exploration, Resources, Long, Synthesis}

class OtherReduceModuleExample(
    @pow2(0, 4) bitWidth: Int,
    @linear(1, 8) nElem: Int
) extends Explorable {
  val op1 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val op2 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val res = IO(Output(UInt(bitWidth.W)))

  val tmp = Wire(Vec(nElem, UInt()))
  tmp := (op1 zip op2).map { case (x, y) => x * y }
  res := tmp.reduceTree((x, y) => x + y, x => x)
}

class GradientDescentStrategySpec extends ExplorationFlatSpec {

  private def run(
      path: String,
      func: MetricMap => Double,
      cmp: (Double, Double) => Boolean,
      initValue: Option[Point] = None
  ): (Seq[Point], Seq[Point]) = {
    val strat = StrategyBuilder(testConfig).withPath(path)
    val results = strat.explore[OtherReduceModuleExample](
        strat.gradient[OtherReduceModuleExample](TransformSeq.resources, func, cmp, initValue)
    )
    val exhaustiveResults = strat.explore[OtherReduceModuleExample](
        strat.sort[OtherReduceModuleExample](TransformSeq.resources, func, cmp)
    )
    (results.toSeq, exhaustiveResults.toSeq)
  }

  "Using increasing function" should
    "properly be explored using gradient descent" taggedAs (Exploration, Resources) in {
    val path = "increasingGradient"
    val (results, exhaustiveResults) = run(
        path = path,
        func = m => if (m("lut") < 500) m("lut") else 0.0,
        cmp = (_ > _)
    )

    results.size should be > (20)
    results.head.cost should be(exhaustiveResults.head.cost)
  }

  "Using decreasing function" should
    "properly be explored using gradient descent" taggedAs (Exploration, Resources) in {
    val path = "decreasingGradient"
    val (results, exhaustiveResults) = run(
        path = path,
        func = 1.0 / _("lut"),
        cmp = (_ < _)
    )
    results.size should be > (20)
    scala.math.abs(results.head.cost - exhaustiveResults.head.cost) should be < (1.0)
  }

  "Changing initial value" should
    "change exploration result using gradient descent" taggedAs (Exploration, Resources) in {
    val path = "decreasingGradient"
    val (results, exhaustiveResults) = run(
        path = path,
        func = 1.0 / _("lut"),
        cmp = (_ < _),
        initValue = Some(Point("bitWidth" -> 16, "nElem" -> 2))
    )
    results.size should be > (20)
    results.head.cost should be(exhaustiveResults.head.cost)
  }

  "Using iterative gradient descent" should
    "enable quicker convergence on best fit" taggedAs (Exploration, Synthesis, Long, Resources) in {
    val strat = StrategyBuilder(testConfig).withPath("iterativeGradientDescent")
    val composedResults =
      strat.explore[OtherReduceModuleExample](
          strat.compose(
              strat.gradient[OtherReduceModuleExample](
                  TransformSeq.resources,
                  m => if (m("lut") > 30.0) 0.0 else m("lut"),
                  (_ > _)
              ),
              strat.sort[OtherReduceModuleExample](
                  TransformSeq.empty,
                  _("lut"),
                  (_ > _)
              ),
              strat.gradient[OtherReduceModuleExample](
                  TransformSeq.synthesis,
                  m => if (m("realLUT") > 30.0) 0.0 else m("realLUT"),
                  (_ > _)
              )
          )
      )
    val simpleResults = strat.explore[OtherReduceModuleExample](
        strat.gradient[OtherReduceModuleExample](
            TransformSeq.synthesis,
            m => if (m("realLUT") > 30.0) 0.0 else m("realLUT"),
            (_ > _),
            numThread = 4
        )
    )
    composedResults.size should be(6)
    simpleResults.size should be(12)
    composedResults.size should be < (simpleResults.size)
    composedResults.head.cost should be < (simpleResults.head.cost)
  }
}
