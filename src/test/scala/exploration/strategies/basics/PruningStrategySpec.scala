/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies

import qece.estimation.transforms.TransformSeq

import qece.exploration.annotations._
import qece.exploration.utils.{ExplorationFlatSpec, Explorable}
import qece.exploration.strategies.utils.Params

import qece.tags.{Synthesis, Long, Resources, Exploration}

import chisel3._

class ReduceModuleExample(
    @pow2(0, 2) bitWidth: Int,
    @linear(1, 4) nElem: Int
) extends Explorable {
  val op1 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val op2 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val res = IO(Output(UInt(bitWidth.W)))

  val tmp = Wire(Vec(nElem, UInt()))
  tmp := (op1 zip op2).map { case (x, y) => x * y }
  res := tmp.reduceTree((x, y) => x + y, x => x)
}

class PruningExplorationSpec extends ExplorationFlatSpec {

  "Simple pruning" should
    "simple enable space reduction" taggedAs (Exploration, Resources) in {
    val strat = StrategyBuilder(testConfig).withPath("simplePruningExploration")
    val results = strat.explore[ReduceModuleExample](
        strat.compose(
            strat.prune[ReduceModuleExample](TransformSeq.resources, m => m("bitWidth") > 1 & m("lut") < 6.0),
            strat.sort[ReduceModuleExample](TransformSeq.empty, _("lut"), (_ > _))
        )
    )
    results.size should be(10)
    results.head.cost should be(60.0)
    results.head.params should be(Params(Seq("bitWidth" -> 4, "nElem" -> 4)))
    results.last.params should be(Params(Seq("bitWidth" -> 1, "nElem" -> 1)))
  }

  "Iterative pruning" should
    "simple enable space reduction" taggedAs (Exploration, Synthesis, Long, Resources) in {
    val strat = StrategyBuilder(testConfig).withPath("iterativePruningExploration")
    val results = strat.explore[ReduceModuleExample](
        strat.compose(
            strat.prune[ReduceModuleExample](TransformSeq.resources, _("lut") > 6.0),
            strat.prune[ReduceModuleExample](TransformSeq.empty, _("lut") < 3.0),
            strat.sort[ReduceModuleExample](TransformSeq.synthesis, m => m("realLUT") / m("nElem"), (_ > _))
        )
    )
    results.size should be(4)
    results.head.cost should be(6.0)
    results.head.params should be(Params(Seq("bitWidth" -> 4, "nElem" -> 1)))
    results.last.params should be(Params(Seq("bitWidth" -> 1, "nElem" -> 3)))
  }
}
