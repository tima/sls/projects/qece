/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies

import qece.exploration.annotations._

import qece.estimation.MetricMap
import qece.estimation.transforms.TransformSeq

import qece.exploration.utils.{ExplorationFlatSpec, Constructor}

import qece.tags.{Exploration, Estimation}
import qece.exploration.strategies.utils._

class ReduceDimensionSpec extends ExplorationFlatSpec {
  val strat         = StrategyBuilder(testConfig).withPath("reduceDimension")
  val constructor   = new Constructor[My4DModule]

  def pruning(m: MetricMap): Boolean = 
    (m("nElem") == 1 && m("bitWidth") == 32) || (m("nConst") == 1) || (m("nConst") == 5 && m("const") == 3)

  "Reducing dimension of SeqSpace" should "properly be working" taggedAs (Exploration) in {
    val toMinSeqStrat = strat.buildStrategy[My4DModule](
      strat.sort[My4DModule](TransformSeq.empty, _ => 0.0, (_<=_)), // force SeqSpace propagation
      strat.prune[My4DModule](TransformSeq.empty, pruning),
      strat.reduceDimension[My4DModule](new qualityOfResult, true)
    )
    val toMinSeqSpace = toMinSeqStrat.explore[My4DModule]
    toMinSeqSpace.size should be (6)
    Seq(
      List("bitWidth" -> 64, "const" -> 3),
      List("bitWidth" -> 64, "const" -> 15),
      List("bitWidth" -> 128, "const" -> 3),
      List("bitWidth" -> 128, "const" -> 15),
      List("bitWidth" -> 32, "const" -> 3),
      List("bitWidth" -> 32, "const" -> 15),
    ).map(l => Point(l)).foreach{
      p => toMinSeqSpace contains p should be (true)
    }
    val toMinMin = toMinSeqSpace.min
    toMinMin.metrics("nElem") should be (2)
    toMinMin.metrics("nConst") should be (2)
    val toMinMax = toMinSeqSpace.max
    toMinMax.metrics("nElem") should be (1)
    toMinMax.metrics("nConst") should be (2)

    val toMaxSeqStrat = strat.buildStrategy[My4DModule](
      strat.sort[My4DModule](TransformSeq.empty, _ => 0.0, (_<=_)), // force SeqSpace propagation
      strat.prune[My4DModule](TransformSeq.empty, 
        m => pruning(m) || m("const") == 3 || (m("nElem") == 4 && m("bitWidth") == 128)),
      strat.reduceDimension[My4DModule](new qualityOfResult, false)
    )
    val toMaxSeqSpace = toMaxSeqStrat.explore[My4DModule]
    toMaxSeqSpace.size should be (3)
    Seq(
      List("bitWidth" -> 64, "const" -> 15),
      List("bitWidth" -> 128, "const" -> 15),
      List("bitWidth" -> 32, "const" -> 15),
    ).map(l => Point(l)).foreach{
      p => toMaxSeqSpace contains p should be (true)
    }
    val toMaxMin = toMaxSeqSpace.min
    toMaxMin.metrics("nElem") should be (4)
    toMaxMin.metrics("nConst") should be (5)
    val toMaxMax = toMaxSeqSpace.max
    toMaxMax.metrics("nElem") should be (3)
    toMaxMax.metrics("nConst") should be (5)
  }

  "Reducing dimension of Space2D" should "properly be working" taggedAs (Exploration) in {
    val toMinMatStrat = strat.buildStrategy[My2DModule](
      strat.prune[My2DModule](TransformSeq.empty, 
        m => (m("nElem") == 1 || (m("nElem") == 2 && m("bitWidth") == 64)) || (m("nElem") == 3 && m("bitWidth") == 32)
      ),
      strat.reduceDimension[My2DModule](new qualityOfResult, true)
    )
    val toMinMatSpace = toMinMatStrat.explore[My2DModule]
    toMinMatSpace.size should be (3)
    Seq(
      List("bitWidth" -> 32),
      List("bitWidth" -> 64),
      List("bitWidth" -> 128)
    ).map(l => Point(l)).foreach{
      p => toMinMatSpace contains p should be (true)
    }
    toMinMatSpace.min.metrics("nElem") should be (2)
    toMinMatSpace.filter(_.metrics("bitWidth") == 64).head.metrics("nElem") should be (3)
    toMinMatSpace.max.metrics("nElem") should be (2)

    val toMaxMatStrat = strat.buildStrategy[My2DModule](
      strat.prune[My2DModule](TransformSeq.empty, 
        m => (m("nElem") == 1 || (m("nElem") == 2 && m("bitWidth") == 64)) || (m("nElem") == 3 && m("bitWidth") == 32)
      ),
      strat.reduceDimension[My2DModule](new qualityOfResult, false)
    )
    val toMaxMatSpace = toMaxMatStrat.explore[My2DModule]
    toMaxMatSpace.size should be (3)
    Seq(
      List("bitWidth" -> 32),
      List("bitWidth" -> 64),
      List("bitWidth" -> 128)
    ).map(l => Point(l)).foreach{
      p => toMaxMatSpace contains p should be (true)
    }
    toMaxMatSpace.foreach(_.metrics("nElem") should be (4))

    val toMinMatStrat2 = strat.buildStrategy[My2DModule](
      strat.prune[My2DModule](TransformSeq.empty, 
        m => (m("nElem") == 1 || (m("nElem") == 2 && m("bitWidth") == 64)) || (m("nElem") == 3 && m("bitWidth") == 32)
      ),
      strat.reduceDimension[My2DModule](new ImpactMetric("nElem"), true)
    )
    val toMinMatSpace2 = toMinMatStrat2.explore[My2DModule]
    toMinMatSpace2.size should be (3)
    Seq(
      List("nElem" -> 2),
      List("nElem" -> 3),
      List("nElem" -> 4)
    ).map(l => Point(l)).foreach{
      p => toMinMatSpace2 contains p should be (true)
    }
    toMinMatSpace2.min.metrics("bitWidth") should be (32)
    toMinMatSpace2.filter(_.metrics("nElem") == 3).head.metrics("bitWidth") should be (64)
    toMinMatSpace2.max.metrics("bitWidth") should be (32)

    val toMaxMatStrat2 = strat.buildStrategy[My2DModule](
      strat.prune[My2DModule](TransformSeq.empty, 
        m => ((m("nElem") == 2 && m("bitWidth") == 64)) || (m("nElem") == 3 && m("bitWidth") == 32)
      ),
      strat.reduceDimension[My2DModule](new ImpactMetric("nElem"), false)
    )
    val toMaxMatSpace2 = toMaxMatStrat2.explore[My2DModule]
    toMaxMatSpace2.size should be (4)
    Seq(
      List("nElem" -> 1),
      List("nElem" -> 2),
      List("nElem" -> 3),
      List("nElem" -> 4)
    ).map(l => Point(l)).foreach{
      p => toMaxMatSpace2 contains p should be (true)
    }
    toMaxMatSpace2.foreach(_.metrics("bitWidth") should be (128))
  }

  "Reducing dimension of Space4D" should "properly be working" taggedAs (Exploration) in {
    val myStrat = strat.buildStrategy[My4DModule](
      strat.prune[My4DModule](TransformSeq.empty, pruning),
      strat.reduceDimension[My4DModule](new qualityOfResult, true)
    )
    val mySpace = myStrat.explore[My4DModule]
    mySpace.size should be (6)
    Seq(
      List("bitWidth" -> 32, "const" -> 3),
      List("bitWidth" -> 32, "const" -> 15),
      List("bitWidth" -> 64, "const" -> 3),
      List("bitWidth" -> 64, "const" -> 15),
      List("bitWidth" -> 128, "const" -> 3),
      List("bitWidth" -> 128, "const" -> 15),
    ).map(l => Point(l)).foreach{
      p => mySpace contains p should be (true)
    }
    mySpace.min.metrics("nElem") should be (2)
    mySpace.min.metrics("nConst") should be (2)
    mySpace.max.metrics("nElem") should be (1)
    mySpace.max.metrics("nConst") should be (2)
  }

  "Reducing dimension before sorting" should "properly be working" taggedAs (Exploration, Estimation) in {
    val myStrat = strat.buildStrategy[My4DModule](
      strat.prune[My4DModule](TransformSeq.empty, pruning),
      strat.reduceDimension[My4DModule](new qualityOfResult, true),
      strat.sort[My4DModule](TransformSeq.resources, _("lut"), (_<_))
    )
    val mySpace = myStrat.explore[My4DModule]
    mySpace.size should be (6)
    mySpace.head.metrics("bitWidth") should be (64)
  }
}
