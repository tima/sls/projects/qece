/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import qece.estimation.MetricMap

import qece.exploration.utils.{Constructor, Explorable, ExplorationFlatSpec}
import qece.exploration.annotations._

import chisel3._

class My1DModule(
    @linear(1, 4) nElem: Int
) extends Explorable {
  val bitWidth = 4
  val op1      = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val op2      = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val out      = IO(Output(UInt()))

  out := VecInit((op1 zip op2).map { case (x, y) => x * y }).reduceTree(_ + _)
}

class My2DModule(
    @ImpactMetric("nElem") @linear(1, 4) nElem: Int,
    @qualityOfResult @ImpactMetric("test") @pow2(5, 7) bitWidth: Int
) extends Explorable {
  val op1 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val op2 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val out = IO(Output(UInt()))

  out := VecInit((op1 zip op2).map { case (x, y) => x * y }).reduceTree(_ + _)
}

class My3DModule(
    @linear(1, 4) @ImpactMetric("test") nElem: Int,
    @qualityOfResult @ImpactMetric("test") @pow2(5, 7) bitWidth: Int,
    @enum(15, 3) @ImpactMetric("qece") const: Int
) extends Explorable {
  val op1 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val op2 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val out = IO(Output(UInt()))
  val co  = IO(Output(UInt()))

  out := VecInit((op1 zip op2).map { case (x, y) => x * y }).reduceTree(_ + _)
  co := WireInit(const.U)
}

class My4DModule(
    @linear(1, 4) nElem: Int,
    @qualityOfResult @ImpactMetric("test") @pow2(5, 7) bitWidth: Int,
    @enum(3, 15) @qualityOfResult const: Int,
    @linear(1, 5) nConst: Int
) extends Explorable {
  val op1 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val op2 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val out = IO(Output(UInt()))
  val co  = IO(Output(Vec(nConst, UInt())))

  out := VecInit((op1 zip op2).map { case (x, y) => x * y }).reduceTree(_ + _)
  for (i <- 0 until nConst) {
    co(i) := WireInit(const.U(bitWidth.W))
  }
}

class My5DModule(
    @linear(1, 4) nElem: Int,
    @qualityOfResult @ImpactMetric("test") @pow2(5, 7) bitWidth: Int,
    @enum(3, 15) const: Int,
    @linear(1, 5) @qualityOfResult nConst: Int,
    @pow2(2, 6) @qualityOfResult zero: Int
) extends Explorable {
  val op1 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val op2 = IO(Input(Vec(nElem, UInt(bitWidth.W))))
  val out = IO(Output(UInt()))
  val co  = IO(Output(Vec(nConst, UInt())))
  val ze  = IO(Output(Vec(zero, UInt())))

  out := VecInit((op1 zip op2).map { case (x, y) => x * y }).reduceTree(_ + _)
  for (i <- 0 until nConst) {
    co(i) := WireInit(const.U)
  }
  for (i <- 0 to zero) {
    ze(i) := WireInit(0.U)
  }
}

class MatrixSpaceSpec extends ExplorationFlatSpec {
  "Space1D methods" should "be properly working on 1D modules" in {
    val const = new Constructor[My1DModule]

    // testing constructor
    val space = const.buildMatrixSpace

    // testing size
    space.size should be(4)

    // testing filter
    val filtered = space.filter(_.metrics("nElem") == 1)
    filtered.size should be(1)

    //testing sparsity
    space.asInstanceOf[MatrixSpace[My1DModule]].isSparse should be (false)
    filtered.asInstanceOf[MatrixSpace[My1DModule]].isSparse should be (true)

    // testing foreach
    filtered.foreach(_.metrics("nElem") == 1 should be(true))

    // testing toSeq
    filtered.toSeq.size should be(1)

    // testing mapPoint
    val enhanced = filtered.mapPoint(p => p.copy(metrics = p.metrics ++ MetricMap("test" -> 1.0)))
    enhanced.size should be(1)
    enhanced.foreach(p => p.metrics("test") should be(1.0))

    // testing map
    val map = enhanced.map(p => p.metrics("test"))
    map.foreach(_ should be(1.0))

    // testing filterNot
    val filteredNot = space.filterNot(_.metrics("nElem") == 1)
    filteredNot.size should be(3)

    // testing indexWhere on Point
    space.asInstanceOf[MatrixSpace[My1DModule]].indexWhere(Point(List("nElem" -> 1))) should be(List(0))
    filtered.asInstanceOf[MatrixSpace[My1DModule]].indexWhere(Point(List("nElem" -> 5))) should be(List(-1))

    // testing indexWhere on Seq
    space.asInstanceOf[MatrixSpace[My1DModule]].indexWhere(Seq(1)) should be(List(0))
    filtered.asInstanceOf[MatrixSpace[My1DModule]].indexWhere(Seq(5)) should be(List(-1))

    // testing min and max
    val (min, max) = (space.min, space.max)
    min.params.values should be(List(1))
    max.params.values should be(List(4))

    // testing getNeighbours
    space.getNeighbours(min, Norm.One).size should be(1)
    space.getNeighbours(min, Norm.One, maxDist = 2).size should be(2)
    space.getNeighbours(space.getNeighbours(min).head, Norm.One).size should be(2)
    space.getNeighbours(space.getNeighbours(min).head, Norm.Max).size should be(2)
    space.getNeighbours(min, Norm.Max).size should be(1)
    space.getNeighbours(min, Norm.Max, maxDist = 2).size should be(2)

    // testing findMiddle
    val middle = space.findMiddle(min, max)
    middle.params.values should be(List(2))
    (space.getNeighbours(middle) contains Point(List("nElem" -> 1))) should be(true)
    space.getNeighbours(middle).size should be(2)
  }

  "Space2D methods" should "be properly working on 2D modules" in {
    val const = new Constructor[My2DModule]

    // testing constructor
    val space = const.buildMatrixSpace

    // testing size
    space.size should be(12)

    // testing filter
    val filtered = space.filter(p => p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1)
    filtered.size should be(6)

    //testing sparsity
    space.asInstanceOf[MatrixSpace[My2DModule]].isSparse should be (false)
    filtered.asInstanceOf[MatrixSpace[My2DModule]].isSparse should be (true)

    // testing foreach
    filtered.foreach(p => (p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1) should be(true))

    // testing toSeq
    filtered.toSeq.size should be(6)

    // testing mapPoint
    val enhanced = filtered.mapPoint(p => p.copy(metrics = p.metrics ++ MetricMap("test" -> 1.0)))
    enhanced.size should be(6)
    enhanced.foreach(p => p.metrics("test") should be(1.0))

    // testing map
    val map = enhanced.map(p => p.metrics("test"))
    map.foreach(_ should be(1.0))

    // testing filterNot
    val filteredNot = space.filterNot(p => p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1)
    filteredNot.size should be(6)

    // testing indexWhere on Point
    space.asInstanceOf[MatrixSpace[My2DModule]].indexWhere(Point(List("nElem" -> 1, "bitWidth" -> 32))) should be(
        List(0, 0)
    )
    filtered.asInstanceOf[MatrixSpace[My2DModule]].indexWhere(Point(List("nElem" -> 5, "bitWidth" -> 32))) should be(
        List(-1, 0)
    )

    // testing indexWhere on Seq
    space.asInstanceOf[MatrixSpace[My2DModule]].indexWhere(Seq(1, 32)) should be(List(0, 0))
    filtered.asInstanceOf[MatrixSpace[My2DModule]].indexWhere(Seq(5, 32)) should be(List(-1, 0))

    // testing min and max
    val (min, max) = (space.min, space.max)
    min.params.values should be(List(1, 32))
    max.params.values should be(List(4, 128))

    // testing getNeighbours
    space.getNeighbours(min, Norm.One).size should be(2)
    space.getNeighbours(min, Norm.One, maxDist = 2).size should be(5)
    space.getNeighbours(space.getNeighbours(min).head, Norm.One).size should be(3)
    space.getNeighbours(space.getNeighbours(min).head, Norm.Max).size should be(5)
    space.getNeighbours(min, Norm.Max).size should be(3)
    space.getNeighbours(min, Norm.Max, maxDist = 2).size should be(8)

    // testing findMiddle
    val middle = space.findMiddle(min, max)
    middle.params.values should be(List(2, 64))
    (space.getNeighbours(middle) contains Point(List("nElem" -> 1, "bitWidth" -> 64))) should be(true)
    space.getNeighbours(middle).size should be(4)

    // testing dimension operations
    val rQOR = space.reduceDimension(new qualityOfResult)
    rQOR.foreach(p => (p.metrics("nElem") == 1) should be(true))
    rQOR.size should be(3)
    rQOR.asInstanceOf[MatrixSpace[_]].dimension should be(1)

    val rTest = space.reduceDimension(new ImpactMetric("test"))
    rTest.foreach(p => (p.metrics("nElem") == 1) should be(true))
    rTest.size should be(3)
    rTest.asInstanceOf[MatrixSpace[_]].dimension should be(1)

    val nTest = rTest.rebuildSpace(new ImpactMetric("test"))(space)
    (nTest == space) should be(true)
  }

  "Space3D methods" should "be properly working on 3D modules" in {
    val const = new Constructor[My3DModule]

    // testing constructor
    val space = const.buildMatrixSpace

    // testing size
    space.size should be(24)

    // testing dimension
    space.asInstanceOf[MatrixSpace[_]].dimension should be(3)

    // testing filter
    val filtered = space.filter(p => p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1)
    filtered.size should be(12)

    //testing sparsity
    space.asInstanceOf[MatrixSpace[My3DModule]].isSparse should be (false)
    filtered.asInstanceOf[MatrixSpace[My3DModule]].isSparse should be (true)

    // testing foreach
    filtered.foreach(p => (p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1) should be(true))

    // testing toSeq
    filtered.toSeq.size should be(12)

    // testing mapPoint
    val enhanced = filtered.mapPoint(p => p.copy(metrics = p.metrics ++ MetricMap("test" -> 1.0)))
    enhanced.size should be(12)
    enhanced.foreach(p => p.metrics("test") should be(1.0))

    // testing map
    val map = enhanced.map(p => p.metrics("test"))
    map.foreach(_ should be(1.0))

    // testing filterNot
    val filteredNot = space.filterNot(p => p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1)
    filteredNot.size should be(12)

    // testing indexWhere on Point
    space.asInstanceOf[MatrixSpace[_]].indexWhere(Point(List("nElem" -> 1, "bitWidth" -> 32, "const" -> 15))) should be(
        List(0, 0, 0)
    )
    filtered
      .asInstanceOf[MatrixSpace[_]]
      .indexWhere(Point(List("nElem" -> 5, "bitWidth" -> 32, "const" -> 3))) should be(List(-1, 0, 1))

    // testing indexWhere on Seq
    space.asInstanceOf[MatrixSpace[_]].indexWhere(Seq(1, 32, 3)) should be(List(0, 0, 1))
    filtered.asInstanceOf[MatrixSpace[_]].indexWhere(Seq(5, 32, 3)) should be(List(-1, 0, 1))

    // testing min and max
    val (min, max) = (space.min, space.max)
    min.params.values should be(List(1, 32, 3))
    max.params.values should be(List(4, 128, 15))

    // testing getNeighbours
    space.getNeighbours(min, Norm.One).size should be(3)
    space.getNeighbours(min, Norm.One, maxDist = 2).size should be(8)
    space.getNeighbours(space.getNeighbours(min).head, Norm.One).size should be(3)
    space.getNeighbours(space.getNeighbours(min).head, Norm.Max).size should be(7)
    space.getNeighbours(min, Norm.Max).size should be(7)
    space.getNeighbours(min, Norm.Max, maxDist = 2).size should be(17)

    // testing findMiddle
    val middle = space.findMiddle(min, max)
    middle.params.values should be(List(2, 64, 15))
    (space.getNeighbours(middle) contains Point(List("nElem" -> 1, "bitWidth" -> 64, "const" -> 15))) should be(true)
    space.getNeighbours(middle).size should be(5)

    // testing dimension operations
    val rQOR = space.reduceDimension(new qualityOfResult)
    rQOR.foreach(p => (p.metrics("nElem") == 1 && p.metrics("const") == 15) should be(true))
    rQOR.size should be(3)
    rQOR.asInstanceOf[MatrixSpace[_]].dimension should be(1)

    val rTest = space.reduceDimension(new ImpactMetric("test"))
    rTest.foreach(p => (p.metrics("const") == 15) should be(true))
    rTest.size should be(12)
    rTest.asInstanceOf[MatrixSpace[_]].dimension should be(2)

    val rQece = space.reduceDimension(new ImpactMetric("qece"))
    rQece.foreach(p => (p.metrics("nElem") == 1 && p.metrics("bitWidth") == 32) should be(true))
    rQece.size should be(2)
    rQece.asInstanceOf[MatrixSpace[_]].dimension should be(1)

    val nQece = rQece.rebuildSpace(new ImpactMetric("qece"))(space)
    (nQece == space) should be(true)
  }

  "Space4D methods" should "be properly working on 4D modules" in {
    val const = new Constructor[My4DModule]

    // testing constructor
    val space = const.buildMatrixSpace

    // testing size
    space.size should be(120)

    // testing filter
    val filtered = space.filter(p => p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1)
    filtered.size should be(60)

    //testing sparsity
    space.asInstanceOf[MatrixSpace[My4DModule]].isSparse should be (false)
    filtered.asInstanceOf[MatrixSpace[My4DModule]].isSparse should be (true)

    // testing foreach
    filtered.foreach(p => (p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1) should be(true))

    // testing toSeq
    filtered.toSeq.size should be(60)

    // testing mapPoint
    val enhanced = filtered.mapPoint(p => p.copy(metrics = p.metrics ++ MetricMap("test" -> 1.0)))
    enhanced.size should be(60)
    enhanced.foreach(p => p.metrics("test") should be(1.0))

    // testing map
    val map = enhanced.map(p => p.metrics("test"))
    map.foreach(_ should be(1.0))

    // testing filterNot
    val filteredNot = space.filterNot(p => p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1)
    filteredNot.size should be(60)

    // testing indexWhere on Point
    space
      .asInstanceOf[MatrixSpace[My4DModule]]
      .indexWhere(Point(List("nElem" -> 1, "bitWidth" -> 32, "const" -> 15, "nConst" -> 3))) should be(List(0, 0, 1, 2))
    filtered
      .asInstanceOf[MatrixSpace[My4DModule]]
      .indexWhere(Point(List("nElem" -> 5, "bitWidth" -> 32, "const" -> 3, "nConst" -> 0))) should be(
        List(-1, 0, 0, -1)
    )

    // testing indexWhere on Seq
    space.asInstanceOf[MatrixSpace[My4DModule]].indexWhere(Seq(1, 32, 3, 3)) should be(List(0, 0, 0, 2))
    filtered.asInstanceOf[MatrixSpace[My4DModule]].indexWhere(Seq(5, 32, 3, 74)) should be(List(-1, 0, 0, -1))

    // testing min and max
    val (min, max) = (space.min, space.max)
    min.params.values should be(List(1, 32, 3, 1))
    max.params.values should be(List(4, 128, 15, 5))

    // testing getNeighbours
    space.getNeighbours(min, Norm.One).size should be(4)
    space.getNeighbours(min, Norm.One, maxDist = 2).size should be(13)
    space.getNeighbours(space.getNeighbours(min).head, Norm.One).size should be(5)
    space.getNeighbours(space.getNeighbours(min).head, Norm.Max).size should be(23)
    space.getNeighbours(min, Norm.Max).size should be(15)
    space.getNeighbours(min, Norm.Max, maxDist = 2).size should be(53)

    // testing findMiddle
    val middle = space.findMiddle(min, max)
    middle.params.values should be(List(2, 64, 3, 3))
    (space.getNeighbours(middle) contains Point(
        List("nElem" -> 1, "bitWidth" -> 64, "const" -> 3, "nConst" -> 3)
    )) should be(true)
    space.getNeighbours(middle).size should be(7)

    // testing dimension operations
    val rQOR = space.reduceDimension(new qualityOfResult)
    rQOR.foreach(p => (p.metrics("nElem") == 1 && p.metrics("nConst") == 1) should be(true))
    rQOR.size should be(6)
    rQOR.asInstanceOf[MatrixSpace[_]].dimension should be(2)

    val rTest = space.reduceDimension(new ImpactMetric("test"))
    rTest.foreach(p => (p.metrics("nElem") == 1 && p.metrics("const") == 3 && p.metrics("nConst") == 1) should be(true))
    rTest.size should be(3)
    rTest.asInstanceOf[MatrixSpace[_]].dimension should be(1)

    val nTest = rTest.rebuildSpace(new ImpactMetric("test"))(space)
    (nTest == space) should be(true)
  }

  "Space5D methods" should "be properly working on 5D modules" in {
    val const = new Constructor[My5DModule]

    // testing constructor
    val space = const.buildMatrixSpace

    // testing size
    space.size should be(600)

    // testing filter
    val filtered = space.filter(p => p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1)
    filtered.size should be(300)

    //testing sparsity
    space.asInstanceOf[MatrixSpace[My5DModule]].isSparse should be (false)
    filtered.asInstanceOf[MatrixSpace[My5DModule]].isSparse should be (true)

    // testing foreach
    filtered.foreach(p => (p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1) should be(true))

    // testing toSeq
    filtered.toSeq.size should be(300)

    // testing mapPoint
    val enhanced = filtered.mapPoint(p => p.copy(metrics = p.metrics ++ MetricMap("test" -> 1.0)))
    enhanced.size should be(300)
    enhanced.foreach(p => p.metrics("test") should be(1.0))

    // testing map
    val map = enhanced.map(p => p.metrics("test"))
    map.foreach(_ should be(1.0))

    // testing filterNot
    val filteredNot = space.filterNot(p => p.metrics("bitWidth") == 64 || p.metrics("nElem") == 1)
    filteredNot.size should be(300)

    // testing indexWhere on Point
    space
      .asInstanceOf[MatrixSpace[My5DModule]]
      .indexWhere(Point(List("nElem" -> 1, "bitWidth" -> 32, "const" -> 15, "nConst" -> 3, "zero" -> 8))) should be(
        List(0, 0, 1, 2, 1)
    )
    filtered
      .asInstanceOf[MatrixSpace[My5DModule]]
      .indexWhere(Point(List("nElem" -> 5, "bitWidth" -> 32, "const" -> 3, "nConst" -> 0, "zero" -> 64))) should be(
        List(-1, 0, 0, -1, 4)
    )

    // testing indexWhere on Seq
    space.asInstanceOf[MatrixSpace[My5DModule]].indexWhere(Seq(1, 32, 3, 3, 8)) should be(List(0, 0, 0, 2, 1))
    filtered.asInstanceOf[MatrixSpace[My5DModule]].indexWhere(Seq(5, 32, 3, 74, 4)) should be(List(-1, 0, 0, -1, 0))

    // testing min and max
    val (min, max) = (space.min, space.max)
    min.params.values should be(List(1, 32, 3, 1, 4))
    max.params.values should be(List(4, 128, 15, 5, 64))

    // testing getNeighbours
    space.getNeighbours(min, Norm.One).size should be(5)
    space.getNeighbours(min, Norm.One, maxDist = 2).size should be(19)
    space.getNeighbours(space.getNeighbours(min).head, Norm.One).size should be(6)
    space.getNeighbours(space.getNeighbours(min).head, Norm.Max).size should be(47)
    space.getNeighbours(min, Norm.Max).size should be(31)
    space.getNeighbours(min, Norm.Max, maxDist = 2).size should be(161)

    // testing findMiddle
    val middle = space.findMiddle(min, max)
    middle.params.values should be(List(2, 64, 3, 3, 16))
    (space.getNeighbours(middle) contains Point(
        List("nElem" -> 1, "bitWidth" -> 64, "const" -> 3, "nConst" -> 3, "zero" -> 16)
    )) should be(true)
    space.getNeighbours(middle).size should be(9)

    // testing dimension operations
    val rQOR = space.reduceDimension(new qualityOfResult)
    rQOR.foreach(p => (p.metrics("nElem") == 1 && p.metrics("const") == 3) should be(true))
    rQOR.size should be(75)
    rQOR.asInstanceOf[MatrixSpace[_]].dimension should be(3)

    val rTest = space.reduceDimension(new ImpactMetric("test"))
    rTest.foreach(p =>
      (p.metrics("nElem") == 1 && p.metrics("const") == 3 &&
        p.metrics("nConst") == 1 && p.metrics("zero") == 4) should be(true)
    )
    rTest.size should be(3)
    rTest.asInstanceOf[MatrixSpace[_]].dimension should be(1)

    val nTest = rTest.rebuildSpace(new ImpactMetric("test"))(space)
    (nTest == space) should be(true)

    (rQOR.rebuildSpace(new qualityOfResult)(space) == space) should be(true)
  }

  "SeqSpace" should "be properly used to build MatrixSpace" in {
    val c1D = new Constructor[My1DModule]
    val s1D = c1D.buildSeqSpace
    val m1D = MatrixSpace(s1D)
    s1D.size should be > (0)
    (s1D == m1D) should be(true)

    val c2D = new Constructor[My2DModule]
    val s2D = c2D.buildSeqSpace
    val m2D = MatrixSpace(s2D)
    s2D.size should be > (0)
    (s2D == m2D) should be(true)

    val c3D = new Constructor[My3DModule]
    val s3D = c3D.buildSeqSpace
    val m3D = MatrixSpace(s3D)
    s3D.size should be > (0)
    (s3D == m3D) should be(true)

    val c4D = new Constructor[My4DModule]
    val s4D = c4D.buildSeqSpace
    val m4D = MatrixSpace(s4D)
    s4D.size should be > (0)
    (s4D == m4D) should be(true)

    val c5D = new Constructor[My5DModule]
    val s5D = c5D.buildSeqSpace
    val m5D = MatrixSpace(s5D)
    s5D.size should be > (0)
    (s5D == m5D) should be(true)
  }
}
