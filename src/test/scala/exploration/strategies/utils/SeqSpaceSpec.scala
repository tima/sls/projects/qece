/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import qece.estimation.MetricMap

import qece.exploration.utils.{Constructor, ExplorationFlatSpec, MyModule}
import qece.exploration.annotations.qualityOfResult

class SeqSpaceSpec extends ExplorationFlatSpec {
  "SeqSpace methods" should "be properly working" in {
    val const   = new Constructor[MyModule]
    val space   = const.buildSeqSpace
    val reduced = space.reduceDimension(new qualityOfResult)

    val pMin  = space.min
    val pMax  = space.max
    val rpMin = reduced.min
    val rpMax = reduced.max

    pMin.params.values should be(List(0, 32, 17, 4))
    pMax.params.values should be(List(3, 128, 50, 64))
    // rpMin.params.values should be(List(0, 32, 17, 4))
    // rpMax.params.values should be(List(0, 128, 50, 4))
    rpMin.params.values should be(List(32, 17))
    rpMin.metrics("nElem") should be (0)
    rpMin.metrics("nConst") should be (4)
    rpMax.params.values should be(List(128, 50))
    rpMax.metrics("nElem") should be (0)
    rpMax.metrics("nConst") should be (4)

    space.size should be(180)
    reduced.size should be(9)
    reduced.rebuildSpace(new qualityOfResult)(space) should be(space)

    space.findMiddle(pMin, pMin) should be(pMin)

    val middle = space.findMiddle(pMin, pMax)
    middle.params.values should be(List(1, 64, 34, 32))
    val rMiddle = reduced.findMiddle(rpMin, rpMax)
    // rMiddle.params.values should be(List(0, 64, 34, 4))
    rMiddle.params.values should be(List(64, 34))
    rMiddle.metrics("nElem") should be (0)
    rMiddle.metrics("nConst") should be (4)

    // test equality override
    pMin.copy(metrics = MetricMap.empty) should be(space.min)
    (space contains space.min.copy(metrics = MetricMap.empty)) should be(true)

    // ==== Neighbourhood ====
    space.getNeighbours(middle).size should be(8)
    reduced.getNeighbours(rMiddle).size should be(4)

    space.getNeighbours(middle, Norm.Max).size should be(80)
    reduced.getNeighbours(rMiddle, Norm.Max).size should be(8)

    space.getNeighbours(pMin).size should be(4)
    reduced.getNeighbours(rpMin).size should be(2)

    space.getNeighbours(pMin, Norm.Max).size should be(15)
    reduced.getNeighbours(rpMin, Norm.Max).size should be(3)

    space.getNeighbours(pMin, maxDist = 0).size should be(0)

    space.getNeighbours(pMin, maxDist = 2).size should be(14)
    reduced.getNeighbours(rpMin, maxDist = 2).size should be(5)

    space.getNeighbours(middle, maxDist = 2).size should be(34)
    reduced.getNeighbours(rMiddle, maxDist = 2).size should be(8)
  }
}
