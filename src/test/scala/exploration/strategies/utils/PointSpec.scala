/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies.utils

import qece.estimation.MetricMap

import qece.exploration.utils.{Constructor, ExplorationFlatSpec, MyModule}
import qece.exploration.annotations.qualityOfResult

class PointSpec extends ExplorationFlatSpec {
  "Point helpers for spaces" should "be properly working" in {
    val const   = new Constructor[MyModule]
    val space   = const.buildSeqSpace.toSeq
    val reduced = Point.reduceDimension[MyModule](new qualityOfResult)(space)

    val pMin  = Point.min(space)
    val pMax  = Point.max(space)
    val rpMin = Point.min(reduced)
    val rpMax = Point.max(reduced)

    (EmptyPoint == EmptyPoint) should be(true)

    pMin.params.values should be(List(0, 32, 17, 4))
    pMax.params.values should be(List(3, 128, 50, 64))
    // rpMin.params.values should be(List(0, 32, 17, 4))
    // rpMax.params.values should be(List(0, 128, 50, 4))
    rpMin.params.values should be(List(32, 17))
    rpMax.params.values should be(List(128, 50))

    space.size should be(180)
    reduced.size should be(9)
    Point.rebuildSpace[MyModule](new qualityOfResult)(space, reduced) should be(space)

    pMin.findMiddle(space, pMin) should be(pMin)

    val middle = pMin.findMiddle(space, pMax)
    middle.params.values should be(List(1, 64, 34, 32))
    val rMiddle = rpMin.findMiddle(reduced, rpMax)
    // rMiddle.params.values should be(List(0, 64, 34, 4))
    rMiddle.params.values should be(List(64, 34))

    // test equality override
    pMin.copy(metrics = MetricMap.empty) should be(Point.min(space))
    (space contains Point.min(space).copy(metrics = MetricMap.empty)) should be(true)

    // ==== Neighbourhood ====
    // println(s"${pMin.params}\n====\n${pMin.getNeighbours(space, Norm.Max).map(_.params).mkString("\n")}")
    middle.getNeighbours(space).size should be(8)
    rMiddle.getNeighbours(reduced).size should be(4)

    middle.getNeighbours(space, Norm.Max).size should be(80)
    rMiddle.getNeighbours(reduced, Norm.Max).size should be(8)

    pMin.getNeighbours(space).size should be(4)
    rpMin.getNeighbours(reduced).size should be(2)

    pMin.getNeighbours(space, Norm.Max).size should be(15)
    rpMin.getNeighbours(reduced, Norm.Max).size should be(3)

    pMin.getNeighbours(space, maxDist = 0).size should be(0)

    pMin.getNeighbours(space, maxDist = 2).size should be(14)
    rpMin.getNeighbours(reduced, maxDist = 2).size should be(5)

    middle.getNeighbours(space, maxDist = 2).size should be(34)
    rMiddle.getNeighbours(reduced, maxDist = 2).size should be(8)
  }
}
