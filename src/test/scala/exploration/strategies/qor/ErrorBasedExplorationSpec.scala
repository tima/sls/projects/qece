/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies

import qece.Utils
import qece._

import qece.exploration.components.DotProduct
import qece.exploration.annotations._
import qece.exploration.utils.{Constructor, ExplorationFlatSpec}

import qece.estimation.qor.{ErrorFeaturedPeekPokeTester, NormalDistribution}
import qece.estimation.transforms.TransformSeq

import qece.tags._

import chisel3._
import chisel3.util._

import scala.collection.mutable.ArrayBuffer

import logger.LazyLogging

class DotProductTester(
    nElem: Int,
    parallelism: Int,
    nbSimu: Int = 10,
    workload: Seq[Double]
)(m: DotProduct)
    extends ErrorFeaturedPeekPokeTester(m) {
  val nElemByCycle = scala.math.pow(2, parallelism).toInt

  def goldenReference(samples: Array[(Double, Double)]): Double =
    samples.map { case (x, y) => x * y }.reduce(_ + _)

  def computeHardware(samples: Array[(Double, Double)]): Double = {
    step(1)
    poke(m.accuReset, true)
    val (op1, op2) = samples.unzip
    for (index <- 0 until nElem / nElemByCycle) {
      for (i <- 0 until nElemByCycle) {
        pokeFixedPoint(m.vec1(i), op1(index * nElemByCycle + i))
        pokeFixedPoint(m.vec2(i), op2(index * nElemByCycle + i))
      }
      step(1)
      poke(m.accuReset, false)
    }
    peekFixedPoint(m.out)
  }

  val results   = new ArrayBuffer[(Double, Double)]()
  var workIndex = 0

  (0 until nbSimu).foreach { _ =>
    val samples = Array.tabulate(nElem)(_ => (workload(workIndex) -> workload(workIndex + 1)))
    workIndex += 2
    results.append((computeHardware(samples) -> goldenReference(samples)))
  }

  val (hw, sw) = results.toArray.unzip
  setNormalizedRootMeanSquareError(hw, sw)
}

class ErrorBasedExplorationSpec extends ExplorationFlatSpec with LazyLogging {

  def simpleTest(
      bitWidth: Int,
      binaryPoint: Int,
      nElem: Int,
      parallelism: Int,
      nbSimu: Int = 10,
      workload: Seq[Double]
  ): (Boolean, Double) = {
    var error = 0.0
    val result = iotesters.Driver.execute(
        Array(
            "--target-dir",
            "test_run_dir/test_qormodule",
            "--top-name",
            "DotProduct"
        ),
        () => new DotProduct(bitWidth, binaryPoint, nElem, parallelism)
    ) { c =>
      val r = new DotProductTester(nElem, parallelism, nbSimu, workload)(c); error = r.getError; r
    }
    (result, error)
  }

  "DotProduct tester" should "work properly" taggedAs (QoR) in {
    val bitWidthList          = List(24, 32, 43)
    val binaryPointFactorList = List(1.5, 1.8, 2.0)
    val nElemList             = List(16, 32)
    val nbSimu                = 100
    val workload              = (new NormalDistribution(0, 1)).seq(nbSimu * nElemList.max * 2)
    val maxError              = 1.0
    val errors                = new ArrayBuffer[String]()
    for (bitWidth <- bitWidthList; binaryPointFactor <- binaryPointFactorList; nElem <- nElemList) {
      for (parallelism <- 1 until log2Up(nElem)) {
        val binaryPoint     = (bitWidth / binaryPointFactor).toInt
        val (result, error) = simpleTest(bitWidth, binaryPoint, nElem, parallelism, nbSimu, workload)
        val txt             = s"Error for params [$bitWidth, $binaryPoint, $nElem, $parallelism, $nbSimu] was $error"
        logger.info(txt)
        result should be(true)
        if (error > maxError) errors += txt
      }
    }
    errors.foreach(println)
    errors.size should be(0)
  }

  "Exhaustive pruning" should "properly be done" taggedAs (Exploration, QoR) in {
    val nbSimu   = 100
    val workload = (new NormalDistribution(0, 1)).seq(nbSimu * new Constructor[DotProduct].getMax("nElem") * 2)
    val builder  = StrategyBuilder(testConfig).withPath("ErrorExploration")
    val strat = builder.buildStrategy(
        builder.prune[DotProduct](
            TransformSeq.simulation(m =>
              (
                  c =>
                    new DotProductTester(
                        m("nElem").toInt,
                        m("parallelism").toInt,
                        nbSimu,
                        workload
                    )(c)
              )
            ),
            m => (m.error > 0.1),
            metric = Some(new qualityOfResult)
        )
    )
    val results = strat.explore[DotProduct]
    results.size should be > (0)
    results.foreach(_.metrics.error should be < (0.1))
  }

  "Quick pruning" should "properly be done" taggedAs (Exploration, QoR) in {
    val nbSimu   = 100
    val workload = (new NormalDistribution(0, 1)).seq(nbSimu * new Constructor[DotProduct].getMax("nElem") * 2)
    val builder  = StrategyBuilder(testConfig).withPath("ErrorExploration")
    val strat = builder.buildStrategy(
        builder.quickPrune[DotProduct](
            TransformSeq.simulation(m =>
              (
                  c =>
                    new DotProductTester(
                        m("nElem").toInt,
                        m("parallelism").toInt,
                        nbSimu,
                        workload
                    )(c)
              )
            ),
            m => (m.error > 0.1),
            metric = Some(new qualityOfResult)
        )
    )
    val exhaustive = builder.buildStrategy(
        builder.prune[DotProduct](
            TransformSeq.simulation(m =>
              (
                  c =>
                    new DotProductTester(
                        m("nElem").toInt,
                        m("parallelism").toInt,
                        nbSimu,
                        workload
                    )(c)
              )
            ),
            m => (m.error > 0.1),
            metric = Some(new qualityOfResult)
        )
    )
    val results   = strat.explore[DotProduct]
    val exResults = exhaustive.explore[DotProduct]
    results.foreach(_.metrics.error should be < (0.1))
    exResults.foreach(_.metrics.error should be < (0.1))
    exResults.map(_.params).foreach(results.map(_.params).contains(_) should be(true))
  }
}

object GenerateQuickPruningData extends App {
  val c = new Constructor[DotProduct]

  // val nbSimuList  = Seq(1000)
  val nbSimuList = Seq(10, 50, 100, 1000)
  // val meanList    = Seq(0)
  val meanList = Seq(0, 32, 64)
  // val stdList     = Seq(1)
  val stdList   = Seq(1, 4, 10)
  val builder   = StrategyBuilder(testConfig).withPath("ErrorExploration")
  val baseDir   = s"csv/dp/nElemFixed${c.getMin("nElem")}"
  val threshold = 0.01
  val logFile   = "run.log"

  s"Started at ${Utils.getDate}\n".writeTo(logFile)
  for (nbSimu <- nbSimuList; mean <- meanList; std <- stdList) {
    try {
      val workload = (new NormalDistribution(mean, std)).seq(nbSimu * c.getMax("nElem") * 2)
      val name = Seq(
          s"d${c.getMin("dynamic")}-${c.getMax("dynamic")}",
          s"p${c.getMin("precision")}-${c.getMax("precision")}",
          s"e${c.getMin("nElem")}-${c.getMax("nElem")}",
          s"N$mean-$std",
          s"s$nbSimu"
      ).mkString("", "_", "")
      val spaceSize     = c.getSpaceSize(Some(new qualityOfResult))
      val fullSpaceSize = c.getSpaceSize()

      val exhaustive = builder.buildStrategy(
          builder.prune[DotProduct](
              TransformSeq.simulation(m =>
                (
                    c =>
                      new DotProductTester(
                          m("nElem").toInt,
                          m("parallelism").toInt,
                          nbSimu,
                          workload
                      )(c)
                )
              ),
              _ => (false),
              metric = Some(new qualityOfResult)
          )
      )
      val exStartTime = Utils.getTime
      exhaustive.explore[DotProduct]
      val exEndTime = Utils.getTime
      val exTime    = exEndTime.getTime() - exStartTime.getTime()

      exhaustive.writeBack(s"$baseDir/$name/exhaustivePrune.csv")

      val exhaustiveFull = builder.buildStrategy(
          builder.prune[DotProduct](
              TransformSeq.simulation(m =>
                (
                    c =>
                      new DotProductTester(
                          m("nElem").toInt,
                          m("parallelism").toInt,
                          nbSimu,
                          workload
                      )(c)
                )
              ),
              _ => (false)
          )
      )
      val exFullStartTime = Utils.getTime
      exhaustiveFull.explore[DotProduct]
      val exFullEndTime = Utils.getTime
      val exFullTime    = exFullEndTime.getTime() - exFullStartTime.getTime()

      val strat = builder.buildStrategy(
          builder.quickPrune[DotProduct](
              TransformSeq.simulation(m =>
                (
                    c =>
                      new DotProductTester(
                          m("nElem").toInt,
                          m("parallelism").toInt,
                          nbSimu,
                          workload
                      )(c)
                )
              ),
              m => (m.error > threshold),
              metric = Some(new qualityOfResult)
          )
      )
      val quStartTime = Utils.getTime
      strat.explore[DotProduct]
      val quEndTime = Utils.getTime
      val quTime    = quEndTime.getTime() - quStartTime.getTime()

      strat.writeBack(s"$baseDir/$name/quickPrune.csv")
      val quSize = strat.getResults[DotProduct].head.metrics("nbSimu").toInt

      val stratFromSeq = builder.buildStrategy(
          builder.sort[DotProduct](
              TransformSeq.empty,
              func = _("dynamic"),
              cmp = (_ < _)
          ),
          builder.quickPrune[DotProduct](
              TransformSeq.simulation(m =>
                (
                    c =>
                      new DotProductTester(
                          m("nElem").toInt,
                          m("parallelism").toInt,
                          nbSimu,
                          workload
                      )(c)
                )
              ),
              m => (m.error > threshold),
              metric = Some(new qualityOfResult)
          )
      )
      val quFromSeqStartTime = Utils.getTime
      stratFromSeq.explore[DotProduct]
      val quFromSeqEndTime = Utils.getTime
      val quFromSeqTime    = quFromSeqEndTime.getTime() - quFromSeqStartTime.getTime()
      val quFromSeqSize    = stratFromSeq.getResults[DotProduct].head.metrics("nbSimu").toInt

      // writing config to file
      val config = s"$baseDir/$name/config.csv"
      (s"name;nbSimu;exhaustiveTime;exhaustiveSize;quickPruningTime;quickPruningSize;" +
        s"mean;std;exFullTime;exFullSize;fromSeqTime;fromSeqSize\n").writeTo(config)

      (s"$name;$nbSimu;$exTime;$spaceSize;$quTime;$quSize;" +
        s"$mean;$std;$exFullTime;$fullSpaceSize;$quFromSeqTime;$quFromSeqSize").appendTo(config)
    } catch {
      case _: Throwable =>
        s"[${Utils.getDate}]: FAILURE on $nbSimu simulations with N($mean,$std)\n".appendTo(logFile)
    }
  }
  s"Ended at ${Utils.getDate}\n".appendTo(logFile)
}
