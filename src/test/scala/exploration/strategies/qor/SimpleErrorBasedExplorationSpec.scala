/** Part of QECE (Quick Exploration using Chisel Estimators) program. Copyright (C) <2021> <Bruno FERRES>
 *
 *  This program is developped at TIMA, Grenoble. Please contact Bruno FERRES (bruno.ferres@grenoble-inp.org) or Olivier
 *  MULLER (olivier.muller@univ-grenoble-alpes.fr) for more informations.
 *
 *  This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *  <https://www.gnu.org/licenses/>.
 */
package qece.exploration.strategies

import qece.exploration.annotations._

import qece.estimation.qor.{SimpleFixedPointModule, SimpleFixedPointTester, NormalDistribution}

import qece.estimation.transforms.TransformSeq

import qece.exploration.utils.ExplorationFlatSpec

import qece.tags._

class SimpleErrorBasedExplorationSpec extends ExplorationFlatSpec {

  "Exhaustive pruning" should "properly be done" taggedAs (Exploration, Synthesis, Long, Resources, QoR) in {
    val nbSimu       = 20
    val distribution = new NormalDistribution(16, 3)
    val builder      = StrategyBuilder.withPath("ErrorExploration")
    val strat = builder.buildStrategy(
        builder.prune[SimpleFixedPointModule](
            TransformSeq.simulation(m => (c => new SimpleFixedPointTester(m("nElem").toInt, nbSimu, distribution)(c))),
            m => (m.error > 0.01)
        ),
        builder.sort[SimpleFixedPointModule](
            TransformSeq.synthesis,
            m => m("realFreq"),
            (_ > _)
        )
    )
    val results = strat.explore[SimpleFixedPointModule]
    results.size should be >= (4)
    results.head.cost should be > (85.0)
    results.head.cost should be < (90.0)
  }

  "Dimension reduced pruning" should "properly be done" taggedAs (Exploration, Resources, QoR) in {
    val nbSimu       = 20
    val distribution = new NormalDistribution(16, 3)
    val builder      = StrategyBuilder.withPath("ErrorExploration")
    val strat = builder.buildStrategy(
        builder.prune[SimpleFixedPointModule](
            TransformSeq.simulation(m => (c => new SimpleFixedPointTester(m("nElem").toInt, nbSimu, distribution)(c))),
            m => (m.error > 0.01),
            metric = Some(new qualityOfResult)
        ),
        builder.sort[SimpleFixedPointModule](
            TransformSeq.resources,
            m => m("lut"),
            (_ > _)
        )
    )
    val results = strat.explore[SimpleFixedPointModule]
    results.size should be > (3)
    results.filter(_.params("nElem") == 2).foreach { p =>
      results.map(_.params) contains p.params.update("nElem", 4) should be(true)
      results.map(_.params) contains p.params.update("nElem", 8) should be(true)
    }
    results.head.cost should be(108.0)
  }
}
