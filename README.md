# QECE


**QECE** (*Quick Exploration using Chisel Estimators*) is a **chisel based** project to provide circuit estimators and exploration functions for flexible FPGA design processes.
<br/>
For more information, please take a look at the [wiki](https://gricad-gitlab.univ-grenoble-alpes.fr/tima/sls/projects/qece/-/wikis/home).

### Reference

If you plan on using QECE in your own work and publish it, please consider citing the following reference:
```
@article{10.1145/3590769,
  author = {Ferres, Bruno and Muller, Olivier and Rousseau, Fr\'{e}d\'{e}ric},
  title = {A Chisel Framework for Flexible Design Space Exploration through a Functional Approach},
  year = {2023},
  publisher = {Association for Computing Machinery},
  address = {New York, NY, USA},
  issn = {1084-4309},
  url = {https://doi.org/10.1145/3590769},
  doi = {10.1145/3590769},
  journal = {ACM Trans. Des. Autom. Electron. Syst.},
}
```
