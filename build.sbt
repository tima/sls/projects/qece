// See README.md for license details.

ThisBuild / scalaVersion     := "2.12.12"
ThisBuild / version          := "1.1.0"
ThisBuild / organization     := "fr.tima"

parallelExecution in Test := true

lazy val root = (project in file("."))
.settings(
    name := "qece",
    logLevel := Level.Warn,
    libraryDependencies ++= 
    Seq(
        "edu.berkeley.cs" %% "chisel3" % "3.4.3",
        "edu.berkeley.cs" %% "chisel-iotesters" % "1.5.3",
        "edu.berkeley.cs" %% "chiseltest" % "0.3.3" % "test"
    ) ++ Seq( // used for reflectivity
        "org.scala-lang" % "scala-compiler" % scalaVersion.value
    ) ++ Seq(
        "org.apache.commons" % "commons-math3" % "3.3"
    ) ++ Seq( // used for json ser/des
        "io.circe" %% "circe-core",
        "io.circe" %% "circe-generic",
        "io.circe" %% "circe-parser"
    ).map(_ % "0.12.3"),
    scalacOptions ++= Seq(
        "-Xsource:2.11",
        "-language:existentials",
        "-language:implicitConversions",
        "-language:reflectiveCalls",
        "-deprecation",
        "-feature",
        "-unchecked",
        "-Ywarn-unused",
        "-Xcheckinit"
    ),
    addCompilerPlugin("edu.berkeley.cs" % "chisel3-plugin" % "3.4.2" cross CrossVersion.full),
    addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full)
)

/* ---------- LINTING ------------- */
// Necessary for scalafix
inThisBuild(
    List(
        scalaVersion := scalaVersion.value,
        semanticdbEnabled := true,
        semanticdbVersion := scalafixSemanticdb.revision
    )
)

addCommandAlias("lint", "scalafmt; test:scalafmt; scalafix; test:scalafix")
addCommandAlias("lintCheck", "; scalastyle; test:scalastyle" +
                             "; scalafmtCheck; test:scalafmtCheck" +
                             "; scalafix --check; test:scalafix --check")

addCommandAlias("disableParallel", "set parallelExecution in Test := false;")
addCommandAlias("enableParallel", "set parallelExecution in Test := true;")

/* ------------ TEST --------------- */
// non regression tests, 
addCommandAlias("unitTests", "testOnly -- -l Long -l Synthesis -l Deprecated")

// Disabling parallelExecution on heavy tests to avoid crashes
addCommandAlias("longTests", "disableParallel; testOnly -- -l Deprecated; enableParallel")
addCommandAlias("synthesisTests", "disableParallel; testOnly -- -n Synthesis -l Deprecated; enableParallel")
